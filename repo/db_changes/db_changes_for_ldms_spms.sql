-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.37-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table pids_02_11.employeesperformance
DROP TABLE IF EXISTS `employeesperformance`;
CREATE TABLE IF NOT EXISTS `employeesperformance` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` bigint(50) NOT NULL,
  `BranchRefId` bigint(50) NOT NULL,
  `EmployeesId` varchar(255) DEFAULT NULL,
  `EmployeesRefId` int(10) DEFAULT NULL,
  `PositionRefId` int(10) DEFAULT NULL,
  `DepartmentRefId` int(10) DEFAULT NULL,
  `DivisionRefId` int(10) DEFAULT NULL,
  `Semester` varchar(255) DEFAULT NULL,
  `YearPerformed` int(4) DEFAULT NULL,
  `OverallScore` decimal(9,2) DEFAULT NULL,
  `NumericalRating` decimal(9,2) DEFAULT NULL,
  `Adjectival` int(1) DEFAULT NULL,
  `TimePerformed` datetime DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table pids_02_11.ldmscompetency
DROP TABLE IF EXISTS `ldmscompetency`;
CREATE TABLE IF NOT EXISTS `ldmscompetency` (
  `RefId` int(50) NOT NULL AUTO_INCREMENT,
  `PositionRefId` int(50) DEFAULT NULL,
  `Type` varchar(300) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Level` varchar(300) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table pids_02_11.ldmscompetency_assessment
DROP TABLE IF EXISTS `ldmscompetency_assessment`;
CREATE TABLE IF NOT EXISTS `ldmscompetency_assessment` (
  `RefId` int(50) NOT NULL AUTO_INCREMENT,
  `LDMSCompetencyRefId` int(50) DEFAULT NULL,
  `EmployeesRefId` int(50) DEFAULT NULL,
  `PositionRefId` int(50) DEFAULT NULL,
  `DepartmentRefId` int(50) DEFAULT NULL,
  `YearConducted` int(4) DEFAULT NULL,
  `SelfAssessment` int(1) DEFAULT NULL,
  `Supervisor` varchar(300) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `IsIntervention` varchar(5) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table pids_02_11.ldmslndintervention
DROP TABLE IF EXISTS `ldmslndintervention`;
CREATE TABLE IF NOT EXISTS `ldmslndintervention` (
  `RefId` int(50) NOT NULL AUTO_INCREMENT,
  `EmployeesRefId` int(50) NOT NULL DEFAULT '0',
  `PositionRefId` int(50) NOT NULL DEFAULT '0',
  `DepartmentRefId` int(50) NOT NULL DEFAULT '0',
  `Name` varchar(300) DEFAULT NULL,
  `StartDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `Provider` varchar(300) DEFAULT NULL,
  `Cost` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table pids_02_11.ldmslndprogram
DROP TABLE IF EXISTS `ldmslndprogram`;
CREATE TABLE IF NOT EXISTS `ldmslndprogram` (
  `RefId` int(50) NOT NULL AUTO_INCREMENT,
  `EmployeesRefId` int(50) NOT NULL DEFAULT '0',
  `PositionRefId` int(50) NOT NULL DEFAULT '0',
  `DepartmentRefId` int(50) NOT NULL DEFAULT '0',
  `DevelopmentArea` varchar(300) DEFAULT NULL,
  `CompletionDate` date DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table pids_02_11.ldmsreturnobligation
DROP TABLE IF EXISTS `ldmsreturnobligation`;
CREATE TABLE IF NOT EXISTS `ldmsreturnobligation` (
  `RefId` int(50) NOT NULL AUTO_INCREMENT,
  `EmployeesRefId` int(50) NOT NULL DEFAULT '0',
  `LDMSLNDInterventionRefId` int(50) NOT NULL DEFAULT '0',
  `PositionRefId` int(50) NOT NULL DEFAULT '0',
  `DepartmentRefId` int(50) NOT NULL DEFAULT '0',
  `Rating` int(50) NOT NULL DEFAULT '0',
  `Equivalent` varchar(300) NOT NULL DEFAULT '0',
  `Name` varchar(300) DEFAULT NULL,
  `ServiceStartDate` date DEFAULT NULL,
  `InterventionStartDate` date DEFAULT NULL,
  `InterventionEndDate` date DEFAULT NULL,
  `ServedStartDate` date DEFAULT NULL,
  `ReturnService` varchar(300) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
