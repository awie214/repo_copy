-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.31-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5289
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for phcc
CREATE DATABASE IF NOT EXISTS `phcc` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `phcc`;

-- Dumping structure for table phcc.absences
CREATE TABLE IF NOT EXISTS `absences` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Type` varchar(50) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.accessemployees
CREATE TABLE IF NOT EXISTS `accessemployees` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` int(10) DEFAULT NULL,
  `BranchRefId` int(10) DEFAULT NULL,
  `SysUserRefId` bigint(50) NOT NULL,
  `EmployeesRefId` bigint(50) NOT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`),
  KEY `SysUserRefId` (`SysUserRefId`),
  KEY `SysUserRefId_2` (`SysUserRefId`,`EmployeesRefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.accountability
CREATE TABLE IF NOT EXISTS `accountability` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` bigint(50) DEFAULT NULL,
  `BranchRefId` bigint(50) DEFAULT NULL,
  `EmployeesRefId` bigint(50) NOT NULL,
  `CustodianRefId` bigint(50) DEFAULT NULL,
  `TransferFromRefId` bigint(50) NOT NULL,
  `TransferToRefId` bigint(50) NOT NULL,
  `TransferedBy` varchar(50) NOT NULL,
  `AccountabilityName` varchar(300) NOT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.accumcreditbalance
CREATE TABLE IF NOT EXISTS `accumcreditbalance` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` int(10) DEFAULT NULL,
  `BranchRefId` int(10) DEFAULT NULL,
  `EmployeesRefId` bigint(50) DEFAULT NULL,
  `NameCredits` varchar(2) CHARACTER SET latin1 DEFAULT NULL,
  `AccumDate` date DEFAULT NULL,
  `Balance` decimal(10,3) DEFAULT NULL,
  `Remarks` varchar(300) CHARACTER SET latin1 DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `Data` varchar(1) CHARACTER SET latin1 DEFAULT NULL,
  `Earnings` decimal(10,3) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=114 DEFAULT CHARSET=utf32 COLLATE=utf32_bin;

-- Data exporting was unselected.
-- Dumping structure for table phcc.adjustments
CREATE TABLE IF NOT EXISTS `adjustments` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(10) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `TaxType` varchar(50) DEFAULT NULL,
  `Amount` decimal(6,2) DEFAULT NULL,
  `PayrollGroup` varchar(100) DEFAULT NULL,
  `ITRclassification` varchar(100) DEFAULT NULL,
  `AlphalistClassification` varchar(100) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.agency
CREATE TABLE IF NOT EXISTS `agency` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  `IsPrivate` int(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.ams_employees
CREATE TABLE IF NOT EXISTS `ams_employees` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` int(10) DEFAULT NULL,
  `BranchRefId` int(10) DEFAULT NULL,
  `EmployeesRefId` bigint(50) DEFAULT NULL,
  `JOStartDate` date DEFAULT NULL,
  `JOEndDate` date DEFAULT NULL,
  `AgencyId` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `IsAutoDTR` int(1) DEFAULT NULL,
  `HiredDate` date DEFAULT NULL,
  `AssumptionDate` date DEFAULT NULL,
  `ResignedDate` date DEFAULT NULL,
  `RehiredDate` date DEFAULT NULL,
  `StartDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  `AgencyRefId` int(10) DEFAULT NULL,
  `PositionItemRefId` int(10) DEFAULT NULL,
  `PositionRefId` int(10) DEFAULT NULL,
  `SalaryGradeRefId` int(10) DEFAULT NULL,
  `StepIncrementRefId` int(10) DEFAULT NULL,
  `SalaryAmount` decimal(10,0) DEFAULT NULL,
  `OfficeRefId` int(10) DEFAULT NULL,
  `DepartmentRefId` int(10) DEFAULT NULL,
  `DivisionRefId` int(10) DEFAULT NULL,
  `EmpStatusRefId` int(10) DEFAULT NULL,
  `PayPeriod` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `PayRateRefId` int(50) DEFAULT NULL,
  `InterimPositionRefId` bigint(20) DEFAULT NULL,
  `InterimOfficeRefId` bigint(20) DEFAULT NULL,
  `InterimDivisionRefId` bigint(20) DEFAULT NULL,
  `WorkScheduleRefId` bigint(20) DEFAULT NULL,
  `CutOffRefId` bigint(20) DEFAULT NULL,
  `LeavePolicyRefId` bigint(20) DEFAULT NULL,
  `OverTimePolicyRefId` bigint(20) DEFAULT NULL,
  `BiometricsID` varchar(50) COLLATE utf32_bin DEFAULT NULL,
  `Remarks` varchar(300) CHARACTER SET latin1 DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `Data` varchar(1) CHARACTER SET latin1 DEFAULT NULL,
  `LeavePolicyGroupRefId` bigint(20) DEFAULT NULL,
  `OverTimePolicyGroupRefId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`),
  UNIQUE KEY `EmployeesRefId` (`EmployeesRefId`)
) ENGINE=InnoDB AUTO_INCREMENT=146 DEFAULT CHARSET=utf32 COLLATE=utf32_bin;

-- Data exporting was unselected.
-- Dumping structure for table phcc.ams_employeessuspension
CREATE TABLE IF NOT EXISTS `ams_employeessuspension` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` int(10) DEFAULT NULL,
  `BranchRefId` int(10) DEFAULT NULL,
  `EmployeesRefId` bigint(50) NOT NULL,
  `ams_EmployeesRefId` bigint(50) NOT NULL,
  `StartDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  `Reason` varchar(300) CHARACTER SET latin1 DEFAULT NULL,
  `Remarks` varchar(300) CHARACTER SET latin1 DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `Data` varchar(1) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Data exporting was unselected.
-- Dumping structure for table phcc.annualtaxtable
CREATE TABLE IF NOT EXISTS `annualtaxtable` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(10) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Below` decimal(6,2) DEFAULT NULL,
  `Above` decimal(6,2) DEFAULT NULL,
  `Rate` varchar(255) DEFAULT NULL,
  `EffectivityDate` date DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.applicant
CREATE TABLE IF NOT EXISTS `applicant` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` int(10) DEFAULT NULL COMMENT 'Company',
  `BranchRefId` int(10) DEFAULT NULL COMMENT 'Branch',
  `LastName` varchar(200) DEFAULT NULL,
  `FirstName` varchar(200) DEFAULT NULL,
  `MiddleName` varchar(50) DEFAULT NULL COMMENT 'Middle Name',
  `ExtName` varchar(20) DEFAULT NULL COMMENT 'Ext. Name',
  `NickName` varchar(20) DEFAULT NULL COMMENT 'Nick Name',
  `ContactNo` varchar(20) DEFAULT NULL COMMENT 'Contact No',
  `BirthDate` date DEFAULT NULL COMMENT 'Date of Birth ',
  `BirthPlace` varchar(20) DEFAULT NULL COMMENT 'Place of Birth ',
  `EmailAdd` varchar(100) DEFAULT NULL COMMENT 'Email Address',
  `Sex` varchar(1) DEFAULT NULL COMMENT 'Sex',
  `CivilStatus` varchar(2) DEFAULT NULL COMMENT 'Civil Status',
  `CitizenshipRefId` bigint(50) DEFAULT NULL COMMENT 'Citizenship',
  `isFilipino` int(1) DEFAULT NULL COMMENT 'is Filipino?',
  `isByBirthOrNatural` int(1) DEFAULT NULL COMMENT 'By Birth Or Natural',
  `CountryCitizenRefId` bigint(10) DEFAULT NULL COMMENT 'Country Of Citizen',
  `Height` decimal(6,2) DEFAULT NULL COMMENT 'Height',
  `Weight` decimal(6,2) DEFAULT NULL COMMENT 'Weight',
  `BloodTypeRefId` bigint(50) DEFAULT NULL COMMENT 'Blood Type',
  `PAGIBIG` varchar(60) DEFAULT NULL COMMENT 'PAGIBIG',
  `GSIS` varchar(60) DEFAULT NULL COMMENT 'GSIS',
  `PHIC` varchar(60) DEFAULT NULL COMMENT 'PHIC',
  `TIN` varchar(60) DEFAULT NULL COMMENT 'TIN',
  `SSS` varchar(60) DEFAULT NULL COMMENT 'SSS',
  `GovtIssuedID` varchar(100) NOT NULL,
  `GovtIssuedIDNo` varchar(50) NOT NULL,
  `GovtIssuedIDPlace` varchar(50) NOT NULL,
  `GovtIssuedIDDate` date DEFAULT NULL,
  `GovtIssuedIDValidUntil` date DEFAULT NULL,
  `AgencyId` varchar(30) DEFAULT NULL COMMENT 'Employees Agency No.',
  `ResiHouseNo` varchar(30) NOT NULL DEFAULT '' COMMENT 'Residence House No.',
  `ResiStreet` varchar(100) NOT NULL DEFAULT '' COMMENT 'Residence Street',
  `ResiSubd` varchar(100) NOT NULL,
  `ResiBrgy` varchar(100) NOT NULL DEFAULT '' COMMENT 'Residence Brgy.',
  `ResiAddCityRefId` int(5) DEFAULT NULL COMMENT 'Residence City Add.',
  `ResiAddProvinceRefId` int(5) DEFAULT NULL COMMENT 'Residence Province Add.',
  `ResiCountryRefId` bigint(10) DEFAULT NULL COMMENT 'Residence Country',
  `ResidentTelNo` varchar(10) DEFAULT NULL COMMENT 'Resident Tel.No',
  `PermanentHouseNo` varchar(30) NOT NULL DEFAULT '' COMMENT 'Permanent House No.',
  `PermanentStreet` varchar(100) NOT NULL DEFAULT '' COMMENT 'Permanent Street ',
  `PermanentSubd` varchar(100) NOT NULL,
  `PermanentBrgy` varchar(100) NOT NULL DEFAULT '' COMMENT 'Permanent Brgy.',
  `PermanentAddCityRefId` int(5) DEFAULT NULL COMMENT 'Permanent City Add.',
  `PermanentAddProvinceRefId` int(5) DEFAULT NULL COMMENT 'Permanent Province Add.',
  `PermanentCountryRefId` bigint(10) DEFAULT NULL COMMENT 'Permanent Country',
  `TelNo` varchar(10) DEFAULT NULL COMMENT 'Tel No.',
  `MobileNo` varchar(10) DEFAULT NULL COMMENT 'Mobile No.',
  `PositionItemRefId` bigint(50) DEFAULT NULL,
  `PicFilename` varchar(50) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.applicantchild
CREATE TABLE IF NOT EXISTS `applicantchild` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `EmployeesRefId` bigint(50) DEFAULT NULL,
  `CompanyRefId` int(10) DEFAULT NULL,
  `BranchRefId` int(10) DEFAULT NULL,
  `FullName` varchar(50) DEFAULT NULL,
  `BirthDate` date DEFAULT NULL,
  `Gender` varchar(1) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.applicanteduc
CREATE TABLE IF NOT EXISTS `applicanteduc` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `EmployeesRefId` bigint(50) DEFAULT NULL,
  `CompanyRefId` int(10) DEFAULT NULL,
  `BranchRefId` int(10) DEFAULT NULL,
  `LevelType` tinyint(1) DEFAULT NULL,
  `SchoolsRefId` smallint(5) DEFAULT NULL,
  `CourseRefId` smallint(5) DEFAULT NULL,
  `YearGraduated` int(4) DEFAULT NULL,
  `HighestGrade` varchar(100) DEFAULT NULL,
  `DateFrom` varchar(10) DEFAULT NULL,
  `DateTo` varchar(10) DEFAULT NULL,
  `Honors` varchar(300) DEFAULT NULL,
  `Present` smallint(1) DEFAULT '0',
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.applicantelegibility
CREATE TABLE IF NOT EXISTS `applicantelegibility` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `EmployeesRefId` bigint(50) DEFAULT NULL,
  `CompanyRefId` int(10) DEFAULT NULL,
  `BranchRefId` int(10) DEFAULT NULL,
  `CareerServiceRefId` int(10) DEFAULT NULL,
  `Rating` int(10) DEFAULT NULL,
  `ExamDate` date DEFAULT NULL,
  `ExamPlace` varchar(50) DEFAULT NULL,
  `LicenseNo` varchar(50) DEFAULT NULL,
  `LicenseReleasedDate` date DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.applicantfamily
CREATE TABLE IF NOT EXISTS `applicantfamily` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `EmployeesRefId` bigint(50) DEFAULT NULL,
  `CompanyRefId` int(10) DEFAULT NULL,
  `BranchRefId` int(10) DEFAULT NULL,
  `SpouseLastName` varchar(50) DEFAULT NULL,
  `SpouseFirstName` varchar(50) DEFAULT NULL,
  `SpouseMiddleName` varchar(50) DEFAULT NULL,
  `SpouseExtName` varchar(15) DEFAULT NULL,
  `SpouseBirthDate` date DEFAULT NULL,
  `OccupationsRefId` int(10) DEFAULT NULL,
  `EmployersName` varchar(50) DEFAULT NULL,
  `BusinessAddress` varchar(50) DEFAULT NULL,
  `SpouseMobileNo` varchar(10) DEFAULT NULL,
  `FatherLastName` varchar(50) DEFAULT NULL,
  `FatherFirstName` varchar(50) DEFAULT NULL,
  `FatherMiddleName` varchar(50) DEFAULT NULL,
  `FatherExtName` varchar(15) DEFAULT NULL,
  `MotherLastName` varchar(50) DEFAULT NULL,
  `MotherFirstName` varchar(50) DEFAULT NULL,
  `MotherMiddleName` varchar(50) DEFAULT NULL,
  `MotherExtName` varchar(15) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.applicantotherinfo
CREATE TABLE IF NOT EXISTS `applicantotherinfo` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `EmployeesRefId` bigint(50) DEFAULT NULL,
  `CompanyRefId` int(10) DEFAULT NULL,
  `BranchRefId` int(10) DEFAULT NULL,
  `Skills` varchar(300) DEFAULT NULL,
  `Recognition` varchar(300) DEFAULT NULL,
  `Affiliates` varchar(300) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.applicantpdsq
CREATE TABLE IF NOT EXISTS `applicantpdsq` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `EmployeesRefId` bigint(50) DEFAULT NULL,
  `CompanyRefId` int(10) DEFAULT NULL,
  `BranchRefId` int(10) DEFAULT NULL,
  `Q1a` int(1) DEFAULT '0' COMMENT '34. Within the third degree?',
  `Q1aexp` varchar(150) DEFAULT NULL,
  `Q1b` int(1) DEFAULT '0' COMMENT '34. Within the fourth degree (for Local Government Employees)?',
  `Q1bexp` varchar(150) DEFAULT NULL,
  `Q2a` int(1) DEFAULT '0' COMMENT '35 Have you ever been found guilty of any administrative offense?',
  `Q2aexp` varchar(150) DEFAULT NULL,
  `Q2b` int(1) DEFAULT '0' COMMENT '35 Have you been criminally charged before any court?',
  `Q2bexp` varchar(150) DEFAULT NULL,
  `Q2DateFiled` date DEFAULT NULL,
  `Q2CaseStatus` varchar(250) DEFAULT NULL,
  `Q3a` int(1) DEFAULT '0' COMMENT '36 Have you ever been convicted of any crime or violation of any law, decree, ordinance, or regulation by any court or tribunal?',
  `Q3aexp` varchar(150) DEFAULT NULL,
  `Q4a` int(1) DEFAULT '0' COMMENT '37 Have you ever been separated from the service in any of the following modes: resignation, retirement, dropped from the rolls, dismissal, termination, end of term, finished contract, AWOL or phased out, in the public or private sector?',
  `Q4aexp` varchar(150) DEFAULT NULL,
  `Q5a` int(1) DEFAULT '0' COMMENT '38 Have you ever been a candidate in a national or local election (except Barangay Election)?',
  `Q5aexp` varchar(150) DEFAULT NULL,
  `Q5b` int(1) DEFAULT '0' COMMENT '38 Have you resigned from the government service during the three (3)-month period before the last election to promote/actively campaign for a national or local candidate?',
  `Q5bexp` varchar(150) DEFAULT NULL,
  `Q6a` int(1) DEFAULT '0' COMMENT '39 Have you acquired the status of an immigrant or permanent resident of another country?',
  `Q6aexp` varchar(150) DEFAULT NULL,
  `Q7a` int(1) NOT NULL COMMENT '40 Are you a member of any indigenous group?',
  `Q7aexp` varchar(150) NOT NULL,
  `Q7b` int(11) NOT NULL COMMENT '40 Are you a person with disability?',
  `Q7bexp` varchar(150) NOT NULL,
  `Q7c` int(1) NOT NULL COMMENT '40 Are you a solo parent?',
  `Q7cexp` varchar(150) NOT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.applicantreference
CREATE TABLE IF NOT EXISTS `applicantreference` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `EmployeesRefId` bigint(50) DEFAULT NULL,
  `CompanyRefId` int(10) DEFAULT NULL,
  `BranchRefId` int(10) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Address` varchar(300) DEFAULT NULL,
  `ContactNo` varchar(11) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.applicanttraining
CREATE TABLE IF NOT EXISTS `applicanttraining` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `EmployeesRefId` bigint(50) DEFAULT NULL,
  `CompanyRefId` int(10) DEFAULT NULL,
  `BranchRefId` int(10) DEFAULT NULL,
  `SeminarsRefId` int(10) DEFAULT NULL,
  `StartDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  `NumofHrs` int(5) DEFAULT NULL,
  `SponsorRefId` int(5) DEFAULT NULL,
  `SeminarPlaceRefId` int(5) DEFAULT NULL,
  `SeminarTypeRefId` int(5) DEFAULT NULL,
  `Description` varchar(200) NOT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.applicantvoluntary
CREATE TABLE IF NOT EXISTS `applicantvoluntary` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `EmployeesRefId` bigint(50) DEFAULT NULL,
  `CompanyRefId` int(10) DEFAULT NULL,
  `BranchRefId` int(10) DEFAULT NULL,
  `OrganizationRefId` int(10) DEFAULT NULL,
  `StartDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  `NumofHrs` int(5) DEFAULT NULL,
  `WorksNature` varchar(50) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.applicantworkexperience
CREATE TABLE IF NOT EXISTS `applicantworkexperience` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `EmployeesRefId` bigint(50) DEFAULT NULL,
  `CompanyRefId` int(10) DEFAULT NULL,
  `BranchRefId` int(10) DEFAULT NULL,
  `AgencyRefId` int(10) DEFAULT NULL,
  `PositionRefId` int(10) DEFAULT NULL,
  `EmpStatusRefId` int(10) DEFAULT NULL,
  `SalaryGradeRefId` int(10) DEFAULT NULL,
  `StepIncrementRefId` int(10) DEFAULT NULL,
  `PayrateRefId` int(10) DEFAULT NULL,
  `WorkStartDate` date DEFAULT NULL,
  `WorkEndDate` date DEFAULT NULL,
  `SalaryAmount` decimal(6,2) DEFAULT NULL,
  `LWOP` varchar(200) DEFAULT NULL,
  `Reason` varchar(200) DEFAULT NULL,
  `ExtDate` date DEFAULT NULL,
  `SeparatedDate` date DEFAULT NULL,
  `ApptStatusRefId` smallint(10) DEFAULT NULL,
  `isGovtService` tinyint(1) DEFAULT NULL,
  `PayGroup` varchar(20) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.apptstatus
CREATE TABLE IF NOT EXISTS `apptstatus` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `ApptType` smallint(6) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.attachdoctype
CREATE TABLE IF NOT EXISTS `attachdoctype` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` int(10) DEFAULT NULL,
  `BranchRefId` int(10) DEFAULT NULL,
  `Code` varchar(15) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Description` varchar(250) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.attendance_request
CREATE TABLE IF NOT EXISTS `attendance_request` (
  `RefId` int(11) NOT NULL AUTO_INCREMENT,
  `EmployeesRefId` int(50) DEFAULT NULL,
  `CompanyRefId` int(50) DEFAULT NULL,
  `BranchRefId` int(50) DEFAULT NULL,
  `ApprovedByRefId` bigint(50) DEFAULT NULL,
  `Type` varchar(100) DEFAULT NULL,
  `FiledDate` date DEFAULT NULL,
  `AppliedDateFor` date DEFAULT NULL,
  `Remarks` varchar(500) DEFAULT NULL,
  `Reason` varchar(500) DEFAULT NULL,
  `Status` varchar(500) DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.authority
CREATE TABLE IF NOT EXISTS `authority` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Name` varchar(300) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.awards
CREATE TABLE IF NOT EXISTS `awards` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(10) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.bank
CREATE TABLE IF NOT EXISTS `bank` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(10) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.bankbranch
CREATE TABLE IF NOT EXISTS `bankbranch` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(10) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Address` varchar(300) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.benefits
CREATE TABLE IF NOT EXISTS `benefits` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(10) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `TaxType` varchar(50) DEFAULT NULL,
  `MinimisType` varchar(50) DEFAULT NULL,
  `ComputationType` varchar(50) DEFAULT NULL,
  `PayrollGroup` varchar(50) DEFAULT NULL,
  `ITRClassification` varchar(100) DEFAULT NULL,
  `AlphalistClassification` varchar(100) DEFAULT NULL,
  `IncludeComputation` varchar(300) DEFAULT NULL,
  `Amount` decimal(6,2) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.bloodtype
CREATE TABLE IF NOT EXISTS `bloodtype` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.branch
CREATE TABLE IF NOT EXISTS `branch` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Address` varchar(300) DEFAULT NULL,
  `CityRefId` bigint(5) DEFAULT NULL,
  `ProvinceRefId` bigint(5) DEFAULT NULL,
  `ZipCode` varchar(4) DEFAULT NULL,
  `CountryRefId` bigint(10) DEFAULT NULL,
  `TelNo` varchar(10) DEFAULT NULL,
  `MobileNo` varchar(10) DEFAULT NULL,
  `ContactPerson` varchar(50) DEFAULT NULL,
  `Website` varchar(100) DEFAULT NULL,
  `EmailAdd` varchar(100) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.careerservice
CREATE TABLE IF NOT EXISTS `careerservice` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`),
  UNIQUE KEY `Code` (`Code`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.casenature
CREATE TABLE IF NOT EXISTS `casenature` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.changeshift_request
CREATE TABLE IF NOT EXISTS `changeshift_request` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` bigint(50) NOT NULL,
  `BranchRefId` bigint(50) NOT NULL,
  `WorkScheduleRefId` bigint(50) NOT NULL,
  `StartDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  `FiledDate` date DEFAULT NULL,
  `EmployeesRefId` bigint(50) NOT NULL,
  `Status` varchar(50) NOT NULL,
  `ApprovedByRefId` bigint(50) NOT NULL,
  `Reason` varchar(300) NOT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.citizenship
CREATE TABLE IF NOT EXISTS `citizenship` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.city
CREATE TABLE IF NOT EXISTS `city` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `ProvinceRefId` bigint(10) DEFAULT NULL,
  `ZipCode` int(6) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Class` varchar(50) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  `isCity` int(1) DEFAULT '0',
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`),
  KEY `idx_Province` (`ProvinceRefId`)
) ENGINE=InnoDB AUTO_INCREMENT=2281 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.company
CREATE TABLE IF NOT EXISTS `company` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) DEFAULT NULL,
  `BranchRefId` bigint(10) NOT NULL,
  `isMain` smallint(1) NOT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `CountryRefId` bigint(10) NOT NULL,
  `Address` varchar(300) DEFAULT NULL,
  `CityRefId` int(5) DEFAULT NULL,
  `ProvinceRefId` int(5) DEFAULT NULL,
  `ZipCode` varchar(4) DEFAULT NULL,
  `TelNo` varchar(10) DEFAULT NULL,
  `MobileNo` varchar(10) DEFAULT NULL,
  `ContactPerson` varchar(50) DEFAULT NULL,
  `Website` varchar(100) DEFAULT NULL,
  `EmailAdd` varchar(100) DEFAULT NULL,
  `Label1` varchar(60) DEFAULT NULL,
  `Name1` varchar(60) DEFAULT NULL,
  `Position1` varchar(60) DEFAULT NULL,
  `Label2` varchar(60) DEFAULT NULL,
  `Name2` varchar(60) DEFAULT NULL,
  `Position2` varchar(60) DEFAULT NULL,
  `Label3` varchar(60) DEFAULT NULL,
  `Name3` varchar(60) DEFAULT NULL,
  `Position3` varchar(60) DEFAULT NULL,
  `Label4` varchar(60) DEFAULT NULL,
  `Name4` varchar(60) DEFAULT NULL,
  `Position4` varchar(60) DEFAULT NULL,
  `CompanyType` int(1) DEFAULT NULL,
  `SmallLogo` varchar(100) DEFAULT NULL,
  `BigLogo` varchar(100) NOT NULL,
  `PatternLogo` varchar(100) NOT NULL,
  `Logo` varchar(50) DEFAULT NULL,
  `Color1` varchar(10) NOT NULL,
  `Color2` varchar(10) NOT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`),
  KEY `RefId_2` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.companysettings
CREATE TABLE IF NOT EXISTS `companysettings` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` int(10) DEFAULT NULL,
  `BranchRefId` int(10) DEFAULT NULL,
  `Systems` varchar(300) COLLATE utf32_bin DEFAULT NULL,
  `Remarks` varchar(300) CHARACTER SET latin1 DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `Data` varchar(1) CHARACTER SET latin1 DEFAULT NULL,
  `pds_rowWorkExperience` int(10) DEFAULT NULL,
  `pds_rowVoluntary` int(10) DEFAULT NULL,
  `pds_rowLearning` int(10) DEFAULT NULL,
  `pds_rowEligibilty` int(10) DEFAULT NULL,
  `pds_rowOtherInfo` int(10) DEFAULT NULL,
  `pds_trHeight` varchar(100) COLLATE utf32_bin DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf32 COLLATE=utf32_bin;

-- Data exporting was unselected.
-- Dumping structure for table phcc.country
CREATE TABLE IF NOT EXISTS `country` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(10) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=199 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.course
CREATE TABLE IF NOT EXISTS `course` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Level` int(1) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `Category` varchar(50) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=256 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.coursecategory
CREATE TABLE IF NOT EXISTS `coursecategory` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.cutoff
CREATE TABLE IF NOT EXISTS `cutoff` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `FirstCutOff` smallint(2) DEFAULT NULL,
  `SecondCutOff` smallint(2) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.deductions
CREATE TABLE IF NOT EXISTS `deductions` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(10) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `TaxType` varchar(50) DEFAULT NULL,
  `Amount` decimal(6,2) DEFAULT NULL,
  `PayrollGroup` varchar(100) DEFAULT NULL,
  `ITRclassification` varchar(100) DEFAULT NULL,
  `AlphalistClassification` varchar(100) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.department
CREATE TABLE IF NOT EXISTS `department` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`),
  UNIQUE KEY `Code` (`Code`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.designation
CREATE TABLE IF NOT EXISTS `designation` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.division
CREATE TABLE IF NOT EXISTS `division` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `IsInterim` int(11) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`),
  KEY `idx2` (`Name`,`IsInterim`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.dpcr_core_functions
CREATE TABLE IF NOT EXISTS `dpcr_core_functions` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `spms_dpcr_id` bigint(50) DEFAULT NULL,
  `objective` varchar(300) DEFAULT NULL,
  `percent` varchar(10) DEFAULT NULL,
  `success_indicator` varchar(300) DEFAULT NULL,
  `sitype` varchar(300) DEFAULT NULL,
  `q1` int(1) DEFAULT NULL,
  `q2` int(1) DEFAULT NULL,
  `q3` int(1) DEFAULT NULL,
  `q4` int(1) DEFAULT NULL,
  `rating` varchar(300) DEFAULT NULL,
  `budget` decimal(15,2) DEFAULT NULL,
  `remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.dpcr_strategic_priorities
CREATE TABLE IF NOT EXISTS `dpcr_strategic_priorities` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `spms_dpcr_id` bigint(50) DEFAULT NULL,
  `objective` varchar(300) DEFAULT NULL,
  `percent` varchar(10) DEFAULT NULL,
  `success_indicator` varchar(300) DEFAULT NULL,
  `sitype` varchar(300) DEFAULT NULL,
  `q1` int(1) DEFAULT NULL,
  `q2` int(1) DEFAULT NULL,
  `q3` int(1) DEFAULT NULL,
  `q4` int(1) DEFAULT NULL,
  `rating` varchar(300) DEFAULT NULL,
  `budget` decimal(15,2) DEFAULT NULL,
  `remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.dpcr_support_functions
CREATE TABLE IF NOT EXISTS `dpcr_support_functions` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `spms_dpcr_id` bigint(50) DEFAULT NULL,
  `objective` varchar(300) DEFAULT NULL,
  `percent` varchar(10) DEFAULT NULL,
  `success_indicator` varchar(300) DEFAULT NULL,
  `sitype` varchar(300) DEFAULT NULL,
  `q1` int(1) DEFAULT NULL,
  `q2` int(1) DEFAULT NULL,
  `q3` int(1) DEFAULT NULL,
  `q4` int(1) DEFAULT NULL,
  `rating` varchar(300) DEFAULT NULL,
  `budget` decimal(15,2) DEFAULT NULL,
  `remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.dtr_process
CREATE TABLE IF NOT EXISTS `dtr_process` (
  `RefId` int(100) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` int(10) DEFAULT NULL,
  `BranchRefId` int(10) DEFAULT NULL,
  `EmployeesRefId` int(10) DEFAULT NULL,
  `WorkScheduleRefId` int(10) DEFAULT NULL,
  `Month` varchar(50) DEFAULT NULL,
  `Year` varchar(50) DEFAULT NULL,
  `Total_Regular_Hr` int(10) DEFAULT NULL,
  `Total_Excess_Hr` int(10) DEFAULT NULL,
  `Total_COC_Hr` int(10) DEFAULT NULL,
  `Total_OT_Hr` int(10) DEFAULT NULL,
  `Total_Tardy_Hr` int(10) DEFAULT NULL,
  `Total_Undertime_Hr` int(10) DEFAULT NULL,
  `Total_Absent_Count` int(10) DEFAULT NULL,
  `Total_Tardy_Count` int(10) DEFAULT NULL,
  `Total_Undertime_Count` int(10) DEFAULT NULL,
  `Total_Present_Count` int(10) DEFAULT NULL,
  `Days_Absent` varchar(300) DEFAULT NULL,
  `Days_Undertime` varchar(300) DEFAULT NULL,
  `Days_Tardy` varchar(300) DEFAULT NULL,
  `VL_Days` varchar(300) DEFAULT NULL,
  `FL_Days` varchar(300) DEFAULT NULL,
  `SL_Days` varchar(300) DEFAULT NULL,
  `SPL_Days` varchar(300) DEFAULT NULL,
  `VL_Earned` decimal(10,4) DEFAULT NULL,
  `SL_Earned` decimal(10,4) DEFAULT NULL,
  `SPL_Used` int(10) DEFAULT NULL,
  `VL_Used` int(10) DEFAULT NULL,
  `SL_Used` int(10) DEFAULT NULL,
  `Tardy_Deduction_EQ` decimal(10,4) DEFAULT NULL,
  `Undertime_Deduction_EQ` decimal(10,4) DEFAULT NULL,
  `Regular_Hr_EQ` decimal(10,4) DEFAULT NULL,
  `Total_Days_EQ` decimal(10,4) DEFAULT NULL,
  `Total_Absent_EQ` decimal(10,4) DEFAULT NULL,
  `01_Late` int(11) DEFAULT NULL,
  `01_UT` int(11) DEFAULT NULL,
  `01_Absent` int(11) DEFAULT NULL,
  `02_Late` int(11) DEFAULT NULL,
  `02_UT` int(11) DEFAULT NULL,
  `02_Absent` int(11) DEFAULT NULL,
  `03_Late` int(11) DEFAULT NULL,
  `03_UT` int(11) DEFAULT NULL,
  `03_Absent` int(11) DEFAULT NULL,
  `04_Late` int(11) DEFAULT NULL,
  `04_UT` int(11) DEFAULT NULL,
  `04_Absent` int(11) DEFAULT NULL,
  `05_Late` int(11) DEFAULT NULL,
  `05_UT` int(11) DEFAULT NULL,
  `05_Absent` int(11) DEFAULT NULL,
  `06_Late` int(11) DEFAULT NULL,
  `06_UT` int(11) DEFAULT NULL,
  `06_Absent` int(11) DEFAULT NULL,
  `07_Late` int(11) DEFAULT NULL,
  `07_UT` int(11) DEFAULT NULL,
  `07_Absent` int(11) DEFAULT NULL,
  `08_Late` int(11) DEFAULT NULL,
  `08_UT` int(11) DEFAULT NULL,
  `08_Absent` int(11) DEFAULT NULL,
  `09_Late` int(11) DEFAULT NULL,
  `09_UT` int(11) DEFAULT NULL,
  `09_Absent` int(11) DEFAULT NULL,
  `10_Late` int(11) DEFAULT NULL,
  `10_UT` int(11) DEFAULT NULL,
  `10_Absent` int(11) DEFAULT NULL,
  `11_Late` int(11) DEFAULT NULL,
  `11_UT` int(11) DEFAULT NULL,
  `11_Absent` int(11) DEFAULT NULL,
  `12_Late` int(11) DEFAULT NULL,
  `12_UT` int(11) DEFAULT NULL,
  `12_Absent` int(11) DEFAULT NULL,
  `13_Late` int(11) DEFAULT NULL,
  `13_UT` int(11) DEFAULT NULL,
  `13_Absent` int(11) DEFAULT NULL,
  `14_Late` int(11) DEFAULT NULL,
  `14_UT` int(11) DEFAULT NULL,
  `14_Absent` int(11) DEFAULT NULL,
  `15_Late` int(11) DEFAULT NULL,
  `15_UT` int(11) DEFAULT NULL,
  `15_Absent` int(11) DEFAULT NULL,
  `16_Late` int(11) DEFAULT NULL,
  `16_UT` int(11) DEFAULT NULL,
  `16_Absent` int(11) DEFAULT NULL,
  `17_Late` int(11) DEFAULT NULL,
  `17_UT` int(11) DEFAULT NULL,
  `17_Absent` int(11) DEFAULT NULL,
  `18_Late` int(11) DEFAULT NULL,
  `18_UT` int(11) DEFAULT NULL,
  `18_Absent` int(11) DEFAULT NULL,
  `19_Late` int(11) DEFAULT NULL,
  `19_UT` int(11) DEFAULT NULL,
  `19_Absent` int(11) DEFAULT NULL,
  `20_Late` int(11) DEFAULT NULL,
  `20_UT` int(11) DEFAULT NULL,
  `20_Absent` int(11) DEFAULT NULL,
  `21_Late` int(11) DEFAULT NULL,
  `21_UT` int(11) DEFAULT NULL,
  `21_Absent` int(11) DEFAULT NULL,
  `22_Late` int(11) DEFAULT NULL,
  `22_UT` int(11) DEFAULT NULL,
  `22_Absent` int(11) DEFAULT NULL,
  `23_Late` int(11) DEFAULT NULL,
  `23_UT` int(11) DEFAULT NULL,
  `23_Absent` int(11) DEFAULT NULL,
  `24_Late` int(11) DEFAULT NULL,
  `24_UT` int(11) DEFAULT NULL,
  `24_Absent` int(11) DEFAULT NULL,
  `25_Late` int(11) DEFAULT NULL,
  `25_UT` int(11) DEFAULT NULL,
  `25_Absent` int(11) DEFAULT NULL,
  `26_Late` int(11) DEFAULT NULL,
  `26_UT` int(11) DEFAULT NULL,
  `26_Absent` int(11) DEFAULT NULL,
  `27_Late` int(11) DEFAULT NULL,
  `27_UT` int(11) DEFAULT NULL,
  `27_Absent` int(11) DEFAULT NULL,
  `28_Late` int(11) DEFAULT NULL,
  `28_UT` int(11) DEFAULT NULL,
  `28_Absent` int(11) DEFAULT NULL,
  `29_Late` int(11) DEFAULT NULL,
  `29_UT` int(11) DEFAULT NULL,
  `29_Absent` int(11) DEFAULT NULL,
  `30_Late` int(11) DEFAULT NULL,
  `30_UT` int(11) DEFAULT NULL,
  `30_Absent` int(11) DEFAULT NULL,
  `31_Late` int(11) DEFAULT NULL,
  `31_UT` int(11) DEFAULT NULL,
  `31_Absent` int(11) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateBy` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  `Processed` int(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.dtr_remarks
CREATE TABLE IF NOT EXISTS `dtr_remarks` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `EmployeesRefId` bigint(50) DEFAULT NULL,
  `AttendanceDate` date DEFAULT NULL,
  `Remarks` varchar(500) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.empinformation
CREATE TABLE IF NOT EXISTS `empinformation` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` int(10) DEFAULT NULL,
  `BranchRefId` int(10) DEFAULT NULL,
  `EmployeesRefId` bigint(50) NOT NULL,
  `HiredDate` date DEFAULT NULL,
  `AssumptionDate` date DEFAULT NULL,
  `ResignedDate` date DEFAULT NULL,
  `RehiredDate` date DEFAULT NULL,
  `StartDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  `AgencyRefId` int(10) DEFAULT NULL,
  `PositionItemRefId` int(10) DEFAULT NULL,
  `PositionRefId` int(10) DEFAULT NULL,
  `UnitsRefId` decimal(10,2) DEFAULT NULL,
  `SalaryGradeRefId` int(10) DEFAULT NULL,
  `StepIncrementRefId` int(10) DEFAULT NULL,
  `SalaryAmount` decimal(10,0) DEFAULT NULL,
  `SalaryAmountTwo` decimal(10,2) DEFAULT NULL,
  `OfficeRefId` int(10) DEFAULT NULL,
  `DepartmentRefId` int(10) DEFAULT NULL,
  `DivisionRefId` int(10) DEFAULT NULL,
  `EmpStatusRefId` int(10) DEFAULT NULL,
  `PayPeriod` varchar(50) DEFAULT NULL,
  `PayRateRefId` int(50) DEFAULT NULL,
  `WorkScheduleRefId` bigint(20) DEFAULT NULL,
  `InterimPositionRefId` bigint(20) DEFAULT NULL,
  `InterimOfficeRefId` bigint(20) DEFAULT NULL,
  `InterimDivisionRefId` bigint(20) DEFAULT NULL,
  `ApptStatusRefId` bigint(20) DEFAULT NULL,
  `DesignationRefId` bigint(20) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  `JobGradeRefId` int(10) DEFAULT NULL,
  `TransferTo` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`),
  UNIQUE KEY `EmployeesRefId` (`EmployeesRefId`)
) ENGINE=InnoDB AUTO_INCREMENT=214 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.empipcr
CREATE TABLE IF NOT EXISTS `empipcr` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` int(10) DEFAULT NULL,
  `BranchRefId` int(10) DEFAULT NULL,
  `EmployeesRefId` bigint(50) NOT NULL,
  `StrategicObjective` varchar(300) DEFAULT NULL,
  `SuccessIndicator` varchar(300) DEFAULT NULL,
  `ActualAccomplishment` varchar(300) DEFAULT NULL,
  `QualityRate` int(1) DEFAULT '0',
  `EfficiencyRate` int(1) DEFAULT '0',
  `TimelinessRate` int(1) DEFAULT '0',
  `AverageRate` int(1) DEFAULT '0',
  `Recommendations` varchar(300) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.employees
CREATE TABLE IF NOT EXISTS `employees` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` int(10) NOT NULL DEFAULT '0' COMMENT 'Company',
  `BranchRefId` int(10) NOT NULL DEFAULT '0' COMMENT 'Branch',
  `LastName` varchar(250) DEFAULT NULL,
  `FirstName` varchar(250) DEFAULT NULL,
  `MiddleName` varchar(250) DEFAULT NULL,
  `ExtName` varchar(20) DEFAULT NULL,
  `NickName` varchar(20) DEFAULT NULL,
  `ContactNo` varchar(20) DEFAULT NULL COMMENT 'Contact No',
  `BirthDate` date DEFAULT NULL COMMENT 'Date of Birth ',
  `BirthPlace` varchar(100) DEFAULT NULL COMMENT 'Place of Birth ',
  `EmailAdd` varchar(100) DEFAULT NULL COMMENT 'Email Address',
  `Sex` varchar(1) DEFAULT NULL COMMENT 'Sex',
  `CivilStatus` varchar(2) DEFAULT NULL COMMENT 'Civil Status',
  `CitizenshipRefId` bigint(50) DEFAULT NULL COMMENT 'Citizenship',
  `isFilipino` int(1) DEFAULT NULL COMMENT 'is Filipino?',
  `isByBirthOrNatural` int(1) DEFAULT NULL COMMENT 'By Birth Or Natural',
  `CountryCitizenRefId` bigint(10) DEFAULT NULL COMMENT 'Country Of Citizen',
  `Height` decimal(6,2) DEFAULT NULL COMMENT 'Height',
  `Weight` decimal(6,2) DEFAULT NULL COMMENT 'Weight',
  `BloodTypeRefId` bigint(50) DEFAULT NULL COMMENT 'Blood Type',
  `PAGIBIG` varchar(60) DEFAULT NULL COMMENT 'PAGIBIG',
  `GSIS` varchar(60) DEFAULT NULL COMMENT 'GSIS',
  `PHIC` varchar(60) DEFAULT NULL COMMENT 'PHIC',
  `TIN` varchar(60) DEFAULT NULL COMMENT 'TIN',
  `SSS` varchar(60) DEFAULT NULL COMMENT 'SSS',
  `GovtIssuedID` varchar(100) DEFAULT NULL,
  `GovtIssuedIDNo` varchar(50) DEFAULT NULL,
  `GovtIssuedIDPlace` varchar(50) DEFAULT NULL,
  `GovtIssuedIDDate` date DEFAULT NULL,
  `GovtIssuedIDValidUntil` date DEFAULT NULL,
  `AgencyId` varchar(30) DEFAULT NULL COMMENT 'Employees Agency No.',
  `BiometricsID` varchar(45) CHARACTER SET utf32 COLLATE utf32_bin DEFAULT NULL,
  `ResiHouseNo` varchar(255) DEFAULT NULL,
  `ResiStreet` varchar(400) DEFAULT '' COMMENT 'Residence Street',
  `ResiSubd` varchar(255) DEFAULT NULL,
  `ResiBrgy` varchar(255) DEFAULT NULL,
  `ResiAddCityRefId` int(5) DEFAULT NULL COMMENT 'Residence City Add.',
  `ResiAddProvinceRefId` int(5) DEFAULT NULL COMMENT 'Residence Province Add.',
  `ResiCountryRefId` bigint(10) DEFAULT NULL COMMENT 'Residence Country',
  `ResidentTelNo` varchar(10) DEFAULT NULL COMMENT 'Resident Tel.No',
  `PermanentHouseNo` varchar(255) DEFAULT NULL,
  `PermanentStreet` varchar(400) DEFAULT '' COMMENT 'Permanent Street ',
  `PermanentSubd` varchar(255) DEFAULT NULL,
  `PermanentBrgy` varchar(255) DEFAULT NULL,
  `PermanentAddCityRefId` int(5) DEFAULT NULL COMMENT 'Permanent City Add.',
  `PermanentAddProvinceRefId` int(5) DEFAULT NULL COMMENT 'Permanent Province Add.',
  `PermanentCountryRefId` bigint(10) DEFAULT NULL COMMENT 'Permanent Country',
  `TelNo` varchar(11) DEFAULT NULL COMMENT 'Tel No.',
  `MobileNo` varchar(11) DEFAULT NULL COMMENT 'Mobile No.',
  `PicFilename` varchar(50) DEFAULT NULL,
  `Inactive` tinyint(4) DEFAULT NULL,
  `pw` varchar(300) DEFAULT NULL,
  `isLogin` int(1) DEFAULT NULL,
  `Terminal` varchar(50) DEFAULT NULL,
  `Session` varchar(255) DEFAULT NULL,
  `UserName` varchar(50) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`),
  KEY `idxLastName` (`CompanyRefId`,`BranchRefId`,`LastName`)
) ENGINE=InnoDB AUTO_INCREMENT=214 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.employeesattendance
CREATE TABLE IF NOT EXISTS `employeesattendance` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` bigint(50) NOT NULL,
  `BranchRefId` bigint(50) NOT NULL,
  `EmployeesRefId` int(10) DEFAULT NULL,
  `Terminal` varchar(100) DEFAULT NULL,
  `SensorID` varchar(45) CHARACTER SET utf32 COLLATE utf32_bin DEFAULT NULL,
  `AttendanceDate` date DEFAULT NULL,
  `AttendanceTime` bigint(50) NOT NULL,
  `CheckTime` bigint(50) DEFAULT NULL,
  `KindOfEntry` int(2) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=768 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.employeesauthority
CREATE TABLE IF NOT EXISTS `employeesauthority` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` bigint(50) NOT NULL,
  `BranchRefId` bigint(50) NOT NULL,
  `EmployeesRefId` bigint(50) NOT NULL,
  `AbsencesRefId` bigint(50) DEFAULT NULL,
  `ApplicationDateFrom` date DEFAULT NULL,
  `ApplicationDateTo` date DEFAULT NULL,
  `FromTime` int(11) DEFAULT NULL,
  `ToTime` int(11) DEFAULT NULL,
  `FiledDate` date DEFAULT NULL,
  `Status` varchar(100) DEFAULT NULL,
  `ApprovedByRefId` bigint(50) DEFAULT NULL,
  `Reason` varchar(300) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `Destination` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  `Whole` int(11) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=125 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.employeeschild
CREATE TABLE IF NOT EXISTS `employeeschild` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `EmployeesRefId` bigint(50) DEFAULT NULL,
  `CompanyRefId` int(10) DEFAULT NULL,
  `BranchRefId` int(10) DEFAULT NULL,
  `FullName` varchar(300) DEFAULT NULL,
  `BirthDate` date DEFAULT NULL,
  `Gender` varchar(1) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`),
  KEY `Index 3` (`CompanyRefId`,`BranchRefId`,`EmployeesRefId`,`FullName`,`BirthDate`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.employeescreditbalance
CREATE TABLE IF NOT EXISTS `employeescreditbalance` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `NameCredits` varchar(100) NOT NULL,
  `CompanyRefId` bigint(50) NOT NULL,
  `BranchRefId` bigint(50) NOT NULL,
  `EmployeesRefId` bigint(50) DEFAULT NULL,
  `EffectivityYear` int(4) DEFAULT NULL,
  `BegBalAsOfDate` date DEFAULT NULL,
  `BeginningBalance` decimal(10,3) DEFAULT NULL,
  `OutstandingBalance` decimal(10,3) DEFAULT NULL,
  `ForceLeave` int(4) DEFAULT NULL,
  `ExpiryDate` date DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  `EffectivityDate` date DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=419 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.employeescto
CREATE TABLE IF NOT EXISTS `employeescto` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` bigint(50) NOT NULL,
  `BranchRefId` bigint(50) NOT NULL,
  `EmployeesRefId` bigint(50) NOT NULL,
  `ApplicationDateFrom` date DEFAULT NULL,
  `ApplicationDateTo` date DEFAULT NULL,
  `FiledDate` date DEFAULT NULL,
  `Hours` int(10) DEFAULT NULL,
  `Status` varchar(100) DEFAULT NULL,
  `ApprovedByRefId` bigint(50) DEFAULT NULL,
  `Reason` varchar(300) DEFAULT NULL,
  `Balance` decimal(10,3) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`),
  KEY `EmpRefId` (`EmployeesRefId`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.employeesdevtplan
CREATE TABLE IF NOT EXISTS `employeesdevtplan` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `EmployeesRefId` bigint(50) DEFAULT NULL,
  `CompanyRefId` int(10) DEFAULT NULL,
  `BranchRefId` int(10) DEFAULT NULL,
  `DesiredYears` int(10) DEFAULT NULL,
  `SupervisorName` varchar(255) COLLATE utf32_bin DEFAULT NULL,
  `Purpose1` int(1) DEFAULT '0' COMMENT 'To meet the competencies of current position',
  `Purpose2` int(1) DEFAULT '0' COMMENT 'To increase the level of competencies of current position',
  `Purpose3` int(1) DEFAULT '0' COMMENT 'To meet the competencies of the next higher position',
  `Purpose4` int(1) DEFAULT '0' COMMENT 'To acquire new competencies accross different functions/position',
  `Purpose5` int(1) DEFAULT '0',
  `Purpose5Others` varchar(255) COLLATE utf32_bin DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `Data` varchar(1) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_bin;

-- Data exporting was unselected.
-- Dumping structure for table phcc.employeeseduc
CREATE TABLE IF NOT EXISTS `employeeseduc` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `EmployeesRefId` bigint(50) DEFAULT NULL,
  `CompanyRefId` int(10) DEFAULT NULL,
  `BranchRefId` int(10) DEFAULT NULL,
  `LevelType` tinyint(1) DEFAULT NULL,
  `SchoolsRefId` smallint(5) DEFAULT NULL,
  `CourseRefId` smallint(5) DEFAULT NULL,
  `YearGraduated` int(4) DEFAULT NULL,
  `HighestGrade` varchar(100) DEFAULT NULL,
  `DateFrom` int(4) DEFAULT NULL,
  `DateTo` int(4) DEFAULT NULL,
  `Honors` varchar(300) DEFAULT NULL,
  `Present` smallint(1) DEFAULT '0',
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.employeeselegibility
CREATE TABLE IF NOT EXISTS `employeeselegibility` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `EmployeesRefId` bigint(50) DEFAULT NULL,
  `CompanyRefId` int(10) DEFAULT NULL,
  `BranchRefId` int(10) DEFAULT NULL,
  `CareerServiceRefId` int(10) DEFAULT NULL,
  `Rating` decimal(5,2) DEFAULT NULL,
  `ExamDate` date DEFAULT NULL,
  `ExamPlace` varchar(250) DEFAULT NULL,
  `LicenseNo` varchar(50) DEFAULT NULL,
  `LicenseReleasedDate` date DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.employeesfamily
CREATE TABLE IF NOT EXISTS `employeesfamily` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `EmployeesRefId` bigint(50) DEFAULT NULL,
  `CompanyRefId` int(10) DEFAULT NULL,
  `BranchRefId` int(10) DEFAULT NULL,
  `SpouseLastName` varchar(250) DEFAULT NULL,
  `SpouseFirstName` varchar(250) DEFAULT NULL,
  `SpouseMiddleName` varchar(250) DEFAULT NULL,
  `SpouseExtName` varchar(15) DEFAULT NULL,
  `SpouseBirthDate` date DEFAULT NULL,
  `OccupationsRefId` int(10) DEFAULT NULL,
  `EmployersName` varchar(250) DEFAULT NULL,
  `BusinessAddress` varchar(250) DEFAULT NULL,
  `SpouseMobileNo` varchar(11) DEFAULT NULL,
  `FatherLastName` varchar(250) DEFAULT NULL,
  `FatherFirstName` varchar(250) DEFAULT NULL,
  `FatherMiddleName` varchar(250) DEFAULT NULL,
  `FatherExtName` varchar(15) DEFAULT NULL,
  `MotherLastName` varchar(250) DEFAULT NULL,
  `MotherFirstName` varchar(250) DEFAULT NULL,
  `MotherMiddleName` varchar(250) DEFAULT NULL,
  `MotherExtName` varchar(15) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`),
  KEY `Index 3` (`CompanyRefId`,`BranchRefId`,`EmployeesRefId`),
  KEY `Index 4` (`EmployeesRefId`)
) ENGINE=InnoDB AUTO_INCREMENT=196 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.employeeslearningappplan
CREATE TABLE IF NOT EXISTS `employeeslearningappplan` (
  `RefId` int(10) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` int(10) DEFAULT NULL,
  `BranchRefId` int(10) DEFAULT NULL,
  `EmployeesRefId` int(10) DEFAULT NULL,
  `OfficeRefId` int(10) DEFAULT NULL,
  `DateConducted` date DEFAULT NULL,
  `Interventions` varchar(255) DEFAULT NULL,
  `TargetToEnhance` varchar(255) DEFAULT NULL,
  `Q1Ans` varchar(255) DEFAULT NULL COMMENT 'Learning Goals What skills, knowledge and attitude do I require to achieve competency target?',
  `Q2Ans` varchar(255) DEFAULT NULL COMMENT 'Current Status What level of skills, knowledge and attitude do I have now with respect to this learning goal?',
  `Q3Ans` varchar(255) DEFAULT NULL COMMENT 'Learning Strategies How will I reach my learning goal?',
  `Q4Ans` varchar(255) DEFAULT NULL COMMENT 'Required Resources What resources do I need to achieve this learning goal?',
  `Q5Ans` varchar(255) DEFAULT NULL COMMENT 'Key Performance Indicators How can I demonstrate to myself and others that I have achieved this learning goal?',
  `LastUpdateBy` varchar(255) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` timestamp NULL DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.employeesleave
CREATE TABLE IF NOT EXISTS `employeesleave` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` bigint(50) NOT NULL,
  `BranchRefId` bigint(50) NOT NULL,
  `EmployeesRefId` bigint(50) NOT NULL,
  `LeavesRefId` bigint(50) DEFAULT NULL,
  `SPLType` varchar(10) DEFAULT NULL,
  `FiledDate` date DEFAULT NULL,
  `ApplicationDateFrom` date DEFAULT NULL,
  `ApplicationDateTo` date DEFAULT NULL,
  `isForceLeave` tinyint(1) DEFAULT NULL,
  `IncludeSatSun` tinyint(1) DEFAULT NULL,
  `Status` varchar(100) DEFAULT NULL,
  `WithPay` tinyint(1) DEFAULT NULL,
  `ApprovedByRefId` bigint(50) DEFAULT NULL,
  `Reason` varchar(300) DEFAULT NULL,
  `Balance` decimal(9,3) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=130 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.employeesleavemonetization
CREATE TABLE IF NOT EXISTS `employeesleavemonetization` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` bigint(50) NOT NULL,
  `BranchRefId` bigint(50) NOT NULL,
  `EmployeesRefId` bigint(50) NOT NULL,
  `VLBalance` decimal(10,3) DEFAULT NULL,
  `VLValue` decimal(10,3) DEFAULT NULL,
  `VLAmount` decimal(10,3) DEFAULT NULL,
  `SLBalance` decimal(10,3) DEFAULT NULL,
  `SLValue` decimal(10,3) DEFAULT NULL,
  `SLAmount` decimal(10,3) DEFAULT NULL,
  `FiledDate` date DEFAULT NULL,
  `Status` varchar(100) DEFAULT NULL,
  `ApprovedByRefId` bigint(50) DEFAULT NULL,
  `Reason` varchar(300) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  `IsHalf` int(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.employeeslogin
CREATE TABLE IF NOT EXISTS `employeeslogin` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `EmployeesRefId` bigint(50) NOT NULL,
  `LoginDate` date DEFAULT NULL,
  `LoginTime` time DEFAULT NULL,
  `LoginAs` varchar(50) DEFAULT NULL,
  `Terminal` varchar(50) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=1902 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.employeesmovement
CREATE TABLE IF NOT EXISTS `employeesmovement` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` int(10) DEFAULT NULL,
  `BranchRefId` int(10) DEFAULT NULL,
  `EmployeesRefId` bigint(50) DEFAULT NULL,
  `ApptStatusRefId` int(10) DEFAULT NULL,
  `AgencyRefId` int(10) DEFAULT NULL,
  `PositionItemRefId` int(10) DEFAULT NULL,
  `PositionRefId` int(10) DEFAULT NULL,
  `DepartmentRefId` int(10) DEFAULT NULL,
  `OfficeRefId` int(10) DEFAULT NULL,
  `DivisionRefId` int(10) DEFAULT NULL,
  `EmpStatusRefId` int(10) DEFAULT NULL,
  `SalaryGradeRefId` int(10) DEFAULT NULL,
  `JobGradeRefId` int(10) DEFAULT NULL,
  `DesignationRefId` int(10) DEFAULT NULL,
  `StepIncrementRefId` int(10) DEFAULT NULL,
  `SalaryAmount` decimal(10,0) DEFAULT NULL,
  `isGovtService` tinyint(1) DEFAULT NULL,
  `EffectivityDate` date DEFAULT NULL,
  `ExpiryDate` date DEFAULT NULL,
  `TrnDate` date DEFAULT NULL,
  `TrnTime` time DEFAULT NULL,
  `IncumbentEmployeesRefId` int(10) DEFAULT NULL,
  `IncumbentSeparationReason` varchar(150) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  `NewEmployeesId` varchar(100) DEFAULT NULL,
  `LWOP` int(10) unsigned zerofill DEFAULT NULL,
  `LWOPDate` date DEFAULT NULL,
  `Cause` varchar(250) DEFAULT NULL,
  `Present` smallint(1) DEFAULT '0',
  `ApptType` smallint(1) DEFAULT NULL,
  `isServiceRecord` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`),
  KEY `RefId_2` (`RefId`),
  KEY `RefId_3` (`RefId`),
  KEY `RefId_4` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.employeesopcr
CREATE TABLE IF NOT EXISTS `employeesopcr` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` int(10) DEFAULT NULL,
  `BranchRefId` int(10) DEFAULT NULL,
  `EmployeesRefId` bigint(50) NOT NULL,
  `StrategicObjective` varchar(300) DEFAULT NULL,
  `SuccessIndicator` varchar(300) DEFAULT NULL,
  `AllotedBudget` decimal(6,2) DEFAULT '0.00',
  `DivisionAccountable` varchar(300) DEFAULT NULL,
  `ActualAccomplishment` varchar(300) DEFAULT NULL,
  `QualityRate` int(1) DEFAULT '0',
  `EfficiencyRate` int(1) DEFAULT '0',
  `TimelinessRate` int(1) DEFAULT '0',
  `AverageRate` int(1) DEFAULT '0',
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.employeesotherinfo
CREATE TABLE IF NOT EXISTS `employeesotherinfo` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `EmployeesRefId` bigint(50) DEFAULT NULL,
  `CompanyRefId` int(10) DEFAULT NULL,
  `BranchRefId` int(10) DEFAULT NULL,
  `Skills` varchar(300) DEFAULT NULL,
  `Recognition` varchar(300) DEFAULT NULL,
  `Affiliates` varchar(300) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.employeespdsq
CREATE TABLE IF NOT EXISTS `employeespdsq` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `EmployeesRefId` bigint(50) DEFAULT NULL,
  `CompanyRefId` int(10) DEFAULT NULL,
  `BranchRefId` int(10) DEFAULT NULL,
  `Q1a` int(1) DEFAULT '0' COMMENT '34. Within the third degree?',
  `Q1aexp` varchar(150) DEFAULT NULL,
  `Q1b` int(1) DEFAULT '0' COMMENT '34. Within the fourth degree (for Local Government Employees)?',
  `Q1bexp` varchar(150) DEFAULT NULL,
  `Q2a` int(1) DEFAULT '0' COMMENT '35 Have you ever been found guilty of any administrative offense?',
  `Q2aexp` varchar(150) DEFAULT NULL,
  `Q2b` int(1) DEFAULT '0' COMMENT '35 Have you been criminally charged before any court?',
  `Q2bexp` varchar(150) DEFAULT NULL,
  `Q2DateFiled` date DEFAULT NULL,
  `Q2CaseStatus` varchar(250) DEFAULT NULL,
  `Q3a` int(1) DEFAULT '0' COMMENT '36 Have you ever been convicted of any crime or violation of any law, decree, ordinance, or regulation by any court or tribunal?',
  `Q3aexp` varchar(150) DEFAULT NULL,
  `Q4a` int(1) DEFAULT '0' COMMENT '37 Have you ever been separated from the service in any of the following modes: resignation, retirement, dropped from the rolls, dismissal, termination, end of term, finished contract, AWOL or phased out, in the public or private sector?',
  `Q4aexp` varchar(150) DEFAULT NULL,
  `Q5a` int(1) DEFAULT '0' COMMENT '38 Have you ever been a candidate in a national or local election (except Barangay Election)?',
  `Q5aexp` varchar(150) DEFAULT NULL,
  `Q5b` int(1) DEFAULT '0' COMMENT '38 Have you resigned from the government service during the three (3)-month period before the last election to promote/actively campaign for a national or local candidate?',
  `Q5bexp` varchar(150) DEFAULT NULL,
  `Q6a` int(1) DEFAULT '0' COMMENT '39 Have you acquired the status of an immigrant or permanent resident of another country?',
  `Q6aexp` varchar(150) DEFAULT NULL,
  `Q7a` int(1) DEFAULT NULL COMMENT '40 Are you a member of any indigenous group?',
  `Q7aexp` varchar(150) DEFAULT NULL,
  `Q7b` int(11) DEFAULT NULL,
  `Q7bexp` varchar(150) DEFAULT NULL,
  `Q7c` int(1) DEFAULT NULL COMMENT '40 Are you a solo parent?',
  `Q7cexp` varchar(150) DEFAULT NULL,
  `Remarks` varchar(255) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=184 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.employeesperformance
CREATE TABLE IF NOT EXISTS `employeesperformance` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` bigint(50) NOT NULL,
  `BranchRefId` bigint(50) NOT NULL,
  `EmployeesId` varchar(255) DEFAULT NULL,
  `EmployeesRefId` int(10) DEFAULT NULL,
  `Semester` varchar(255) DEFAULT NULL,
  `YearPerformed` int(4) DEFAULT NULL,
  `OverallScore` decimal(9,2) DEFAULT NULL,
  `NumericalRating` decimal(9,2) DEFAULT NULL,
  `Adjectival` int(1) DEFAULT NULL,
  `TimePerformed` datetime DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.employeesrata
CREATE TABLE IF NOT EXISTS `employeesrata` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` bigint(50) DEFAULT NULL,
  `BranchRefId` bigint(50) DEFAULT NULL,
  `EmployeesId` varchar(255) DEFAULT NULL,
  `EmployeesRefId` int(10) DEFAULT NULL,
  `RA` decimal(6,2) DEFAULT NULL,
  `TA` decimal(6,2) DEFAULT NULL,
  `Honoraria` decimal(6,2) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.employeesrecognitionandreward
CREATE TABLE IF NOT EXISTS `employeesrecognitionandreward` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` bigint(50) NOT NULL,
  `BranchRefId` bigint(50) NOT NULL,
  `EmployeesId` varchar(255) DEFAULT NULL,
  `EmployeesRefId` int(10) DEFAULT NULL,
  `AwardsRefId` int(10) DEFAULT NULL,
  `YearReceived` date DEFAULT NULL,
  `TimeReceived` datetime DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.employeesreference
CREATE TABLE IF NOT EXISTS `employeesreference` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `EmployeesRefId` bigint(50) DEFAULT NULL,
  `CompanyRefId` int(10) DEFAULT NULL,
  `BranchRefId` int(10) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Address` varchar(300) DEFAULT NULL,
  `ContactNo` varchar(11) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.employeesrequirements
CREATE TABLE IF NOT EXISTS `employeesrequirements` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` int(10) DEFAULT NULL,
  `BranchRefId` int(10) DEFAULT NULL,
  `ApplicantRefId` bigint(50) NOT NULL,
  `PositionRefId` bigint(50) NOT NULL,
  `DivisionRefId` bigint(50) NOT NULL,
  `EmpStatusRefId` bigint(50) NOT NULL,
  `HiredDate` date DEFAULT NULL,
  `CompletionStatus` varchar(300) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.employeesrequirementsdetail
CREATE TABLE IF NOT EXISTS `employeesrequirementsdetail` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `EmpRequirementsRefId` bigint(50) DEFAULT NULL,
  `DateSubmit` date DEFAULT NULL,
  `RequirementsRefId` bigint(50) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.employeesspecialpayroll
CREATE TABLE IF NOT EXISTS `employeesspecialpayroll` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `EmployeesId` varchar(255) DEFAULT NULL,
  `CompanyRefId` bigint(50) DEFAULT NULL,
  `BranchRefId` bigint(50) DEFAULT NULL,
  `EmployeesRefId` int(10) DEFAULT NULL,
  `BenefitRefId` int(10) DEFAULT NULL,
  `TaxRate` decimal(6,2) DEFAULT NULL,
  `TaxAmount` decimal(6,2) DEFAULT NULL,
  `DeductionRefId` int(50) DEFAULT NULL,
  `Netpay` decimal(6,2) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.employeestraining
CREATE TABLE IF NOT EXISTS `employeestraining` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `EmployeesRefId` bigint(50) DEFAULT NULL,
  `CompanyRefId` int(10) DEFAULT NULL,
  `BranchRefId` int(10) DEFAULT NULL,
  `SeminarsRefId` int(10) DEFAULT NULL,
  `StartDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  `Present` int(1) DEFAULT NULL,
  `NumofHrs` int(5) DEFAULT NULL,
  `SponsorRefId` int(5) DEFAULT NULL,
  `SeminarPlaceRefId` int(5) DEFAULT NULL,
  `SeminarPlace` varchar(300) DEFAULT NULL,
  `SeminarTypeRefId` int(5) DEFAULT NULL,
  `SeminarClassRefId` int(5) DEFAULT NULL,
  `Description` varchar(200) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=205 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.employeesvoluntary
CREATE TABLE IF NOT EXISTS `employeesvoluntary` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `EmployeesRefId` bigint(50) DEFAULT NULL,
  `CompanyRefId` int(10) DEFAULT NULL,
  `BranchRefId` int(10) DEFAULT NULL,
  `OrganizationRefId` int(10) DEFAULT NULL,
  `StartDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  `Present` int(1) DEFAULT NULL,
  `NumofHrs` int(5) DEFAULT NULL,
  `WorksNature` varchar(200) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.employeesworkexperience
CREATE TABLE IF NOT EXISTS `employeesworkexperience` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `EmployeesRefId` bigint(50) DEFAULT NULL,
  `CompanyRefId` int(10) DEFAULT NULL,
  `BranchRefId` int(10) DEFAULT NULL,
  `AgencyRefId` int(10) DEFAULT NULL,
  `PositionRefId` int(10) DEFAULT NULL,
  `EmpStatusRefId` int(10) DEFAULT NULL,
  `SalaryGradeRefId` int(10) DEFAULT NULL,
  `StepIncrementRefId` int(10) DEFAULT NULL,
  `JobGradeRefId` int(10) DEFAULT NULL,
  `PayrateRefId` int(10) DEFAULT NULL,
  `WorkStartDate` date DEFAULT NULL,
  `WorkEndDate` date DEFAULT NULL,
  `Present` int(11) DEFAULT NULL,
  `SalaryAmount` decimal(9,2) DEFAULT NULL,
  `YearRate` decimal(9,2) DEFAULT NULL,
  `LWOP` varchar(200) DEFAULT NULL,
  `Reason` varchar(200) DEFAULT NULL,
  `ExtDate` date DEFAULT NULL,
  `SeparatedDate` date DEFAULT NULL,
  `ApptStatusRefId` smallint(10) DEFAULT NULL,
  `isGovtService` tinyint(1) DEFAULT NULL,
  `isServiceRecord` tinyint(1) DEFAULT NULL,
  `PayGroup` varchar(20) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.employees_work_experience_attachments
CREATE TABLE IF NOT EXISTS `employees_work_experience_attachments` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` int(11) DEFAULT NULL,
  `BranchRefId` int(11) DEFAULT NULL,
  `EmployeesRefId` int(11) DEFAULT NULL,
  `WorkExperienceRefId` int(11) DEFAULT NULL,
  `PositionRefId` int(11) DEFAULT NULL,
  `OfficeRefId` int(11) DEFAULT NULL,
  `AgencyRefId` int(11) DEFAULT NULL,
  `StartDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  `Supervisor` varchar(300) DEFAULT NULL,
  `Location` varchar(300) DEFAULT NULL,
  `Accomplishments` varchar(1500) DEFAULT NULL,
  `Duties` varchar(1500) DEFAULT NULL,
  `LastUpdateBy` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.employee_pm
CREATE TABLE IF NOT EXISTS `employee_pm` (
  `id` int(21) NOT NULL AUTO_INCREMENT,
  `idno` varchar(6) DEFAULT NULL,
  `date` varchar(100) DEFAULT NULL,
  `overall_point_score` varchar(11) DEFAULT NULL,
  `numerical_rating` varchar(11) DEFAULT NULL,
  `adjectival` varchar(100) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.employee_rr
CREATE TABLE IF NOT EXISTS `employee_rr` (
  `id` int(21) NOT NULL AUTO_INCREMENT,
  `idno` varchar(6) DEFAULT NULL,
  `award` varchar(100) DEFAULT NULL,
  `year_received` date DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.emprequirements
CREATE TABLE IF NOT EXISTS `emprequirements` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` int(10) DEFAULT NULL,
  `BranchRefId` int(10) DEFAULT NULL,
  `ApplicantRefId` bigint(50) DEFAULT NULL,
  `PositionRefId` bigint(50) DEFAULT NULL,
  `DivisionRefId` bigint(50) DEFAULT NULL,
  `EmpStatusRefId` bigint(50) DEFAULT NULL,
  `HiredDate` date DEFAULT NULL,
  `CompletionStatus` varchar(300) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.emprequirementsdetail
CREATE TABLE IF NOT EXISTS `emprequirementsdetail` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `EmpRequirementsRefId` bigint(50) DEFAULT NULL,
  `DateSubmit` date DEFAULT NULL,
  `RequirementsRefId` bigint(50) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.empstatus
CREATE TABLE IF NOT EXISTS `empstatus` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  `IsPrivate` int(1) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `category` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.fl_cancellation_request
CREATE TABLE IF NOT EXISTS `fl_cancellation_request` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` bigint(50) NOT NULL,
  `BranchRefId` bigint(50) NOT NULL,
  `EmployeesRefId` bigint(50) NOT NULL,
  `Status` varchar(50) DEFAULT NULL,
  `ApprovedByRefId` bigint(50) DEFAULT NULL,
  `Reason` varchar(300) DEFAULT NULL,
  `EffectivityYear` int(4) DEFAULT NULL,
  `FiledDate` date DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  `EmployeesLeaveRefId` bigint(50) DEFAULT NULL,
  `LeavesRefId` bigint(50) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.forms
CREATE TABLE IF NOT EXISTS `forms` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `SystemRefId` int(10) DEFAULT NULL,
  `UseFor` int(10) DEFAULT NULL,
  `FileName` varchar(255) COLLATE utf32_bin DEFAULT NULL,
  `Code` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `Name` varchar(300) CHARACTER SET latin1 DEFAULT NULL,
  `Description` varchar(300) COLLATE utf32_bin DEFAULT NULL,
  `Remarks` varchar(300) CHARACTER SET latin1 DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `Data` varchar(1) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf32 COLLATE=utf32_bin;

-- Data exporting was unselected.
-- Dumping structure for table phcc.gsispolicy
CREATE TABLE IF NOT EXISTS `gsispolicy` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(10) DEFAULT NULL,
  `PolicyName` varchar(30) DEFAULT NULL,
  `PayPeriod` varchar(50) DEFAULT NULL,
  `DeductionPeriod` varchar(50) DEFAULT NULL,
  `PolicyType` varchar(50) DEFAULT NULL,
  `BasedOn` varchar(50) DEFAULT NULL,
  `Computation` bigint(10) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  `Name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.holiday
CREATE TABLE IF NOT EXISTS `holiday` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` int(11) NOT NULL,
  `BranchRefId` int(11) NOT NULL,
  `Code` varchar(50) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `StartDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  `StartTime` int(4) DEFAULT NULL,
  `EndTime` int(4) DEFAULT NULL,
  `isLegal` tinyint(1) DEFAULT NULL,
  `isApplyEveryYr` tinyint(1) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.interimdivision
CREATE TABLE IF NOT EXISTS `interimdivision` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(200) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Description` varchar(200) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.interimoffice
CREATE TABLE IF NOT EXISTS `interimoffice` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(200) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Description` varchar(200) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.interimposition
CREATE TABLE IF NOT EXISTS `interimposition` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(200) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Description` varchar(200) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.ipar_core_functions
CREATE TABLE IF NOT EXISTS `ipar_core_functions` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `spms_ipar_id` bigint(50) DEFAULT NULL,
  `objective` varchar(300) DEFAULT NULL,
  `percent` varchar(10) DEFAULT NULL,
  `success_indicator` varchar(300) DEFAULT NULL,
  `sitype` varchar(300) DEFAULT NULL,
  `accomplishment` varchar(300) DEFAULT NULL,
  `numerical` varchar(300) DEFAULT NULL,
  `adjectival` varchar(300) DEFAULT NULL,
  `remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.ipar_strategic_priorities
CREATE TABLE IF NOT EXISTS `ipar_strategic_priorities` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `spms_ipar_id` bigint(50) DEFAULT NULL,
  `objective` varchar(300) DEFAULT NULL,
  `percent` varchar(10) DEFAULT NULL,
  `success_indicator` varchar(300) DEFAULT NULL,
  `sitype` varchar(300) DEFAULT NULL,
  `accomplishment` varchar(300) DEFAULT NULL,
  `numerical` varchar(300) DEFAULT NULL,
  `adjectival` varchar(300) DEFAULT NULL,
  `remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.ipar_support_functions
CREATE TABLE IF NOT EXISTS `ipar_support_functions` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `spms_ipar_id` bigint(50) DEFAULT NULL,
  `objective` varchar(300) DEFAULT NULL,
  `percent` varchar(10) DEFAULT NULL,
  `success_indicator` varchar(300) DEFAULT NULL,
  `sitype` varchar(300) DEFAULT NULL,
  `accomplishment` varchar(300) DEFAULT NULL,
  `numerical` varchar(300) DEFAULT NULL,
  `adjectival` varchar(300) DEFAULT NULL,
  `remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.ipcr_core_functions
CREATE TABLE IF NOT EXISTS `ipcr_core_functions` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `spms_ipcr_id` bigint(50) DEFAULT NULL,
  `objective` varchar(300) DEFAULT NULL,
  `percent` varchar(10) DEFAULT NULL,
  `success_indicator` varchar(300) DEFAULT NULL,
  `sitype` varchar(300) DEFAULT NULL,
  `q1` int(1) DEFAULT NULL,
  `q2` int(1) DEFAULT NULL,
  `q3` int(1) DEFAULT NULL,
  `q4` int(1) DEFAULT NULL,
  `rating` varchar(300) DEFAULT NULL,
  `remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.ipcr_strategic_priorities
CREATE TABLE IF NOT EXISTS `ipcr_strategic_priorities` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `spms_ipcr_id` bigint(50) DEFAULT NULL,
  `objective` varchar(300) DEFAULT NULL,
  `percent` varchar(10) DEFAULT NULL,
  `success_indicator` varchar(300) DEFAULT NULL,
  `sitype` varchar(300) DEFAULT NULL,
  `q1` int(1) DEFAULT NULL,
  `q2` int(1) DEFAULT NULL,
  `q3` int(1) DEFAULT NULL,
  `q4` int(1) DEFAULT NULL,
  `rating` varchar(300) DEFAULT NULL,
  `remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.ipcr_support_functions
CREATE TABLE IF NOT EXISTS `ipcr_support_functions` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `spms_ipcr_id` bigint(50) DEFAULT NULL,
  `objective` varchar(300) DEFAULT NULL,
  `percent` varchar(10) DEFAULT NULL,
  `success_indicator` varchar(300) DEFAULT NULL,
  `sitype` varchar(300) DEFAULT NULL,
  `q1` int(1) DEFAULT NULL,
  `q2` int(1) DEFAULT NULL,
  `q3` int(1) DEFAULT NULL,
  `q4` int(1) DEFAULT NULL,
  `rating` varchar(300) DEFAULT NULL,
  `remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.issues
CREATE TABLE IF NOT EXISTS `issues` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` bigint(10) NOT NULL,
  `BranchRefId` bigint(10) NOT NULL,
  `Issue` varchar(50) DEFAULT NULL,
  `Description` varchar(300) NOT NULL,
  `CreatedBy` varchar(10) DEFAULT NULL,
  `ImageFile` varchar(100) DEFAULT NULL,
  `Status` varchar(10) DEFAULT NULL,
  `AssignedTo` varchar(100) NOT NULL,
  `CurrentlyWorkingBy` varchar(100) NOT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.issuescomments
CREATE TABLE IF NOT EXISTS `issuescomments` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` bigint(50) DEFAULT NULL,
  `BranchRefId` bigint(50) DEFAULT NULL,
  `issuesRefId` bigint(50) DEFAULT NULL,
  `Comments` varchar(300) DEFAULT NULL,
  `CommentBy` varchar(100) DEFAULT NULL,
  `AssignedTo` varchar(100) NOT NULL,
  `PictureComments` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.jobgrade
CREATE TABLE IF NOT EXISTS `jobgrade` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `Name` varchar(300) CHARACTER SET latin1 DEFAULT NULL,
  `Step1` decimal(9,2) DEFAULT NULL,
  `AnnualS1` decimal(9,2) DEFAULT NULL,
  `EffectivityDateS1` date DEFAULT NULL,
  `Step2` decimal(9,2) DEFAULT NULL,
  `AnnualS2` decimal(9,2) DEFAULT NULL,
  `EffectivityDateS2` date DEFAULT NULL,
  `Step3` decimal(9,2) DEFAULT NULL,
  `AnnualS3` decimal(9,2) DEFAULT NULL,
  `EffectivityDateS3` date DEFAULT NULL,
  `Step4` decimal(9,2) DEFAULT NULL,
  `AnnualS4` decimal(9,2) DEFAULT NULL,
  `EffectivityDateS4` date DEFAULT NULL,
  `Step5` decimal(9,2) DEFAULT NULL,
  `AnnualS5` decimal(9,2) DEFAULT NULL,
  `EffectivityDateS5` decimal(9,2) DEFAULT NULL,
  `Step6` decimal(9,2) DEFAULT NULL,
  `AnnualS6` decimal(9,2) DEFAULT NULL,
  `EffectivityDateS6` date DEFAULT NULL,
  `Step7` decimal(9,2) DEFAULT NULL,
  `AnnualS7` decimal(9,2) DEFAULT NULL,
  `EffectivityDateS7` date DEFAULT NULL,
  `Step8` decimal(9,2) DEFAULT NULL,
  `AnnualS8` decimal(9,2) DEFAULT NULL,
  `EffectivityDateS8` date DEFAULT NULL,
  `Remarks` varchar(300) CHARACTER SET latin1 DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `Data` varchar(1) CHARACTER SET latin1 DEFAULT NULL,
  UNIQUE KEY `refid` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf32 COLLATE=utf32_bin;

-- Data exporting was unselected.
-- Dumping structure for table phcc.leavecreditsearnedwopay
CREATE TABLE IF NOT EXISTS `leavecreditsearnedwopay` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `EffectivityDate` date NOT NULL,
  `NoOfDaysPresent` decimal(6,3) DEFAULT NULL,
  `NoOfDaysLeaveWOP` decimal(6,3) DEFAULT NULL,
  `LeaveCreditsEarned` decimal(6,3) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.leavepolicy
CREATE TABLE IF NOT EXISTS `leavepolicy` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Name` varchar(300) DEFAULT NULL,
  `LeavePolicyGroupRefId` int(10) DEFAULT NULL,
  `LeavesRefId` bigint(10) NOT NULL,
  `Accumulating` tinyint(1) DEFAULT NULL,
  `OffsetLate` tinyint(1) DEFAULT NULL,
  `OffsetUT` tinyint(1) DEFAULT NULL,
  `OffsetIfNoFile` tinyint(1) DEFAULT NULL,
  `ForceLeaveApplied` tinyint(1) DEFAULT NULL,
  `AffectedByAbs` tinyint(1) DEFAULT NULL,
  `Value` decimal(6,2) DEFAULT NULL,
  `MaxForceLeave` varchar(10) NOT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.leavepolicygroup
CREATE TABLE IF NOT EXISTS `leavepolicygroup` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(10) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Description` varchar(100) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.leaves
CREATE TABLE IF NOT EXISTS `leaves` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `DefValue` decimal(6,2) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.loans
CREATE TABLE IF NOT EXISTS `loans` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(10) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `LoanType` varchar(50) DEFAULT NULL,
  `Category` varchar(50) DEFAULT NULL,
  `GSISExcelCol` varchar(50) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.locations
CREATE TABLE IF NOT EXISTS `locations` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.modules
CREATE TABLE IF NOT EXISTS `modules` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `SystemRefId` int(10) DEFAULT NULL,
  `ScrnId` varchar(100) COLLATE utf32_bin DEFAULT NULL,
  `UseFor` int(10) DEFAULT NULL,
  `Code` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `Ordinal` int(6) DEFAULT NULL,
  `Name` varchar(300) CHARACTER SET latin1 DEFAULT NULL,
  `Icons` varchar(50) COLLATE utf32_bin DEFAULT NULL,
  `Filename` varchar(100) COLLATE utf32_bin DEFAULT NULL,
  `Description` varchar(300) COLLATE utf32_bin DEFAULT NULL,
  `Remarks` varchar(300) CHARACTER SET latin1 DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `Data` varchar(1) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf32 COLLATE=utf32_bin COMMENT='System Modules';

-- Data exporting was unselected.
-- Dumping structure for table phcc.newsignup
CREATE TABLE IF NOT EXISTS `newsignup` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` bigint(50) DEFAULT NULL,
  `BranchRefId` bigint(50) DEFAULT NULL,
  `CompanyId` varchar(10) DEFAULT NULL,
  `LastName` varchar(50) DEFAULT NULL,
  `FirstName` varchar(50) DEFAULT NULL,
  `DepartmentRefId` bigint(50) DEFAULT NULL,
  `DivisionRefId` bigint(50) DEFAULT NULL,
  `SecQues1` varchar(300) DEFAULT NULL,
  `AnsQues1` varchar(300) DEFAULT NULL,
  `SecQues2` varchar(300) DEFAULT NULL,
  `AnsQues2` varchar(300) DEFAULT NULL,
  `Status` varchar(300) DEFAULT NULL,
  `ApprovedByRefId` bigint(50) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.occupations
CREATE TABLE IF NOT EXISTS `occupations` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Description` varchar(200) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.office
CREATE TABLE IF NOT EXISTS `office` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) DEFAULT NULL,
  `MFO_PAP` varchar(50) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `IsInterim` int(1) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.officesuspension
CREATE TABLE IF NOT EXISTS `officesuspension` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` bigint(50) NOT NULL,
  `BranchRefId` bigint(50) NOT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `StartDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  `StartTime` int(11) DEFAULT NULL,
  `EndTime` int(11) DEFAULT NULL,
  `EmployeesRefId` bigint(50) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  `Whole` int(11) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.organization
CREATE TABLE IF NOT EXISTS `organization` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(300) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Address` varchar(200) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.overtimepolicy
CREATE TABLE IF NOT EXISTS `overtimepolicy` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Name` varchar(300) DEFAULT NULL,
  `OvertimePolicyGroupRefId` int(10) DEFAULT NULL,
  `ForfeitedIfLate` tinyint(1) DEFAULT NULL,
  `ForfeitedIfUT` tinyint(1) DEFAULT NULL,
  `ForfeitedIfLeave` tinyint(1) DEFAULT NULL,
  `ForfeitedIfAbsPrevDay` tinyint(1) DEFAULT NULL,
  `PrevDay` varchar(5) DEFAULT NULL,
  `MaxOTWorkingDays` int(10) DEFAULT NULL,
  `MaxOTSaturday` int(10) DEFAULT NULL,
  `MaxOTHolidays` int(10) DEFAULT NULL,
  `MaxOTMonth` int(10) DEFAULT NULL,
  `COCRatesWorkingDays` decimal(10,3) DEFAULT NULL,
  `COCRatesSaturday` decimal(10,3) DEFAULT NULL,
  `COCRatesHolidays` decimal(10,3) DEFAULT NULL,
  `MaxUnusedCOCYear` int(10) DEFAULT NULL,
  `ValidYears` int(10) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  `WithPay` int(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`),
  UNIQUE KEY `Name` (`Name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.overtimepolicygroup
CREATE TABLE IF NOT EXISTS `overtimepolicygroup` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(10) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Description` varchar(100) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.overtime_request
CREATE TABLE IF NOT EXISTS `overtime_request` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` bigint(50) NOT NULL,
  `BranchRefId` bigint(50) NOT NULL,
  `StartDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  `FiledDate` date DEFAULT NULL,
  `EmployeesRefId` bigint(50) NOT NULL,
  `WithPay` tinyint(1) DEFAULT NULL,
  `Status` varchar(50) DEFAULT NULL,
  `ApprovedByRefId` bigint(50) DEFAULT NULL,
  `Reason` varchar(300) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  `OTClass` int(1) DEFAULT NULL,
  `OTLocation` int(1) DEFAULT NULL,
  `FromTime` int(4) DEFAULT NULL,
  `ToTime` int(4) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pagibigpolicy
CREATE TABLE IF NOT EXISTS `pagibigpolicy` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(10) DEFAULT NULL,
  `PolicyName` varchar(30) DEFAULT NULL,
  `PayPeriod` varchar(50) DEFAULT NULL,
  `DeductionPeriod` varchar(50) DEFAULT NULL,
  `PolicyType` varchar(50) DEFAULT NULL,
  `BasedOn` varchar(50) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  `Name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pagibigtable
CREATE TABLE IF NOT EXISTS `pagibigtable` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(10) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Below` decimal(6,2) DEFAULT NULL,
  `Above` decimal(6,2) DEFAULT NULL,
  `EmployeeShare` decimal(6,2) DEFAULT NULL,
  `EmployerShare` decimal(6,2) DEFAULT NULL,
  `EffectivityDate` date DEFAULT NULL,
  `MaxEmployeeShare` decimal(6,2) DEFAULT NULL,
  `MaxEmployerShare` decimal(6,2) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.payperiod
CREATE TABLE IF NOT EXISTS `payperiod` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(10) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.payrate
CREATE TABLE IF NOT EXISTS `payrate` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.philhealthpolicy
CREATE TABLE IF NOT EXISTS `philhealthpolicy` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(10) DEFAULT NULL,
  `PolicyName` varchar(30) DEFAULT NULL,
  `PayPeriod` varchar(50) DEFAULT NULL,
  `DeductionPeriod` varchar(50) DEFAULT NULL,
  `PolicyType` varchar(50) DEFAULT NULL,
  `BasedOn` varchar(50) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  `Name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.philhealthtable
CREATE TABLE IF NOT EXISTS `philhealthtable` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(10) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `EffectivityDate` date DEFAULT NULL,
  `SalaryBracket` int(10) DEFAULT NULL,
  `SalaryRangeFrom` decimal(6,2) DEFAULT NULL,
  `SalaryRangeTo` decimal(6,2) DEFAULT NULL,
  `SalaryBase` decimal(6,2) DEFAULT NULL,
  `MonthlyContribution` varchar(255) DEFAULT NULL,
  `EmployeeShare` decimal(6,2) DEFAULT NULL,
  `EmployerShare` decimal(6,2) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_adjustments
CREATE TABLE IF NOT EXISTS `pms_adjustments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(225) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `payroll_group` varchar(225) DEFAULT NULL,
  `tax_type` varchar(225) DEFAULT NULL,
  `itr_classification` varchar(225) DEFAULT NULL,
  `alphalist_classification` varchar(225) DEFAULT NULL,
  `amount` decimal(9,2) DEFAULT NULL,
  `remarks` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_annualtax_policy
CREATE TABLE IF NOT EXISTS `pms_annualtax_policy` (
  `id` bigint(50) NOT NULL AUTO_INCREMENT,
  `from_year` varchar(225) DEFAULT NULL,
  `to_year` varchar(225) DEFAULT NULL,
  `below_amount` decimal(12,2) DEFAULT NULL,
  `above_amount` decimal(12,2) DEFAULT NULL,
  `rate_percentage` decimal(12,2) DEFAULT NULL,
  `rate_amount` decimal(12,2) DEFAULT NULL,
  `excess_amount` decimal(12,2) DEFAULT NULL,
  `effectivity_date` date DEFAULT NULL,
  `remarks` varchar(300) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_annual_tax_rates
CREATE TABLE IF NOT EXISTS `pms_annual_tax_rates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_number` varchar(225) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `basic_amount` decimal(10,0) DEFAULT NULL,
  `contribution_amount` decimal(10,0) DEFAULT NULL,
  `pera_amount` decimal(10,0) DEFAULT NULL,
  `rata_amount` decimal(10,0) DEFAULT NULL,
  `longevity_amount` decimal(10,0) DEFAULT NULL,
  `hp_amount` decimal(10,0) DEFAULT NULL,
  `tax_amount` decimal(10,0) DEFAULT NULL,
  `for_month` varchar(50) DEFAULT NULL,
  `for_year` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_attendance_info
CREATE TABLE IF NOT EXISTS `pms_attendance_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `actual_workdays` int(11) DEFAULT NULL,
  `adjust_workdays` int(11) DEFAULT NULL,
  `total_workdays` decimal(9,2) DEFAULT NULL,
  `actual_absence` int(11) DEFAULT NULL,
  `adjust_absence` int(11) DEFAULT NULL,
  `total_absence` decimal(9,2) DEFAULT NULL,
  `actual_tardines` int(11) DEFAULT NULL,
  `adjust_tardines` int(11) DEFAULT NULL,
  `total_tardines` decimal(9,2) DEFAULT NULL,
  `actual_undertime` int(11) DEFAULT NULL,
  `adjust_undertime` int(11) DEFAULT NULL,
  `total_undertime` decimal(9,2) DEFAULT NULL,
  `month` varchar(225) DEFAULT NULL,
  `year` varchar(225) DEFAULT NULL,
  `employee_status` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_bankbranches
CREATE TABLE IF NOT EXISTS `pms_bankbranches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(225) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `address` varchar(225) DEFAULT NULL,
  `remarks` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_banks
CREATE TABLE IF NOT EXISTS `pms_banks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(225) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `branch_name` varchar(225) DEFAULT NULL,
  `bank_accountno` varchar(225) DEFAULT NULL,
  `remarks` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_beginning_balances
CREATE TABLE IF NOT EXISTS `pms_beginning_balances` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `premium_amount` decimal(9,2) DEFAULT NULL,
  `tax_witheld` decimal(9,2) DEFAULT NULL,
  `basic_pay` decimal(9,2) DEFAULT NULL,
  `overtime_pay` decimal(9,2) DEFAULT NULL,
  `thirteen_month_pay` decimal(9,2) DEFAULT NULL,
  `deminimis` decimal(9,2) DEFAULT NULL,
  `other_salaries` decimal(9,2) DEFAULT NULL,
  `taxable_basic_pay` decimal(9,2) DEFAULT NULL,
  `taxable_overtime_pay` decimal(9,2) DEFAULT NULL,
  `taxable_thirteen_month_pay` decimal(9,2) DEFAULT NULL,
  `taxable_other_salaries` decimal(9,2) DEFAULT NULL,
  `as_of_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_benefitinfo_transactions
CREATE TABLE IF NOT EXISTS `pms_benefitinfo_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `employee_number` varchar(225) DEFAULT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `benefit_id` int(11) DEFAULT NULL,
  `benefit_info_id` int(11) DEFAULT NULL,
  `amount` decimal(9,2) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `year` varchar(50) DEFAULT NULL,
  `month` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=384 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_benefits
CREATE TABLE IF NOT EXISTS `pms_benefits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(225) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `amount` decimal(9,2) DEFAULT NULL,
  `de_minimis_type` varchar(225) DEFAULT NULL,
  `computation_type` varchar(225) DEFAULT NULL,
  `payroll_group` varchar(225) DEFAULT NULL,
  `tax_type` varchar(225) DEFAULT NULL,
  `itr_classification` varchar(225) DEFAULT NULL,
  `alphalist_classification` varchar(225) DEFAULT NULL,
  `remarks` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_benefitsinfo
CREATE TABLE IF NOT EXISTS `pms_benefitsinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `employeeinfo_id` int(11) DEFAULT NULL,
  `benefit_id` int(11) DEFAULT NULL,
  `benefit_description` varchar(225) DEFAULT NULL,
  `benefit_amount` decimal(9,2) DEFAULT NULL,
  `benefit_pay_period` varchar(225) DEFAULT NULL,
  `benefit_pay_sub` varchar(225) DEFAULT NULL,
  `benefit_effectivity_date` date DEFAULT NULL,
  `date_from` date DEFAULT NULL,
  `date_to` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `employee_number` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=290 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_deductioninfo
CREATE TABLE IF NOT EXISTS `pms_deductioninfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employeeinfo_id` int(11) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `deduction_id` int(11) DEFAULT NULL,
  `deduct_pay_period` varchar(225) DEFAULT NULL,
  `deduct_amount` decimal(9,2) DEFAULT NULL,
  `deduct_date_start` date DEFAULT NULL,
  `deduct_date_end` date DEFAULT NULL,
  `deduct_date_terminated` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `employee_number` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_deductioninfo_transactions
CREATE TABLE IF NOT EXISTS `pms_deductioninfo_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `employee_number` varchar(225) DEFAULT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `deduction_id` int(11) DEFAULT NULL,
  `deduction_info_id` int(11) DEFAULT NULL,
  `amount` decimal(9,2) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `year` varchar(50) DEFAULT NULL,
  `month` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_deductions
CREATE TABLE IF NOT EXISTS `pms_deductions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(225) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `payroll_group` varchar(225) DEFAULT NULL,
  `tax_type` varchar(225) DEFAULT NULL,
  `itr_classification` varchar(225) DEFAULT NULL,
  `alphalist_classification` varchar(225) DEFAULT NULL,
  `amount` decimal(9,2) DEFAULT NULL,
  `remarks` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_departments
CREATE TABLE IF NOT EXISTS `pms_departments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_divisions
CREATE TABLE IF NOT EXISTS `pms_divisions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_employees
CREATE TABLE IF NOT EXISTS `pms_employees` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `employee_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middlename` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extension_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nickname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `birth_place` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `civil_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `citizenship_id` int(11) DEFAULT NULL,
  `citizenship_country_id` int(11) DEFAULT NULL,
  `filipino` tinyint(1) DEFAULT NULL,
  `naturalized` tinyint(1) DEFAULT NULL,
  `height` decimal(4,2) DEFAULT NULL,
  `weight` decimal(4,2) DEFAULT NULL,
  `blood_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pagibig` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gsis` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `philhealth` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sss` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_issued_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_issued_id_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_issued_id_place` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_issued_id_date_issued` date DEFAULT NULL,
  `govt_issued_valid_until` date DEFAULT NULL,
  `agency_employee_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `biometrics` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `house_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subdivision` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brgy` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `permanent_house_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_subdivision` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_brgy` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_city_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_province_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_country_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_telephone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `with_setup` int(11) DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=225 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_employee_information
CREATE TABLE IF NOT EXISTS `pms_employee_information` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `division_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `office_id` int(11) DEFAULT NULL,
  `employee_status_id` int(11) DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  `position_item_id` int(11) DEFAULT NULL,
  `hired_date` date DEFAULT NULL,
  `assumption_date` date DEFAULT NULL,
  `resigned_date` date DEFAULT NULL,
  `rehired_date` date DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `pay_period` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pay_rate_id` int(11) DEFAULT NULL,
  `work_schedule_id` int(11) DEFAULT NULL,
  `appointment_status_id` int(11) DEFAULT NULL,
  `designation_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `employee_number` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=172 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_employee_status
CREATE TABLE IF NOT EXISTS `pms_employee_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_gsispolicy
CREATE TABLE IF NOT EXISTS `pms_gsispolicy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `policy_name` varchar(225) DEFAULT NULL,
  `pay_period` varchar(225) DEFAULT NULL,
  `deduction_period` varchar(225) DEFAULT NULL,
  `policy_type` varchar(225) DEFAULT NULL,
  `based_on` varchar(225) DEFAULT NULL,
  `computation` varchar(225) DEFAULT NULL,
  `ee_percentage` decimal(9,2) DEFAULT NULL,
  `er_percentage` decimal(9,2) DEFAULT NULL,
  `remarks` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_jobgrade
CREATE TABLE IF NOT EXISTS `pms_jobgrade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_grade` varchar(225) DEFAULT NULL,
  `step1` decimal(9,2) DEFAULT NULL,
  `step2` decimal(9,2) DEFAULT NULL,
  `step3` decimal(9,2) DEFAULT NULL,
  `step4` decimal(9,2) DEFAULT NULL,
  `step5` decimal(9,2) DEFAULT NULL,
  `step6` decimal(9,2) DEFAULT NULL,
  `step7` decimal(9,2) DEFAULT NULL,
  `step8` decimal(9,2) DEFAULT NULL,
  `remarks` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_leave
CREATE TABLE IF NOT EXISTS `pms_leave` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `rata_id` int(11) DEFAULT NULL,
  `leave_type` varchar(225) DEFAULT NULL,
  `number_of_leave_field` int(11) DEFAULT NULL,
  `leave_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_leave_monetization_transactions
CREATE TABLE IF NOT EXISTS `pms_leave_monetization_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `jobgrade_id` int(11) DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  `salary_amount` decimal(9,2) DEFAULT NULL,
  `net_amount` decimal(9,2) DEFAULT NULL,
  `factor_rate` decimal(9,2) DEFAULT NULL,
  `number_of_days` int(11) DEFAULT NULL,
  `remarks` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `employee_number` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_loaninfo_transactions
CREATE TABLE IF NOT EXISTS `pms_loaninfo_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_number` varchar(225) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `loan_id` int(11) DEFAULT NULL,
  `loan_info_id` int(11) DEFAULT NULL,
  `amount` decimal(9,2) DEFAULT NULL,
  `year` varchar(50) DEFAULT NULL,
  `month` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_loans
CREATE TABLE IF NOT EXISTS `pms_loans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(225) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `loan_type` varchar(225) DEFAULT NULL,
  `category` varchar(225) DEFAULT NULL,
  `gsis_excel_col` varchar(225) DEFAULT NULL,
  `amount` decimal(9,2) DEFAULT NULL,
  `remarks` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_loansinfo
CREATE TABLE IF NOT EXISTS `pms_loansinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employeeinfo_id` int(11) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `loan_id` int(11) DEFAULT NULL,
  `loan_totalamount` decimal(9,2) DEFAULT NULL,
  `loan_totalbalance` decimal(9,2) DEFAULT NULL,
  `loan_amortization` decimal(9,2) DEFAULT NULL,
  `loan_pay_period` varchar(225) DEFAULT NULL,
  `loan_date_granted` date DEFAULT NULL,
  `loan_date_started` date DEFAULT NULL,
  `loan_date_end` date DEFAULT NULL,
  `loan_date_terminated` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `employee_number` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_nonplantilla_employeeinfo
CREATE TABLE IF NOT EXISTS `pms_nonplantilla_employeeinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `taxpolicy_id` int(11) DEFAULT NULL,
  `taxpolicy_two_id` int(11) DEFAULT NULL,
  `atm_no` int(11) DEFAULT NULL,
  `daily_rate_amount` decimal(9,2) DEFAULT NULL,
  `overtime_balance_amount` decimal(9,2) DEFAULT NULL,
  `tax_id_number` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `employee_number` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_nonplantilla_transactions
CREATE TABLE IF NOT EXISTS `pms_nonplantilla_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `salaryinfo_id` int(11) DEFAULT NULL,
  `division_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `position_item_id` int(11) DEFAULT NULL,
  `office_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `employee_status_id` int(11) DEFAULT NULL,
  `employeeinfo_id` int(11) DEFAULT NULL,
  `actual_basicpay` decimal(9,2) DEFAULT NULL,
  `adjust_basicpay` decimal(9,2) DEFAULT NULL,
  `total_basicpay` decimal(9,2) DEFAULT NULL,
  `actual_absences` int(11) DEFAULT NULL,
  `adjust_absences` int(11) DEFAULT NULL,
  `total_absences` decimal(9,2) DEFAULT NULL,
  `actual_tardines` decimal(9,2) DEFAULT NULL,
  `adjust_tardines` decimal(9,2) DEFAULT NULL,
  `total_tardines` decimal(9,2) DEFAULT NULL,
  `actual_undertime` int(11) DEFAULT NULL,
  `adjust_undertime` int(11) DEFAULT NULL,
  `total_undertime` decimal(9,2) DEFAULT NULL,
  `monthly_rate_amount` decimal(9,2) DEFAULT NULL,
  `annual_rate_amount` decimal(9,2) DEFAULT NULL,
  `basic_net_pay` decimal(9,2) DEFAULT NULL,
  `actual_contribution` decimal(9,2) DEFAULT NULL,
  `adjust_contribution` decimal(9,2) DEFAULT NULL,
  `total_contribution` decimal(9,2) DEFAULT NULL,
  `actual_loan` decimal(9,2) DEFAULT NULL,
  `adjust_loan` decimal(9,2) DEFAULT NULL,
  `total_loan` decimal(9,2) DEFAULT NULL,
  `actual_otherdeduct` decimal(9,2) DEFAULT NULL,
  `adjust_otherdeduct` decimal(9,2) DEFAULT NULL,
  `total_otherdeduct` decimal(9,2) DEFAULT NULL,
  `net_deduction` decimal(9,2) DEFAULT NULL,
  `tax_rate_amount_one` decimal(9,2) DEFAULT NULL,
  `tax_rate_amount_two` decimal(9,2) DEFAULT NULL,
  `allowances` decimal(9,2) DEFAULT NULL,
  `gross_pay` decimal(9,2) DEFAULT NULL,
  `gross_taxable_pay` decimal(9,2) DEFAULT NULL,
  `net_pay` decimal(9,2) DEFAULT NULL,
  `pay_period` varchar(225) DEFAULT NULL,
  `sub_pay_period` varchar(225) DEFAULT NULL,
  `hold` int(11) DEFAULT NULL,
  `year` varchar(225) DEFAULT NULL,
  `month` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `employee_number` varchar(225) DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  `actual_basicpay_amount` decimal(9,2) DEFAULT NULL,
  `adjust_basicpay_amount` decimal(9,2) DEFAULT NULL,
  `total_basicpay_amount` decimal(9,2) DEFAULT NULL,
  `actual_absences_amount` decimal(9,2) DEFAULT NULL,
  `adjust_absences_amount` decimal(9,2) DEFAULT NULL,
  `total_absences_amount` decimal(9,2) DEFAULT NULL,
  `actual_tardiness_amount` decimal(9,2) DEFAULT NULL,
  `adjust_tardiness_amount` decimal(9,2) DEFAULT NULL,
  `total_tardiness_amount` decimal(9,2) DEFAULT NULL,
  `actual_undertime_amount` decimal(9,2) DEFAULT NULL,
  `adjust_undertime_amount` decimal(9,2) DEFAULT NULL,
  `total_undertime_amount` decimal(9,2) DEFAULT NULL,
  `actual_workdays` int(11) DEFAULT NULL,
  `adjust_workdays` int(11) DEFAULT NULL,
  `actual_tardiness` int(11) DEFAULT NULL,
  `adjust_tardiness` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_offices
CREATE TABLE IF NOT EXISTS `pms_offices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_overtime
CREATE TABLE IF NOT EXISTS `pms_overtime` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `previous_balance` decimal(9,2) DEFAULT NULL,
  `used_amount` decimal(9,2) DEFAULT NULL,
  `available_balance` decimal(9,2) DEFAULT NULL,
  `actual_regular_overtime` decimal(9,2) DEFAULT NULL,
  `adjust_regular_overtime` decimal(9,2) DEFAULT NULL,
  `total_regular_amount` decimal(9,2) DEFAULT NULL,
  `actual_special_overtime` decimal(9,2) DEFAULT NULL,
  `adjust_special_overtime` decimal(9,2) DEFAULT NULL,
  `total_special_amount` decimal(9,2) DEFAULT NULL,
  `actual_regular_holiday_overtime` decimal(9,2) DEFAULT NULL,
  `adjust_regular_holiday_overtime` decimal(9,2) DEFAULT NULL,
  `total_regular_holiday_amount` decimal(9,2) DEFAULT NULL,
  `total_overtime_amount` decimal(9,2) DEFAULT NULL,
  `year` varchar(50) DEFAULT NULL,
  `month` varchar(50) DEFAULT NULL,
  `pay_period` varchar(50) DEFAULT NULL,
  `sub_pay_period` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `employee_number` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_pagibigpolicy
CREATE TABLE IF NOT EXISTS `pms_pagibigpolicy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `policy_name` varchar(225) DEFAULT NULL,
  `pay_period` varchar(225) DEFAULT NULL,
  `deduction_period` varchar(225) DEFAULT NULL,
  `policy_type` varchar(225) DEFAULT NULL,
  `based_on` varchar(225) DEFAULT NULL,
  `value` decimal(9,2) DEFAULT NULL,
  `remarks` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_payroll_information
CREATE TABLE IF NOT EXISTS `pms_payroll_information` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bp_no` varchar(225) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `gsispolicy_id` int(11) DEFAULT NULL,
  `pagibigpolicy_id` int(11) DEFAULT NULL,
  `philhealthpolicy_id` int(11) DEFAULT NULL,
  `taxpolicy_id` int(11) DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `bankbranch_id` int(11) DEFAULT NULL,
  `wagestatus_id` int(11) DEFAULT NULL,
  `providentfund_id` int(11) DEFAULT NULL,
  `daily_rate_amount` decimal(9,2) DEFAULT NULL,
  `monthly_rate_amount` decimal(9,2) DEFAULT NULL,
  `annual_rate_amount` decimal(9,2) DEFAULT NULL,
  `pagibig_contribution` decimal(9,2) DEFAULT NULL,
  `er_pagibig_share` decimal(9,2) DEFAULT NULL,
  `philhealth_contribution` decimal(9,2) DEFAULT NULL,
  `er_philhealth_share` decimal(9,2) DEFAULT NULL,
  `gsis_contribution` decimal(9,2) DEFAULT NULL,
  `er_gsis_share` decimal(9,2) DEFAULT NULL,
  `tax_contribution` decimal(9,2) DEFAULT NULL,
  `overtime_balance_amount` decimal(9,2) DEFAULT NULL,
  `atm_no` varchar(225) DEFAULT NULL,
  `pagibig2` decimal(9,2) DEFAULT NULL,
  `pagibig_personal` decimal(9,2) DEFAULT NULL,
  `no_ofdays_inayear` int(11) DEFAULT NULL,
  `no_ofdays_inamonth` int(11) DEFAULT NULL,
  `total_hours_inaday` int(11) DEFAULT NULL,
  `tax_payperiod` varchar(255) DEFAULT NULL,
  `tax_bracket` varchar(255) DEFAULT NULL,
  `tax_bracket_amount` decimal(9,2) DEFAULT NULL,
  `tax_inexcess` decimal(9,2) DEFAULT NULL,
  `union_dues` decimal(9,2) DEFAULT NULL,
  `mid_year_bonus` tinyint(4) DEFAULT NULL,
  `year_end_bonus` tinyint(4) DEFAULT NULL,
  `tax_id_number` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `employee_number` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=176 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_philhealthpolicy
CREATE TABLE IF NOT EXISTS `pms_philhealthpolicy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `policy_name` varchar(225) DEFAULT NULL,
  `pay_period` varchar(225) DEFAULT NULL,
  `deduction_period` varchar(225) DEFAULT NULL,
  `policy_type` varchar(225) DEFAULT NULL,
  `based_on` varchar(225) DEFAULT NULL,
  `below` decimal(9,4) DEFAULT NULL,
  `above` decimal(9,2) DEFAULT NULL,
  `remarks` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_positionitem_setup
CREATE TABLE IF NOT EXISTS `pms_positionitem_setup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `positionitem_id` int(11) DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  `salarygrade_id` int(11) DEFAULT NULL,
  `jobgrade_id` int(11) DEFAULT NULL,
  `step_inc` varchar(225) DEFAULT NULL,
  `amount` decimal(9,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_positions
CREATE TABLE IF NOT EXISTS `pms_positions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=210 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_position_items
CREATE TABLE IF NOT EXISTS `pms_position_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(300) DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  `position_level_id` int(11) DEFAULT NULL,
  `position_classification_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=154 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_previous_employer
CREATE TABLE IF NOT EXISTS `pms_previous_employer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `allowances` decimal(9,2) DEFAULT NULL,
  `thirteen_month_pay` decimal(9,2) DEFAULT NULL,
  `deminimis_amount` decimal(9,2) DEFAULT NULL,
  `tax_witheld` decimal(9,2) DEFAULT NULL,
  `premium_amount` decimal(9,2) DEFAULT NULL,
  `taxable_basic_pay` decimal(9,2) DEFAULT NULL,
  `taxable_thirteen_month_pay` decimal(9,2) DEFAULT NULL,
  `taxable_allowances` decimal(9,2) DEFAULT NULL,
  `as_of_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_providentfund
CREATE TABLE IF NOT EXISTS `pms_providentfund` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `policy_name` varchar(225) DEFAULT NULL,
  `pay_period` varchar(225) DEFAULT NULL,
  `deduction_period` varchar(225) DEFAULT NULL,
  `policy_type` varchar(225) DEFAULT NULL,
  `based_on` varchar(225) DEFAULT NULL,
  `ee_share` decimal(9,2) DEFAULT NULL,
  `er_share` decimal(9,2) DEFAULT NULL,
  `remarks` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_rata
CREATE TABLE IF NOT EXISTS `pms_rata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `position_item_id` int(11) DEFAULT NULL,
  `office_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `number_of_actual_work` int(11) DEFAULT NULL,
  `number_of_leave_field` int(11) DEFAULT NULL,
  `number_of_work_days` int(11) DEFAULT NULL,
  `number_of_used_vehicles` int(11) DEFAULT NULL,
  `percentage_of_rata` varchar(225) DEFAULT NULL,
  `percentage_of_rata_value` decimal(9,2) DEFAULT NULL,
  `transportation_amount` decimal(9,2) DEFAULT NULL,
  `representation_amount` decimal(9,2) DEFAULT NULL,
  `year` varchar(225) DEFAULT NULL,
  `month` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `employee_number` varchar(225) DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_rata_deductions
CREATE TABLE IF NOT EXISTS `pms_rata_deductions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_number` varchar(225) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `deduction_id` int(11) DEFAULT NULL,
  `rata_id` int(11) DEFAULT NULL,
  `tax_deduction` decimal(9,2) DEFAULT NULL,
  `ra_deduction` decimal(9,2) DEFAULT NULL,
  `ta_deduction` decimal(9,2) DEFAULT NULL,
  `year` varchar(225) DEFAULT NULL,
  `month` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_rates
CREATE TABLE IF NOT EXISTS `pms_rates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rate` decimal(9,2) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_salarygrade
CREATE TABLE IF NOT EXISTS `pms_salarygrade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `salary_grade` varchar(225) DEFAULT NULL,
  `step1` decimal(9,2) DEFAULT NULL,
  `step2` decimal(9,2) DEFAULT NULL,
  `step3` decimal(9,2) DEFAULT NULL,
  `step4` decimal(9,2) DEFAULT NULL,
  `step5` decimal(9,2) DEFAULT NULL,
  `step6` decimal(9,2) DEFAULT NULL,
  `step7` decimal(9,2) DEFAULT NULL,
  `step8` decimal(9,2) DEFAULT NULL,
  `remarks` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_salaryinfo
CREATE TABLE IF NOT EXISTS `pms_salaryinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `salarygrade_id` int(11) DEFAULT NULL,
  `jobgrade_id` int(11) DEFAULT NULL,
  `positionitem_id` int(11) DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  `step_inc` varchar(225) DEFAULT NULL,
  `salary_description` varchar(225) DEFAULT NULL,
  `salary_old_rate` decimal(9,2) DEFAULT NULL,
  `salary_adjustment` decimal(9,2) DEFAULT NULL,
  `salary_new_rate` decimal(9,2) DEFAULT NULL,
  `salary_effectivity_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `employee_number` varchar(225) DEFAULT NULL,
  `employee_status_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=172 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_salary_adjustments
CREATE TABLE IF NOT EXISTS `pms_salary_adjustments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `employee_number` varchar(225) DEFAULT NULL,
  `old_basic_pay_amount` decimal(13,2) DEFAULT NULL,
  `new_basic_pay_amount` decimal(13,2) DEFAULT NULL,
  `salary_adjustment_amount` decimal(13,2) DEFAULT NULL,
  `gsis_cont_amount` decimal(13,2) DEFAULT NULL,
  `philhealth_cont_amount` decimal(13,2) DEFAULT NULL,
  `provident_fund_amount` decimal(13,2) DEFAULT NULL,
  `wtax_amount` decimal(13,2) DEFAULT NULL,
  `first_deduction_amount` decimal(13,2) DEFAULT NULL,
  `second_deduction_amount` decimal(13,2) DEFAULT NULL,
  `third_deduction_amount` decimal(13,2) DEFAULT NULL,
  `fourth_deduction_amount` decimal(13,2) DEFAULT NULL,
  `date_from` date DEFAULT NULL,
  `date_to` date DEFAULT NULL,
  `transaction_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_specialpayroll_transactions
CREATE TABLE IF NOT EXISTS `pms_specialpayroll_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `benefit_info_id` int(11) DEFAULT NULL,
  `position_item_id` int(11) DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  `division_id` int(11) DEFAULT NULL,
  `office_id` int(11) DEFAULT NULL,
  `percentage` decimal(9,2) DEFAULT NULL,
  `cost_uniform_amount` decimal(9,2) DEFAULT NULL,
  `no_of_months_entitled` decimal(9,2) DEFAULT NULL,
  `cash_gift_amount` decimal(9,2) DEFAULT NULL,
  `amount` decimal(9,2) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `year` varchar(50) DEFAULT NULL,
  `month` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `employee_number` varchar(225) DEFAULT NULL,
  `benefit_id` int(11) DEFAULT NULL,
  `cna_amount` decimal(9,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=417 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_special_annual_tax
CREATE TABLE IF NOT EXISTS `pms_special_annual_tax` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_number` varchar(225) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `clothing_allowance_amount` decimal(13,2) DEFAULT NULL,
  `cash_gift_amount` decimal(13,2) DEFAULT NULL,
  `loyalty_amount` decimal(13,2) DEFAULT NULL,
  `performance_base_amount` decimal(13,2) DEFAULT NULL,
  `collective_negotiation_amount` decimal(13,2) DEFAULT NULL,
  `pei_amount` decimal(13,2) DEFAULT NULL,
  `mid_year_amount` decimal(13,2) DEFAULT NULL,
  `year_end_amount` decimal(13,2) DEFAULT NULL,
  `honoraria_amount` decimal(13,2) DEFAULT NULL,
  `for_year` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_step_increments
CREATE TABLE IF NOT EXISTS `pms_step_increments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `employee_number` varchar(225) DEFAULT NULL,
  `old_basic_pay_amount` decimal(13,2) DEFAULT NULL,
  `new_basic_pay_amount` decimal(13,2) DEFAULT NULL,
  `salary_adjustment_amount` decimal(13,2) DEFAULT NULL,
  `gross_pay_amount` decimal(13,2) DEFAULT NULL,
  `gsis_cont_amount` decimal(13,2) DEFAULT NULL,
  `philhealth_cont_amount` decimal(13,2) DEFAULT NULL,
  `provident_fund_amount` decimal(13,2) DEFAULT NULL,
  `wtax_amount` decimal(13,2) DEFAULT NULL,
  `first_deduction_amount` decimal(13,2) DEFAULT NULL,
  `second_deduction_amount` decimal(13,2) DEFAULT NULL,
  `third_deduction_amount` decimal(13,2) DEFAULT NULL,
  `fourth_deduction_amount` decimal(13,2) DEFAULT NULL,
  `date_from` date DEFAULT NULL,
  `date_to` date DEFAULT NULL,
  `transaction_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_taxannualtable
CREATE TABLE IF NOT EXISTS `pms_taxannualtable` (
  `id` bigint(50) NOT NULL AUTO_INCREMENT,
  `code` varchar(10) DEFAULT NULL,
  `name` varchar(300) DEFAULT NULL,
  `below` decimal(6,2) DEFAULT NULL,
  `above` decimal(6,2) DEFAULT NULL,
  `rate` varchar(255) DEFAULT NULL,
  `effectivity_date` date DEFAULT NULL,
  `remarks` varchar(300) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_taxespolicy
CREATE TABLE IF NOT EXISTS `pms_taxespolicy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `policy_name` varchar(225) DEFAULT NULL,
  `pay_period` varchar(225) DEFAULT NULL,
  `deduction_period` varchar(225) DEFAULT NULL,
  `policy_type` varchar(225) DEFAULT NULL,
  `based_on` varchar(225) DEFAULT NULL,
  `computation` varchar(225) DEFAULT NULL,
  `is_withholding` varchar(225) DEFAULT NULL,
  `job_grade_rate` decimal(9,2) DEFAULT NULL,
  `remarks` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_taxtable
CREATE TABLE IF NOT EXISTS `pms_taxtable` (
  `id` bigint(50) NOT NULL AUTO_INCREMENT,
  `payperiod` varchar(225) DEFAULT NULL,
  `salary_bracket_level1` decimal(9,2) DEFAULT NULL,
  `salary_bracket_level2` decimal(9,2) DEFAULT NULL,
  `salary_bracket_level3` decimal(9,2) DEFAULT NULL,
  `salary_bracket_level4` decimal(9,2) DEFAULT NULL,
  `salary_bracket_level5` decimal(9,2) DEFAULT NULL,
  `salary_bracket_level6` decimal(9,2) DEFAULT NULL,
  `status` varchar(225) DEFAULT NULL,
  `effectivity_date` date DEFAULT NULL,
  `remarks` varchar(300) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_transactions
CREATE TABLE IF NOT EXISTS `pms_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `salaryinfo_id` int(11) DEFAULT NULL,
  `division_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `position_item_id` int(11) DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  `office_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `empstatus_id` int(11) DEFAULT NULL,
  `employeeinfo_id` int(11) DEFAULT NULL,
  `actual_basicpay` decimal(9,2) DEFAULT NULL,
  `adjust_basicpay` decimal(9,2) DEFAULT NULL,
  `total_basicpay` decimal(9,2) DEFAULT NULL,
  `actual_absences` int(11) DEFAULT NULL,
  `adjust_absences` int(11) DEFAULT NULL,
  `total_absences` decimal(9,2) DEFAULT NULL,
  `actual_tardines` decimal(9,2) DEFAULT NULL,
  `adjust_tardines` decimal(9,2) DEFAULT NULL,
  `total_tardines` decimal(9,2) DEFAULT NULL,
  `actual_undertime` int(11) DEFAULT NULL,
  `adjust_undertime` int(11) DEFAULT NULL,
  `total_undertime` decimal(9,2) DEFAULT NULL,
  `basic_net_pay` decimal(9,2) DEFAULT NULL,
  `actual_contribution` decimal(9,2) DEFAULT NULL,
  `adjust_contribution` decimal(9,2) DEFAULT NULL,
  `total_contribution` decimal(9,2) DEFAULT NULL,
  `actual_loan` decimal(9,2) DEFAULT NULL,
  `adjust_loan` decimal(9,2) DEFAULT NULL,
  `total_loan` decimal(9,2) DEFAULT NULL,
  `actual_otherdeduct` decimal(9,2) DEFAULT NULL,
  `adjust_otherdeduct` decimal(9,2) DEFAULT NULL,
  `total_otherdeduct` decimal(9,2) DEFAULT NULL,
  `net_deduction` decimal(9,2) DEFAULT NULL,
  `allowances` decimal(9,2) DEFAULT NULL,
  `gross_pay` decimal(9,2) DEFAULT NULL,
  `gross_taxable_pay` decimal(9,2) DEFAULT NULL,
  `net_pay` decimal(9,2) DEFAULT NULL,
  `pay_period` varchar(225) DEFAULT NULL,
  `hold` int(11) DEFAULT NULL,
  `year` varchar(225) DEFAULT NULL,
  `month` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `employee_number` varchar(225) DEFAULT NULL,
  `actual_workdays` int(11) DEFAULT NULL,
  `adjust_workdays` int(11) DEFAULT NULL,
  `actual_tardiness` int(11) DEFAULT NULL,
  `adjust_tardiness` int(11) DEFAULT NULL,
  `actual_basicpay_amount` decimal(9,2) DEFAULT NULL,
  `adjust_basicpay_amount` decimal(9,2) DEFAULT NULL,
  `total_basicpay_amount` decimal(9,2) DEFAULT NULL,
  `actual_absences_amount` decimal(9,2) DEFAULT NULL,
  `adjust_absences_amount` decimal(9,2) DEFAULT NULL,
  `total_absences_amount` decimal(9,2) DEFAULT NULL,
  `actual_tardines_amount` decimal(9,2) DEFAULT NULL,
  `adjust_tardines_amount` decimal(9,2) DEFAULT NULL,
  `total_tardines_amount` decimal(9,2) DEFAULT NULL,
  `actual_undertime_amount` decimal(9,2) DEFAULT NULL,
  `adjust_undertime_amount` decimal(9,2) DEFAULT NULL,
  `total_undertime_amount` decimal(9,2) DEFAULT NULL,
  `ecc_amount` decimal(9,2) DEFAULT NULL,
  `tax_amount` decimal(9,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=574 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.pms_wagerates
CREATE TABLE IF NOT EXISTS `pms_wagerates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wage_region` varchar(225) DEFAULT NULL,
  `wage_rate` decimal(9,2) DEFAULT NULL,
  `effectivity_date` date DEFAULT NULL,
  `based_on` varchar(225) DEFAULT NULL,
  `remarks` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.position
CREATE TABLE IF NOT EXISTS `position` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `IsInterim` int(1) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  `IsPrivate` int(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=238 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.positionclassification
CREATE TABLE IF NOT EXISTS `positionclassification` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) NOT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.positionitem
CREATE TABLE IF NOT EXISTS `positionitem` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `PositionRefId` bigint(50) DEFAULT NULL,
  `PositionLevelRefId` bigint(50) DEFAULT NULL,
  `PositionClassificationRefId` bigint(50) DEFAULT NULL,
  `OfficeRefId` bigint(50) DEFAULT NULL,
  `SalaryGradeRefId` int(10) DEFAULT NULL,
  `JobGradeRefId` int(10) DEFAULT NULL,
  `DivisionRefId` int(10) DEFAULT NULL,
  `StepIncrementRefId` int(10) DEFAULT NULL,
  `SalaryAmount` decimal(15,2) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `EducRequirements` text,
  `WorkExpRequirements` text,
  `TrainingRequirements` text,
  `EligibilityRequirements` text,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=185 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.positionlevel
CREATE TABLE IF NOT EXISTS `positionlevel` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) NOT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.province
CREATE TABLE IF NOT EXISTS `province` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(255) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Capital` varchar(50) DEFAULT NULL,
  `Area` varchar(50) DEFAULT NULL,
  `Founded` varchar(50) DEFAULT NULL,
  `Division` varchar(50) DEFAULT NULL,
  `Region` varchar(50) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.reports
CREATE TABLE IF NOT EXISTS `reports` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` bigint(50) DEFAULT NULL,
  `BranchRefId` bigint(50) DEFAULT NULL,
  `CompanyCode` varchar(10) DEFAULT NULL,
  `Code` varchar(200) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Label` varchar(200) DEFAULT NULL,
  `Filename` varchar(100) DEFAULT NULL,
  `SystemRefId` int(11) DEFAULT NULL,
  `Owner` varchar(300) DEFAULT NULL,
  `ExtType` varchar(5) DEFAULT NULL,
  `For` varchar(100) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.requirements
CREATE TABLE IF NOT EXISTS `requirements` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(10) NOT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Description` varchar(300) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.responsibilitycenter
CREATE TABLE IF NOT EXISTS `responsibilitycenter` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(10) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Description` varchar(200) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.salarygrade
CREATE TABLE IF NOT EXISTS `salarygrade` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `Name` varchar(300) CHARACTER SET latin1 DEFAULT NULL,
  `Step1` decimal(9,2) DEFAULT NULL,
  `EffectivityDateS1` date DEFAULT NULL,
  `Step2` decimal(9,2) DEFAULT NULL,
  `EffectivityDateS2` date DEFAULT NULL,
  `Step3` decimal(9,2) DEFAULT NULL,
  `EffectivityDateS3` date DEFAULT NULL,
  `Step4` decimal(9,2) DEFAULT NULL,
  `EffectivityDateS4` date DEFAULT NULL,
  `Step5` decimal(9,2) DEFAULT NULL,
  `EffectivityDateS5` date DEFAULT NULL,
  `Step6` decimal(9,2) DEFAULT NULL,
  `EffectivityDateS6` date DEFAULT NULL,
  `Step7` decimal(9,2) DEFAULT NULL,
  `EffectivityDateS7` date DEFAULT NULL,
  `Step8` decimal(9,2) DEFAULT NULL,
  `EffectivityDateS8` date DEFAULT NULL,
  `Remarks` varchar(300) CHARACTER SET latin1 DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `Data` varchar(1) CHARACTER SET latin1 DEFAULT NULL,
  UNIQUE KEY `refid` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf32 COLLATE=utf32_bin;

-- Data exporting was unselected.
-- Dumping structure for table phcc.schedinterview
CREATE TABLE IF NOT EXISTS `schedinterview` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` int(10) DEFAULT NULL,
  `BranchRefId` int(10) DEFAULT NULL,
  `ApplicantRefId` bigint(50) NOT NULL,
  `PositionItemRefId` varchar(300) DEFAULT NULL,
  `DateApplied` date DEFAULT NULL,
  `InitialDateInterview` date DEFAULT NULL,
  `InitialInterviewByRefId` bigint(50) NOT NULL COMMENT 'Employees RefId',
  `InitialInterviewStatus` varchar(100) DEFAULT NULL,
  `FinalDateInterview` date DEFAULT NULL,
  `FinalInterviewByRefId` bigint(50) NOT NULL COMMENT 'Employees RefId',
  `FinalInterviewStatus` varchar(100) DEFAULT NULL,
  `FinalStatus` varchar(100) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.schools
CREATE TABLE IF NOT EXISTS `schools` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `Name` varchar(300) CHARACTER SET latin1 DEFAULT NULL,
  `LevelType` int(1) DEFAULT NULL,
  `Offer1` int(1) DEFAULT '0',
  `Offer2` int(1) DEFAULT '0',
  `Offer3` int(1) DEFAULT '0',
  `Offer4` int(1) DEFAULT '0',
  `Offer5` int(1) DEFAULT '0',
  `Remarks` varchar(300) CHARACTER SET latin1 DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `Data` varchar(1) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`),
  UNIQUE KEY `Name` (`Name`),
  KEY `Index 3` (`Name`)
) ENGINE=InnoDB AUTO_INCREMENT=28815 DEFAULT CHARSET=utf32 COLLATE=utf32_bin;

-- Data exporting was unselected.
-- Dumping structure for table phcc.sections
CREATE TABLE IF NOT EXISTS `sections` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) NOT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `PositionRefId` int(10) NOT NULL,
  `PositionLevelRefId` int(10) NOT NULL,
  `PositionClassificationRefId` int(10) NOT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.seminarclass
CREATE TABLE IF NOT EXISTS `seminarclass` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.seminarplace
CREATE TABLE IF NOT EXISTS `seminarplace` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.seminars
CREATE TABLE IF NOT EXISTS `seminars` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Description` varchar(200) DEFAULT NULL,
  `SponsorRefId` bigint(10) DEFAULT NULL,
  `SeminarClassRefId` bigint(10) DEFAULT NULL,
  `NumofHrs` int(5) DEFAULT NULL,
  `SeminarTypeRefId` bigint(10) DEFAULT NULL,
  `SeminarPlaceRefId` bigint(10) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=116 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.seminarsponsor
CREATE TABLE IF NOT EXISTS `seminarsponsor` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.seminartype
CREATE TABLE IF NOT EXISTS `seminartype` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.signatories
CREATE TABLE IF NOT EXISTS `signatories` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(15) DEFAULT NULL,
  `Label` varchar(50) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `PositionRefId` bigint(50) DEFAULT NULL,
  `DivisionRefId` bigint(50) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.slvlearneddaily
CREATE TABLE IF NOT EXISTS `slvlearneddaily` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `EffectivityDate` date NOT NULL,
  `NoOfDays` int(10) DEFAULT NULL,
  `VLEarned` decimal(6,3) DEFAULT NULL,
  `SLEarned` decimal(6,3) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.slvlearnedmonthly
CREATE TABLE IF NOT EXISTS `slvlearnedmonthly` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `EffectivityDate` date NOT NULL,
  `NoOfMonths` int(10) DEFAULT NULL,
  `VLEarned` decimal(6,2) DEFAULT NULL,
  `SLEarned` decimal(6,2) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=433 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.spms_dpcr
CREATE TABLE IF NOT EXISTS `spms_dpcr` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` bigint(50) DEFAULT NULL,
  `BranchRefId` bigint(50) DEFAULT NULL,
  `EmployeesRefId` bigint(50) DEFAULT NULL,
  `AgencyId` varchar(50) DEFAULT NULL,
  `Quarter` int(1) DEFAULT NULL,
  `Year` int(4) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.spms_ipar
CREATE TABLE IF NOT EXISTS `spms_ipar` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` bigint(50) DEFAULT NULL,
  `BranchRefId` bigint(50) DEFAULT NULL,
  `EmployeesRefId` bigint(50) DEFAULT NULL,
  `AgencyId` varchar(50) DEFAULT NULL,
  `Quarter` int(1) DEFAULT NULL,
  `Year` int(4) DEFAULT NULL,
  `AverageNumerical` decimal(5,2) DEFAULT NULL,
  `AverageAdjectival` decimal(5,2) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.spms_ipcr
CREATE TABLE IF NOT EXISTS `spms_ipcr` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` bigint(50) DEFAULT NULL,
  `BranchRefId` bigint(50) DEFAULT NULL,
  `EmployeesRefId` bigint(50) DEFAULT NULL,
  `AgencyId` varchar(50) DEFAULT NULL,
  `Quarter` int(1) DEFAULT NULL,
  `Year` int(4) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.sponsor
CREATE TABLE IF NOT EXISTS `sponsor` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.stepincrement
CREATE TABLE IF NOT EXISTS `stepincrement` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) NOT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.sysgroup
CREATE TABLE IF NOT EXISTS `sysgroup` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(10) NOT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Description` varchar(300) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`),
  KEY `RefId_2` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.sysgroupaccess
CREATE TABLE IF NOT EXISTS `sysgroupaccess` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` bigint(10) NOT NULL,
  `BranchRefId` bigint(10) NOT NULL,
  `SysModulesRefId` bigint(50) NOT NULL,
  `SysGroupRefId` bigint(50) NOT NULL,
  `CanAdd` tinyint(1) NOT NULL,
  `CanEdit` tinyint(1) NOT NULL,
  `CanDelete` tinyint(1) NOT NULL,
  `CanPrint` tinyint(1) NOT NULL,
  `CanView` tinyint(1) NOT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`),
  UNIQUE KEY `idx2` (`SysGroupRefId`,`SysModulesRefId`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.sysmodules
CREATE TABLE IF NOT EXISTS `sysmodules` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) DEFAULT NULL,
  `dbTable` varchar(100) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Title` varchar(50) DEFAULT NULL,
  `ColumnHeader` varchar(300) DEFAULT NULL,
  `ColumnField` varchar(300) DEFAULT NULL,
  `SysMember` varchar(15) DEFAULT NULL,
  `Description` varchar(300) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`),
  KEY `RefId_2` (`RefId`),
  KEY `RefId_3` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.sysparam
CREATE TABLE IF NOT EXISTS `sysparam` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `Values` varchar(300) CHARACTER SET latin1 DEFAULT NULL,
  `Description` varchar(300) COLLATE utf32_bin DEFAULT NULL,
  `Remarks` varchar(300) CHARACTER SET latin1 DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `Data` varchar(1) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`),
  UNIQUE KEY `code` (`Code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_bin;

-- Data exporting was unselected.
-- Dumping structure for table phcc.system
CREATE TABLE IF NOT EXISTS `system` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `Name` varchar(300) CHARACTER SET latin1 DEFAULT NULL,
  `ShortName` varchar(50) COLLATE utf32_bin DEFAULT NULL,
  `Icons` varchar(100) COLLATE utf32_bin DEFAULT NULL,
  `DefaultModule` varchar(100) COLLATE utf32_bin DEFAULT NULL,
  `Remarks` varchar(300) CHARACTER SET latin1 DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `Data` varchar(1) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf32 COLLATE=utf32_bin;

-- Data exporting was unselected.
-- Dumping structure for table phcc.sysuser
CREATE TABLE IF NOT EXISTS `sysuser` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` int(10) DEFAULT NULL,
  `BranchRefId` int(10) DEFAULT NULL,
  `EmployeesRefId` int(10) NOT NULL,
  `Password` varchar(50) DEFAULT NULL,
  `UserName` varchar(50) DEFAULT NULL,
  `Session` varchar(100) DEFAULT NULL,
  `Level` int(1) DEFAULT NULL,
  `SysGroupRefId` bigint(10) DEFAULT NULL,
  `isLogin` tinyint(1) DEFAULT '0',
  `Terminal` varchar(50) DEFAULT NULL,
  `LastLoginDate` date DEFAULT NULL,
  `LastLoginTime` time DEFAULT NULL,
  `ModulesAccess` varchar(1000) DEFAULT NULL,
  `TrnAccess` varchar(50) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  `SecurityQuestion1` varchar(200) DEFAULT NULL,
  `SecurityQuestion2` varchar(200) DEFAULT NULL,
  `SecurityAns1` varchar(100) DEFAULT NULL,
  `SecurityAns2` varchar(100) DEFAULT NULL,
  `Remarks` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`),
  UNIQUE KEY `EmployeesRefId` (`EmployeesRefId`),
  UNIQUE KEY `UserName_idx` (`UserName`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.sysusersystems
CREATE TABLE IF NOT EXISTS `sysusersystems` (
  `RefId` int(10) NOT NULL AUTO_INCREMENT,
  `EmployeesRefId` int(10) DEFAULT NULL,
  `CompanyRefId` int(10) DEFAULT NULL,
  `BranchRefId` int(10) DEFAULT NULL,
  `SysUserRefId` int(10) DEFAULT NULL,
  `SystemRefId` int(10) DEFAULT NULL,
  `Remarks` varchar(255) DEFAULT NULL,
  `Ordinal` int(11) DEFAULT NULL,
  `Show` int(10) DEFAULT NULL,
  `LastUpdateBy` varchar(255) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.taxpolicy
CREATE TABLE IF NOT EXISTS `taxpolicy` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(10) DEFAULT NULL,
  `PolicyName` varchar(30) DEFAULT NULL,
  `PayPeriod` varchar(50) DEFAULT NULL,
  `DeductionPeriod` varchar(50) DEFAULT NULL,
  `PolicyType` varchar(50) DEFAULT NULL,
  `BasedOn` varchar(50) DEFAULT NULL,
  `Computation` bigint(10) DEFAULT NULL,
  `IsWitholding` int(1) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  `Name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.taxstatustable
CREATE TABLE IF NOT EXISTS `taxstatustable` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(10) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `TaxStatus` varchar(30) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Exemption` decimal(6,2) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.taxtable
CREATE TABLE IF NOT EXISTS `taxtable` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Status` varchar(300) DEFAULT NULL,
  `Exemption` decimal(6,2) DEFAULT NULL,
  `SalaryBracketLevel1` decimal(9,2) DEFAULT NULL,
  `SalaryBracketLevel2` decimal(9,2) DEFAULT NULL,
  `SalaryBracketLevel3` decimal(9,2) DEFAULT NULL,
  `SalaryBracketLevel4` decimal(9,2) DEFAULT NULL,
  `SalaryBracketLevel5` decimal(9,2) DEFAULT NULL,
  `SalaryBracketLevel6` decimal(9,2) DEFAULT NULL,
  `SalaryBracketLevel7` decimal(9,2) DEFAULT NULL,
  `SalaryBracketLevel8` decimal(9,2) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.trnlog
CREATE TABLE IF NOT EXISTS `trnlog` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` bigint(50) NOT NULL,
  `BranchRefId` bigint(50) NOT NULL,
  `TableName` varchar(50) DEFAULT NULL,
  `TrnDate` date DEFAULT NULL,
  `TrnType` varchar(1) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=1782 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.units
CREATE TABLE IF NOT EXISTS `units` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) NOT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.updates201
CREATE TABLE IF NOT EXISTS `updates201` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` int(10) DEFAULT NULL,
  `BranchRefId` int(10) DEFAULT NULL,
  `EmployeesRefId` bigint(50) NOT NULL,
  `TableName` varchar(50) DEFAULT NULL,
  `TabsName` varchar(50) DEFAULT NULL,
  `FieldsEdit` varchar(50) DEFAULT NULL,
  `OldValue` varchar(50) DEFAULT NULL,
  `NewValue` varchar(100) DEFAULT NULL,
  `TrnDate` date DEFAULT NULL,
  `TrnTime` time DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  `ApprovedDate` date DEFAULT NULL,
  `ApprovedBy` int(10) DEFAULT NULL,
  `Status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.vacantposition
CREATE TABLE IF NOT EXISTS `vacantposition` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` int(10) DEFAULT NULL,
  `BranchRefId` int(10) DEFAULT NULL,
  `PositionItemRefId` int(10) DEFAULT NULL,
  `SalaryAmount` decimal(6,2) DEFAULT NULL,
  `Plantilla` varchar(250) DEFAULT NULL,
  `EducAttain` varchar(300) DEFAULT NULL,
  `RelevantExperience` varchar(300) DEFAULT NULL,
  `CivilService` varchar(300) DEFAULT NULL,
  `RelevantTraining` varchar(300) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.wagerate
CREATE TABLE IF NOT EXISTS `wagerate` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(10) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `WageRegion` varchar(30) DEFAULT NULL,
  `EffectivityDate` date DEFAULT NULL,
  `WageRate` decimal(6,2) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.workinghrsconversion
CREATE TABLE IF NOT EXISTS `workinghrsconversion` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `EffectivityDate` date NOT NULL,
  `Type` varchar(3) DEFAULT NULL,
  `NoOf` int(10) DEFAULT NULL,
  `EquivalentDay` decimal(6,3) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc.workschedule
CREATE TABLE IF NOT EXISTS `workschedule` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` bigint(50) NOT NULL,
  `BranchRefId` bigint(50) NOT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `AutoLB` int(11) DEFAULT NULL,
  `ScheduleType` varchar(2) DEFAULT NULL,
  `SundayIn` int(9) DEFAULT NULL,
  `SundayCBOutAM` int(9) DEFAULT NULL,
  `SundayCBInAM` int(9) DEFAULT NULL,
  `SundayLBOut` int(9) DEFAULT NULL,
  `SundayLBIn` int(9) DEFAULT NULL,
  `SundayCBOutPM` int(9) DEFAULT NULL,
  `SundayCBInPM` int(9) DEFAULT NULL,
  `SundayOut` int(9) DEFAULT NULL,
  `SundayisStrictLB` int(1) DEFAULT NULL,
  `SundayisFlexiLB` int(1) DEFAULT NULL,
  `SundayisFlexi` int(1) DEFAULT NULL,
  `SundayFlexiTime` int(9) DEFAULT NULL,
  `SundayisRestDay` int(1) DEFAULT NULL,
  `SundayOTIn` int(9) DEFAULT NULL,
  `SundayOTMinHrs` int(9) DEFAULT NULL,
  `SundayOTNeedApproval` int(1) DEFAULT NULL,
  `SundayOTAdvance` int(1) DEFAULT NULL,
  `SundayOTAdvanceIn` int(9) DEFAULT NULL,
  `MondayIn` int(9) DEFAULT NULL,
  `MondayCBOutAM` int(9) DEFAULT NULL,
  `MondayCBInAM` int(9) DEFAULT NULL,
  `MondayLBOut` int(9) DEFAULT NULL,
  `MondayLBIn` int(9) DEFAULT NULL,
  `MondayCBOutPM` int(9) DEFAULT NULL,
  `MondayCBInPM` int(9) DEFAULT NULL,
  `MondayOut` int(9) DEFAULT NULL,
  `MondayisStrictLB` int(1) DEFAULT NULL,
  `MondayisFlexiLB` int(1) DEFAULT NULL,
  `MondayisFlexi` int(1) DEFAULT NULL,
  `MondayFlexiTime` int(9) DEFAULT NULL,
  `MondayisRestDay` int(1) DEFAULT NULL,
  `MondayOTIn` int(9) DEFAULT NULL,
  `MondayOTMinHrs` int(9) DEFAULT NULL,
  `MondayOTNeedApproval` int(1) DEFAULT NULL,
  `MondayOTAdvance` int(1) DEFAULT NULL,
  `MondayOTAdvanceIn` int(9) DEFAULT NULL,
  `TuesdayIn` int(9) DEFAULT NULL,
  `TuesdayCBOutAM` int(9) DEFAULT NULL,
  `TuesdayCBInAM` int(9) DEFAULT NULL,
  `TuesdayLBOut` int(9) DEFAULT NULL,
  `TuesdayLBIn` int(9) DEFAULT NULL,
  `TuesdayCBOutPM` int(9) DEFAULT NULL,
  `TuesdayCBInPM` int(9) DEFAULT NULL,
  `TuesdayOut` int(9) DEFAULT NULL,
  `TuesdayisStrictLB` int(1) DEFAULT NULL,
  `TuesdayisFlexiLB` int(1) DEFAULT NULL,
  `TuesdayisFlexi` int(1) DEFAULT NULL,
  `TuesdayFlexiTime` int(9) DEFAULT NULL,
  `TuesdayisRestDay` int(1) DEFAULT NULL,
  `TuesdayOTIn` int(9) DEFAULT NULL,
  `TuesdayOTMinHrs` int(9) DEFAULT NULL,
  `TuesdayOTNeedApproval` int(1) DEFAULT NULL,
  `TuesdayOTAdvance` int(1) DEFAULT NULL,
  `TuesdayOTAdvanceIn` int(9) DEFAULT NULL,
  `WednesdayIn` int(9) DEFAULT NULL,
  `WednesdayCBOutAM` int(9) DEFAULT NULL,
  `WednesdayCBInAM` int(9) DEFAULT NULL,
  `WednesdayLBOut` int(9) DEFAULT NULL,
  `WednesdayLBIn` int(9) DEFAULT NULL,
  `WednesdayCBOutPM` int(9) DEFAULT NULL,
  `WednesdayCBInPM` int(9) DEFAULT NULL,
  `WednesdayOut` int(9) DEFAULT NULL,
  `WednesdayisStrictLB` int(1) DEFAULT NULL,
  `WednesdayisFlexiLB` int(1) DEFAULT NULL,
  `WednesdayisFlexi` int(1) DEFAULT NULL,
  `WednesdayFlexiTime` int(9) DEFAULT NULL,
  `WednesdayisRestDay` int(1) DEFAULT NULL,
  `WednesdayOTIn` int(9) DEFAULT NULL,
  `WednesdayOTMinHrs` int(9) DEFAULT NULL,
  `WednesdayOTNeedApproval` int(1) DEFAULT NULL,
  `WednesdayOTAdvance` int(1) DEFAULT NULL,
  `WednesdayOTAdvanceIn` int(9) DEFAULT NULL,
  `ThursdayIn` int(9) DEFAULT NULL,
  `ThursdayCBOutAM` int(9) DEFAULT NULL,
  `ThursdayCBInAM` int(9) DEFAULT NULL,
  `ThursdayLBOut` int(9) DEFAULT NULL,
  `ThursdayLBIn` int(9) DEFAULT NULL,
  `ThursdayCBOutPM` int(9) DEFAULT NULL,
  `ThursdayCBInPM` int(9) DEFAULT NULL,
  `ThursdayOut` int(9) DEFAULT NULL,
  `ThursdayisStrictLB` int(1) DEFAULT NULL,
  `ThursdayisFlexiLB` int(1) DEFAULT NULL,
  `ThursdayisFlexi` int(1) DEFAULT NULL,
  `ThursdayFlexiTime` int(9) DEFAULT NULL,
  `ThursdayisRestDay` int(1) DEFAULT NULL,
  `ThursdayOTIn` int(9) DEFAULT NULL,
  `ThursdayOTMinHrs` int(9) DEFAULT NULL,
  `ThursdayOTNeedApproval` int(1) DEFAULT NULL,
  `ThursdayOTAdvance` int(1) DEFAULT NULL,
  `ThursdayOTAdvanceIn` int(9) DEFAULT NULL,
  `FridayIn` int(9) DEFAULT NULL,
  `FridayCBOutAM` int(9) DEFAULT NULL,
  `FridayCBInAM` int(9) DEFAULT NULL,
  `FridayLBOut` int(9) DEFAULT NULL,
  `FridayLBIn` int(9) DEFAULT NULL,
  `FridayCBOutPM` int(9) DEFAULT NULL,
  `FridayCBInPM` int(9) DEFAULT NULL,
  `FridayOut` int(9) DEFAULT NULL,
  `FridayisStrictLB` int(1) DEFAULT NULL,
  `FridayisFlexiLB` int(1) DEFAULT NULL,
  `FridayisFlexi` int(1) DEFAULT NULL,
  `FridayFlexiTime` int(9) DEFAULT NULL,
  `FridayisRestDay` int(1) DEFAULT NULL,
  `FridayOTIn` int(9) DEFAULT NULL,
  `FridayOTMinHrs` int(9) DEFAULT NULL,
  `FridayOTNeedApproval` int(1) DEFAULT NULL,
  `FridayOTAdvance` int(1) DEFAULT NULL,
  `FridayOTAdvanceIn` int(9) DEFAULT NULL,
  `SaturdayIn` int(9) DEFAULT NULL,
  `SaturdayCBOutAM` int(9) DEFAULT NULL,
  `SaturdayCBInAM` int(9) DEFAULT NULL,
  `SaturdayLBOut` int(9) DEFAULT NULL,
  `SaturdayLBIn` int(9) DEFAULT NULL,
  `SaturdayCBOutPM` int(9) DEFAULT NULL,
  `SaturdayCBInPM` int(9) DEFAULT NULL,
  `SaturdayOut` int(9) DEFAULT NULL,
  `SaturdayisStrictLB` int(1) DEFAULT NULL,
  `SaturdayisFlexiLB` int(1) DEFAULT NULL,
  `SaturdayisFlexi` int(1) DEFAULT NULL,
  `SaturdayFlexiTime` int(9) DEFAULT NULL,
  `SaturdayisRestDay` int(1) DEFAULT NULL,
  `SaturdayOTIn` int(9) DEFAULT NULL,
  `SaturdayOTMinHrs` int(9) DEFAULT NULL,
  `SaturdayOTNeedApproval` int(1) DEFAULT NULL,
  `SaturdayOTAdvance` int(1) DEFAULT NULL,
  `SaturdayOTAdvanceIn` int(9) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
