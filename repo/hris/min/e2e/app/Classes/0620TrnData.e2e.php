<?php
   class Transaction {
      public function insertEmpCreditBal() {
         $EmpRefId    = getvalue("sint_EmployeesRefId");
         $NameCredits = getvalue("char_NameCredits");
         $Remarks     = getvalue("char_Remarks");
         $RefId       = realEscape(getvalue("hRefId")) ;
         if ($NameCredits != "") {
            if ($NameCredits == "OT") {
               $EffectivityYear     = setValue_RefId(getvalue("sint_EffectivityYear"));
               $BegBalAsOfDate      = realEscape(getvalue("date_BegBalAsOfDate"));
               $OutstandingBalance  = setValue_RefId(getvalue("sint_BeginningBalance"));
               $ExpiryDate          = realEscape(getvalue("date_ExpiryDate"));
               $Fields = "`BegBalAsOfDate`,`BeginningBalance`,`ExpiryDate`,`EffectivityYear`,`NameCredits`,`EmployeesRefId`,`Remarks`,";
               $Values = "'$BegBalAsOfDate','$OutstandingBalance','$ExpiryDate','$EffectivityYear','$NameCredits','$EmpRefId','$Remarks',";
               $FldnVal = "BegBalAsOfDate = '$BegBalAsOfDate', BeginningBalance =  '$OutstandingBalance', ExpiryDate = '$ExpiryDate', ";
               $FldnVal .= "EffectivityYear = '$EffectivityYear', NameCredits = '$NameCredits', Remarks = '$Remarks', ";
            } else {
               $EffectivityYear     = setValue_RefId(getvalue("sint_EffectivityYear"));
               $BegBalAsOfDate      = realEscape(getvalue("date_BegBalAsOfDate"));
               $BeginningBalance    = setValue_RefId(getvalue("sint_BeginningBalance"));
               if ($NameCredits == "VL") {
                  $ForceLeave = setValue_RefId(getvalue("sint_ForceLeave"));
                  $Fields = "`BegBalAsOfDate`,`BeginningBalance`,`ForceLeave`,`EffectivityYear`,`NameCredits`,`EmployeesRefId`,`Remarks`,";
                  $Values = "'$BegBalAsOfDate','$BeginningBalance','$ForceLeave','$EffectivityYear','$NameCredits','$EmpRefId','$Remarks',";
                  $FldnVal = "BegBalAsOfDate = '$BegBalAsOfDate', BeginningBalance = '$BeginningBalance', Remarks = '$Remarks', ";
                  $FldnVal .= "ForceLeave = '$ForceLeave', EffectivityYear = '$EffectivityYear', NameCredits = '$NameCredits',";
               } else {
                  $Fields = "`BegBalAsOfDate`,`BeginningBalance`,`EffectivityYear`,`NameCredits`,`EmployeesRefId`,`Remarks`,";
                  $Values = "'$BegBalAsOfDate','$BeginningBalance','$EffectivityYear','$NameCredits','$EmpRefId','$Remarks',";
                  $FldnVal = "NameCredits = '$NameCredits', Remarks = '$Remarks', BegBalAsOfDate = '$BegBalAsOfDate', BeginningBalance = '$BeginningBalance', EffectivityYear = '$EffectivityYear', ";
               }
            }
            if ($RefId > 0) {
               $updateResult = f_SaveRecord("EDITSAVE","employeescreditbalance",$FldnVal,$RefId);
               if ($updateResult != "") {
                  echo $updateResult;
               }
            } else {
               $SaveSuccessfull = f_SaveRecord("NEWSAVE","employeescreditbalance",$Fields,$Values);
               if (!is_numeric($SaveSuccessfull)) {
                  echo $SaveSuccessfull;
               }   
            }
         } else {
            echo "NO Credits Name ...";
         }
      }

      public function trnSaveRecord ($post) {
         include_once "conn.e2e.php";

         $moduleContent = file_get_contents(json.$post['json'].".json");
         $module     = json_decode($moduleContent, true);
         $File       = $module["File"];
         $Fields     = "";
         $Values     = "";
         foreach ($module["Fields"] as $fieldName) {
            $Fields .= "`".$fieldName."`, ";
            $Values .= "'".mysqli_real_escape_string($conn,$post[$fieldName])."', ";
         }
         $SaveSuccessfull = f_SaveRecord("NEWSAVE",strtolower($module["Table"]),$Fields,$Values);
         if (is_numeric($SaveSuccessfull)) {
            echo 'gotoscrn("'.$File.'","&SaveSuccess=yes&NewRecord='.$SaveSuccessfull.'");';
         } else {
            echo 'console.log("'.$SaveSuccessfull.'");';
         }

      }

      public function trnLoadRecord($refid) {
         $moduleContent = file_get_contents(json.getvalue("json").".json");
         $module     = json_decode($moduleContent, true);
         $row = FindFirst($module["Table"],"WHERE RefId = ".$refid,"*");
         foreach ($module["Fields"] as $fieldName) {
            echo
            'setValueByName("'.$fieldName.'","'.$row[$fieldName].'");'."\n";
         }

         if (getvalue("trnmode") == "MODIFY") {
            echo '$("#FieldEntry .saveFields--").prop("disabled",false);';
         }
         else if (getvalue("trnmode") == "VIEW") {
            echo '$("#FieldEntry .saveFields--").prop("disabled",true);'."\n";
         }
      }

      public function trnLoadDefValue() {
         $moduleContent = file_get_contents(json.getvalue("json").".json");
         $module        = json_decode($moduleContent, true);
         $defvalue      = $module["DefaultValue"];
         $i = 0;
         foreach ($module["Fields"] as $fieldName) {
            echo 'setValueByName("'.$fieldName.'","'.$defvalue[$i].'")'."\n";
            $i++;
         }

      }
      public function trnDeleteRecord () {

      }
   }
?>




