<div class="row">
   <div class="col-xs-12">
      <div id="spGridTable_lpgroup">
         <?php
            $table = "leavepolicygroup";
            $sql = "SELECT * FROM `$table` ORDER BY RefId Desc LIMIT 100";
            doGridTable("leavepolicygroup",
                        ["Code","Name","Description"],
                        ["Code","Name","Description"],
                        $sql,
                        [true,true,true,false],
                        "gridTable_lpgroup");
         ?>
      </div>
      <button type="button" class="btn-cls4-sea"
           id="btnINSERTLPG" name="btnINSERTLPG">
         <i class="fa fa-file" aria-hidden="true"></i>
         &nbsp;Insert New
      </button>
   </div>
</div>


