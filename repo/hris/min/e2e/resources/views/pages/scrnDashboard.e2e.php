<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_Dashboard"); ?>"></script>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"pis"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar($modTitle); ?>
            <div class="container-fluid margin-top">
               <div class="row">
                  <div class="col-xs-2"></div>
                  <div class="col-xs-8">
                     <?php
                        $newly_hired      = 0;
                        $emp_count        = 0;
                        $emp_bday         = 0;
                        $three            = 0;
                        $fivebelow        = 0;
                        $six_ten          = 0;
                        $eleven_twenty    = 0;
                        $twentyone_thirty = 0;
                        $thirtyup         = 0;
                        $curr_date        = date("Y",time());
                        $audit_trail      = 0;

                        $audit_trail_rs = SelectEach("updates201","WHERE TrnDate >= '".date("Y-m-d",time())."' and TrnDate <= '".date("Y-m-d",time())."'");
                        if ($audit_trail_rs) {
                           $audit_trail = mysqli_num_rows($audit_trail_rs);
                        }

                        $emp_count = SelectEach("employees","WHERE (Inactive != 1 OR Inactive IS NULL)");
                        if (mysqli_num_rows($emp_count) > 0) {
                           $emp_count = mysqli_num_rows($emp_count);
                        }
                        //----------------------------------------------------
                        //----------------------------------------------------
                        //----------------------------------------------------
                        $emp_bday_where = "WHERE MONTH(BirthDate) = ".date("m",time());
                        $rs = SelectEach("employees",$emp_bday_where);
                        if ($rs) {
                           $emp_bday = mysqli_num_rows($rs);
                        } else {
                           $emp_bday = 0;
                        }
                        //----------------------------------------------------
                        //----------------------------------------------------
                        //----------------------------------------------------

                        $past_month = date("Y",time())."-01-01";
                        $newly_hired_rs = SelectEach("empinformation","WHERE HiredDate > '$past_month'");
                        if ($newly_hired_rs) {
                           while ($newly_hired_row = mysqli_fetch_assoc($newly_hired_rs)) {
                              $newly_hired++;
                           }
                        }
                        //----------------------------------------------------
                        //----------------------------------------------------
                        //----------------------------------------------------

                        $six_ten_where          = "WHERE YEAR(HiredDate) >= '2008' AND YEAR(HiredDate) < 2013";
                        $eleven_twenty_where    = "WHERE YEAR(HiredDate) >= '1998' AND YEAR(HiredDate) < 2008";
                        $twentyone_thirty_where = "WHERE YEAR(HiredDate) >= '1988' AND YEAR(HiredDate) < 1998";
                        $thirtyup_where         = "WHERE YEAR(HiredDate) <= '1987'";
                        $fivebelow_where        = "WHERE YEAR(HiredDate) >= '2013'";
                        $three_where        = "WHERE YEAR(HiredDate) == '2016'";

                        $years_rs = SelectEach("empinformation","WHERE HiredDate != ''");
                        if ($years_rs) {
                           while ($year_row = mysqli_fetch_assoc($years_rs)) {
                              $hired = date("Y",strtotime($year_row["HiredDate"]));
                              $diff = $curr_date - $hired;
                              $diff = $diff + 1;
                              if ($diff == 3) {
                                 $three++;
                              } else if ($diff <= 5) {
                                 $fivebelow++;
                              } else if ($diff >= 6 && $diff <= 10) {
                                 $six_ten++;
                              } else if ($diff >= 11 && $diff <= 20) {
                                 $eleven_twenty++;
                              } else if ($diff >= 21 && $diff <= 30) {
                                 $twentyone_thirty++;
                              } else if ($diff >= 30) {
                                 $thirtyup++;
                              }
                           }
                        }
                     ?>

                     <?php spacer(5) ?>
                     <div class="panel-group">
                        <div class="panel panel-default">
                           <div class="panel-heading">TOTAL NUMBER OF EMPLOYMENT</div>
                           <div class="panel-body">
                              <ul class="list-group">
                                 <li class="list-group-item counts--" onclick="showEmp('Employees','Name',0);">
                                    TOTAL ACTIVE EMPLOYEES
                                    <span class="badge"><?php echo $emp_count; ?></span>
                                 </li>
                                 <li class="list-group-item counts--" onclick="clickRpt('rpt_Newly_Hired','');">
                                    NEWLY HIRED
                                    <span class="badge"><?php echo $newly_hired; ?></span>
                                 </li>
                                 
                              </ul>
                           </div>
                        </div>
                        <div class="panel panel-default">
                           <div class="panel-heading ">NOTIFICATION FOR</div>
                           <div class="panel-body">
                              <ul class="list-group">
                                 <li class="list-group-item counts--" onclick="showEmp('BirthDate','With Birthdays this month',0);">
                                    BIRTHDAY CELEBRANTS FOR THE MONTH
                                    <span class="badge"><?php echo $emp_bday; ?></span>
                                 </li>
                                 <li class="list-group-item counts--" onclick="clickRpt('rptAuditTrails&txtTrnFrom=<?php echo date("Y-m-d",time())?>&txtTrnTo=<?php echo date("Y-m-d",time())?>','');">
                                    EMPLOYEES WITH PDS UPDATES
                                    <span class="badge"><?php echo $audit_trail; ?></span>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>          
               </div>
            </div>
            <!-- Modal -->
            <div class="modal fade border0" id="prnModal" role="dialog">
               <div class="modal-dialog border0" style="padding:0px;width:97%;height:92%;">

                  <div class="mypanel border0" style="height:100%;">
                     <div class="panel-top bgSilver">
                        <a href="#" data-toggle="tooltip" data-placement="top" title="Print Now" id="btnPRINTNOW">
                           <i class="fa fa-print" aria-hidden="true"></i>
                        </a>
                        <label class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                  </div>
               </div>
            </div>
            <?php
               footer();
               include "varHidden.e2e.php";
               doHidden("hRptFile","DashboardRpt","");
            ?>
         </div>
      </form>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_Dashboard"); ?>"></script>
   </body>
</html>



