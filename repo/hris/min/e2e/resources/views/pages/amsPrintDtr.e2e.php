<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script>
         $(document).ready(function () {
            $("#btnEXIT").click(function () {
               gotoscrn("amsTrnDTR","");
            });
            $("#btnGENERATE").click(function(){
               var url = "ReportCaller.e2e.php?file=" + "amsRptPrintDtr" + "&";
               //url += getRPTCriteria("rptCriteria");
               $("#prnModal").modal();
               $("#rptContent").attr("src",url);
            });
         });
      </script>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"ams"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar ($paramTitle); ?>
            <div class="container-fluid margin-top">
               <div class="row">
                  <div class="col-xs-12" id="div_CONTENT">
                     <div class="row panel-top">PRINT DTR</div>
                     <div class="row panel-mid">
                        <?php require_once "incEmpSearchCriteria.e2e.php"; ?>
                        <div class="row margin-top txt-center">
                           <button type="button"
                                class="btn-cls4-sea trnbtn"
                                id="btnGENERATE" name="btnGENERATE">
                              <i class="fa fa-file" aria-hidden="true"></i>
                              &nbsp;GENERATE REPORT
                           </button>
                           <button type="button"
                                class="btn-cls4-red trnbtn"
                                id="btnEXIT" name="btnEXIT">
                              <i class="fa fa-times" aria-hidden="true"></i>
                              &nbsp;EXIT
                           </button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Modal -->
            <div class="modal fade border0" id="prnModal" role="dialog">
               <div class="modal-dialog border0" style="padding:0px;width:97%;height:92%;">

                  <div class="mypanel border0" style="height:100%;">
                     <div class="panel-top bgSilver">
                        <a href="#" data-toggle="tooltip" data-placement="top" title="Print Now" id="btnPRINTNOW">
                           <i class="fa fa-print" aria-hidden="true"></i>
                        </a>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                  </div>
               </div>
            </div>
            <?php
               footer();
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
</html>



