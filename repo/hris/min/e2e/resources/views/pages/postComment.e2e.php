<?php
   session_start();
   include_once "constant.e2e.php";
   include_once $_SESSION['Classes'].'0620functions.e2e.php';


   function PostNewComment() {
      $comments = getvalue("char_Comments");
      $commentby = getvalue("char_CommentBy");
      $issuesRefId = getvalue("sint_issuesRefId");
      $ForwardTo = getvalue("drpForward");
      if (!empty($_FILES["imageComment"]["tmp_name"])) {
         $t = time();
         $filename = base64_encode(date("Ymd",$t).date("His",$t)."_".getvalue("char_CommentBy")."_".getvalue("sint_issuesRefId"));
         $target_dir = path."uploads/";
         $old_filename = basename($_FILES['imageComment']['name']);
         $imageFileType = pathinfo($target_dir.$old_filename,PATHINFO_EXTENSION);
         $new_filename = $filename.".".$imageFileType;
         if(move_uploaded_file($_FILES["imageComment"]["tmp_name"], pathPublic."images/Comments/" . $new_filename)){
            $Fields = "`issuesRefId`, `Comments`, `CommentBy`, `PictureComments`,`AssignedTo`,";
            $Values = "$issuesRefId, '$comments', '$commentby', '$new_filename','$ForwardTo',";
         }
      } else {
         $Fields = "`issuesRefId`, `Comments`, `CommentBy`,`AssignedTo`,";
         $Values = "$issuesRefId, '$comments', '$commentby','$ForwardTo',";
      }
      $SaveSuccessfull = f_SaveRecord("NEWSAVE","issuescomments",$Fields,$Values);
      if ($ForwardTo != "0") {
         $Fields = "CurrentlyWorkingBy='$ForwardTo',";
         $EditSuccessfull = f_SaveRecord("EDITSAVE","issues",$Fields,$issuesRefId);
      }
      return;
   }

   function PostNewIssues() {
      if (
            getvalue("char_Issue") != "" &&
            getvalue("char_Description") != ""
         )
      {
         $issue = getvalue("char_Issue");
         $desc = getvalue("char_Description");
         $createdby = getvalue("char_CreatedBy");
         $status = getvalue("char_Status");
         $Assigned = getvalue("drpAssigned");
         if (!empty($_FILES['postImage']['tmp_name'])) {
            $t = time();
            $filename = base64_encode(date("Ymd",$t).date("His",$t)."_".getvalue("char_CreatedBy"));
            $target_dir = path."uploads/";
            $old_filename = basename($_FILES['postImage']['name']);
            $imageFileType = pathinfo($target_dir.$old_filename,PATHINFO_EXTENSION);
            $new_filename = $filename.".".$imageFileType;
            if(move_uploaded_file($_FILES['postImage']['tmp_name'], pathPublic."images/Comments/" . $new_filename)){
               $Fields = "`Issue`, `Description`, `CreatedBy`, `ImageFile`, `Status`, `AssignedTo`,";
               $Values = "'$issue', '$desc', '$createdby', '$new_filename', '$status', '$Assigned',";
               $SaveSuccessfull = f_SaveRecord("NEWSAVE","issues",$Fields,$Values);
               if (!is_numeric($SaveSuccessfull)) {
                  echo $SaveSuccessfull;
               } else {
                  header("Location:scrnIssues.e2e.php?id=".$SaveSuccessfull);
               }
            } else {
              echo $_FILES['postImage']['name']. " KO";
            }
         } else {
            $Fields = "`Issue`, `Description`, `CreatedBy`, `Status`, `AssignedTo`,";
            $Values = "'$issue', '$desc', '$createdby', '$status', '$Assigned',";
            $SaveSuccessfull = f_SaveRecord("NEWSAVE","issues",$Fields,$Values);
            echo $SaveSuccessfull;
            if (!is_numeric($SaveSuccessfull)) {
               echo $SaveSuccessfull;
            } else {
               header("Location:scrnIssues.e2e.php?id=".$SaveSuccessfull);
            }
         }
      }
   }

   function ChangeStatus() {
      $status = getvalue("id");
      $issuesRefId = getvalue("refid");
      $Fields = "Status='$status',";
      $SaveSuccessfull = f_SaveRecord("EDITSAVE","issues",$Fields,$issuesRefId);
      if ($SaveSuccessfull == "") {
         echo '$("#issuesStatus").html("'.$status.'");';
      }
   }
   
   function insert($params) {
      $tblno = getvalue("tbl");
      $Remarks = getvalue("Remarks");
      switch ($tblno) {
         case 1:
            $table = "slvlearnedmonthly";
            $NoOfMonths = getvalue("txtNumMonths");
            $VLEarned = getvalue("txtVLEarned");
            $SLEarned = getvalue("txtSLEarned");
            $Fields = "`NoOfMonths`, `VLEarned`, `SLEarned`,`Remarks`,";
            $Values = "'$NoOfMonths', '$VLEarned', '$SLEarned','$Remarks',";
         break;
         case 2:
            $table = "slvlearneddaily";
            $Fields = "`issuesRefId`, `Comments`, `CommentBy`,`AssignedTo`,";
            $Values = "$issuesRefId, '$comments', '$commentby','$ForwardTo',";
         break;
      }
      $SaveSuccessfull = f_SaveRecord("NEWSAVE",$table,$Fields,$Values);
      if (is_numeric($SaveSuccessfull)) {
         updateListTable($tblno);
      } else {
         echo 'alert("'.$SaveSuccessfull.'");';
      }
   }

   $func = getvalue("fn");
   $func();
?>