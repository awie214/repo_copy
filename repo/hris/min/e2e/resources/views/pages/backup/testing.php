<?php
	include 'conn.e2e.php';
	include 'constant.e2e.php';
	include pathClass.'0620functions.e2e.php';
	//include 'upl/FnUpload.php';
	function saveFM($table,$flds,$vals,$name,$where = "") {
		include 'conn.e2e.php';
		$table 			= strtolower($table);
		$where 			= "WHERE Name = '$name' ".$where;
		$check_FM 		= FindFirst($table,$where,"RefId");
		if (is_numeric($check_FM)) {
			return $check_FM;
		} else {
			$date_today    	= date("Y-m-d",time());
	        $curr_time     	= date("H:i:s",time());
	        $trackingA_fld 	= "`LastUpdateDate`, `LastUpdateTime`, `LastUpdateBy`, `Data`";
	        $trackingA_val 	= "'$date_today', '$curr_time', 'Admin', 'A'";
	        $flds   	   	= $flds.$trackingA_fld;
	        $vals   		= $vals.$trackingA_val;
	        $sql 			= "INSERT INTO $table ($flds) VALUES ($vals)";
	        $rs 			= mysqli_query($conn,$sql) or die(mysqli_error($conn));
	        if ($rs) {
	        	return mysqli_insert_id($conn);
	        } else {
	        	echo $rs."<br>$sql";
	        }	
		}
			
	}
	if (!isset($_GET["test"])) {
		echo '<h3>No Parameter</h3>';
		return false;
	}
	$test = $_GET["test"];
	
	if ($test == 1) {
		$sql = "SELECT * from employees inner join pms_employees WHERE employees.FirstName = pms_employees.firstname AND employees.LastName = pms_employees.lastname ORDER BY employees.LastName";
		$rs = mysqli_query($conn,$sql);
		echo '<table border=1 width="100%">';
		echo '
			<tr>
				<td>LastName</td>
				<td>FirstName</td>
				<td>AgencyId</td>
				<td>employee_number</td>
				<td>lastname</td>
				<td>firstname</td>
			</tr>
		';
		while ($row = mysqli_fetch_assoc($rs)) {
			$emprefid = $row["id"];
			$AgencyId = $row["AgencyId"];
			$LastName = $row["LastName"];
			$FirstName = $row["FirstName"];
			$employee_number = $row["employee_number"];
			$lastname = $row["lastname"];
			$firstname = $row["firstname"];
			$update = "UPDATE pms_employees SET employee_number = '$AgencyId' WHERE id='$emprefid'";
			$result = mysqli_query($conn,$update);
			echo '
				<tr>
					<td>'.$LastName.'</td>
					<td>'.$FirstName.'</td>
					<td>'.$AgencyId.'</td>
					<td>'.$employee_number.'</td>
					<td>'.$emprefid." -> ".$lastname.'</td>
					<td>'.$firstname.'</td>
				</tr>
			';
			
			if ($result) {
				echo "Update $FirstName employee number. -> $update<br>";
			} else {
				echo $update." -> ".$result."<br>";
			}
		}
		echo '</table>';
	} else if ($test == 2) {
		//update empinformation SET HiredDate = NULL, AssumptionDate = NULL, StartDate = NULL, EndDate = NULL, ResignedDate = NULL
		echo '<table border=1 width="100%">';
		echo '
			<tr>
				<td>Start Date</td>
				<td>End Date</td>
				<td>Position</td>
				<td>Salary Amount</td>
				<td>Salary Grade</td>
				<td>Step</td>
				<td>Appt Status</td>
			</tr>
		';
		$where = "WHERE employee_personal_information_sheet_id = 4";
		$where = "";
		$emp = SelectEach("employees","");
		$active = 0;
		$inactive = 0;
		if ($emp) {
			while ($emp_row = mysqli_fetch_assoc($emp)) {
				$emprefid = $emp_row["RefId"];
				$where = "WHERE employee_personal_information_sheet_id = '$emprefid'";
				$sql = "SELECT * FROM employee_work_experiences $where ORDER BY start_date DESC LIMIT 1";
				$rs = mysqli_query($conn,$sql);
				if ($rs) {
					while ($row = mysqli_fetch_assoc($rs)) {
						$start_date = $row["start_date"];
						$end_date = $row["end_date"];
						$position_title = $row["position_title"];
						$monthly_salary = $row["monthly_salary"];
						$salary_grade = intval($row["salary_grade"]);
						$step = intval($row["step"]);
						$status_of_appointment = $row["status_of_appointment"];
						if ($end_date == "") {
							$active++;
						} else {
							$inactive++;
						}
						$Fld = "";
						$Val = "";
						$FldnVal = "";
						if ($end_date != "") {
							$update = "Inactive = 1, ";
							$result_update = f_SaveRecord("EDITSAVE","employees",$update,$emprefid);
							if ($result != "") {
								echo $result."<br>";
							}
						}
						if (
							strtolower($status_of_appointment) == "contractual" || 
							strtolower($status_of_appointment) == "co-terminous w/ pascn" ||
							strtolower($status_of_appointment) == "coterminus"
						) {
							//$Fld .= "`StartDate`,`AssumptionDate`,";
							//$Val .= "'$start_date','$start_date',";
							$FldnVal .= "`StartDate` = '$start_date', `AssumptionDate` = '$start_date',";
							if ($end_date != "") {
								//$Fld .= "`EndDate`,";
								//$Val .= "'$end_date',";
								$FldnVal .= "`EndDate` = '$end_date',";
							}
						} else {
							//$Fld .= "HiredDate, AssumptionDate, ";
							//$Val .= "'$start_date','$start_date', ";
							$FldnVal .= "`HiredDate` = '$start_date', `AssumptionDate` = '$start_date', ";
							if ($end_date != "") {
								//$Fld .= "`ResignedDate`,";
								//$Val .= "'$end_date',";
								$FldnVal .= "`ResignedDate` = '$end_date',";
							}
						}
						if ($salary_grade > 0) {
							$SalaryGradeRefId = FindFirst("salarygrade","WHERE Name = '$salary_grade'","RefId");
							if ($SalaryGradeRefId) {
								//$Fld .= "`SalaryGradeRefId`,";
								//$Val .= "'$SalaryGradeRefId',";
								$FldnVal .= "`SalaryGradeRefId` = '$SalaryGradeRefId',";
							}
						}
						if ($step > 0) {
							$StepIncrementRefId = FindFirst("stepincrement","WHERE Name = 'STEP-$step'","RefId");
							if ($StepIncrementRefId) {
								//$Fld .= "`StepIncrementRefId`,";
								//$Val .= "'$StepIncrementRefId',";
								$FldnVal .= "`StepIncrementRefId` = '$StepIncrementRefId',";
							}
						}
						if (intval($monthly_salary) > 0) {
							//$Fld .= "`SalaryAmount`,";
							//$Val .= "'$monthly_salary',";
							$FldnVal .= "`SalaryAmount` = '$monthly_salary',";
						}
						if ($position_title != "") {
							$PositionRefId = saveFM("position","`Name`,","`$position_title`",$position_title);
							if (is_numeric($PositionRefId)) {
								//$Fld .= "`PositionRefId`,";
								//$Val .= "'$PositionRefId',";
								$FldnVal .= "`PositionRefId` = '$PositionRefId',";	
							}
						}
						if ($status_of_appointment != "") {
							$status_of_appointment = strtoupper($status_of_appointment);
							$EmpStatusRefId = saveFM("empstatus","`Name`,","`$status_of_appointment`",$status_of_appointment);
							if (is_numeric($EmpStatusRefId)) {
								//$Fld .= "`EmpStatusRefId`,";
								//$Val .= "'$EmpStatusRefId',";
								$FldnVal .= "`EmpStatusRefId` = '$EmpStatusRefId',";	
							}
						}
						$empinfo = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","RefId");
						if ($empinfo) {
							$result = f_SaveRecord("EDITSAVE","empinformation",$FldnVal,$empinfo,"System");
							if ($result != "") {
								echo "Error $emprefid.<br>";
							}
						}
						echo '
							<tr>
							<td>'.$start_date.'</td>
							<td>'.$end_date.'</td>
							<td>'.$position_title.'</td>
							<td>'.$monthly_salary.'</td>
							<td>'.$salary_grade.'</td>
							<td>'.$step.'</td>
							<td>'.$status_of_appointment.'</td>
							</tr>
						';
						
					}
				}		
			}
		}
		echo '
			<tr>
				<td>Active</td>
				<td>'.$active.'</td>
				<td>Inactive</td>
				<td>'.$inactive.'</td>
			</tr>
			';
		echo '</table>';
	} else if ($test == "3") {
		$rs = SelectEach("employees","WHERE (Inactive != 1 OR Inactive IS NULL)");
		if ($rs) {
			while ($row = mysqli_fetch_assoc($rs)) {
				$emprefid = $row["RefId"];
				$pms_empinfo = sync_pms($emprefid,2);
				$pms_salaryinfo = sync_pms($emprefid,3);
				if ($pms_empinfo != "") {
					echo "Error in Updating PMS Emp Info -> $pms_empinfo.<br>";
				} else {
					echo "Pms Emp Info Updated.<br>";
				}
				if ($pms_salaryinfo != "") {
					echo "Error in Updating PMS Salary Info -> $pms_salaryinfo.<br>";
				} else {
					echo "Pms Salary Info Updated.<br>";
				}
			}
		}
	} else if ($test == "4") {
		$rs = SelectEach("employees"," LIMIT 5");
		while ($row = mysqli_fetch_assoc($rs)) {
			$data["array"] = $row;
			echo "<pre>";
			echo json_encode(array_values($data["array"]),JSON_PRETTY_PRINT);
			echo "</pre>";
		}
		
	} else if ($test == "5") {
		function get_empid() {
			include 'conn.e2e.php';
			$sql = "SELECT AgencyId FROM employees WHERE AgencyId LIKE 'P20%' ORDER BY AgencyId DESC LIMIT 1";
			$rs = mysqli_query($conn,$sql);
			if ($rs) {
				while ($row = mysqli_fetch_assoc($rs)) {
					$empid = $row["AgencyId"];
					$empid_arr = explode("-", $empid);
					if (strlen($empid_arr[1]) > 3) {
						return $empid_arr[0]."-0".($empid_arr[1] + 1);
					} else {
						return $empid_arr[0]."-".($empid_arr[1] + 1);
					}
					
				}
			}	
		}
		//echo get_empid();
		
	} else if ($test == "6") {
		echo $_SERVER["DOCUMENT_ROOT"] . "/repo/hris/min/e2e/mdb/sample.mdb";
		/*$dbName = $_SERVER["DOCUMENT_ROOT"] . "/repo/hris/min/e2e/mdb/sample.mdb";
	   	$driver = 'MDBTools';
	   	$dataSourceName = "odbc:Driver=$driver;DBQ=$dbName;Uid=;Pwd=;";
	   	$connection = new PDO($dataSourceName);
	   	$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	   	var_dump($connection);*/
	   	
	   	/*$mdbFilename = $_SERVER["DOCUMENT_ROOT"] . "/repo/hris/min/e2e/mdb/sample.mdb";
	   	$user = "";
	   	$password = "";
	   	$connection = odbc_connect("Driver={Microsoft Access Driver (*.mdb)};Dbq=$mdbFilename", $user, $password);
	   	var_dump($connection);*/

	   	/*$db = $_SERVER["DOCUMENT_ROOT"] . "/repo/hris/min/e2e/mdb/sample.mdb";
		$connection = new COM('ADODB.Connection');
		$connection->Open("DRIVER={Driver do Microsoft Access (*.mdb)}; DBQ=$db");
		var_dump($connection);*/

	} else if ($test == "7") {
		$mconn = mysqli_connect("localhost","root","","mirdc_old");
		$sql = "SELECT * FROM tblservicerecord";
		$rs = mysqli_query($mconn,$sql);
		if ($rs) {
			$str = "";
			while ($row = mysqli_fetch_assoc($rs)) {
				$empid = $row["empNumber"];
				$start_date = $row["serviceFromDate"];
				$end_date = $row["serviceToDate"];
				$plantilla = $row["positionCode"];
				$position = $row["positionDesc"];
				$salary = $row["salary"];
				$agency = $row["stationAgency"];
				$trigger = $row["governService"];
				$separation = $row["separationDate"];
				$remarks = $row["remarks"];
				$lwop = $row["lwop"];
				$empstatus = $row["appointmentCode"];
				$check = "SELECT `appointmentDesc` FROM tblappointment WHERE appointmentCode = '$empstatus'";
				$chech_rs = mysqli_query($mconn,$check);
				if ($chech_rs) {
					$check_row = mysqli_fetch_assoc($chech_rs);
					$empstatus = $check_row["appointmentDesc"];
				}
				$division = $row["branch"];
				$trigger = strtolower($trigger);
				$str .= $trigger."|".$empid."|".$start_date."|".$end_date."|".$plantilla."|".$position."|".$salary."|".$agency;
				$str .= $separation."|".$remarks."|".$lwop."|".$empstatus."|".$division;
				echo $division."<br>";
			}
			$myfile = fopen("upl/csv/service_record_21.txt", "a") or die("Unable to open file!");
			fwrite($myfile,"\n".$str);
			fclose($myfile);
		}

	} else if ($test == "8") {

		//header("Content-type: application/vnd.ms-word");
		//header("Content-Disposition: attachment;Filename=document_name.doc");

		echo "<html>";
		echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=Windows-1252\">";
		echo "<body>";
		echo "<b>My first document</b>";
		echo "</body>";
		echo "</html>";
	} else if ($test == "9") {
		$rs = SelectEach("employees","ORDER BY AgencyId");
		if ($rs) {
			while ($row = mysqli_fetch_assoc($rs)) {
				$AgencyId 	= $row["AgencyId"];
				$FirstName 	= $row["FirstName"];
				$LastName 	= $row["LastName"];
				$MiddleName = $row["MiddleName"];
				$str = $AgencyId.",".$LastName.",".$FirstName.",".$MiddleName."<br>";
				echo $str;
			}
		}
	} else if ($test == "10") {
		$emprefid = "123";
		$from 		= "2018-10-29";
		$to 		= "2018-10-29";
		echo count_leave($emprefid,$from,$to);
	} else if ($test == "11") {
		include 'conn.e2e.php';
		mysqli_query($conn,"TRUNCATE pms_employees");
		mysqli_query($conn,"TRUNCATE pms_employee_information");
		mysqli_query($conn,"TRUNCATE pms_salaryinfo");
		$Sync     = file_get_contents("upl/json/pis_pms_sync.json");
        $Sync_arr = json_decode($Sync, true);
		$rs = SelectEach("employees","WHERE (Inactive = '0' OR Inactive IS NULL)");
		if ($rs) {
			while ($row = mysqli_fetch_assoc($rs)) {
				$pms_employees_Fld 	= "active, ";
				$pms_employees_Val 	= "1, ";
				$pms_employee_information_Fld = "";
				$pms_employee_information_Val = "";
				$pms_salaryinfo_Fld = "";
				$pms_salaryinfo_Val = "";
				$emprefid 			= $row["RefId"];
				foreach ($Sync_arr["pms_employees"] as $key => $value) {
					if ($row[$key] != "") {
						$pms_employees_Fld .= "`".$value."`,";
						$pms_employees_Val .= "'".realEscape($row[$key])."',";
					}
				}
				$empinfo_row = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","*");
				if ($empinfo_row) {
					foreach ($Sync_arr["pms_employee_information"] as $nkey => $nvalue) {
						if ($empinfo_row[$nkey] != "") {
							$pms_employee_information_Fld .= "`".$nvalue."`,";
							$pms_employee_information_Val .= "'".$empinfo_row[$nkey]."',";
						}
					}
					foreach ($Sync_arr["pms_salaryinfo"] as $nkey => $nvalue) {
						if ($empinfo_row[$nkey] != "") {
							$pms_salaryinfo_Fld .= "`".$nvalue."`,";
							$pms_salaryinfo_Val .= "'".$empinfo_row[$nkey]."',";
						}
					}	
				}
				$save_pms_employee = savePMS("pms_employees",$pms_employees_Fld,$pms_employees_Val);
				if (is_numeric($save_pms_employee)) {
					echo "Successfully Save in PMS Employee.<br>";
					if ($pms_employee_information_Fld != "") {
						$pms_employee_information_Fld .= "`employee_id`,";
						$pms_employee_information_Val .= "'".$save_pms_employee."',";
						$save_pms_employee_information = savePMS("pms_employee_information",$pms_employee_information_Fld,$pms_employee_information_Val);
						if (is_numeric($save_pms_employee_information)) {
							echo "Successfully Save in PMS Employee Information.<br>";
						} else {
							echo "Error Saving in PMS Employee Information.<br>";
						}
					}
					if ($pms_salaryinfo_Fld != "") {
						$pms_salaryinfo_Fld .= "`employee_id`,";
						$pms_salaryinfo_Val .= "'".$save_pms_employee."',";
						$save_pms_salaryinfo = savePMS("pms_salaryinfo",$pms_salaryinfo_Fld,$pms_salaryinfo_Val);
						if (is_numeric($save_pms_salaryinfo)) {
							echo "Successfully Save in PMS Salary Info.<br>";
						} else {
							echo "Error Saving in PMS Salary Info.<br>";
						}
					}
				}
				
			}
		}
	} else if ($test == "12") {
		$rs = SelectEach("employees","");
		$count = 0;
		if ($rs) {
			while ($row = mysqli_fetch_assoc($rs)) {
				$emprefid = $row["RefId"];
				$FirstName = $row["FirstName"];
				$LastName = $row["LastName"];
				$AgencyId = $row["AgencyId"];
				$empinfo = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","*");
				if ($empinfo) {
					
					$AssumptionDate = $empinfo["AssumptionDate"];
					if ($AssumptionDate == "") {
						$count++;
						echo $count.". ".$FirstName." ".$LastName.".<br>";
					}
				}
			}
		}
	} else if ($test == "13") {
		$obj = fopen("upl/csv/second_tranche.csv", "r");
		echo '{<br>';
		while(!feof($obj)) {
			$str = fgets($obj);
			if ($str != "") {
				$row = explode(",", $str);	
				
					echo '"'.$row[0].'": {<br>';
					echo '"1":"'.$row[1].'",<br>';
					echo '"2":"'.$row[2].'",<br>';
					echo '"3":"'.$row[3].'",<br>';
					echo '"4":"'.$row[4].'",<br>';
					echo '"5":"'.$row[5].'",<br>';
					echo '"6":"'.$row[6].'",<br>';
					echo '"7":"'.$row[7].'",<br>';
					echo '"8":"'.$row[8].'",<br>';
					echo '},<br>';
				
			}
		}
		echo '},<br>';
	} else if ($test == "14") {
		include 'conn.e2e.php';
		$rs = SelectEach("employees","");
		if ($rs) {
			while ($row = mysqli_fetch_assoc($rs)) {
				$refid = $row["RefId"];
				$AgencyId = $row["AgencyId"];
				$update = "UPDATE employees SET BiometricsID = '$AgencyId' WHERE RefId = '$refid'";
				$update_rs = mysqli_query($conn,$update);
				
			}
		}
	} else if ($test == 15) {
		include 'conn.e2e.php';
		$rs = SelectEach("employees","");
		if ($rs) {
			while ($row = mysqli_fetch_assoc($rs)) {
				$emprefid = $row["RefId"];
				$empid = $row["AgencyId"];
				$sql = "UPDATE employees SET UserName = '$empid' WHERE RefId = '$emprefid'";
				$result = mysqli_query($conn,$sql);
				if ($result) {
					echo "Updated Login of ".$row["LastName"]." ".$row["FirstName"]."<br>";
				}
			}
		}
	}
?>