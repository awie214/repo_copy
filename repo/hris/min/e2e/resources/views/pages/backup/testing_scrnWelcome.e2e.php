<?php
   include 'conn.e2e.php';
   $ttcount_arr = array();
   if (getvalue("hCompanyID") != "2") {
      $disabled = "";
   } else {
      $disabled = "disabled";
   }
   $disabled = "";
   $EmpRefId = getvalue("hEmpRefId");
?>
<!DOCTYPE>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script type="text/javascript" src="<?php echo $_SESSION["login"] ?>"></script>
      <script type="text/javascript" src="<?php echo $path; ?>js/jsSHAver2/src/sha.js"></script>
      <link rel="stylesheet" href="<?php echo $path."/css/sideBar.css"; ?>">
      <style type="text/css">
         #PIS_Header, #AMS_Header, #PMS_Header, #myrequest {cursor: pointer;}
      </style>
      <script language="JavaScript">
         $(document).ready(function () {
            

         });
      </script>
   </head>
   <body>
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <nav class="navbar navbar-fixed-top">
            <div class="sysNameHolder sysBG" style="border-bottom:3px solid #fff;">
               <?php
                  $TRNBTN = 0;
                  $title = "";
                  $Logout = true;
                  include $files["inc"]["hdr"];
               ?>
               <span class="sysName">
                  <?php
                     echo $settings["Title"];
                  ?>
               </span>
            </div>
         </nav>
         <div style="margin-top:60px;">
            <?php
               if (!$isUser) {
                  doSideBarMain();
            ?>
                  <div class="container-fluid" id="mainScreen">
                     <?php doTitleBar("DASHBOARD / REMINDERS"); ?>
                     <div class="row">
                        <div class="col-xs-6">
                           <?php
                              $rowcount = 0;
                              $whereClause = "WHERE CompanyRefId = ".getvalue("hCompanyID")." AND BranchRefId = ".getvalue("hBranchID");
                              $emp_nopic = $whereClause." AND PicFilename IS NULL or PicFilename = ''";
                              $rs = SelectEach("employees",$emp_nopic);
                              if ($rs) {
                                 $emp_nopic = mysqli_num_rows($rs);
                              } else {
                                 $emp_nopic = 0;
                              }

                              $emp_noLN = $whereClause." AND (LastName IS NULL or LastName = '')";
                              $rs = SelectEach("employees",$emp_noLN);
                              if ($rs) {
                                 $emp_noLN = mysqli_num_rows($rs);
                              } else {
                                 $emp_noLN = 0;
                              }

                              $emp_noFN = $whereClause." AND (FirstName IS NULL or FirstName = '')";
                              $rs = SelectEach("employees",$emp_noFN);
                              if ($rs) {
                                 $emp_noFN = mysqli_num_rows($rs);
                              } else {
                                 $emp_noFN = 0;
                              }

                              $emp_count = $whereClause;
                              $rs = SelectEach("employees",$emp_count);
                              if ($rs) {
                                 $emp_count = mysqli_num_rows($rs);
                              } else {
                                 $emp_count = 0;
                              }

                              $emp_noPHIC = $whereClause." AND (PHIC IS NULL or PHIC = '' or PHIC = 'N/A')";
                              $rs = SelectEach("employees",$emp_noPHIC);
                              if ($rs) {
                                 $emp_noPHIC = mysqli_num_rows($rs);
                              } else {
                                 $emp_noPHIC = 0;
                              }

                              $emp_noGSIS = $whereClause." AND (GSIS IS NULL or GSIS = '' or GSIS = 'N/A')";
                              $rs = SelectEach("employees",$emp_noGSIS);
                              if ($rs) {
                                 $emp_noGSIS = mysqli_num_rows($rs);
                              } else {
                                 $emp_noGSIS = 0;
                              }

                              $emp_noSSS = $whereClause." AND (SSS IS NULL or SSS = '' or SSS = 'N/A')";
                              $rs = SelectEach("employees",$emp_noSSS);
                              if ($rs) {
                                 $emp_noSSS = mysqli_num_rows($rs);
                              } else {
                                 $emp_noSSS = 0;
                              }

                              $emp_noTIN = $whereClause." AND (TIN IS NULL or TIN = '' or TIN = 'N/A')";
                              $rs = SelectEach("employees",$emp_noTIN);
                              if ($rs) {
                                 $emp_noTIN = mysqli_num_rows($rs);
                              } else {
                                 $emp_noTIN = 0;
                              }

                              $emp_noPAGIBIG = $whereClause." AND (PAGIBIG IS NULL or PAGIBIG = '' or PAGIBIG = 'N/A')";
                              $rs = SelectEach("employees",$emp_noPAGIBIG);
                              if ($rs) {
                                 $emp_noPAGIBIG = mysqli_num_rows($rs);
                              } else {
                                 $emp_noPAGIBIG = 0;
                              }

                              $emp_noAgencyId = $whereClause." AND AgencyId IS NULL or AgencyId = ''";
                              $rs = SelectEach("employees",$emp_noAgencyId);
                              if ($rs) {
                                 $emp_noAgencyId = mysqli_num_rows($rs);
                              } else {
                                 $emp_noAgencyId = 0;
                              }

                              $emp_bday = $whereClause." AND MONTH(BirthDate) = ".date("m",time());
                              $rs = SelectEach("employees",$emp_bday);
                              if ($rs) {
                                 $emp_bday = mysqli_num_rows($rs);
                              } else {
                                 $emp_bday = 0;
                              }

                              $emp_anniv = $whereClause." AND MONTH(RehiredDate) = ".date("m",time());
                              $rs = SelectEach("empinformation",$emp_anniv);
                              if ($rs) {
                                 $emp_anniv = mysqli_num_rows($rs);
                              } else {
                                 $emp_anniv = 0;
                              }




                              $tt_empdoc_arr = array();
                              $emp_count = 0;
                              $rs_emp = SelectEach("employees",$whereClause." AND (Inactive != 1 OR Inactive IS NULL)");
                              if ($rs_emp) {
                                 $emp_count = mysqli_num_rows($rs_emp);   
                              }
                              

                              $sql = "SELECT * FROM attachdoctype";
                              $rs = mysqli_query($conn,$sql);
                              if (mysqli_num_rows($rs) > 0) {
                                 while ($row = mysqli_fetch_assoc($rs)) {  
                                    $c = $row["Code"];
                                    $n = $row["Name"];
                                    $ttcount_arr["CODE"][$c] = [$n,$emp_count];
                                 }
                              }

                              $dir = path."images/EmpDocument";
                              if (is_dir($dir)){
                                 if ($dh = opendir($dir)){
                                      while ($file = readdir($dh))
                                    {
                                       if (stripos($file,".png") > 0 ||
                                             stripos($file,".jpg") > 0 ||
                                             stripos($file,".gif") > 0 ||
                                             stripos($file,".jpeg") > 0 ||
                                             stripos($file,".pdf") > 0
                                          ) {
                                          $arr = explode("_", $file);
                                          $type = $arr[0];
                                          $emprefid = explode(".", $arr[1])[0];
                                          $check_doctype = FindFirst("attachdoctype","WHERE Code = '".$type."'","RefId");
                                          if ($check_doctype) {
                                             $count = $ttcount_arr["CODE"][$type][1];
                                             $rs = FindFirst("employees","WHERE RefId = ".$emprefid,"*");
                                             if ($rs) {
                                                $count--;
                                                $ttcount_arr["CODE"][$type][1] = $count;
                                             }   
                                          }
                                          
                                       }  
                                    }
                                    closedir($dh);
                                 }
                              }

                           ?>

                           <?php spacer(5) ?>
                           <div class="panel-group">
                              <div class="panel panel-default">
                                 <div class="panel-heading">TOTAL NUMBER OF EMPLOYMENT</div>
                                 <div class="panel-body">
                                    <ul class="list-group">
                                       <li class="list-group-item counts--" onclick="showEmp('Employees','Name',0);">
                                          TOTAL ACTIVE EMPLOYEES
                                          <span class="badge"><?php echo $emp_count; ?></span>
                                       </li>
                                       <li class="list-group-item counts--" onclick="alert('On-going');">
                                          NEWLY REGULAR
                                          <span class="badge"><?php echo 0; ?></span>
                                       </li>
                                       <li class="list-group-item counts--" onclick="alert('On-going');">
                                          END OF CONTRACT
                                          <span class="badge"><?php echo 0; ?></span>
                                       </li>
                                       <!--
                                       <li class="list-group-item counts--" onclick="alert('On-going');">
                                          RESIGNED / TERMINATED
                                          <span class="badge"><?php echo 0; ?></span>
                                       </li>
                                       -->
                                    </ul>
                                 </div>
                              </div>
                              <div class="panel panel-default">
                                 <div class="panel-heading ">NOTIFICATION FOR</div>
                                 <div class="panel-body">
                                    <ul class="list-group">
                                       <li class="list-group-item counts--" onclick="showEmp('BirthDate','With Birthdays this month',0);">
                                          BIRTHDAY CELEBRANTS FOR THE MONTH
                                          <span class="badge"><?php echo $emp_bday; ?></span>
                                       </li>
                                       <!-- <li class="list-group-item counts--" onclick="alert('On-going');">
                                          ANNIVERSARY
                                          <span class="badge"><?php echo $emp_anniv; ?></span>
                                       </li>
                                       <li class="list-group-item counts--" onclick="alert('On-going');">
                                          PROMOTED
                                          <span class="badge"><?php echo 0; ?></span>
                                       </li>
                                       <li class="list-group-item counts--" onclick="alert('On-going');">
                                          RETIREES
                                          <span class="badge"><?php echo 0; ?></span>
                                       </li> -->
                                    </ul>
                                 </div>
                              </div>
                              <div class="panel panel-default">
                                 <div class="panel-heading ">STEP INCREMENT AND LOYALTY</div>
                                 <div class="panel-body">
                                    <ul class="list-group">
                                       <li class="list-group-item counts--" onclick="alert('On-going');">3 YEARS
                                          <span class="badge"><?php echo 0; ?></span>
                                       </li>
                                       <li class="list-group-item counts--" onclick="alert('On-going');">4 - 10 YEARS
                                          <span class="badge"><?php echo 0; ?></span>
                                       </li>
                                       <li class="list-group-item counts--" onclick="alert('On-going');">11 - 15 YEARS
                                          <span class="badge"><?php echo 0; ?></span>
                                       </li>
                                       <li class="list-group-item counts--" onclick="alert('On-going');">16 - UP
                                          <span class="badge"><?php echo 0; ?></span>
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                              <!-- <div class="panel panel-default">
                                 <div class="panel-heading ">Reminder:</div>
                                 <div class="panel-body">
                                    <ul class="list-group">
                                       <li class="list-group-item counts--" onclick="showEmp('PicFilename','With No Pictures',0);">
                                          Employees with No Pictures:
                                          <span class="badge"><?php echo $emp_nopic; ?></span>
                                       </li>
                                       <li class="list-group-item counts--" onclick="showEmp('LastName','With No Last Name',0);">
                                          Employees with No Last Name:
                                          <span class="badge"><?php echo $emp_noLN; ?></span>
                                       </li>
                                       <li class="list-group-item counts--" onclick="showEmp('FirstName','With No First Name',0);">
                                          Employees with No First Name:
                                          <span class="badge"><?php echo $emp_noFN; ?></span>
                                       </li>
                                       <li class="list-group-item counts--" onclick="showEmp('PHIC','With No Philhealth',0);">
                                          Employees with No Philhealth:
                                          <span class="badge"><?php echo $emp_noPHIC; ?></span>
                                       </li>
                                       <li class="list-group-item counts--" onclick="showEmp('GSIS','With No GSIS',0);">
                                          Employees with No GSIS:
                                          <span class="badge"><?php echo $emp_noGSIS; ?></span>
                                       </li>
                                       <li class="list-group-item counts--" onclick="showEmp('SSS','With No SSS',0);">
                                          Employees with No SSS:
                                          <span class="badge"><?php echo $emp_noSSS; ?></span>
                                       </li>
                                       <li class="list-group-item counts--" onclick="showEmp('TIN','With No TIN',0);">
                                          Employees with No TIN:
                                          <span class="badge"><?php echo $emp_noTIN; ?></span>
                                       </li>
                                       <li class="list-group-item counts--" onclick="showEmp('PAGIBIG','With No PAGIBIG',0);">
                                          Employees with No PAGIBIG:
                                          <span class="badge"><?php echo $emp_noPAGIBIG; ?></span>
                                       </li>
                                       <li class="list-group-item counts--" onclick="showEmp('AgencyId','With No Agency ID',0);">
                                          Employees with No Agency ID:
                                          <span class="badge"><?php echo $emp_noAgencyId; ?></span>
                                       </li>
                                    </ul>
                                 </div>
                              </div> -->
                           </div>
                        </div>
                        <style type="text/css">
                           .card {
                              border: 1px solid #00e600; 
                              border-radius: 5px; 
                              background: #0067a7; 
                              color: white;
                              padding: 10px;
                              font-weight: 600;
                           }
                           .card:hover {
                              transition: 0.2s;
                              box-shadow: 3px 3px 3px gray;
                           }
                           .card-body {
                              border: 1px solid gray; 
                              background: #fffce4; 
                              color: black;
                              border-radius: 5px;
                           }
                           .card-body:hover {
                              transition: 0.5s;
                              -ms-transform: rotate(-3deg);
                              -webkit-transform: rotate(-3deg);
                              transform: rotate(-3deg);
                           }
                        </style>
                        <div class="col-xs-6 padd5">
                           <div class="panel panel-default">
                              <div class="panel-heading ">PENDING AVAILMENTS:</div>
                              <div class="panel-body">
                                 <div class="row">
                                    <div class="col-xs-6" style="padding: 15px;">
                                       <div class="row card">
                                          <div class="col-xs-12">
                                             <div class="row text-center">
                                                <div class="col-xs-12">
                                                   LEAVE
                                                </div>
                                             </div>
                                             <div class="row margin-top">
                                                <div class="col-xs-2"></div>
                                                <div class="col-xs-8 text-center card-body" style="">
                                                   <br>
                                                   <br>
                                                   10
                                                   <br>
                                                   <br>
                                                   &nbsp;
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <ul class="list-group">
                                    <li class="list-group-item counts--" onclick="gotoscrn('amsAvailLeave','');">
                                       Leave Applications:
                                       <?php 
                                          $leave_count = SelectEach("employeesleave","WHERE Status IS NULL");
                                          if ($leave_count) {
                                             $leave_count = mysqli_num_rows($leave_count);
                                          } else {
                                             $leave_count = 0;
                                          }
                                       ?>
                                       <span class="badge"><?php echo $leave_count; ?></span>
                                    </li>
                                    <li class="list-group-item counts--" onclick="gotoscrn('amsAvailAuthority','');">
                                       Office Authority Applications:
                                       <?php 
                                          $authority_count = SelectEach("employeesauthority","WHERE Status IS NULL");
                                          if ($authority_count) {
                                             $authority_count = mysqli_num_rows($authority_count);
                                          } else {
                                             $authority_count = 0;
                                          }
                                       ?>
                                       <span class="badge"><?php echo $authority_count; ?></span>
                                    </li>
                                    <li class="list-group-item counts--" onclick="gotoscrn('amsAvailAuthority','');">
                                       CTO Applications:
                                       <?php 
                                          $CTO_count = SelectEach("employeescto","WHERE Status IS NULL");
                                          if ($CTO_count) {
                                             $CTO_count = mysqli_num_rows($CTO_count);
                                          } else {
                                             $CTO_count = 0;
                                          }
                                       ?>
                                       <span class="badge"><?php echo $CTO_count; ?></span>
                                    </li>
                                    <li class="list-group-item counts--" onclick="gotoscrn('amsAvailLeaveMonitization','');">
                                       Leave Monetization Applications:
                                       <?php 
                                          $monetization_count = SelectEach("employeesleavemonetization","WHERE Status IS NULL");
                                          if ($monetization_count) {
                                             $monetization_count = mysqli_num_rows($monetization_count);
                                          } else {
                                             $monetization_count = 0;
                                          }
                                       ?>
                                       <span class="badge"><?php echo $monetization_count; ?></span>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- Modal -->
                  <div class="modal fade border0" id="prnModal" role="dialog">
                     <div class="modal-dialog border0" style="padding:0px;width:97%;height:92%;">

                        <div class="mypanel border0" style="height:100%;">
                           <div class="panel-top bgSilver">
                              <a href="#" data-toggle="tooltip" data-placement="top" title="Print Now" id="btnPRINTNOW">
                                 <i class="fa fa-print" aria-hidden="true"></i>
                              </a>
                              <label class="close" data-dismiss="modal">&times;</button>
                           </div>
                           <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                        </div>
                     </div>
                  </div>
            <?php

               } else {
                  /*
                     USER SIDE SCRN WELCOME
                  */
                  spacer(25);
            ?>
                  
                  <div class="row" style="margin:0px;">
                     <div class="col-xs-6" style="margin:0px;">
                        <?php
                           include 'conn.e2e.php';
                           $sql = "SELECT * FROM `employees` WHERE RefId = ".getvalue("hEmpRefId");
                           $rs = mysqli_query($conn,$sql);
                           if (mysqli_num_rows($rs) > 0)
                           {
                              $row = mysqli_fetch_assoc($rs)

                        ?>
                              <table>
                                 <tr>
                                    <td width="50%" align="center" valign="middle">
                                       <p>
                                       <?php
                                          if ($row['PicFilename'] != "") {
                                             if (file_exists(img($row['CompanyRefId']."/EmployeesPhoto/".$row['PicFilename']))) {
                                                echo '<img src="'.img($row['CompanyRefId']."/EmployeesPhoto/".$row['PicFilename']).'" style="width:150px;">';
                                             } else {
                                                echo '<img src="'.img("nopic.png").'" style="width:150px;">';
                                             }
                                          } else {
                                             echo '<img src="'.img("nopic.png").'" style="width:150px;">';
                                          }
                                       ?>
                                       </p>
                                       <p>
                                       <button
                                          class="btn-cls2-red"
                                          id="btnChangePW" name="btnChangePW">Change Password
                                       </button>
                                       </p>
                                    </td>
                                    <td width="50%">
                                       <?php
                                          $result = FindFirst('empinformation',"WHERE CompanyRefId = $CompanyId AND BranchRefId = $BranchId AND EmployeesRefId = ".$row["RefId"],"*");
                                          $info = array_merge($row,$result);
                                          $templ->doEmployeeInfo($info);
                                       ?>
                                    </td>
                                 </tr>
                              </table>   
                        <?php
                           }
                        ?>
                     </div>
                     <div class="col-xs-1"></div>
                     <div class="col-xs-4">
                        <div id="panelReminders">
                           <div class="row">
                              <div class="col-xs-12">
                                 <div class="panel-top" for="PIS_menu"id="PIS_Header">PERSONAL INFORMATION SYSTEM (PIS)</div>
                                 <div class="panel-mid" style="border-bottom: 1px solid black;" id="PIS_menu">
                                    <div class="row">
                                       <div class="col-xs-12">
                                          <button type="button"
                                               class="Menu btn-cls2-tree"
                                               pre="scrn"
                                               route="201File"
                                                  id="userPDS">
                                             <li><u>PERSONAL DATA SHEET (PDS)</u></li>
                                          </button>
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-12">
                                          <button type="button"
                                               class="Menu btn-cls2-tree"
                                               pre="scrn"
                                               route="EmpAttach" <?php echo $disabled; ?>>
                                             <li><u>201 FILE ATTACHMENTS</u></li>
                                          </button>
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-12">
                                          <button type="button"
                                               class="Menu btn-cls2-tree"
                                               pre="scrn"
                                               route="201Update" <?php echo $disabled; ?>>
                                             <li><u>201 FILE UPDATE</u></li>
                                          </button>
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-12">
                                          <button type="button"
                                               class="Menu btn-cls2-tree"
                                               pre="user"
                                               route="SERVICERECORD_1000" <?php echo $disabled; ?>>
                                             <li><u>SERVICE RECORD</u></li>
                                          </button>
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-12">
                                          <button type="button"
                                               class="Menu btn-cls2-tree"
                                               pre="user"
                                               route="Work_Exp_Attachment" <?php echo $disabled; ?>>
                                             <li><u>WORK EXPERIENCE ATTACHMENT</u></li>
                                          </button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-12">
                              <div class="panel-top" id="AMS_Header">ATTENDANCE MANAGEMENT SYSTEM (AMS)</div>
                                 <div class="panel-mid" style="border-bottom: 1px solid black;" id="AMS_menu">
                                    <div class="row">
                                       <div class="col-xs-12">
                                          <button type="button" 
                                               class="Menu btn-cls2-tree"
                                               pre="ams"
                                               route="TrnDTR"
                                               id="userDTR" <?php echo $disabled; ?>>
                                          <li><u>DAILY TIME RECORD (DTR)</u></li>
                                       </button>
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-12">
                                          <button type="button"
                                               class="Menu btn-cls2-tree"
                                               pre="ams"
                                               route="LeaveCard" <?php echo $disabled; ?>>
                                             <li><u>LEAVE CARD</u></li>
                                          </button>
                                       </div>
                                    </div>
                                    <?php bar();?>
                                    <!-- <div class="row margin-top">
                                       <div class="col-xs-12">
                                          <button type="button"
                                               class="Menu btn-cls2-tree"
                                               pre="ams"
                                               route="ReqChangeShift" <?php //echo $disabled; ?>>
                                             <li><u>REQUEST FOR CHANGE SHIFT</u></li>
                                          </button>
                                       </div>
                                    </div> -->
                                    <div class="row margin-top">
                                       <div class="col-xs-12">
                                          <button type="button"
                                               class="Menu btn-cls2-tree"
                                               pre="ams"
                                               route="ReqOvertime" <?php echo $disabled; ?>>
                                             <li><u>REQUEST FOR OVERTIME</u></li>
                                          </button>
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-12">
                                          <button type="button"
                                               class="Menu btn-cls2-tree"
                                               pre="ams"
                                               route="ReqForceLeave" <?php echo $disabled; ?>>
                                             <li><u>REQUEST FOR CANCELLATION OF LEAVE</u></li>
                                          </button>
                                       </div>
                                    </div>
                                    <?php bar();?>
                                    <div class="row margin-top">
                                       <div class="col-xs-12">
                                          <button type="button"
                                               class="Menu btn-cls2-tree"
                                               pre="user"
                                               route="AvailLeave" <?php echo $disabled; ?>>
                                             <li><u>AVAILMENT OF LEAVE</u></li>
                                          </button>
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-12">
                                          <button type="button"
                                               class="Menu btn-cls2-tree"
                                               pre="ams"
                                               route="AvailCTO" <?php echo $disabled; ?>>
                                             <li><u>AVAILMENT OF CTO</u></li>
                                          </button>
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-12">
                                          <button type="button"
                                               class="Menu btn-cls2-tree"
                                               pre="ams"
                                               route="AvailAuthority" <?php echo $disabled; ?>>
                                             <li><u>AVAILMENT OF OFFICE AUTHORITY</u></li>
                                          </button>
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-12">
                                          <button type="button"
                                               class="Menu btn-cls2-tree"
                                               pre="ams"
                                               route="AvailLeaveMonitization" <?php echo $disabled; ?>>
                                             <li><u>AVAILMENT OF LEAVE MONETIZATION</u></li>
                                          </button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-12">
                                 <div class="panel-top" id="PMS_Header">PAYROLL MANAGEMENT SYSTEM (PMS)</div>
                                 <div class="panel-mid" style="border-bottom: 1px solid black;" id="PMS_menu">
                                    <?php
                                       switch (getvalue("hCompanyID")) {
                                          case '14':
                                             echo '
                                                <div class="row margin-top">
                                                   <div class="col-xs-12">
                                                      <button type="button"
                                                           class="Menu btn-cls2-tree"
                                                           pre="user"
                                                           route="PAYSLIP_14"
                                                          >
                                                         <li><u>PAYSLIP</u></li>
                                                      </button>
                                                   </div>
                                                </div>
                                             ';
                                             break;
                                          case '2':
                                             echo '
                                                <div class="row margin-top">
                                                   <div class="col-xs-12">
                                                      <button type="button"
                                                           class="Menu btn-cls2-tree"
                                                           pre="user"
                                                           route="PAYSLIP_2"
                                                          '.$disabled.'>
                                                         <li><u>PAYSLIP</u></li>
                                                      </button>
                                                   </div>
                                                </div>
                                             ';
                                             break;
                                          case '28':
                                             echo '
                                                <div class="row margin-top">
                                                   <div class="col-xs-12">
                                                      <button type="button"
                                                           class="Menu btn-cls2-tree"
                                                           pre="user"
                                                           route="PAYSLIP_28"
                                                          >
                                                         <li><u>PAYSLIP</u></li>
                                                      </button>
                                                   </div>
                                                </div>
                                             ';
                                             break;
                                          case '21':
                                             echo '
                                                <div class="row margin-top">
                                                   <div class="col-xs-12">
                                                      <button type="button"
                                                           class="Menu btn-cls2-tree"
                                                           pre="user"
                                                           route="PAYSLIP_21"
                                                          >
                                                         <li><u>PAYSLIP</u></li>
                                                      </button>
                                                   </div>
                                                </div>
                                             ';
                                             break;
                                          default:
                                             echo '
                                                <div class="row margin-top">
                                                   <div class="col-xs-12">
                                                      <button type="button"
                                                           class="Menu btn-cls2-tree"
                                                           pre="user"
                                                           route="PAYSLIP_21"
                                                          >
                                                         <li><u>PAYSLIP</u></li>
                                                      </button>
                                                   </div>
                                                </div>
                                             ';
                                             break;
                                       }
                                    ?>
                                 </div>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-12">
                                 <div class="panel-top" id="myrequest">MY REQUEST(S)</div>
                                 <div class="panel-mid" id="myrequestView" style="border-bottom: 1px solid black;">
                                    <div class="row" style="margin:0px;">
                                       <div class="col-xs-12">
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-12">
                                 <?php
                                    $emp_row = FindFirst("employees","WHERE RefId = $EmpRefId","`BiometricsID`, `CompanyRefId`");
                                    if ($emp_row) {
                                       $CompanyRefId = $emp_row["CompanyRefId"];
                                       $BioID = $emp_row["BiometricsID"];
                                       switch ($CompanyRefId) {
                                          case '2':
                                             include 'inc/inc_emp_notif_2.e2e.php';
                                             break;
                                          case '14':
                                             include 'inc/inc_emp_notif_14.e2e.php';
                                             break;
                                       }
                                    }
                                 ?>
                              </div>
                           </div>
                        </div>
                        <div class="mypanel margin-top" id="panelChangePW" style="display:none">
                           <div class="panel-top">Change Password</div>
                           <div class="panel-mid">
                              <div class="row">
                                 <div class="col-xs-6" style="margin-left: 20px;">
                                    <div class="row">
                                       <div class="form-group">
                                          <label>Current Password:</label>
                                          <input type="password" name="currentToken" id="currentToken" class="form-input">
                                       </div>
                                    </div>
                                    <?php entryAlert("cuPW","Wrong Current Password !!!"); ?>
                                    <div class="row">
                                       <div class="form-group">
                                          <label>New Password:</label>
                                          <input type="password" name="newToken" id="newToken" class="form-input">
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="form-group">
                                          <label>Re-Type Password:</label>
                                          <input type="password" name="reToken" id="reToken" class="form-input">
                                       </div>
                                    </div>
                                    <?php entryAlert("rePW","Mismacthed of New Password !!!"); ?>
                                    <div class="row">
                                       <div class="form-group">
                                          <button
                                          class="btn-cls2-sea"
                                          id="btnChangeNow" name="btnChangeNow">Change Now
                                          </button>
                                          <button
                                          class="btn-cls2-red"
                                          id="btnChangeCancel" name="btnChangeCancel">Cancel
                                          </button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="panel-bottom"></div>
                        </div>
                     </div>   
                  </div>
                  
            <?php
               }
               footer();
               include "varHidden.e2e.php";
               doHidden("hRptFile","DashboardRpt","");
            ?>
         </div>
      </form>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_Dashboard"); ?>"></script>
   </body>
</html>


