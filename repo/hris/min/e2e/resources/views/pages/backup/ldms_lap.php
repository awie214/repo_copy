<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_IndividualDevtPlan"); ?>"></script>
      <script language="JavaScript">
         $(document).ready(function () {
            $(".newDataLibrary").hide();
            $("select").css("width","100%");
         });
         function selectMe(emprefid) {
            $("[name=\'hEmpRefId\']").val(emprefid);
            $.get("ldmsEmpDetail.e2e.php",
            {
               EmpRefId:emprefid,
               hCompanyID:$("#hCompanyID").val(),
               hBranchID:$("#hBranchID").val(),
               hEmpRefId:$("#hEmpRefId").val(),
               hUserRefId:$("#hUserRefId").val()
            },
            function(data,status) {
               if (status == "success") {
                  try {
                     eval(data);
                  } catch (e) {
                      if (e instanceof SyntaxError) {
                          alert(e.message);
                      }
                  }
               }
            });
         }
      </script>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"ldms"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar("LEARNING APPLICATION PLAN"); ?>
            <div class="container-fluid margin-top">  
               <div class="mypanel">
                  <div class="row">
                     <div class="col-sm-3">
                        <?php
                           employeeSelector();
                        ?>
                     </div>
                     <div class="col-xs-9" style="padding: 10px;">
                        <div class="row" style="padding-left: 5px;">
                           <button type="button" class="btn-cls4-sea" id="btnsaveDevPlan">
                              <i class="fa fa-floppy-o" aria-hidden="true"></i>
                              &nbsp;Save
                           </button>
                           <input type="hidden" name="fn" value="saveDevPlan">
                        </div>
                        <div class="panel-top margin-top">
                           LEARNING APPLICATION PLAN
                        </div>
                        <div class="panel-mid" style="padding: 15px;">
                           <div class="row">
                              <div class="col-xs-6">
                                 <div class="row" style="border: 1px solid black; padding: 10px;">
                                    <div class="col-xs-12">
                                       <div class="form-group">
                                          <div class="row">
                                             <div class="col-xs-12">
                                                <label>LEARNER</label>
                                                <input type="text" class="form-input" id="employeename" readonly>
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-6">
                                                <label>OFFICE</label>
                                                <?php
                                                   createSelect("Office",
                                                                "sint_OfficeRefId",
                                                                "",100,"Name","Select Office","");
                                                ?>
                                             </div>
                                             <div class="col-xs-6">
                                                <label>DATE CONDUCTED</label>
                                                <input type="text" class="form-input date--" name="date_DateConducted">
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-12">
                                                <label>Title of Interventions</label>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="col-xs-12">
                                                <input type="text" class="form-input">
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-12">
                                                <label>Specific Competency Targets to Develop/Enhance</label>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="col-xs-12">
                                                <input type="text" class="form-input">
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <?php spacer(8);?>
                                 <div class="row" style="padding: 10px;">
                                    <div class="col-xs-12">
                                       <div class="form-group">
                                          <div class="row margin-top">
                                             <div class="col-xs-12">
                                                <label>Learning Goals What skills, knowledge and attitude do I require to achieve competency target? (MUST be a SMART objective)</label>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="col-xs-12">
                                                <input type="text" class="form-input">
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-xs-6">
                                 <div class="row" style="padding: 10px;">
                                    <div class="col-xs-12">
                                       <div class="row margin-top">
                                          <label>Current Status What level of skills, knowledge and attitude do I have now with respect to this learning goal?</label>
                                          <input type="text" class="form-input">
                                       </div>
                                       <?php spacer(20); ?>
                                       <div class="row margin-top">
                                          <label>Learning Strategies How will I reach my learning goal?</label>
                                          <input type="text" class="form-input">
                                       </div>
                                       <?php spacer(20); ?>
                                       <div class="row margin-top">
                                          <label>Required Resources What resources do I need to achieve this learning goal?</label>
                                          <input type="text" class="form-input">
                                       </div>
                                       <?php spacer(20); ?>
                                       <div class="row margin-top">
                                          <label>Key Performance Indicators How can I demonstrate to myself and others that I have achieved this learning goal?</label>
                                          <input type="text" class="form-input">
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="panel-bottom">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <?php
               footer();
               doHidden("paramTitle",getvalue("paramTitle"),"");
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
</html>



