<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"pis"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar(getvalue("paramTitle")); ?>
            <div class="container-fluid margin-top">
               <div class="row">
                  <div class="col-xs-12">
                     <div id="spGridTable">
                        <?php
                              $sql = "SELECT * FROM `newsignup` ORDER BY RefId Desc LIMIT 500";
                              doGridTable("newsignup",
                                          ["Company Id", "Last Name", "First Name", "Department", "Division", "Status", "Approved By"],
                                          ["CompanyId", "LastName", "FirstName", "DepartmentRefId", "DivisionRefId", "Status", "ApprovedByRefId"],
                                          $sql,
                                          [false,false,false,true],
                                          "gridTable");
                        ?>
                     </div>
                  </div>
               </div>
            </div>
            <?php
               footer();
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
</html>



