<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script type="text/javascript">
         $(document).ready(function(){
            closeNav();
            $("#div_login").hide();
            //$("#div_login").show();
            $("#aLogin").click(function () {
               $("#div_login").show();
               $("#div_logo").hide();
            });
            $("#punchMSG").hide();
            $("#txtEmpIDNO").blur(function () {
               if ($(this).val() != "") {
                  $.post("chkPicEmployees.e2e.php",
                  {
                     val1:$("#txtEmpIDNO").val()
                  },
                  function(data,status) {
                     if (status == "success") {
                        try {
                           if (data.indexOf("Not Found") > 0) {
                              $("[name*='btnPunch']").prop("disabled",true);
                              $("#punchMSG").show();
                              $("#txtEmpIDNO").select();
                              $("#txtEmpIDNO").focus();
                           } else {
                              eval(data);
                              $("#punchMSG").hide();
                           }
                        } catch (e) {
                            if (e instanceof SyntaxError) {
                                alert(e.message);
                            }
                        }
                     }
                     else {
                        alert("Ooops Error : " + status + "[event_txtEmpIDNO]");
                     }
                  });
               }
            });
            $("#inputPW").focus(function () {
               $(this).val("");
               //$('#EmpPic').attr("src","../../../public/images/nopic.png");
               //$('#PunchedList').html("");
            });
            $("#txtEmpIDNO").focus(function () {
               $('#SuccessMSG').hide();
            });
         });
         sessionStorage.clear();
      </script>
      <style>
         .midBody {
            height: calc(100vh - 120px);
         }
         .sysNameHolder {
            height: 100px;
            text-align: center;
            align-items: center;
            margin:0;
         }
         .sysName {
            font-family:'Edwardian Script ITC';
            font-size:30pt;
            font-weight:600;
            display:inline-block;
            vertical-align:middle;
            line-height: 100px;
            margin:0;
         }
         .outer {
             display: table;
             position: absolute;
             height: calc(100% - 130px);
             width: 100%;
             background:transparent;
         }
         .middle {
             display: table-cell;
             vertical-align: middle;
             background:transparent;
         }
         .inner {
             margin-left: auto;
             margin-right: auto;
             height:100%;
             background:#ffffff;
             background:url("<?php echo img("PIDSLogoBig.png"); ?>") no-repeat center;
         }
      </style>
   </head>
   <body>
      <form name="formlogin" method="post">
         <div style="padding:0;">
            <?php $sys->SysHdr($sys,"pis"); ?>
            <div>
               <div class="container-fluid" id="mainScreen">
                  <?php doTitleBar("Employees Time Clock"); ?>
                  <div class="container-fluid margin-top">
                     <div class="row" style="height:100%;margin:0;padding:0;" id="">
                        <div class="col-xs-6 txt-center" style="margin:0;padding:15px;height:100%;">
                           <div>
                              <div style="height:250px;">
                                 <img id="EmpPic" src="<?php echo img("nopic.png"); ?>" style="margin:auto;border:1px solid black;height:100%;">
                              </div><br>
                              <div class="form-group txt-center">
                                 <div>
                                    <label class="control-label" for="txtEmpIDNO">
                                       <i class="fa fa-user-circle" aria-hidden="true"></i>&nbsp;Employees ID No.
                                    </label><br>
                                    <input class="form-input number--" type="text" id="txtEmpIDNO" name="txtEmpIDNO" value="" style="font-size:20pt;width:50%;">
                                    <br><div id="punchMSG" style="padding:5px;color:red;display:none;">Oooops!!! UNAVAILABLE EMPLOYEES ID NO.</div>
                                 </div>
                              </div>
                              <div class="form-group txt-center">
                                <label class="control-label" for="inputPW">Password</label><br>
                                <input class="form-input" type="password" id="inputPW" name="token" value="" style="text-align:center;font-size:20pt;width:50%;">
                              </div>
                              <div class="txt-center">
                                 <button type="button" class="btn-cls4-sea" onclick="TCNow(1);" name="btnPunch1" disabled>Time In</button>
                                 <button type="button" class="btn-cls4-red" onclick="TCNow(2);" name="btnPunch2" disabled>Lunch Out</button>
                                 <button type="button" class="btn-cls4-sea" onclick="TCNow(3);" name="btnPunch3" disabled>Lunch In</button>
                                 <button type="button" class="btn-cls4-red" onclick="TCNow(4);" name="btnPunch4" disabled>Time Out</button>
                                 <button type="button" class="btn-cls4-sea" onclick="TCNow(5);" name="btnPunch5" disabled>OT In</button>
                                 <button type="button" class="btn-cls4-red" onclick="TCNow(6);" name="btnPunch6" disabled>OT Out</button>
                              </div>
                              <div class="alert alert-success margin-top" id="SuccessMSG" style="display:none;">
                                <span style="background:font-size:16pt;font-weight:900;" id="SuccessMSG2"></span>
                              </div>
                           </div>
                        </div>
                        <div class="col-xs-6 txt-center" style="margin:0;padding:15px;height:100%;">
                           <div style="height:200px;margin:0;padding:0;">
                              <div id="PunchedList" style="overflow:auto;height:200px;">-- clear --</div>
                           </div>
                           <!--<div style="height:120px;margin-top:50px;padding:0;">
                              <?php
                                 $t = time();
                                 $date_today = date("Y-m-d",$t);
                                 $criteria = "where AttendanceDate = '$date_today' ORDER BY AttendanceTime DESC LIMIT 1";
                                 $rsLastPunch = f_Find("employeesattendance",$criteria);
                                 $empPicLastPunch = "nopic.png";
                                 $empLastPunch = "&nbsp;";
                                 if ($rsLastPunch) {
                                    $rowLastPunch = $rsLastPunch->fetch_assoc();
                                    $empPicLastPunch = "EmployeesPhoto/".FindFirst("employees","where RefId = ".$rowLastPunch["EmployeesRefId"],"PicFilename");
                                    $empLastPunch = getLabel_AttendanceEntry($rowLastPunch["KindOfEntry"])." - ".date("H:i:s",$rowLastPunch["AttendanceTime"]);
                                 }
                              ?>
                              <img src="<?php echo img($empPicLastPunch); ?>" id="prevPic" style="border:1px solid black;height:100%;">
                              <div class="txt-center" style="margin-top:10px;" id="LastPunch"><?php echo $empLastPunch; ?></div>
                           </div>-->
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <?php
            footer();
            include "varHidden.e2e.php";
         ?>
      </form>
   </body>
</html>



