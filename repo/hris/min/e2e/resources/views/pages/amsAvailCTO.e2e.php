\<?php $module = module("AvailmentCTO"); ?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript">
         $(document).ready(function () {
            $("#btnPRINT").click(function () {
               popPDS("CTO_2","");
            });
            <?php
               if (isset($_GET["errmsg"])) {
                  echo '$.notify("'.$_GET["errmsg"].'");';
               }
            ?>
         });
         function afterDelete() {
            alert("Successfully Deleted");
            gotoscrn($("#hProg").val(),"");
         }
         function selectMe(refid){
            $("#rptContent").attr("src","blank.htm");
            var cid = $("#hCompanyID").val();
            var rptFile = "rpt_CTO_" + cid;
            //rptFile  = "rpt_CTO_2";
            var url = "ReportCaller.e2e.php?file=" + rptFile;
            url += "&refid=" + refid;
            url += "&" + $("[name='hgParam']").val();
            $("#prnModal").modal();
            $("#rptContent").attr("src",url);
         }
      </script>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="postMultiple.e2e.php">
         <?php $sys->SysHdr($sys,"ams"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar("COMPENSATORY TIME OFF APPLICATION"); ?>
            <div class="container-fluid margin-top">
               <div class="row">
                  <div class="col-sm-12" id="div_CONTENT">
                        <div id="divList">
                           <div class="mypanel">
                              <?php
                                 if (getvalue("errmsg") != "") {
                                    msg("System Info",getvalue("errmsg"));
                                 }
                                 //$_SESSION['ERR_MSG'] = $err;
                              ?>
                              <div class="panel-top">List of Compensatory Time Off Application</div>
                              <div class="panel-mid">
                                 <span id="spGridTable">
                                    <?php
                                          $sizeCol = "col-sm-6";
                                          if ($UserCode == "COMPEMP") {
                                             $sizeCol = "col-sm-12";
                                             $gridTableHdr_arr = ["File Date", "Hours", "App. Date From", "App. Date To", "Status"];
                                             $gridTableFld_arr = ["FiledDate", "Hours", "ApplicationDateFrom", "ApplicationDateTo", "Status"];
                                             $sql = "SELECT * FROM $table WHERE CompanyRefId = $CompanyId AND BranchRefId = $BranchId AND EmployeesRefId = $EmployeesRefId ORDER BY FiledDate";
                                             $Action = [false,true,false,true];
                                          }
                                          doGridTable($table,
                                                      $gridTableHdr_arr,
                                                      $gridTableFld_arr,
                                                      $sql,
                                                      $Action,
                                                      "gridTable");
                                    ?>
                                 </span>
                              </div>
                              <div class="panel-bottom">
                                 <?php
                                       btnINRECLO([true,true,false]);
                                 ?>
                                 <!-- <button type="button"
                                      class="btn-cls4-lemon trnbtn"
                                      id="btnPRINT" name="btnPRINT">
                                    <i class="fa fa-print" aria-hidden="true"></i>&nbsp;
                                    PRINT
                                 </button> -->
                              </div>
                           </div>
                        </div>
                        <div id="divView">
                           <div class="row">
                              <div class="col-sm-6">
                                 <div class="mypanel">
                                    <div class="panel-top">
                                       <span id="ScreenMode">AVAILING</span> <?php echo strtoupper($ScreenName); ?>
                                    </div>
                                    <div class="panel-mid">
                                       <div id="EntryScrn">
                                          <div class="row margin-top">
                                             <div class="<?php echo $sizeCol; ?>">
                                                   <div class="row" id="badgeRefId">
                                                      <div class="col-sm-6">
                                                         <ul class="nav nav-pills">
                                                            <li class="active" style="font-size:12pt;font-weight:600;">
                                                               <a>REFID : <span class="badge" style="font-size:12pt;font-weight:600;" id="idRefid">
                                                               </span></a>
                                                            </li>
                                                         </ul>
                                                      </div>
                                                   </div>
                                                   <?php
                                                      $attr = ["br"=>true,
                                                               "type"=>"text",
                                                               "row"=>true,
                                                               "name"=>"date_FiledDate",
                                                               "col"=>"4",
                                                               "id"=>"FiledDate",
                                                               "label"=>"Date File",
                                                               "class"=>"saveFields-- mandatory date--",
                                                               "style"=>"",
                                                               "other"=>""];
                                                      $form->eform($attr);
                                                      echo
                                                      '<div class="row margin-top">
                                                         <div class="col-sm-12">
                                                            <label>Application Date</label><br>
                                                            <div class="row">';
                                                               $attr = ["br"=>true,
                                                                        "type"=>"text",
                                                                        "row"=>false,
                                                                        "name"=>"date_ApplicationDateFrom",
                                                                        "col"=>"6",
                                                                        "id"=>"ApplicationDateFrom",
                                                                        "label"=>"From",
                                                                        "class"=>"saveFields-- mandatory date--",
                                                                        "style"=>"",
                                                                        "other"=>""];
                                                               $form->eform($attr);
                                                               $attr = ["br"=>true,
                                                                        "type"=>"text",
                                                                        "row"=>false,
                                                                        "name"=>"date_ApplicationDateTo",
                                                                        "col"=>"6",
                                                                        "id"=>"ApplicationDateTo",
                                                                        "label"=>"To",
                                                                        "class"=>"saveFields-- mandatory date--",
                                                                        "style"=>"",
                                                                        "other"=>""];
                                                               $form->eform($attr);
                                                      echo
                                                      '
                                                            </div>
                                                         </div>
                                                      </div>';
                                                   ?>
                                                   <div class="row">
                                                      <div class="col-sm-6">
                                                         <div>
                                                            <label>Hours:</label>
                                                            <select class="form-input number-- saveFields--" name="deci_Hours">
                                                               <option value="">Select Hours</option>
                                                               <?php
                                                                  if (getvalue("hCompanyID") == "1000") {
                                                                     echo '
                                                                        <option value="4">4 hrs</option>
                                                                        <option value="5">5 hrs</option>
                                                                        <option value="8">8 hrs</option>
                                                                        <option value="10">10 hrs</option>
                                                                     ';
                                                                  } else {
                                                                     echo '
                                                                        <option value="4">4 hrs</option>
                                                                        <option value="8">8 hrs</option>
                                                                     ';
                                                                  }
                                                               ?>
                                                               
                                                               
                                                            </select>
                                                         </div>
                                                      </div>
                                                      <!--
                                                      <div class="col-sm-6">
                                                         <div>
                                                            <label>Balance:</label>
                                                            <input type="text" class="form-input number-- saveFields--" name="deci_Balance">
                                                         </div>
                                                      </div>
                                                      -->
                                                   </div>
                                                   <div class="row margin-top">
                                                      <div class="col-sm-12">
                                                         <div class="form-group">
                                                            <label class="control-label" for="inputs">Remarks:</label>
                                                            <textarea class="form-input saveFields--" rows="5" name="char_Remarks" placeholder="remarks"></textarea>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <?php
                                                      if ($UserCode == "COMPEMP") {
                                                   ?>
                                                   <div class="row">
                                                      <div class="col-xs-12">
                                                         <label class="control-label" for="inputs">Recommended By:</label>
                                                         <?php
                                                            createSelect("signatories",
                                                                         "sint_Signatory1",
                                                                         "",100,"Name","Select Signatory","",false);
                                                         ?>
                                                      </div>
                                                   </div>
                                                   <div class="row">
                                                      <div class="col-xs-12">
                                                         <label class="control-label" for="inputs">Approved By:</label>
                                                         <?php
                                                            createSelect("signatories",
                                                                         "sint_Signatory2",
                                                                         "",100,"Name","Select Signatory","",false);
                                                         ?>
                                                      </div>
                                                   </div>
                                                   <?php
                                                      }
                                                   ?>
                                             </div>
                                             <div class="col-sm-6 padd5 adminUse--">
                                                <label>Employees Selected</label>
                                                <select multiple id="EmpSelected" name="EmpSelected" class="form-input" style="height:250px;">
                                                </select>
                                                <p>Double Click the items to remove in the List</p>
                                                <input type="hidden" class="saveFields--" name="hEmpSelected">
                                             </div>
                                          </div>
                                       </div>
                                       <?php
                                          spacer(10);
                                          echo
                                          '<button type="submit" class="btn-cls4-sea"
                                                   name="btnLocSAVE" id="LocSAVE">
                                             <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                             &nbsp;Save
                                          </button>';
                                          btnSACABA([false,true,true]);
                                       ?>
                                    </div>
                                    <div class="panel-bottom"></div>
                                 </div>
                              </div>
                              <div class="col-sm-6 adminUse--">
                                 <div class="mypanel">
                                    <div class="panel-top">Employees List</div>
                                    <div class="panel-mid">
                                       <?php include_once "incEmpListSearchCriteria.e2e.php" ?>
                                       <!-- <a href="javascript:void(0);" title="Search Employees">
                                          <i class="fa fa-search" aria-hidden="true"></i>
                                       </a>-->
                                       <div id="empList" style="max-height:300px;overflow:auto;">
                                          <h4>No Employees Selected</h4>
                                       </div>
                                    </div>
                                    <div class="panel-bottom">
                                       <a href="javascript:void(0);" id="clearEmpSelected">Clear Employees Selected</a>&nbsp;
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                  </div>
               </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="prnModal" role="dialog">
               <div class="modal-dialog" style="height:90%;width:80%">
                  <div class="mypanel" style="height:100%;">
                     <div class="panel-top bgSilver">
                        <a href="#" data-toggle="tooltip" data-placement="top" id="btnPRINTNOW">
                           <i class="fa fa-print" aria-hidden="true"></i>
                        </a>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                  </div>
               </div>
            </div>
            <?php
               footer();
               include "varJSON.e2e.php";
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
</html>