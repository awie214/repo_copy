<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_approval") ?>"></script>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"ams"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php
               doTitleBar("For Aprroval > CTO Availment");
               spacer(5);

            ?>
            <div class="row">
               <div class="col-xs-12">
                  <?php
                     $EmpRefId = getvalue("txtRefId");
                     $attr = ["empRefId"=>getvalue("txtRefId"),
                              "empLName"=>getvalue("txtLName"),
                              "empFName"=>getvalue("txtFName"),
                              "empMName"=>getvalue("txtMidName")];
                     $EmpRefId = EmployeesSearch($attr);
                     bar();
                     /*$sql = "SELECT *,t2.RefId as asRefId FROM employees t1 INNER JOIN employeescto t2
                             ON t1.CompanyRefId = t2.CompanyRefId
                             AND t1.BranchRefId = t2.BranchRefId";
                     if (getvalue("txtLName") != "") {
                        $sql .= " AND t1.LastName LIKE '".getvalue("txtLName")."%'";
                     }
                     if (getvalue("txtFName") != "") {
                        $sql .= " AND t1.FirstName LIKE '".getvalue("txtFName")."%'";
                     }
                     if (getvalue("txtMidName") != "") {
                        $sql .= " AND t1.MiddleName LIKE '".getvalue("txtMidName")."%'";
                     }
                     if (getvalue("txtRefId") != "") {
                        $sql .= " AND t2.EmployeesRefId = '".getvalue("txtRefId")."'";
                     } else {
                        $sql .= " AND t1.RefId = t2.EmployeesRefId";
                     }*/


                     $sql = "SELECT *,employeescto.RefId as asRefId FROM employeescto
                     INNER JOIN employees 
                     ON employeescto.CompanyRefId = employees.CompanyRefId
                     AND employeescto.BranchRefId = employees.BranchRefId
                     AND employeescto.EmployeesRefId = employees.RefId";

                     if (getvalue("txtLName") != "") {
                        $sql .= " AND employees.LastName LIKE '".getvalue("txtLName")."%'";
                     }
                     if (getvalue("txtFName") != "") {
                        $sql .= " AND employees.FirstName LIKE '".getvalue("txtFName")."%'";
                     }
                     if (getvalue("txtMidName") != "") {
                        $sql .= " AND employees.MiddleName LIKE '".getvalue("txtMidName")."%'";
                     }
                     if (getvalue("txtRefId") != "") {
                        $sql .= " AND employeescto.EmployeesRefId = '".getvalue("txtRefId")."'";
                     } 
                     $sql .= " AND employeescto.Status IS NULL ORDER BY ApplicationDateFrom LIMIT 100";
                     //$sql .= " AND t2.Status IS NULL ORDER BY ApplicationDateFrom LIMIT 20";
                     $rs = mysqli_query($conn,$sql) or die(mysqli_error($conn));
                  ?>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-1"></div>
               <div class="col-xs-10 padd5">
                  <?php
                     if ($rs) {
                        while ($row = mysqli_fetch_array($rs)) {
                           $refid = $row["asRefId"];
                           $rsEmp = FFirstRefId("employees",$row["EmployeesRefId"],"*");
                           $where = "WHERE EmployeesRefId = ".$rsEmp["RefId"];
                           $empinformation = FindFirst('empinformation',$where,"*");
                           if ($empinformation) {
                              $info = array_merge($rsEmp,$empinformation);

                  ?>
                           <div class="mypanel pull-left padd5" style="margin:5px;width:40%;" id="card_<?php echo $refid; ?>">
                              <div class="panel-top">REF ID:&nbsp;<?php echo $refid; ?></div>
                              <div class="panel-mid">
                                 <div class="row txt-right" style="margin-right:10px;">
                                    <label>DATE FILE:</label><span><?php echo $row["FiledDate"];?></span>
                                 </div>
                                 <?php
                                    
                                    echo '
                                    <div class="row margin-top padd5">
                                       <div class="row margin-top">
                                          <div class="col-sm-4 txt-center">
                                             <div class="border" style="height:1.5in;width:1.3in;">
                                                <img src="'.img($rsEmp['CompanyRefId']."/EmployeesPhoto/".$rsEmp['PicFilename']).'" style="width:100%;height:100%;">
                                             </div>
                                          </div>
                                          <div class="col-sm-8 txt-center padd5">';
                                             $templ->btn_apprvReject(2,$refid);
                                          echo    
                                          '</div>
                                       </div>
                                       <div class="row margin-top">   
                                          <div class="col-sm-12">';
                                             $templ->doEmployeeInfo($info);
                                    echo       
                                          '</div>   
                                       </div>   
                                    </div>';
                                    bar();  
                                 ?>
                                 <div class="row">
                                    <div class="col-xs-6">
                                       <label>START DATE:</label><span style="margin-left:15px;"><?php echo $row["ApplicationDateFrom"];?></span>
                                    </div>
                                    <div class="col-xs-6">
                                       <label>END DATE:</label><span style="margin-left:15px;"><?php echo $row["ApplicationDateTo"];?></span>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-6">
                                       <label>HOURS:</label><a href="#" class="workSched" style="margin-left:15px;"><?php echo $row["Hours"];?></a>
                                    </div>
                                 </div>
                              </div>
                              <div class="panel-bottom"></div>
                           </div>
                        <?php
                           }
                        }
                     } else {
                        alert("Information","No For Approval");
                     }
                  ?>
               </div>
               <div class="col-xs-1"></div>
            </div>
            <?php
               footer();
               $table = "employeescto";
               include "varHidden.e2e.php";
               modalReject();
            ?>
         </div>
      </form>

   </body>
</html>