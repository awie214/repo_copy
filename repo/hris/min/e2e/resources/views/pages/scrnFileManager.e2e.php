<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script src="<?php echo jsCtrl("ctrl_FileManager"); ?>"></script>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"pis"); ?>
         <div class="container-fluid" id="mainScreen">
         <?php doTitleBar($modTitle); ?>
         <div class="container-fluid margin-top10">
            <div class="row">
               <div class="col-xs-12" id="div_CONTENT">
                  <div class="mypanel">
                     <div class="panel-top">Data Library</div>
                     <div class="panel-mid-litebg">
                        <div class="row">
                           <div class="col-xs-3" style="border-right:1px solid var(--border);">
                              <h5 class="strong txt-center" id="titleAdmSetUp">
                                 ADMINISTRATION&nbsp;
                                 <i class="fa fa-sort-desc" aria-hidden="true"></i>
                              </h5>
                              <div class="text-left" style="margin-top:10px;" id="AdmSetUp">
                                 <div class="InfoMenu" id="mSysGroup"><img src="<?php echo img("sysgroup.png"); ?>">&nbsp;System Group</div>
                                 <div class="InfoMenu" id="mSysModules"><img src="<?php echo img("sysmodule.png"); ?>">&nbsp;System Modules</div>
                                 <div class="InfoMenu" id="mSysUsers"><img src="<?php echo img("sysuser.png"); ?>">&nbsp;System Users</div>
                                 <div class="InfoMenu" id="mDocType"><img src="<?php echo img("doctype.png"); ?>">&nbsp;Document Type</div>
                                 <div class="InfoMenu" id="mReports"><img src="<?php echo img("analytics.png"); ?>">&nbsp;Reports</div>
                                 <div class="InfoMenu" id="mForms"><img src="<?php echo img("forms.png"); ?>">
                                    &nbsp;Forms
                                 </div>
                                 <div class="InfoMenu" id="mSystem"><img src="<?php echo img("android.png"); ?>">
                                    &nbsp;Systems
                                 </div>
                                 <div class="InfoMenu" id="mModules"><img src="<?php echo img("lure-module.png"); ?>">
                                    &nbsp;Modules
                                 </div>
                              </div>
                           </div>
                           <div class="col-xs-3" style="border-right:1px solid var(--border);">
                              <h5 class="strong txt-center" id="titleCompSetUp">
                                 COMPANY FILE SETUP&nbsp;
                                 <i class="fa fa-sort-desc" aria-hidden="true" style="font-size:12pt"></i>
                              </h5>
                              <div class="text-left" style="margin-top:10px;" id="CompSetUp">
                                 <div class="InfoMenu" id="mAgency"><img src="<?php echo img("agency.png"); ?>">&nbsp;Agency</div>
                                 <!-- <div class="InfoMenu" id="mBranch"><img src="<?php echo img("branch.png"); ?>">&nbsp;Branch</div> -->
                                 <div class="InfoMenu" id="mDept"><img src="<?php echo img("department.png"); ?>">&nbsp;Department</div>
                                 <?php
                                    if (getvalue("hCompanyID") == "35") {
                                 ?>
                                 <div class="InfoMenu" id="mOffice"><img src="<?php echo img("office.png"); ?>">&nbsp;Section</div>
                                 <?php
                                    } else {
                                 ?>
                                 <div class="InfoMenu" id="mOffice"><img src="<?php echo img("office.png"); ?>">&nbsp;Office</div>
                                 <?php
                                    }
                                 ?>
                                 <div class="InfoMenu" id="mDesign"><img src="<?php echo img("designation.png"); ?>">&nbsp;Designation</div>
                                 <div class="InfoMenu" id="mDiv"><img src="<?php echo img("division.png"); ?>">&nbsp;Division</div>
                                 <div class="InfoMenu" id="mLoc"><img src="<?php echo img("location.png"); ?>">&nbsp;Location</div>
                                 <div class="InfoMenu" id="mPosClass"><img src="<?php echo img("posclass.png"); ?>">&nbsp;Position Classification</div>
                                 <div class="InfoMenu" id="mPosLev"><img src="<?php echo img("poslevel.png"); ?>">&nbsp;Position Level</div>
                                 <div class="InfoMenu" id="mPosName"><img src="<?php echo img("posname.png"); ?>">&nbsp;Position Name</div>
                                 <div class="InfoMenu" id="mPosItem"><img src="<?php echo img("positem.png"); ?>">&nbsp;Plantilla Item</div>
                                 <div class="InfoMenu" id="mSection"><img src="<?php echo img("section.png"); ?>">&nbsp;Branch</div>
                                 <div class="InfoMenu" id="mUnit"><img src="<?php echo img("unit.png"); ?>">&nbsp;Unit</div>
                                 <div class="InfoMenu" id="mPayRate"><img src="<?php echo img("payrate.png"); ?>">&nbsp;Pay Rate</div>
                                 <div class="InfoMenu" id="mPayPeriod"><img src="<?php echo img("doctype.png"); ?>">&nbsp;Pay Period</div>
                                 <div class="InfoMenu" id="mCity"><img src="<?php echo img("city.png"); ?>">&nbsp;City</div>
                                 <div class="InfoMenu" id="mProvince"><img src="<?php echo img("province.png"); ?>">&nbsp;Province</div>
                                 <div class="InfoMenu" id="mRequirements"><img src="<?php echo img("requirements.png"); ?>">&nbsp;Requirements</div>
                                 <div class="InfoMenu" id="mSignatories"><img src="<?php echo img("signatories.png"); ?>">&nbsp;Signatories</div>
                                 <div class="InfoMenu" id="mResponsibility"><img src="<?php echo img("signatories.png"); ?>">&nbsp;Responsibility Center</div>
                                 <div class="InfoMenu" id="mProject"><img src="<?php echo img("signatories.png"); ?>">&nbsp;Projects</div>
                                 <?php spacer(50); ?>
                              </div>
                           </div>
                           <div class="col-xs-3">
                              <h5 class="strong txt-center" id="titleEmpSetUp">
                                 EMPLOYEE FILE SETUP&nbsp;
                                 <i class="fa fa-sort-desc" aria-hidden="true" style="font-size:12pt"></i>
                              </h5>
                              <div class="text-left" style="margin-top:10px;" id="EmpSetUp">
                                 <div class="InfoMenu" id="mEmpStatus"><img src="<?php echo img("empstatus.png"); ?>">&nbsp;Employment Status</div>
                                 <div class="InfoMenu" id="mAppnt"><img src="<?php echo img("appstatus.png"); ?>">&nbsp;Appointment Status</div>
                                 <div class="InfoMenu" id="mBlood"><img src="<?php echo img("blood.png"); ?>">&nbsp;Blood Type</div>
                                 <div class="InfoMenu" id="mCitizen"><img src="<?php echo img("citizenship.png"); ?>">&nbsp;Citizenship</div>
                                 <div class="InfoMenu" id="mOrg"><img src="<?php echo img("organization.png"); ?>">&nbsp;Organization</div>
                                 <?php bar(); ?>
                                 <div class="InfoMenu" id="mCourseCat"><img src="<?php echo img("coursecat.png"); ?>">&nbsp;Course Category</div>
                                 <div class="InfoMenu" id="mCourse"><img src="<?php echo img("course.png"); ?>">&nbsp;Courses</div>
                                 <div class="InfoMenu" id="mSchool"><img src="<?php echo img("schools.png"); ?>">&nbsp;Schools</div>
                                 <div class="InfoMenu" id="mCareer"><img src="<?php echo img("career.png"); ?>">&nbsp;Career Service</div>
                                 <div class="InfoMenu" id="mAward"><img src="<?php echo img("awards.png"); ?>">&nbsp;Awards</div>
                                 <div class="InfoMenu" id="mCountry"><img src="<?php echo img("country.png"); ?>">&nbsp;Country</div>
                                 <?php bar(); ?>
                                 <div class="InfoMenu" id="mSemClass"><img src="<?php echo img("semclass.png"); ?>">&nbsp;Seminar Classification</div>
                                 <div class="InfoMenu" id="mSemTraining"><img src="<?php echo img("semtraining.png"); ?>">&nbsp;Seminar/Training</div>
                                 <div class="InfoMenu" id="mSemPlace"><img src="<?php echo img("semplace.png"); ?>">&nbsp;Seminar Place</div>
                                 <div class="InfoMenu" id="mSemSponsor"><img src="<?php echo img("semsponsor.png"); ?>">&nbsp;Seminar Sponsor</div>
                                 <div class="InfoMenu" id="mSemType"><img src="<?php echo img("semtype.png"); ?>">&nbsp;Seminar Type</div>
                                 <?php bar(); ?>
                                 <div class="InfoMenu" id="mSalGrade"><img src="<?php echo img("salgrade.png"); ?>">&nbsp;Salary Grade</div>
                                 <div class="InfoMenu" id="mJobGrade"><img src="<?php echo img("office.png"); ?>">&nbsp;Job Grade</div>
                                 <div class="InfoMenu" id="mStep"><img src="<?php echo img("stepincrement.png"); ?>">&nbsp;Step Increment</div>
                                 <div class="InfoMenu" id="mNature"><img src="<?php echo img("casenature.png"); ?>">&nbsp;Nature of Case</div>
                                 <div class="InfoMenu" id="mOccupations"><img src="<?php echo img("occupation.png"); ?>">&nbsp;Occupations</div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="panel-bottom"></div>
                  </div>
               </div>
            </div>
         </div>
         <?php
            modalFileManager();
            footer();
            doHidden("fmMenu",getvalue("fmMenu"),"");
            include "varHidden.e2e.php";
         ?>
         </div>
      </form>
   </body>
</html>



