<?php
   function getAmount($table,$where) {
      $result = pms_FindFirst($table,$where,"amount");
      if ($result) {
         return $result;
      } else {
         return 0;
      }
   }
   if (isset($_POST["month_"])) {
      $m = $_POST["month_"];
   } else {
      $m = intval(date("m",time()));
   }
   if (isset($_POST["year_"])) {
      $y = $_POST["year_"];
   } else {
      $y = date("Y",time());
   }
   $month_name = monthName($m,1);
   $pera_id    = 0;
   $rata1_id   = 0;
   $rata2_id   = 0;
   $sa_id      = 0;
   $la_id      = 0;
   $hazard_id  = 0;
   $emprefid   = getvalue("hEmpRefId");
   $AgencyId   = FindFirst("employees","WHERE RefId = '$emprefid'","AgencyId");
   $emp_id     = pms_FindFirst("pms_employees","WHERE employee_number = '$AgencyId'","id");
   $row_emp    = FindFirst("employees","WHERE RefId = '$emprefid'","`LastName`,`FirstName`,`MiddleName`,`ExtName`");
   if ($row_emp) {
      $FullName = $row_emp["LastName"]." ".$row_emp["FirstName"]." ".substr($row_emp["MiddleName"], 0, 1);
   } else {
      $FullName = "";
   }
   $emp_info   = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","`PositionRefId`,`DivisionRefId`");
   if ($emp_info) {
      $Position = $emp_info["PositionRefId"];
      $Position = getRecord("Position",$Position,"Name");
      $Division = $emp_info["DivisionRefId"];
      $Division = getRecord("Division",$Division,"Name");
   } else {
      $Position = $Division = "";
   }
   $benefits_rs   = pms_SelectEach("pms_benefits","");
   if ($benefits_rs) {
      while ($benefit_row = mysqli_fetch_assoc($benefits_rs)) {
         $code = $benefit_row["code"];
         switch ($code) {
            case 'PERA':
               $pera_id    = $benefit_row["id"];
               break;
            case 'RATA1':
               $rata1_id   = $benefit_row["id"];
               break;
            case 'RATA2':
               $rata2_id   = $benefit_row["id"];
               break;
            case 'HP':
               $hazard_id  = $benefit_row["id"];
               break;
            case 'SA':
               $sa_id      = $benefit_row["id"]; 
               break;
            case 'LA':
               $la_id      = $benefit_row["id"];
               break;
         }
      }
   }
   

   $trn_where     = "WHERE employee_id = '$emp_id' AND year = '$y' AND month = '$m'";
   $trn_row       = pms_FindFirst("pms_transactions",$trn_where,"*");

   $pera_where    = $trn_where." AND benefit_id = '$pera_id'";
   $rata1_where   = $trn_where." AND benefit_id = '$rata1_id'";
   $rata2_where   = $trn_where." AND benefit_id = '$rata2_id'";
   $sa_where      = $trn_where." AND benefit_id = '$sa_id'";
   $la_where      = $trn_where." AND benefit_id = '$la_id'";
   $hazard_where  = $trn_where." AND benefit_id = '$hazard_id'";

   
   if ($trn_row) {
      $basic      = $trn_row["total_basicpay_amount"];
      $gsis       = $trn_row["gsis_ee_share"];
      $tax_amount = $trn_row["tax_amount"];
      $deduct     = $trn_row["total_loan"];

      $pera       = getAmount("pms_benefitsinfo_transactions",$pera_where);
      $rata1      = getAmount("pms_benefitsinfo_transactions",$rata1_where);
      $rata2      = getAmount("pms_benefitsinfo_transactions",$rata2_where);
      $hazard     = getAmount("pms_benefitsinfo_transactions",$hazard_where);
      $la         = getAmount("pms_benefitsinfo_transactions",$la_where);
      $sa_row     = pms_FindFirst("pms_benefitsinfo_transactions",$la_where,"*");
      if ($sa_row) {
         $sa_amount     = $sa_row["amount"];
         $sa_UT_amount  = $sa_row["sala_undertime_amount"];
         $sa_Ab_amount  = $sa_row["sala_absent_amount"];
      } else {
         $sa_amount     = 0;
         $sa_UT_amount  = 0;
         $sa_Ab_amount  = 0;
      }

      $payroll_info = pms_FindFirst("pms_payroll_information","WHERE employee_id = '$emp_id'","*");
      if ($payroll_info) {
         $pagibig = $payroll_info["pagibig_contribution"];
         $phic    = $payroll_info["philhealth_contribution"];
      } else {
         $pagibig = 0;
         $phic    = 0;
      }
   } else {
      $deduct        = 0;
      $pagibig       = 0;
      $phic          = 0;
      $tax_amount    = 0;
      $gsis          = 0;
      $basic         = 0;
      $pera          = 0;
      $rata1         = 0;
      $rata2         = 0;
      $hazard        = 0;
      $la            = 0;
      $sa_row        = 0;
      $sa_amount     = 0;
      $sa_UT_amount  = 0;
      $sa_Ab_amount  = 0;
   }
   $rata          = floatval($rata1) + floatval($rata2); 
   $gross         = $basic + $pera + $rata + $hazard + $sa_amount + $la;
   $total_deduct  = $deduct + $gsis + $phic + $pagibig + $sa_UT_amount + $sa_Ab_amount + $tax_amount;
   $net_income    = $gross - $total_deduct;

?>
<!DOCTYPE>
<html>
<head>
	<title></title>
	<?php include_once $files["inc"]["pageHEAD"]; ?>
	<script type="text/javascript">
      $(document).ready(function () {
         $("#btnPrint").click(function () {
            var head = $("head").html();
            printDiv('div_CONTENT',head);
         });
         <?php
            if (isset($_POST["month_"])) {
               $m = $_POST["month_"];
            } else {
               $m = intval(date("m",time()));
            }

            if (isset($_POST["year_"])) {
               $y = $_POST["year_"];
            } else {
               $y = date("Y",time());
            }
            echo '$("#month_").val("'.$m.'");';        
            echo '$("#year_").val("'.$y.'");';        
         ?>
      });
   </script>
   <style type="text/css">
      @media print {
         body {
            font-size: 8pt;
         }
      }
   </style>
</head>
<body onload = "indicateActiveModules();">
   <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
      <?php $sys->SysHdr($sys,"pis"); ?>
      <div class="container-fluid" id="mainScreen">
         <?php doTitleBar ("PAYROLL"); ?>
         <div class="container-fluid margin-top">
            <br>
            <div class="row">
               <div class="col-xs-12">
                  <label>Payroll Period:</label>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-2">
                  <select class="form-input" id="month_" name="month_">
                     <option value="0">Select Month</option>
                     <option value="1">January</option>
                     <option value="2">February</option>
                     <option value="3">March</option>
                     <option value="4">April</option>
                     <option value="5">May</option>
                     <option value="6">June</option>
                     <option value="7">July</option>
                     <option value="8">August</option>
                     <option value="9">September</option>
                     <option value="10">October</option>
                     <option value="11">November</option>
                     <option value="12">December</option>
                  </select>
               </div>
               <div class="col-xs-2">
                  <select class="form-input" id="year_" name="year_">
                     <option value="0">Select Year</option>
                     <?php
                        $start   = date("Y",time()) - 1;
                        $end     = date("Y",time());
                        for ($a=$start; $a <= $end ; $a++) { 
                           echo '<option value="'.$a.'">'.$a.'</option>';
                        }
                     ?>
                  </select>
               </div>
               <div class="col-xs-3">
                  <button type="submit" id="" class="btn-cls4-sea">SUBMIT</button>      
               </div>
               <div class="col-xs-5 text-right">
                  <button type="button" id="btnPrint" class="btn-cls4-lemon">
                     <i class="fa fa-print"></i>&nbsp;
                     PRINT
                  </button>      
               </div>
            </div>
            <br><br>
            <div class="row">
               <div class="col-xs-12" id="div_CONTENT">
                  <div class="container-fluid rptBody">
                     <div class="row">
                     	<div class="col-xs-12">
                     		<?php rptHeader(""); ?>
                     	</div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-2">
                           <b>Employee Name:</b>
                        </div>
                        <div class="col-xs-4">
                           <?php echo strtoupper($FullName); ?>
                        </div>
                        <div class="col-xs-2">
                           <b>Employee No:</b>
                        </div>
                        <div class="col-xs-4">
                           <?php echo $AgencyId; ?>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-2">
                           <b>Position:</b>
                        </div>
                        <div class="col-xs-4">
                           <?php echo strtoupper($Position); ?>
                        </div>
                        <div class="col-xs-2">
                           <b>Division:</b>
                        </div>
                        <div class="col-xs-4">
                           <?php echo strtoupper($Division); ?>
                        </div>
                     </div>
                     <br><br>
                     <div class="row margin-top">
                        <div class="col-xs-12" style="padding: 10px;">
                           <div class="row" style="border:  2px solid black;">
                              <div class="col-xs-12 text-center">
                                 <b>EMPLOYEE PAYSLIP</b>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-3" style="padding: 10px;">
                           <div class="row" style="border:  2px solid black;">
                              <div class="col-xs-12 text-center">
                                 <b>BASIC SALARY</b>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-12">
                                 <div class="row margin-top">
                                    <div class="col-xs-6">
                                       BASIC:
                                    </div>
                                    <div class="col-xs-1">:</div>
                                    <div class="col-xs-5 text-right">
                                       <?php echo number_format($basic,2); ?>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-6">
                                       PERA:
                                    </div>
                                    <div class="col-xs-1">:</div>
                                    <div class="col-xs-5 text-right">
                                       <?php echo number_format($pera,2); ?>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-6">
                                       RATA:
                                    </div>
                                    <div class="col-xs-1">:</div>
                                    <div class="col-xs-5 text-right">
                                       <?php echo number_format($rata,2); ?>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-xs-3" style="padding: 10px;">
                           <div class="row" style="border:  2px solid black;">
                              <div class="col-xs-12 text-center">
                                 <b>BENEFITS</b>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-12">
                                 <div class="row margin-top">
                                    <div class="col-xs-6">
                                       SALA:
                                    </div>
                                    <div class="col-xs-1">:</div>
                                    <div class="col-xs-5 text-right">
                                       <?php echo number_format(($sa_amount + $la),2); ?>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-6">
                                       Hazard Pay:
                                    </div>
                                    <div class="col-xs-1">:</div>
                                    <div class="col-xs-5 text-right">
                                       <?php echo number_format($hazard,2); ?>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-6">
                                       Longevity Pay:
                                    </div>
                                    <div class="col-xs-1">:</div>
                                    <div class="col-xs-5 text-right">
                                       
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-xs-6" style="padding: 10px;">
                           <div class="row" style="border: 2px solid black;">
                              <div class="col-xs-12 text-center">
                                 <b>DEDUCTIONS</b>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-12">
                                 <div class="row margin-top">
                                    <div class="col-xs-3">
                                       GSIS:
                                    </div>
                                    <div class="col-xs-1">:</div>
                                    <div class="col-xs-2 text-right">
                                       <?php echo number_format($gsis,2); ?>
                                    </div>
                                    <div class="col-xs-3">
                                       Withholding Tax:
                                    </div>
                                    <div class="col-xs-1">:</div>
                                    <div class="col-xs-2 text-right">
                                       <?php echo number_format($tax_amount,2); ?>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-3">
                                       Pagibig:
                                    </div>
                                    <div class="col-xs-1">:</div>
                                    <div class="col-xs-2 text-right">
                                       <?php echo number_format($pagibig,2); ?>
                                    </div>
                                    <div class="col-xs-3">
                                       Philhealth:
                                    </div>
                                    <div class="col-xs-1">:</div>
                                    <div class="col-xs-2 text-right">
                                       <?php echo number_format($phic,2); ?>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-3">
                                       Absent SALA:
                                    </div>
                                    <div class="col-xs-1">:</div>
                                    <div class="col-xs-2 text-right">
                                       <?php echo number_format($sa_Ab_amount,2); ?>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-3">
                                       Undertime SALA:
                                    </div>
                                    <div class="col-xs-1">:</div>
                                    <div class="col-xs-2 text-right">
                                       <?php echo number_format($sa_UT_amount,2); ?>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-6"></div>
                        <div class="col-xs-6">
                           <div class="row">
                              <div class="col-xs-6"></div>
                              <div class="col-xs-6">
                                 <?php
                                    $pms_loan = pms_SelectEach("pms_loaninfo_transactions",$trn_where);
                                    if ($pms_loan) {
                                       while ($loan_row = mysqli_fetch_assoc($pms_loan)) {
                                          $loan_id = $loan_row["loan_id"];
                                          $loan_name = pms_FindFirst("pms_loans","WHERE id = '$loan_id'","name");
                                          echo '<div class="row">';
                                          echo '<div class="col-xs-6">';
                                          echo strtoupper($loan_name);
                                          echo '</div>';
                                          echo '<div class="col-xs-1">:</div>';
                                          echo '<div class="col-xs-5 text-right">';
                                          echo number_format($loan_row["amount"],2);
                                          echo '</div>';
                                          echo '</div>';
                                       }
                                    }
                                 ?>
                              </div>
                           </div>
                        </div>
                     </div>
                     <br>
                     <br>
                     <div class="row margin-top">
                        <div class="col-xs-3"></div>
                        <div class="col-xs-3">
                           <div class="row margin-top">
                              <div class="col-xs-6">
                                 <b>GROSS INCOME</b>
                              </div>
                              <div class="col-xs-1">:</div>
                              <div class="col-xs-5 text-right">
                                 <?php echo number_format($gross,2); ?>
                              </div>
                           </div>
                        </div>
                        <div class="col-xs-6">
                           <div class="row margin-top">
                              <div class="col-xs-9 text-right">
                                 <b>TOTAL DEDUCTIONS</b>
                              </div>
                              <div class="col-xs-1">:</div>
                              <div class="col-xs-2 text-right">
                                 <?php echo number_format($total_deduct,2); ?>
                              </div>
                           </div>
                        </div>
                     </div>
                     <br>
                     <div class="row margin-top">
                        <div class="col-xs-3">
                           <div class="row margin-top">
                              <div class="col-xs-6">
                                 <b>NET INCOME</b>
                              </div>
                              <div class="col-xs-1">:</div>
                           </div>
                        </div>
                        <div class="col-xs-3">
                           <div class="row margin-top">
                              <div class="col-xs-6">
                                 <b><?php echo $month_name." ".$y; ?></b>
                              </div>
                              <div class="col-xs-6 text-right">
                                 <?php echo number_format($net_income,2); ?>
                              </div>
                           </div>
                        </div>
                     </div>
                     <br>
                     <div class="row margin-top">
                        <div class="col-xs-3">
                           <div class="row margin-top">
                              <div class="col-xs-6">
                                 <b>NET HALF</b>
                              </div>
                              <div class="col-xs-1">:</div>
                           </div>
                        </div>
                        <div class="col-xs-3">
                           <div class="row margin-top">
                              <div class="col-xs-6">
                                 
                              </div>
                              <div class="col-xs-6 text-right">
                                 <?php echo number_format(($net_income / 2),2); ?>
                              </div>
                           </div>
                        </div>
                     </div>
                     <br>
                     <br>
                     <qoute>
                     	This is a computer generated document and does not require any signature if without alterations
                     </qoute>
                  </div>
               </div>
            </div>
         </div>
         <?php
            footer();
            include "varHidden.e2e.php";
         ?>
      </div>
   </form>
</body>
</html>