<div class="row">
   <div class="col-xs-12">
      <div id="spGridTable_ot">
         <?php
            $table = "overtimepolicy";
            $sql = "SELECT * FROM `$table` ORDER BY RefId Desc LIMIT 100";
            doGridTable($table,
                        ["Name", "Valid Years", "Max OT in Month", "COC Regular Days", "COC Holidays"],
                        ["OvertimePolicyGroupRefId", "ValidYears", "MaxOTMonth", "COCRatesWorkingDays", "COCRatesHolidays"],
                        $sql,
                        [true,true,true,false],
                        "gridTable_overtimepolicy");
         ?>
      </div>
      <button type="button" class="btn-cls4-sea"
           id="btnINSERTOT" name="btnINSERTOT">
         <i class="fa fa-file" aria-hidden="true"></i>
         &nbsp;Insert New
      </button>
   </div>
</div>

<!-- Modal -->
<div class="modal fade modalFieldEntry--" id="modalFieldEntry_OvertimePolicy" role="dialog">
   <div class="modal-dialog" style="width:75%;">
      <div class="mypanel" style="height:100%;">
         <div class="panel-top bgSea">
            <span id="modalTitle_OvertimePolicy" style="font-size:11pt;">Inserting New Overtime Policy</span>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="panel-mid" id="EntryScrn_OvertimePolicy">
            <div class="row margin-top">
               <div class="col-xs-1"></div>
               <div class="col-xs-10">
                  <div class="row margin-top">
                     <div class="col-xs-2 label">
                        <label class="control-label" for="inputs">GROUP OT POLICY:</label>
                     </div>
                     <div class="col-xs-4">
                        <?php createSelect("overtimepolicygroup","sint_OvertimePolicyGroupRefId","",100,"Name","",""); ?>
                     </div>
                  </div>
                  <!--<div class="row margin-top">
                     <div class="col-xs-2 label">
                        <label class="control-label" for="inputs">CODE:</label>
                     </div>
                     <div class="col-xs-4">
                        <input class="form-input saveFields--" type="text" id="" name="char_OTCode">
                     </div>
                  </div>
                  <div class="row margin-top">
                     <div class="col-xs-2 label">
                        <label class="control-label" for="inputs">POLICY NAME:</label>
                     </div>
                     <div class="col-xs-4">
                        <input class="form-input saveFields--" type="text" id="" name="char_OTName">
                     </div>
                     <div class="col-xs-3">
                        <input type="checkbox" class="saveFields--" id="WithPay" name="sint_WithPay">&nbsp;&nbsp;<label for="WithPay">With Pay</label>
                     </div>
                  </div>
                  -->
                  <?php spacer(20); ?>
                  <div class="row">
                     <label class="control-label" for="inputs">OVERTIME POLICY:</label>
                  </div>
                  <div class="row">
                     <div class="col-xs-4">
                        <div class="row margin-top">
                           <label class="control-label" for="inputs">Overtime will be forfeited if:</label>
                        </div>
                        <div class="row margin-top">
                           <input type="checkbox" class="saveFields--" id="Late" name="sint_ForfeitedIfLate" value="0">
                           &nbsp;&nbsp;<label for="Late">Late</label>
                           <!--<input type="hidden" name="sint_ForfeitedIfLate" value="">-->
                        </div>
                        <div class="row margin-top">
                           <input type="checkbox" class="saveFields--" id="Under" name="sint_ForfeitedIfUT" value="0">
                           &nbsp;&nbsp;<label for="Under">Undertime</label>
                           <!--<input type="hidden" name="sint_ForfeitedIfUT" value="0">-->
                        </div>
                        <div class="row margin-top">
                           <input type="checkbox" class="saveFields--" id="OnLeave" name="sint_ForfeitedIfLeave" value="0">
                           &nbsp;&nbsp;<label for="OnLeave">On Leave</label>
                           <!--<input type="hidden" name="sint_ForfeitedIfLeave" value="0">-->
                        </div>
                        <div class="row margin-top">
                           <input type="checkbox" class="saveFields--" id="Absent" name="sint_ForfeitedIfAbsPrevDay" value="0">
                           &nbsp;&nbsp;<label for="Absent">Absent on Previous Day</label>
                           <!--<input type="hidden" name="sint_ForfeitedIfAbsPrevDay" value="0">-->
                        </div>
                        <div class="row margin-top">
                           <select class="form-input saveFields--" name="sint_PrevDay">
                              <option value=""></option>
                              <option value="MON">MONDAY</option>
                              <option value="TUE">TUESDAY</option>
                              <option value="WED">WEDNESDAY</option>
                              <option value="THURS">THURSDAY</option>
                              <option value="FRI">FRIDAY</option>
                              <option value="SAT">SATURDAY</option>
                              <option value="SUN">SUNDAY</option>
                           </select>
                        </div>
                     </div>
                     <div class="col-xs-4">
                        <div class="row margin-top">
                           <label class="control-label" for="inputs">Max O.T per day</label>
                        </div>
                        <div class="row margin-top">
                           <div class="col-xs-6">
                              <label class="control-label" for="inputs">Working Days</label>
                           </div>
                           <div class="col-xs-6">
                              <input class="form-input number-- saveFields--" type="text" id="" name="sint_MaxOTWorkingDays">
                           </div>
                        </div>
                        <div class="row margin-top">
                           <div class="col-xs-6">
                              <label class="control-label" for="inputs">Saturday</label>
                           </div>
                           <div class="col-xs-6">
                              <input class="form-input number-- saveFields--" type="text" id="" name="sint_MaxOTSaturday">
                           </div>
                        </div>
                        <div class="row margin-top">
                           <div class="col-xs-6">
                              <label class="control-label" for="inputs">Holidays</label>
                           </div>
                           <div class="col-xs-6">
                              <input class="form-input number-- saveFields--" type="text" id="" name="sint_MaxOTHolidays">
                           </div>
                        </div>
                     </div>
                     <div class="col-xs-1"></div>
                     <div class="col-xs-3">
                        <div class="row margin-top">
                           <label class="control-label" for="inputs">Max O.T in a month</label>
                        </div>
                        <div class="row margin-top txt-center">
                           <div class="col-xs-12">
                              <input class="form-input number-- saveFields--" type="text" id="" name="sint_MaxOTMonth">
                           </div>
                        </div>
                     </div>
                  </div>

                  <?php spacer(20);?>
                  <div class="row">
                     <div class="col-xs-6">
                        <label class="control-label" for="inputs">Compensatory Overtime Credit Policy</label>
                     </div>
                     <div class="col-xs-6">
                        <label class="control-label" for="inputs">COC Rates</label>
                     </div>
                  </div>
                  <div class="row margin-top">
                     <div class="col-xs-6">
                        <div class="row margin-top">
                           <div class="col-xs-6">
                              <label class="control-label" for="inputs">Max Unused COC for the year</label>
                           </div>
                           <div class="col-xs-5">
                              <input class="form-input number-- saveFields--" type="text" id="" name="sint_MaxUnusedCOCYear">
                           </div>
                        </div>
                        <div class="row margin-top">
                           <div class="col-xs-6">
                              <label class="control-label" for="inputs">Valid for years</label>
                           </div>
                           <div class="col-xs-5">
                              <input class="form-input number-- saveFields--" type="text" id="" name="sint_ValidYears">
                           </div>
                        </div>
                     </div>
                     <div class="col-xs-6">
                        <div class="row margin-top">
                           <div class="col-xs-6">
                              <label class="control-label" for="inputs">Working Days</label>
                           </div>
                           <div class="col-xs-5">
                              <input class="form-input number-- saveFields--" type="text" id="" name="sint_COCRatesWorkingDays">
                           </div>
                        </div>
                        <div class="row margin-top">
                           <div class="col-xs-6">
                              <label class="control-label" for="inputs">Saturday</label>
                           </div>
                           <div class="col-xs-5">
                              <input class="form-input number-- saveFields--" type="text" id="" name="sint_COCRatesSaturday">
                           </div>
                        </div>
                        <div class="row margin-top">
                           <div class="col-xs-6">
                              <label class="control-label" for="inputs">Holidays</label>
                           </div>
                           <div class="col-xs-5">
                              <input class="form-input number-- saveFields--" type="text" id="" name="sint_COCRatesHolidays">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="panel-bottom">
            <div class="row">
               <div class="col-xs-12 txt-center">
                  <?php createButton("Save","btnLocSave","btn-cls4-sea","fa-floppy-o",""); ?>
                  <?php createButton("Cancel","btnLocCancel","btn-cls4-red","fa-undo",""); ?>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>