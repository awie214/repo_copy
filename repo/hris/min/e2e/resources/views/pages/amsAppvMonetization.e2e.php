<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_approval") ?>"></script>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"ams"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php
               doTitleBar("For Aprroval > Leave Monetization");
               spacer(5);
            ?>
            <div class="row">
               <div class="col-xs-12">
                  <?php
                     $EmpRefId = getvalue("txtRefId");
                     $attr = ["empRefId"=>getvalue("txtRefId"),
                              "empLName"=>getvalue("txtLName"),
                              "empFName"=>getvalue("txtFName"),
                              "empMName"=>getvalue("txtMidName")];
                     $EmpRefId = EmployeesSearch($attr);
                     bar();
                     /*$sql = "SELECT *,t2.RefId as asRefId FROM employees t1
                             INNER JOIN employeesleavemonetization t2
                             ON t1.CompanyRefId = t2.CompanyRefId
                             AND t1.BranchRefId = t2.BranchRefId";
                     if (getvalue("txtLName") != "") {
                        $sql .= " AND t1.LastName LIKE '".getvalue("txtLName")."%'";
                     }
                     if (getvalue("txtFName") != "") {
                        $sql .= " AND t1.FirstName LIKE '".getvalue("txtFName")."%'";
                     }
                     if (getvalue("txtMidName") != "") {
                        $sql .= " AND t1.MiddleName LIKE '".getvalue("txtMidName")."%'";
                     }
                     if (getvalue("txtRefId") != "") {
                        $sql .= " AND t2.EmployeesRefId = '".getvalue("txtRefId")."'";
                     } else {
                        $sql .= " AND t1.RefId = t2.EmployeesRefId";
                     }
                     $sql .= " AND t2.Status IS NULL ORDER BY FiledDate LIMIT 20";*/
                     $sql = "SELECT *,employeesleavemonetization.RefId as asRefId FROM employeesleavemonetization
                     INNER JOIN employees 
                     ON employeesleavemonetization.CompanyRefId = employees.CompanyRefId
                     AND employeesleavemonetization.BranchRefId = employees.BranchRefId
                     AND employeesleavemonetization.EmployeesRefId = employees.RefId";

                     if (getvalue("txtLName") != "") {
                        $sql .= " AND employees.LastName LIKE '".getvalue("txtLName")."%'";
                     }
                     if (getvalue("txtFName") != "") {
                        $sql .= " AND employees.FirstName LIKE '".getvalue("txtFName")."%'";
                     }
                     if (getvalue("txtMidName") != "") {
                        $sql .= " AND employees.MiddleName LIKE '".getvalue("txtMidName")."%'";
                     }
                     if (getvalue("txtRefId") != "") {
                        $sql .= " AND employeesleavemonetization.EmployeesRefId = '".getvalue("txtRefId")."'";
                     } 
                     $sql .= " AND employeesleavemonetization.Status IS NULL ORDER BY FiledDate LIMIT 100";
                     $rs = mysqli_query($conn,$sql) or die(mysqli_error($conn));
                  ?>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-1"></div>
               <div class="col-xs-10 padd5">
                  <?php
                     if ($rs) {
                        $recordNum = mysqli_num_rows($rs);
                        while ($row = mysqli_fetch_array($rs)) {
                           $refid = $row["asRefId"];
                           $rsEmp = FFirstRefId("employees",$row["EmployeesRefId"],"*");
                           $where = "WHERE EmployeesRefId = ".$rsEmp["RefId"];
                           $empinformation = FindFirst('empinformation',$where,"*");
                           if ($empinformation) {
                              $info = array_merge($rsEmp,$empinformation);
                  ?>
                           <div class="mypanel pull-left padd5" style="margin:5px;width:40%;" id="card_<?php echo $refid; ?>">
                              <div class="panel-top">REF ID:&nbsp;<?php echo $row['RefId']; ?></div>
                              <div class="panel-mid">
                                 <div class="row txt-right" style="margin-right:10px;">
                                    <label>DATE FILE:</label><span style="margin-left:15px;"><?php echo $row["FiledDate"];?></span>
                                 </div>
                                 <?php
                                    
                                    echo '
                                    <div class="row margin-top padd5">
                                       <div class="row margin-top">
                                          <div class="col-sm-4 txt-center">
                                             <div class="border" style="height:1.5in;width:1.3in;">
                                                <img src="'.img($rsEmp['CompanyRefId']."/EmployeesPhoto/".$rsEmp['PicFilename']).'" style="width:100%;height:100%;">
                                             </div>
                                          </div>
                                          <div class="col-sm-8 txt-center padd5">';
                                             $templ->btn_apprvReject(2,$refid);
                                          echo    
                                          '</div>
                                       </div>
                                       <div class="row margin-top">   
                                          <div class="col-sm-12">';
                                             $templ->doEmployeeInfo($info);
                                    echo       
                                          '</div>   
                                       </div>   
                                    </div>';
                                    bar();  
                                 ?>
                                 <div class="row">
                                    <div class="col-xs-4">
                                       <label>VL Balance:</label>
                                       <span style="margin-left:15px;"><?php echo number_format($row["VLBalance"],2); ?></span>
                                    </div>
                                    <div class="col-xs-4">
                                       <label>VL Value:</label>
                                       <span style="margin-left:15px;"><?php echo number_format($row["VLValue"],2); ?></span>
                                    </div>
                                    <div class="col-xs-4">
                                       <label>VL Amount:</label>
                                       <span style="margin-left:15px;"><?php echo number_format($row["VLAmount"],2); ?></span>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-xs-4">
                                       <label>SL Balance:</label>
                                       <span style="margin-left:15px;"><?php echo number_format($row["SLBalance"],2); ?></span>
                                    </div>
                                    <div class="col-xs-4">
                                       <label>SL Value:</label>
                                       <span style="margin-left:15px;"><?php echo number_format($row["SLValue"],2); ?></span>
                                    </div>
                                    <div class="col-xs-4">
                                       <label>SL Amount:</label>
                                       <span style="margin-left:15px;"><?php echo number_format($row["SLAmount"],2); ?></span>
                                    </div>
                                 </div>
                              </div>
                              <div class="panel-bottom"></div>
                           </div>
                        <?php
                           }
                        }
                     } else {
                        alert("Information","No For Approval");
                     }
                  ?>
               </div>
               <div class="col-xs-1"></div>
            </div>
            <?php
               footer();
               $table = "employeesleavemonetization";
               modalReject();
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>

   </body>
</html>