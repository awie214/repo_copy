<?php
/*
------------------------------------------------------------------------
    File        : pds_Page1.php
    Purpose     :
    Syntax      : PHP / HTML
    Description :
    Author(s)   : Erwin Mendoza / emendoza0620@gmail.com
    Created     :
    Notes       :
------------------------------------------------------------------------
Developer   Date     Changes
--------------------------------------------------------------------------

--------------------------------------------------------------------------*/
if (isset($objPHPExcel)) {
   $workSheet = $objPHPExcel->setActiveSheetIndex(0);
}

$EmpEducVoca_split = false;
$EmpEducColl_split = false;
$EmpEducGrad_split = false;
?>
   <div class="page-- nextpage" id="page1" style="position:relative;">
      <table cellspacing=0 cellpadding=0 title="page 1" class="page1">
         <tr align="center" class="ruler">
            <td width="20px">A</td>
            <td width="120px">B</td>
            <td width="50px">C</td>
            <td width="50px">D</td>
            <td width="50px">E</td>
            <td width="50px">F</td>
            <td width="60px">G</td>
            <td width="60px">H</td>
            <td width="*px">I</td>
            <td width="50px">J</td>
            <td width="50px">K</td>
            <td width="50px">L</td>
            <td width="50px">M</td>
            <td width="50px">N</td>
         </tr>
         <tr>
            <td colspan="14" align="center" style="padding-top:10px;padding-bottom:10px;position:relative;border-bottom: none;">
               <span style="position:absolute;top:1px;left:3px;font-family:calibri;font-size:8pt;text-align:left;"><b><i>CS Form No. 212<br>Revised 2017</i></b></span>
               <span style="font-family:Arial;font-size:16pt;font-weight: 900;">PERSONAL DATA SHEET</span>
            </td>
         </tr>
         <tr>
            <td colspan="14" style="border-bottom: none;">
               <span style="font-family:Arial;font-size:9px;font-weight:600;">
                  <i>
                     <b>
                        WARNING: Any misrepresentation made in the Personal Data Sheet and the Work Experience Sheet shall cause the filing of administrative/criminal case/s against the person concerned.
                     </b>
                  </i>
               </span>
            </td>
         </tr>
         <tr>
            <td colspan="14" style="border-bottom: none;">
               <span style="font-family:Arial;font-size:9px;font-weight:600;">
                  <i>
                     <b>
                        READ THE ATTACHED GUIDE TO FILLING OUT THE PERSONAL DATA SHEET (PDS) BEFORE ACCOMPLISHING THE PDS FORM.
                     </b>
                  </i>
               </span>
            </td>
         </tr>
         <tr>
            <td colspan="10">
               <span style="font-family:Arial;font-size:8px;">
                  Print legibly. Tick appropriate boxes ( <span style="font-family:wingdings">o</span> ) and use separate sheet if necessary. Indicate N/A if not applicable.  DO NOT ABBREVIATE.
               </span>
            </td>
            <td style="background:#bfbfbf;color:white;font-size:9px;font-family:Arial; border-top: 1px solid black;" nowrap>1. CS ID No.</td>
            <td colspan="3" align="right" style="font-size:8px;font-family:Arial; border-top: 1px solid black;">
               (Do not fill up. For CSC use only)
            </td>
         </tr>
         <tr>
            <td colspan="14" class="bgGray tabTitle_td">
               <span class="tabTitle">
                  <i>I. PERSONAL INFORMATION</i>
               </span>
            </td>
         </tr>
         <tr>
            <td class="bgGrayLabel" align="center">
               <span >2.</span>
            </td>
            <td class="bgGrayLabel" style="border-left:0;">
               <span >SURNAME</span>
            </td>
            <td colspan="12">
               <span class="answer" id="page1_C7"><?php if ($MasterFile) {setValue($rsPersonInfo,"LastName");} ?></span>
            </td>
         </tr>
         <tr>
            <td class="bgGrayLabel" class="numerical" style="border-right:0">
               <span >&nbsp;</span>
            </td>
            <td class="bgGrayLabel" style="border-left:0;">
               <span >FIRST NAME</span>
            </td>
            <td colspan="8">
               <span class="answer" id="page1_C8"><?php if ($MasterFile) {setValue($rsPersonInfo,"FirstName");} ?></span>
            </td>
            <td colspan="3" class="bgGrayLabel" nowrap>
               <span >
                  NAME EXTENSION (JR., SR)
               </span>
            </td>
            <td>
               <span class="answer" id="page1_N8"><?php if ($MasterFile) {setValue($rsPersonInfo,"ExtName");} ?></span>
            </td>
         </tr>
         <tr>
            <td class="bgGrayLabel" class="numerical" style="border-right:0">
               <span >&nbsp;</span>
            </td>
            <td class="bgGrayLabel" style="border-left:0;">
               <span style="width:140px;">MIDDLE NAME</span>
            </td>
            <td colspan="12">
               <span class="answer" id="page1_C9"><?php if ($MasterFile) {setValue($rsPersonInfo,"MiddleName");} ?></span>
            </td>
         </tr>
         <tr>
            <td class="bgGrayLabel" align="center">
               <span >3.</span>
            </td>
            <td class="bgGrayLabel" colspan="1" style="border-left:0;">
               <span >DATE OF BIRTH (mm/dd/yyyy)</span>
            </td>
            <td colspan="4">
               <span class="answer" id="page1_E10">
                  <?php

                     if ($MasterFile) {
                        //setValue($rsPersonInfo,"BirthDate");
                        echo dispVal($rsPersonInfo["BirthDate"]);
                     }
                  ?>
               </span>
            </td>
            <td colspan="3" class="bgGrayLabel">
               <span >16. CITIZENSHIP</span>
            </td>
            <td colspan="2" align="center" style="border-bottom: 0px;">
               <?php
                  $isFilipino = false;
                  if ($MasterFile) {
                     if ($rsPersonInfo["isFilipino"] == 1) {
                        $isFilipino = true;
                     }
                  }
                  ($isFilipino ? chkBox("Filipino",1) : chkBox("Filipino",0));
               ?>
            </td>
            <td colspan="3" style="border-bottom: 0px;border-left: 0px;">
               <?php
                  $isDual = false;
                  if ($MasterFile) {
                     if ($rsPersonInfo["isFilipino"] == 0) {
                        $isDual = true;
                     }
                  }
                  ($isDual ? chkBox("Dual Citizenship",1) : chkBox("Dual Citizenship",0));
               ?>
            </td>
         </tr>
         <tr>
            <td class="bgGrayLabel" align="center">
               <span >4.</span>
            </td>
            <td class="bgGrayLabel" style="border-left:0;">
               <span >PLACE OF BIRTH</span>
            </td>
            <td colspan="4">
               <span class="answer" id="page1_C11">
               <?php setValue($rsPersonInfo,"BirthPlace") ?>
               </span>
            </td>
            <td colspan="3" class="bgGrayLabel" align="right" style="border-bottom: 0px;">
               <span >If holder of dual citizenship,</span>
            </td>
            <td colspan="2" style="border-bottom: 0px;"></td>
            <td colspan="3" style="border-bottom: 0px;border-left: 0px;">
               <?php
                  if ($MasterFile) {
                     if ($isFilipino) {
                        chkBox("by birth",0);
                        chkBox("by naturalization",0);
                     } else {
                        if ($rsPersonInfo["isByBirthOrNatural"] == 1) {
                           chkBox("by birth",1);
                           chkBox("by naturalization",0);
                        } else {
                           chkBox("by birth",0);
                           chkBox("by naturalization",1);
                        }
                     }
                  }
               ?>
            </td>
         </tr>
         <tr>
            <td class="bgGrayLabel" align="center">
               <span >5.</span>
            </td>
            <td class="bgGrayLabel" style="border-left:0;">
               <span >SEX</span>
            </td>
            <td colspan="2" align="center">
               <?php
                  $isMale = "";
                  if ($MasterFile) {
                     if ($rsPersonInfo["Sex"] == "M") {
                        $isMale = true;
                     }
                  } else {
                     if (putValue("C12") == "Male") {
                        $isMale = true;
                     }
                  }
                  ($isMale ? chkBox("Male",1) : chkBox("Male",0));
               ?>
            </td>
            <td colspan="2" align="center">
               <?php
                  $isFemale = "";
                  if ($MasterFile) {
                     if ($rsPersonInfo["Sex"] == "F") {
                        $isFemale = true;
                     }
                  } else {
                     if (putValue("C12") == "Female") {
                        $isFemale = true;
                     }
                  }
                  ($isFemale ? chkBox("Female",1) : chkBox("Female",0));
               ?>
            </td>
            <td colspan="3" class="bgGrayLabel" align="right" >
               please indicate the details
            </td>
            <td colspan="5" align="center">
               <div>Pls. indicate country</div>
               <div class="answer" id="page1_J12">
                  <?php
                     if ($MasterFile) {
                        if ($isFilipino) {
                           echo '&nbsp;';
                        } else {
                           if ($MasterFile) dispValue(getRecord("country",$rsPersonInfo["CountryCitizenRefId"],"Name"));
                        }
                     }
                  ?>
                  
               </div>
            </td>
         </tr>
         <tr>
            <td class="bgGrayLabel" align="center" rowspan="4">
               <span >6.</span>
            </td>
            <td class="bgGrayLabel" rowspan="4" align="top" style="border-left:0;">
               <span >CIVIL STATUS</span>
            </td>
            <td colspan="2" style="border-right:0px;border-bottom:0px;">
               <?php
                  $single = 0;
                  if ($MasterFile) {
                     if ($rsPersonInfo["CivilStatus"] == "Si") $single = 1;
                  } else {
                     if (putValue("C13") == "Single") {
                        $single = 1;
                     }
                  }
                  ($single ? chkBox("Single",1) : chkBox("Single",0));

               ?>
            </td>
            <td colspan="2" style="border-left:0px;border-bottom:0px;">
               <?php
                  $married = 0;
                  if ($MasterFile) {
                     if ($rsPersonInfo["CivilStatus"] == "Ma") $married = 1;
                  } else {
                     if (putValue("C13") == "Married") {
                        $married = 1;
                     }
                  }
                  ($married ? chkBox("Married",1) : chkBox("Married",0));
               ?>
            </td>
            <td class="bgGrayLabel" rowspan="6" colspan="2" align="top" style="border-bottom:0">
               <span>17. RESIDENTIAL ADDRESS</span>
            </td>
            <td colspan="4" align="center">
               <span class="answer" id="page1_M13"><?php if ($MasterFile) setValue($rsPersonInfo,"ResiHouseNo"); ?></span>
            </td>
            <td colspan="2" align="center" style="border-left: 0px;">
               <span class="answer" id="page1_I13"><?php if ($MasterFile) setValue($rsPersonInfo,"ResiStreet"); ?></span>
            </td>
         </tr>
         <tr>
            <td colspan="2" style="border-right:0px;border-bottom:0px;">
               <?php
                  $wi = 0;
                  if ($MasterFile) {
                     if ($rsPersonInfo["CivilStatus"] == "Wi") $wi = 1;
                  } else {
                     if (putValue("C13") == "Widowed") {}
                  }
                  ($wi ? chkBox("Widowed",1) : chkBox("Widowed",0));
               ?>
            </td>
            <td colspan="2" style="border-left:0px;border-bottom:0px;">
               <?php
                  $se = 0;
                  if ($MasterFile) {
                     if ($rsPersonInfo["CivilStatus"] == "Se") $se = 1;
                  } else {
                     if (putValue("C13") == "Separated") {}
                  }
                  ($se ? chkBox("Separated",1) : chkBox("Separated",0));
               ?>
            </td>
            <td colspan="4" align="center" class="bgGrayLabel">
               House/Block/Lot No.
            </td>
            <td colspan="2" align="center" class="bgGrayLabel" style="border-left: 0px;">
               Street
            </td>
         </tr>
         <tr>
            <td colspan="2" style="border-right:0px;border-bottom:0px;">
               <?php
                  $ot = 0;
                  if ($MasterFile) {
                     if ($rsPersonInfo["CivilStatus"] == "Ot" || $rsPersonInfo["CivilStatus"] == "An") $ot = 1;
                  } else {
                     if (putValue("C13") == "Others"){};
                  }
                  ($ot ? chkBox("Other/s",1) : chkBox("Other/s",0));
               ?>
            </td>
            <td colspan="2" style="border-left:0px;border-bottom:0px;">&nbsp;</td>
            <td colspan="4" align="center">
               <span class="answer" id="page1_I15"><?php if ($MasterFile) setValue($rsPersonInfo,"ResiSubd"); ?></span>
            </td>
            <td colspan="2" align="center" style="border-left: 0px;">
               <span class="answer" id="page1_M15"><?php if ($MasterFile) setValue($rsPersonInfo,"ResiBrgy"); ?></span>
            </td>
         </tr>
         <tr>
            <td colspan="4">
               <span class="answer">
               <?php
                  if ($MasterFile) {
                     if ($rsPersonInfo["CivilStatus"] == "An") echo "Annulled";
                  } else {
                     echo putValue("C15");
                  }
               ?>
               </span>
            </td>
            <td colspan="4" align="center" class="bgGrayLabel">
               Subdivision/Village
            </td>
            <td colspan="2" align="center" class="bgGrayLabel" style="border-left: 0px;">
               Barangay
            </td>
         </tr>
         <tr>
            <td class="bgGrayLabel" align="center" rowspan="2" align="top">
               <span>7.</span>
            </td>
            <td class="bgGrayLabel" rowspan="2" align="top" style="border-left:0;">
               <span>HEIGHT (m)</span>
            </td>
            <td colspan="4" rowspan="2" align="top">
               <span class="answer" id="page1_C17"><?php if ($MasterFile) setValue($rsPersonInfo,"Height"); ?></span>
            </td>
            <td colspan="4" align="center">
               <span class="answer" id="page1_I17"><?php if ($MasterFile) dispValue(getRecord("city",$rsPersonInfo["ResiAddCityRefId"],"Name")); ?></span>
            </td>
            <td colspan="2" align="center" style="border-left: 0px;">
               <span class="answer" id="page1_M17"><?php if ($MasterFile) dispValue(getRecord("province",$rsPersonInfo["ResiAddProvinceRefId"],"Name")); ?></span>
            </td>
         </tr>
         <tr>
            <td colspan="4" align="center" class="bgGrayLabel">
               City/Municipality
            </td>
            <td colspan="2" align="center" class="bgGrayLabel" style="border-left: 0px;">
               Province
            </td>
         </tr>
         <tr>
            <td class="bgGrayLabel" align="center" >
               <span>8.</span>
            </td>
            <td class="bgGrayLabel" style="border-left:0;">
               <span>WEIGHT (kg)</span>
            </td>
            <td colspan="4">
               <span class="answer" id="page1_C19"><?php if ($MasterFile) setValue($rsPersonInfo,"Weight"); ?></span>
            </td>
            <td class="bgGrayLabel" colspan="2" align="center">
               <span>ZIP CODE</span>
            </td>
            <td colspan="6">
               <span class="answer" id="page1_I19" title="ZipCode cell i19">
                  <?php if ($MasterFile) dispValue(getRecord("city",$rsPersonInfo["ResiAddCityRefId"],"ZipCode")); ?>
               </span>
            </td>
         </tr>
         <tr>
            <td class="bgGrayLabel" align="center" rowspan="2" align="top">
               <span>9.</span>
            </td>
            <td class="bgGrayLabel" rowspan="2" align="top" style="border-left:0;">
               <span>BLOOD TYPE</span>
            </td>
            <td colspan="4" rowspan="2" align="top">
               <span class="answer" id="page1_C20"><?php if ($MasterFile) dispValue(getRecord("bloodtype",$rsPersonInfo["BloodTypeRefId"],"Name")); ?></span>
            </td>
            <td class="bgGrayLabel" colspan="2" rowspan="6" align="top" style="border-bottom:0px;">
               <span>18. PERMANENT ADDRESS</span>
            </td>
            <td colspan="4" align="center">
               <span class="answer" id="page1_M20"><?php if ($MasterFile) setValue($rsPersonInfo,"PermanentHouseNo"); ?></span>
            </td>
            <td colspan="2" align="center" style="border-left:0px;">
               <span class="answer" id="page1_I20"><?php if ($MasterFile) setValue($rsPersonInfo,"PermanentStreet"); ?></span>
            </td>
         </tr>
         <tr>
            <td colspan="4" align="center" class="bgGrayLabel">
               House/Block/Lot No.
            </td>
            <td colspan="2" align="center" class="bgGrayLabel" style="border-left:0px;">
               Street
            </td>
         </tr>
         <tr>
            <td class="bgGrayLabel" align="center" rowspan="2" align="top">
               <span>10.</span>
            </td>
            <td class="bgGrayLabel" rowspan="2" align="top" style="border-left:0;">
               <span>GSIS NO.</span>
            </td>
            <td colspan="4" rowspan="2">
               <span class="answer" id="page1_C22"><?php if ($MasterFile) setValue($rsPersonInfo,"GSIS"); ?></span>
            </td>
            <td colspan="4" align="center">
               <span class="answer" id="page1_I22"><?php if ($MasterFile) setValue($rsPersonInfo,"PermanentSubd"); ?></span>
            </td>
            <td colspan="2" align="center" style="border-left:0px;">
               <span class="answer" id="page1_M22"><?php if ($MasterFile) setValue($rsPersonInfo,"PermanentBrgy"); ?></span>
            </td>
         </tr>
         <tr>
            <td colspan="4" align="center" class="bgGrayLabel">
               Subdivision/Village
            </td>
            <td colspan="2" align="center" class="bgGrayLabel" style="border-left:0px;">
               Barangay
            </td>
         </tr>
         <tr>
            <td class="bgGrayLabel" align="center" rowspan="2">
               <span>11.</span>
            </td>
            <td class="bgGrayLabel" rowspan="2" style="border-left:0;">
               <span>PAG-IBIG NO.</span>
            </td>
            <td colspan="4" rowspan="2">
               <span class="answer" id="page1_C24"><?php if ($MasterFile) setValue($rsPersonInfo,"PAGIBIG"); ?></span>
            </td>
            <td colspan="4" align="center">
               <span class="answer" id="page1_I24"><?php if ($MasterFile) dispValue(getRecord("city",$rsPersonInfo["PermanentAddCityRefId"],"Name")); ?></span>
            </td>
            <td colspan="2" align="center" style="border-left:0px;">
               <span class="answer" id="page1_M24"><?php if ($MasterFile) dispValue(getRecord("province",$rsPersonInfo["PermanentAddProvinceRefId"],"Name")); ?></span>
            </td>
         </tr>
         <tr>
            <td colspan="4" align="center" class="bgGrayLabel">
               City/Municipality
            </td>
            <td colspan="2" align="center" class="bgGrayLabel" style="border-left:0px;">
               Province
            </td>
         </tr>
         <tr>
            <td class="bgGrayLabel" align="center">
               <span>12.</span>
            </td>
            <td class="bgGrayLabel" style="border-left:0;">
               <span>PHILHEALTH NO.</span>
            </td>
            <td colspan="4">
               <span class="answer" id="page1_C26"><?php if ($MasterFile) setValue($rsPersonInfo,"PHIC"); ?></span>
            </td>
            <td class="bgGrayLabel" colspan="2" align="center">
               <span>ZIP CODE</span>
            </td>
            <td colspan="6">
               <span class="answer" id="page1_I26"><?php if ($MasterFile) dispValue(getRecord("city",$rsPersonInfo["PermanentAddCityRefId"],"ZipCode")); ?></span>
            </td>
         </tr>
         <tr>
            <td class="bgGrayLabel" align="center">
               <span>13.</span>
            </td>
            <td class="bgGrayLabel" style="border-left:0;">
               <span>SSS NO.</span>
            </td>
            <td colspan="4">
               <span class="answer" id="page1_C27"><?php if ($MasterFile) setValue($rsPersonInfo,"SSS"); ?></span>
            </td>
            <td class="bgGrayLabel" colspan="2">
               <span>19. TELEPHONE NO.</span>
            </td>
            <td colspan="6">
               <span class="answer" id="page1_I27"><?php if ($MasterFile) setValue($rsPersonInfo,"TelNo"); ?></span>
            </td>
         </tr>
         <tr>
            <td class="bgGrayLabel" align="center">
               <span>14.</span>
            </td>
            <td class="bgGrayLabel" style="border-left:0;">
               <span>TIN NO.</span>
            </td>
            <td colspan="4">
               <span class="answer" id="page1_C28"><?php if ($MasterFile) setValue($rsPersonInfo,"TIN"); ?></span>
            </td>
            <td class="bgGrayLabel" colspan="2">
               <span>20. MOBILE NO.</span>
            </td>
            <td colspan="6">
               <span class="answer" id="page1_I28"><?php if ($MasterFile) setValue($rsPersonInfo,"MobileNo"); ?></span>
            </td>
         </tr>
         <tr>
            <td class="bgGrayLabel" align="center">
               <span>15.</span>
            </td>
            <td class="bgGrayLabel" style="border-left:0;">
               <span>AGENCY EMPLOYEE NO.</span>
            </td>
            <td colspan="4">
               <span class="answer" id="page1_C29"><?php if ($MasterFile) setValue($rsPersonInfo,"AgencyId"); ?></span>
            </td>
            <td class="bgGrayLabel" colspan="2">
               <span>21. E-MAIL ADDRESS (if any)</span>
            </td>
            <td colspan="6">
               <span class="answer" id="page1_I29"><?php if ($MasterFile) setValue($rsPersonInfo,"EmailAdd"); ?></span>
            </td>
         </tr>
      </table>
      <table cellspacing=0 cellpadding=0 title="page 1" class="page1">
         <tr align="center" class="ruler">
            <td width="20px" class="ruler--">A</td>
            <td width="120px" class="ruler--">B</td>
            <td width="50px" class="ruler--">C</td>
            <td width="50px" class="ruler--">D</td>
            <td width="50px" class="ruler--">E</td>
            <td width="50px" class="ruler--">F</td>
            <td width="60px" class="ruler--">G</td>
            <td width="60px" class="ruler--">H</td>
            <td width="*px" class="ruler--">I</td>
            <td width="50px" class="ruler--">J</td>
            <td width="50px" class="ruler--">K</td>
            <td width="50px" class="ruler--">L</td>
            <td width="50px" class="ruler--">M</td>
            <td width="50px" class="ruler--">N</td>
         </tr>
         <tr>
            <td colspan="14" class="bgGray tabTitle_td">
               <span class="tabTitle">
                  <i>II. FAMILY BACKGROUND</i>
               </span>
            </td>
         </tr>
         <tr>
            <td class="bgGrayLabel" align="center" style="border-bottom:0;">
               22.
            </td>
            <td class="bgGrayLabel" style="border-bottom:0;border-left:0;">
               SPOUSE'S SURNAME
            </td>
            <td colspan="6">
               <span class="answer" id="page1_C31"><?php if ($MasterFile) setValue($rsEmpFamily,"SpouseLastName"); ?></span>
            </td>
            <td class="bgGrayLabel" colspan="4" align="center">
               23. NAME OF CHILDREN (Write full name and list all)
            </td>
            <td class="bgGrayLabel b-left" colspan="2" align="center">
               DATE OF BIRTH (mm/dd/yyyy)
            </td>
         </tr>
         <tr>
            <td class="bgGrayLabel" colspan="2" style="border-bottom:0;border-top:0">&nbsp;</td>
            <td colspan="4" rowspan="2" valign="bottom">
               <span class="answer" id="page1_C32"><?php if ($MasterFile) setValue($rsEmpFamily,"SpouseFirstName"); ?></span>
            </td>
            <td class="bgGrayLabel" colspan="2" nowrap>
               NAME EXTENSION (JR., SR)
            </td>
            <td colspan="4" rowspan="2">
               <span class="answer CHILDREN" id="page1_I32"></span>
            </td>
            <td colspan="2" rowspan="2" align="center">
               <span class="answer CHILDREN" id="page1_M32"></span>
            </td>
         </tr>
         <tr>
            <td class="bgGrayLabel" style="border-bottom:0;border-right:0;">&nbsp;</td>
            <td class="bgGrayLabel" style="border-bottom:0;border-left:0;">
               FIRST NAME
            </td>
            <td colspan="2" align="center">
               <span class="answer" id="page1_G33"><?php if ($MasterFile) setValue($rsEmpFamily,"SpouseExtName"); ?></span>
            </td>
         </tr>
         <tr>
            <td class="bgGrayLabel"></td>
            <td class="bgGrayLabel" style="border-left:0;">
               MIDDLE NAME
            </td>
            <td colspan="6">
               <span class="answer" id="page1_C34"><?php if ($MasterFile) setValue($rsEmpFamily,"SpouseMiddleName"); ?></span>
            </td>
            <td colspan="4">
               <span class="answer CHILDREN" id="page1_I33"></span>
            </td>
            <td colspan="2" align="center">
               <span class="answer CHILDREN" id="page1_M33"></span<>
            </td>
         </tr>
         <tr>
            <td class="bgGrayLabel"></td>
            <td class="bgGrayLabel" style="border-left:0;">
               OCCUPATION
            </td>
            <td colspan="6">
               <span class="answer" id="page1_C35">
                  <?php if ($MasterFile) dispValue(getRecord("Occupations",$rsEmpFamily["OccupationsRefId"],"Name")); ?>
               </span>
            </td>
            <td colspan="4">
               <span class="answer CHILDREN" id="page1_I34"></span>
            </td>
            <td colspan="2" align="center">
               <span class="answer CHILDREN" id="page1_M34"></span>
            </td>
         </tr>
         <tr>
            <td class="bgGrayLabel"></td>
            <td class="bgGrayLabel" style="border-left:0;">
               EMPLOYER/BUSINESS NAME
            </td>
            <td colspan="6">
               <span class="answer" id="page1_C36"><?php if ($MasterFile) setValue($rsEmpFamily,"EmployersName"); ?></span>
            </td>
            <td colspan="4">
               <span class="answer CHILDREN" id="page1_I35"></span>
            </td>
            <td colspan="2" align="center">
               <span class="answer CHILDREN" id="page1_M35"></span>
            </td>
         </tr>
         <tr>
            <td class="bgGrayLabel"></td>
            <td class="bgGrayLabel" style="border-left:0;">
               BUSINESS ADDRESS
            </td>
            <td colspan="6">
               <span class="answer" id="page1_C37"><?php if ($MasterFile) setValue($rsEmpFamily,"BusinessAddress"); ?></span>
            </td>
            <td colspan="4">
               <span class="answer CHILDREN" id="page1_I36"></span>
            </td>
            <td colspan="2" align="center">
               <span class="answer CHILDREN" id="page1_M36"></span>
            </td>
         </tr>
         <tr>
            <td class="bgGrayLabel"></td>
            <td class="bgGrayLabel" style="border-left:0;">
               TELEPHONE NO.
            </td>
            <td colspan="6">
               <span class="answer" id="page1_C38"><?php if ($MasterFile) setValue($rsEmpFamily,"SpouseMobileNo"); ?></span>
            </td>
            <td colspan="4">
               <span class="answer CHILDREN" id="page1_I37"></span>
            </td>
            <td colspan="2" align="center">
               <span class="answer CHILDREN" id="page1_M37"></span>
            </td>
         </tr>
         <tr>
            <td class="bgGrayLabel" style="border-bottom:0;">
               24.
            </td>
            <td class="bgGrayLabel" style="border-left:0;border-bottom:0;">
               FATHER'S SURNAME
            </td>
            <td colspan="6">
               <span class="answer" id="page1_C39"><?php if ($MasterFile) setValue($rsEmpFamily,"FatherLastName"); ?></span>
            </td>
            <td colspan="4">
               <span class="answer CHILDREN" id="page1_I38"></span>
            </td>
            <td colspan="2" align="center">
               <span class="answer CHILDREN" id="page1_M38"></span>
            </td>
         </tr>
         <tr>
            <td class="bgGrayLabel" colspan="2" style="border-bottom:0;"></td>
            <td colspan="4" rowspan="2" valign="bottom">
               <span class="answer" id="page1_C40"><?php if ($MasterFile) setValue($rsEmpFamily,"FatherFirstName"); ?></span>
            </td>
            <td class="bgGrayLabel" colspan="2">
               NAME EXTENSION (JR., SR)
            </td>
            <td colspan="4" rowspan="2">
               <span class="answer CHILDREN" id="page1_I39"></span>
            </td>
            <td colspan="2" rowspan="2" align="center">
               <span class="answer CHILDREN" id="page1_M39"></span>
            </td>
         </tr>
         <tr>
            <td class="bgGrayLabel" style="border-bottom:0"></td>
            <td class="bgGrayLabel" style="border-left:0;border-bottom:0;">
               FIRST NAME
            </td>
            <td colspan="2" align="center">
               <span class="answer" id="page1_G41"><?php if ($MasterFile) setValue($rsEmpFamily,"FatherExtName"); ?></span>
            </td>
         </tr>
         <tr>
            <td class="bgGrayLabel"></td>
            <td class="bgGrayLabel" style="border-left:0;">
               MIDDLE NAME
            </td>
            <td colspan="6">
               <span class="answer" id="page1_C42"><?php if ($MasterFile) setValue($rsEmpFamily,"FatherMiddleName"); ?></span>
            </td>
            <td colspan="4">
               <span class="answer CHILDREN" id="page1_I40"></span>
            </td>
            <td colspan="2" align="center">
               <span class="answer CHILDREN" id="page1_M40"></span>
            </td>
         </tr>
         <tr>
            <td class="bgGrayLabel" style="border-bottom:0;">25.</td>
            <td class="bgGrayLabel" style="border-left:0;border-bottom:0;">
               MOTHER'S MAIDEN NAME
            </td>
            <td colspan="6">
               <span class="answer" id="page1_C43"></span>
            </td>
            <td colspan="4">
               <span class="answer CHILDREN" id="page1_I41"></span>
            </td>
            <td colspan="2" align="center">
               <span class="answer CHILDREN" id="page1_M41"></span>
            </td>
         </tr>
         <tr>
            <td class="bgGrayLabel" style="border-bottom:0"></td>
            <td class="bgGrayLabel" style="border-left:0;border-bottom:0">
               SURNAME
            </td>
            <td colspan="6">
               <span class="answer" id="page1_C44"><?php if ($MasterFile) setValue($rsEmpFamily,"MotherLastName"); ?></span>
            </td>
            <td colspan="4">
               <span class="answer CHILDREN" id="page1_I42"></span>
            </td>
            <td colspan="2" align="center">
               <span class="answer CHILDREN" id="page1_M42"></span>
            </td>
         </tr>
         <tr>
            <td class="bgGrayLabel" style="border-bottom:0"></td>
            <td class="bgGrayLabel" style="border-left:0;border-bottom:0">
               FIRST NAME
            </td>
            <td colspan="6">
               <span class="answer" id="page1_C45"><?php if ($MasterFile) setValue($rsEmpFamily,"MotherFirstName"); ?></span>
            </td>
            <td colspan="4">
               <span class="answer CHILDREN" id="page1_I43"></span>
            </td>
            <td colspan="2" align="center">
               <span class="answer CHILDREN" id="page1_M43"></span>
            </td>
         </tr>
         <tr>
            <td class="bgGrayLabel"></td>
            <td class="bgGrayLabel" style="border-left:0;">
               MIDDLE NAME
            </td>
            <td colspan="6">
               <span class="answer" id="page1_C46"><?php if ($MasterFile) setValue($rsEmpFamily,"MotherMiddleName"); ?></span>
            </td>
            <td colspan="6" class="bgGrayLabel" align="center" style="color:#FF0000;">
               <i>(Continue on Separate sheet of necessary)</i>
            </td>
         </tr>
      </table>
      <table border=1 cellspacing=0 cellpadding=0 title="page 1">
         <?php 
            educTHEAD();
         ?>
         <tbody>            
            <tr class="trHeight">
               <td colspan=2 class="bgGrayLabel">
                  ELEMENTARY
               </td>
               <td colspan="4">
                  <span class="answer ELEMENTARY" id="page1_C51"></span>
               </td>
               <td colspan="3">
                  <span class="answer ELEMENTARY" id="page1_F51">N/A</span>
               </td>
               <td align="center">
                  <span class="answer ELEMENTARY" id="page1_J51">N/A</span>
               </td>
               <td align="center">
                  <span class="answer ELEMENTARY" id="page1_K51"></span>
               </td>
               <td align="center">
                  <span class="answer ELEMENTARY" id="page1_L51"></span>
               </td>
               <td align="center">
                  <span class="answer ELEMENTARY" id="page1_M51"></span>
               </td>
               <td align="center">
                  <span class="answer ELEMENTARY" id="page1_N51"></span>
               </td>
            </tr>
            <tr class="trHeight">
               <td colspan=2 class="bgGrayLabel">
                  SECONDARY
               </td>
               <td colspan="4">
                  <span class="answer SECONDARY" id="page1_C53"></span>
               </td>
               <td colspan="3">
                  <span class="answer SECONDARY" id="page1_F53">N/A</span>
               </td>
               <td align="center">
                  <span class="answer SECONDARY" id="page1_J53">N/A</span>
               </td>
               <td align="center">
                  <span class="answer SECONDARY" id="page1_K53"></span>
               </td>
               <td align="center">
                  <span class="answer SECONDARY" id="page1_L53"></span>
               </td>
               <td align="center">
                  <span class="answer SECONDARY" id="page1_M53"></span>
               </td>
               <td align="center">
                  <span class="answer SECONDARY" id="page1_N53"></span>
               </td>
            </tr>

            <?php
               $rsEmpEduc_[1] = $rsEmpEducVoca;
               $rsEmpEduc_[2] = $rsEmpEducColl;
               $rsEmpEduc_[3] = $rsEmpEducGrad;
               $x = 0;
               $rowEducCount = 0;
               $EmpEduc_split = false;
               if ($split["Educ"] != ".") {
                  $pds["rowEduc"] = explode(".",$split["Educ"])[1];
               }
               for ($k=1;$k<=3;$k++) {
                  $x = 0;
                  if ($rsEmpEduc_[$k]) {
                     $rowCount_EmpEduc = mysqli_num_rows($rsEmpEduc_[$k]);
                     //echo "<br>foo-->".$pds["rowEduc"];
                     while ($rowEduc = mysqli_fetch_assoc($rsEmpEduc_[$k])) { 
                        $x++;
                        $rowEducCount++;
                        if ($rowEducCount > $pds["rowEduc"]) {
                           $rowEducCount = 0;
                           $EmpEduc_split = true;
                           $pds["rowEduc"] = 30;
                           rowContinuation(14);
                           echo '</tbody></table>';
                           footer_signdate(false);
                           echo '</div>';
                           //echo "<br>foo-->".$pds["rowEduc"];
                           echo '<table border=1 cellspacing=0 cellpadding=0 title="page 1">';
                           educTHEAD();
                        } 
            ?>    
                        <tr class="trHeight">
                           <td colspan=2 class="bgGrayLabel">
                              <?php 
                                 if ($k == 1 && $x == 1) {  
                                    echo "VOCATIONAL/<br>TRADE COURSE";
                                 } else if ($k == 2 && $x == 1) { 
                                    echo "COLLEGE";
                                 } else if ($k == 3 && $x == 1) { 
                                    echo "GRADUATE STUDIES";
                                 } else {
                                    echo "&nbsp;";
                                 }  
                                 $dateto = $rowEduc["DateTo"];
                                 if ($dateto == 9999) {
                                    $dateto = "ON GOING";
                                 }
                              ?>
                           </td>
                           <td colspan="4">
                              <span class="answer" id="page1_voc_row<?php echo $rowEducCount; ?>_col1"><?php echo dispVal(getRecord("schools",$rowEduc["SchoolsRefId"],"Name")); ?></span>
                           </td>
                           <td colspan="3">
                              <span class="answer" id="page1_voc_row<?php echo $rowEducCount; ?>_col2"><?php echo dispVal(getRecord("course",$rowEduc["CourseRefId"],"Name")); ?></span>
                           </td>
                           <td align="center">
                              <span class="answer" id="page1_voc_row<?php echo $rowEducCount; ?>_col3"><?php echo dispVal($rowEduc["DateFrom"]); ?></span>
                           </td>
                           <td align="center">
                              <span class="answer" id="page1_voc_row<?php echo $rowEducCount; ?>_col4"><?php echo dispVal($dateto); ?></span>
                           </td>
                           <td align="center">
                              <span class="answer" id="page1_voc_row<?php echo $rowEducCount; ?>_col5"><?php echo dispVal($rowEduc["HighestGrade"]); ?></span>
                           </td>
                           <td align="center">
                              <span class="answer" id="page1_voc_row<?php echo $rowEducCount; ?>_col6"><?php echo dispVal($rowEduc["YearGraduated"]); ?></span>
                           </td>
                           <td align="center">
                              <span class="answer" id="page1_voc_row<?php echo $rowEducCount; ?>_col7"><?php echo dispVal($rowEduc["Honors"]); ?></span>
                           </td>
                        </tr>
            <?php  
                     }                      
                  } else {
                     $rowEducCount++;
                     if ($rowEducCount > $pds["rowEduc"]) {
                        $rowEducCount = 0;
                        $EmpEduc_split = true;
                        $pds["rowEduc"] = 30;
                        rowContinuation(14);
                        echo '</tbody></table>test_NA';
                        footer_signdate(false);
                        echo '</div>';
                        echo '<table border=1 cellspacing=0 cellpadding=0 title="page 1">';
                        educTHEAD();
                     } 
            ?>
                     <tr class="trHeight">
                           <td colspan=2 class="bgGrayLabel">
                              <?php 
                                 if ($k == 1) {  
                                    echo "VOCATIONAL/<br>TRADE COURSE";
                                 } else if ($k == 2) { 
                                    echo "COLLEGE";
                                 } else if ($k == 3) { 
                                    echo "GRADUATE STUDIES";
                                 } else {
                                    echo "&nbsp;";
                                 }  
                              ?>
                           </td>
                           <td colspan="4">
                              <span class="answer">N/A</span>
                           </td>
                           <td colspan="3">
                              <span class="answer">N/A</span>
                           </td>
                           <td align="center">
                              <span class="answer">N/A</span>
                           </td>
                           <td align="center">
                              <span class="answer">N/A</span>
                           </td>
                           <td align="center">
                              <span class="answer">N/A</span>
                           </td>
                           <td align="center">
                              <span class="answer">N/A</span>
                           </td>
                           <td align="center">
                              <span class="answer">N/A</span>
                           </td>
                        </tr>
               <?php         
                  }
               }
            ?>
            <?php 
               rowContinuation(14);
            ?>
         </tbody>
      </table>

<?php
   if ($EmpEduc_split && $EmpChild_split) {
      $nextpage = "";
   } 
   if (!$EmpEduc_split && $EmpChild_split) {
      $nextpage = "nextpage";
   }
   if ($EmpChild_split) {
      echo 
      '<div class="page-- '.$nextpage.'" id="page1_1" style="position:relative;">';
         if ($EmpChild_split) {
            echo 
            '<table border=1 cellspacing=0 cellpadding=0>
               <thead>
                  <tr>
                     <th colspan="2" class="bgGray tabTitle_td" align="left">
                        <span class="tabTitle">
                           <i>II. FAMILY BACKGROUND</i>
                        </span>
                     </th>
                  </tr>   
                  <tr>
                     <th class="bgGrayLabel" align="center" width:"60%">
                        23. NAME OF CHILD (Write full name and list all)
                     </th>
                     <th class="bgGrayLabel b-left" align="center" width:"40%">
                        DATE OF BIRTH (mm/dd/yyyy)
                     </th>
                  </tr>   
               </thead>
               <tbody>';
               $rsEmpChild2 = SelectEach("employeeschild","WHERE EmployeesRefId = $empRefId ORDER BY BirthDate DESC LIMIT ".$pds["rowChildren"].", ".($rowCount_EmpChild - $pds["rowChildren"]));
               while ($row = mysqli_fetch_assoc($rsEmpChild2)) {
                     $child_fullname = $row["FullName"];
                     $child_bday = $row["BirthDate"];
                     if (empty($child_fullname)) $child_fullname = "N/A";
                     if (empty($child_bday)) $child_bday = "N/A";
                  echo 
                  '<tr>
                     <td class="answer CHILDREN1" id="page1_1_childname'.$row["RefId"].'">'.strtoupper(dispVal($child_fullname)).'</td>
                     <td class="answer CHILDREN1" id="page1_1_childdate'.$row["RefId"].'" align="center">'.dispVal($child_bday).'</td>
                  </tr>';
               }
               echo          
               '</tbody>
            </table>';
         }
      //footer_signdate(false);
      //echo '</div>';
   }
?>
      <?php 
         footer_signdate(false);
      ?>
   </div>

   
