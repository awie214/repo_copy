<?php
   session_start();
   require_once 'constant.e2e.php';
   require_once pathClass.'0620functions.e2e.php';
   require_once "conn.e2e.php";
   $objName = getvalue("obj");
   $table = strtolower(getvalue("tbl"));
   $frameLocation = getvalue("frmloc");
   if (strtolower($table) != "schools") {
      $where = "ORDER BY Name";
      if ($table == "agency" || $table == "position" || $table == "empstatus") {
         if (getvalue("isGovt") == 1)
            $where = "WHERE isPrivate = 0 OR isPrivate IS NULL ORDER BY Name";
         else
            $where = "WHERE isPrivate = 1 ORDER BY Name";
      } else if ($table == "course") {
         $where = "WHERE Level = ".explode("_",$objName)[2]." ORDER BY Name";
      }

      $rs = SelectEach($table,$where);
      if ($rs) {
         echo
         '$("[name=\''.$objName.'\']").append( $(\'<option>\',{value:0,text:\'SELECT '.$table.'\'}));'."\n";
         while ($row = mysqli_fetch_assoc($rs)) {
            if ($row["Name"] != "") {
               $refid = $row['RefId'];
               $name = str_replace("\r","",$row['Name']);
               $name = rtrim($name,"\r");
               $name = strtoupper(mysqli_real_escape_string($conn,$name));
               $option = "$('<option>',{value:$refid,text:'$name'})";
               echo '$("[name=\''.$objName.'\']").append('.$option.');'."\n";
            }
         }
         echo "\n".'$("[name=\''.$objName.'\']").val("'.FindLast($table,"WHERE Name != ''","RefId").'");'."\n";
         if ($table == "seminars") {
            $result = "";
            $idx = explode("_", $objName)[2];
            $result = getRecord("seminars",FindLast($table,"WHERE Name != ''","RefId"),"Description");
            echo '$("[name=\'char_Description_'.$idx.'\']").val("'.$result.'");';
         }
      }
   }
?>


