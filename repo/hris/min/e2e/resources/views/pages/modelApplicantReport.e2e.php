<?php require_once "incUtilitiesJS.e2e.php"; ?>
<script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
<div class="mypanel">
   <div class="panel-top">
      Applicant's
   </div>
   <div class="panel-mid-litebg">
      <div class="row">
         <div class="col-xs-12">
            <div class="row">
               <div class="col-xs-2">
                  <span class="label txt-center">Applicant Name:</span>
               </div>
               <div class="col-xs-3">
                  <input type="text" class="form-input" name="LName" placeholder="Last Name" autofocus />
               </div>
               <div class="col-xs-3">
                  <input type="text" class="form-input" name="FName" placeholder="First Name" />
               </div>
               <div class="col-xs-2">
                  <input type="text" class="form-input" name="MidName" placeholder="Middle Name" />
               </div>
            </div>
            <div class="row">
               <div class="col-xs-2">
                  <span class="label txt-center">Civil Status</span>
               </div>
               <div class="col-xs-3 margin-top">
                  <select class="form-input" name="CvlStat">
                     <option value=""></option>
                     <option value="Si">Single</option>
                     <option value="Ma">Married</option>
                     <option value="An">Annulled</option>
                     <option value="Wi">Widowed</option>
                     <option value="Se">Separated</option>
                     <option value="Ot">Others, Please specify</option>
                  </select>
               </div>
               <div class="col-xs-4 margin-top">
                  <div class="form-group">
                     <label class="control-label" for="inputs">GENDER</label>&nbsp;
                     <?php createSelectGender("Gender","","style='width:55%;'");?>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-2">
                  <span class="label txt-center">Applicant Status</span>
               </div>
               <div class="col-xs-3">
                  <select class="form-input" name="CvlStat">
                     <option value=""></option>
                     <option value="Hired">Hired</option>
                     <option value="UnHired">Unhired</option>
                  </select>
               </div>
            </div>
            <?php bar();?>
            <div class="row">
               <div class="col-xs-1">
                  <span class="label">Range:</span>
               </div>
               <div class="col-xs-1">
                  <span class="label">From:</span>
               </div>
               <div class="col-xs-3">
                  <input type="text" class="form-input" name="">
               </div>
               <div class="col-xs-1">
                  <span class="label">To:</span>
               </div>
               <div class="col-xs-3">
                  <input type="text" class="form-input" name="">
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-2">
                  <span class="label" name="">For the Year</span>
               </div>
               <div class="col-xs-3">
                  <input type="text" class="form-input" name="">
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-2">
                  <span class="label" name="">For the Month</span>
               </div>
               <div class="col-xs-3">
                  <input type="text" class="form-input" name="">
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-2">
                  <span class="label" name="">As of Date</span>
               </div>
               <div class="col-xs-3">
                  <input type="text" class="form-input" name="">
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="panel-top">
      Show Column
   </div>
   <div class="panel-mid-litebg">
      <div class="row">
         <div class="col-xs-3">
            <input type="checkbox" />&nbsp;<span class="label">Check All</span>
            <?php bar();?>
         </div>
      </div>
      <div class="row margin-top">
         <div class="col-xs-3">
            <input type="checkbox" />&nbsp;<span class="label">Employee Name</span>
         </div>
         <div class="col-xs-3">
            <input type="checkbox" />&nbsp;<span class="label">Date of Birth</span>
         </div>
         <div class="col-xs-3">
            <input type="checkbox" />&nbsp;<span class="label">Place of Birth</span>
         </div>
         <div class="col-xs-3">
            <input type="checkbox" />&nbsp;<span class="label">Sex</span>
         </div>
      </div>
      <div class="row margin-top">
         <div class="col-xs-3">
            <input type="checkbox" />&nbsp;<span class="label">Civil Status</span>
         </div>
         <div class="col-xs-3">
            <input type="checkbox" />&nbsp;<span class="label">Citizenship</span>
         </div>
         <div class="col-xs-3">
            <input type="checkbox" />&nbsp;<span class="label">Height</span>
         </div>
         <div class="col-xs-3">
            <input type="checkbox" />&nbsp;<span class="label">Weight</span>
         </div>
      </div>
      <div class="row margin-top">
         <div class="col-xs-3">
            <input type="checkbox" />&nbsp;<span class="label">Blood Type</span>
         </div>
         <div class="col-xs-3">
            <input type="checkbox" />&nbsp;<span class="label">GSIS No.</span>
         </div>
         <div class="col-xs-3">
            <input type="checkbox" />&nbsp;<span class="label">PAGIBIG No.</span>
         </div>
         <div class="col-xs-3">
            <input type="checkbox" />&nbsp;<span class="label">Residential Address</span>
         </div>
      </div>
      <div class="row margin-top">
         <div class="col-xs-3">
            <input type="checkbox" />&nbsp;<span class="label">ResAdd Zip Code</span>
         </div>
         <div class="col-xs-3">
            <input type="checkbox" />&nbsp;<span class="label">ResAdd Telephone No.</span>
         </div>
         <div class="col-xs-3">
            <input type="checkbox" />&nbsp;<span class="label">Permanent Address</span>
         </div>
         <div class="col-xs-3">
            <input type="checkbox" />&nbsp;<span class="label">PerAdd Zip Code</span>
         </div>
      </div>
      <div class="row margin-top">
         <div class="col-xs-3">
            <input type="checkbox" />&nbsp;<span class="label">PerAdd Telephone No.</span>
         </div>
         <div class="col-xs-3">
            <input type="checkbox" />&nbsp;<span class="label">E-mail Address</span>
         </div>
         <div class="col-xs-3">
            <input type="checkbox" />&nbsp;<span class="label">Mobile No.</span>
         </div>
         <div class="col-xs-3">
            <input type="checkbox" />&nbsp;<span class="label">Agency Employee No.</span>
         </div>
      </div>
      <div class="row margin-top">
         <div class="col-xs-3">
            <input type="checkbox" />&nbsp;<span class="label">TIN No.</span>
         </div>
         <div class="col-xs-3">
            <input type="checkbox" />&nbsp;<span class="label">PHILHEALTH No.</span>
         </div>
      </div>
   </div>
</div>