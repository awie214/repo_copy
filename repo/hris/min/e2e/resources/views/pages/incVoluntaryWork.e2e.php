<div class="mypanel">
   <div class="panel-top" for="VoluntaryWork">
      <div class="row txt-center">
         <div class="col-xs-4">
            NAME AND ADDRESS OF ORGANIZATION<BR>
            (write in full)
         </div>
         <div class="col-xs-3">
            <div class="row">
               INCLUSIVE DATES<br>
               (mm/dd/yyyy)
            </div>
            <div class="row">
               <div class="col-xs-6">FROM</div>
               <div class="col-xs-6">TO</div>
            </div>
         </div>
         <div class="col-xs-1">
            NUMBER OF HOURS
         </div>
         <div class="col-xs-4">
            POSITION NATURE OF WORK
         </div>
      </div>
   </div>
   <div class="panel-mid-litebg" id="VoluntaryWork">
      <?php

         $createEntry = 0;
         if (getvalue("hUserGroup") == "COMPEMP") {
            $rs = SelectEach("employeesvoluntary","WHERE CompanyRefId = $CompanyId AND BranchRefId = $BranchId AND EmployeesRefId = $EmployeesRefId ORDER BY StartDate DESC, RefId DESC");
            $j = 0;
            if ($rs) {
               $createEntry = mysqli_num_rows($rs);
            } else {
               $createEntry = 1;
            }
         } else {
            $createEntry = 1;
         }
         $disabled = "disabled";
         for ($j=1;$j<=$createEntry;$j++) {

      ?>
      <div id="EntryVolWork_<?php echo $j; ?>" class="entry201">
         <input type="checkbox" id="VolWork_<?php echo $j; ?>" name="chkVolWork_<?php echo $j; ?>" class="enabler-- voluntary_fpreview" refid="" 
         fldName="bint_OrganizationRefId_<?php echo $j; ?>,
         date_StartDate_<?php echo $j; ?>,
         date_EndDate_<?php echo $j; ?>,
         sint_NumofHrs_<?php echo $j; ?>,
         char_WorksNature_<?php echo $j; ?>" 
         idx="<?php echo $j; ?>">
         <input type="hidden" name="voluntaryRefId_<?php echo $j; ?>" >
         <label for="VolWork_<?php echo $j; ?>" class="btn-cls2-sea"><b>EDIT VOLUNTARY WORK #<?php echo $j; ?></b></label>
         <div class="row margin-top">
            <div class="col-xs-4 txt-center">
               <?php
                  createSelect("Organization",
                               "bint_OrganizationRefId_".$j,
                               "",100,"Name","Select Organization",$disabled)
               ?>
            </div>
            <div class="col-xs-3">
               <div class="row">
                  <div class="pull-left" style="margin-right:5px;">
                     <input type="text" class="form-input date-- saveFields-- valDate-- dateRange" tabname="Voluntary Work" placeholder="Start Date"
                     name="date_StartDate_<?php echo $j; ?>" id="VStartDate_<?php echo $j; ?>" <?php echo $disabled; ?> readonly>
                  </div>
                  <div class="pull-left" style="margin-right:5px;">
                     <input type="text" class="form-input date-- saveFields-- valDate-- dateRange" tabname="Voluntary Work"
                     name="date_EndDate_<?php echo $j; ?>" placeholder="End Date" id="VEndDate_<?php echo $j; ?>" <?php echo $disabled; ?> readonly>
                  </div>
                  <div class="pull-left">
                     <?php doChkPresent($j,7); ?>
                  </div>
               </div>
            </div>
            <div class="col-xs-1">
               <input type="text" class="form-input number-- saveFields-- number--" tabname="Voluntary Work" placeholder="Hrs.?"
               name="sint_NumofHrs_<?php echo $j; ?>" <?php echo $disabled; ?>>
            </div>
            <div class="col-xs-4">
               <input type="text" class="form-input saveFields-- uCase--" tabname="Voluntary Work" placeholder="Nature of Work"
               name="char_WorksNature_<?php echo $j; ?>" <?php echo $disabled; ?>>
            </div>
         </div>
      </div>
      <?php } ?>
   </div>
   <div class="panel-bottom bgSilver"><a href="javascript:void(0);" class="addRow" id="addRowVolWork">Add Row</a></div>
</div>
<script type="text/javascript">
   $("#VoluntaryWork [name*='bint_OrganizationRefId_'], [id*='VStartDate_'], [id*='VEndDate_']").addClass("mandatory");
   $("#VoluntaryWork .saveFields--").attr("tabname","Voluntay Work");
</script>