   <link rel="icon" href="../../../public/images/2/pcclogo_circle.png" type="image/gif" sizes="16x16">
   <title>(HRIS) E2E Solutions Management Phils.</title>
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
   <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <meta name="Author" content="E2E Solutions Management Phils.">
   <meta name="Programmer" content="EBMendoza">

   <?php
      include_once 'constant.e2e.php';
      include_once pathClass.'SysFunctions.e2e.php';
      $dbg = true;
      $sys = new SysFunctions();
      $local = 1;

      if ($local) { ?>
      <link href="<?php echo $sys->publicPath("css/font-awesome/css/font-awesome.min.css"); ?>" rel="stylesheet" type="text/css">
      <script type="text/javascript" src="<?php echo $sys->publicPath("jquery/jquery.js"); ?>"></script>
      <script type="text/javascript" src="<?php echo $sys->publicPath("js/angular.min.js"); ?>"></script>
      <link rel="stylesheet" href="<?php echo $sys->publicPath("bs/css/bootstrap.min.css"); ?>" crossorigin="anonymous">
      <script src="<?php echo $sys->publicPath("bs/js/bootstrap.js"); ?>"></script>

      <!-- DATE PICKER -->
      <!--
      <link href="<?php //echo $sys->publicPath("jqueryui/jquery-ui.css"); ?>" rel="stylesheet">
      <script type="text/javascript" src="<?php //echo $sys->publicPath("jqueryui/jquery-ui.js"); ?>"></script>

      <script type="text/javascript" src="<?php //echo $sys->publicPath("js/jquery.plugin.min.js"); ?>"></script>
      <script type="text/javascript" src="<?php //echo $sys->publicPath("js/jquery.datepick.js"); ?>"></script>
      -->
      <link href="<?php echo $sys->publicPath("datatables/jquery.dataTables.min.css"); ?>" rel="stylesheet">
      <script type="text/javascript" src="<?php echo $sys->publicPath("datatables/jquery.dataTables.min.js"); ?>"></script>
      <script type="text/javascript" src="<?php echo $sys->publicPath("js/notify.js"); ?>"></script>
   <?php } else { ?>
      <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
      <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
   <?php } ?>

   <?php 
      if (
            !isset($_SESSION["0620utilities"]) || 
            !isset($_SESSION["0620functions"]) ||
            !isset($_SESSION["0620sys"]) ||
            !isset($_SESSION["load"]) ||
            !isset($_SESSION["0620SystemRoute"]) ||
            !isset($_SESSION["jquery_utilities"]) ||
            !isset($_SESSION["cssFilePath"]) ||
            !isset($_SESSION["tip"]) 
         ) 
      {
         $sessExpired = fopen($sys->publicPath("syslog/sessExpired_".date("Ymd",time()).".log"), "a+");
         fwrite($sessExpired, "Session: ".$session." ----- hEmpRefId: ".getvalue("hEmpRefId")."----- hUserRefId: ".getvalue("hUserRefId"));
         header ("Location:idx.e2e.php?sessExpired=yes&msg=Undefined Index...");
      }
   ?>

   <script type="text/javascript" src="<?php echo $_SESSION["0620utilities"]; ?>?t=<?php echo md5(time()); ?>"></script>
   <script type="text/javascript" src="<?php echo $_SESSION["0620functions"]; ?>?t=<?php echo md5(time()); ?>"></script>
   <script type="text/javascript" src="<?php echo $_SESSION["0620sys"]; ?>?t=<?php echo md5(time()); ?>"></script>
   <script type="text/javascript" src="<?php echo $_SESSION["load"]; ?>?t=<?php echo md5(time()); ?>"></script>
   <script type="text/javascript" src="<?php echo $_SESSION["0620SystemRoute"]; ?>?t=<?php echo md5(time()); ?>"></script>
   <script type="text/javascript" src="<?php echo $_SESSION["jquery_utilities"]; ?>?t=<?php echo md5(time()); ?>"></script>
   <script type="text/javascript" src="<?php echo path."js/jsSHAver2/src/sha.js" ?>?t=<?php echo md5(time()); ?>"></script>
   <script type="text/javascript" src="<?php echo path."js/modal.js" ?>?t=<?php echo md5(time()); ?>"></script>
   <link rel="stylesheet" href="<?php echo $_SESSION["cssFilePath"]; ?>">
   <link rel="stylesheet" href="<?php echo $_SESSION["tip"]; ?>">

   <link href="<?php echo $sys->publicPath("datepicker/css/datepicker.css"); ?>" rel="stylesheet">
   <script type="text/javascript" src="<?php echo $sys->publicPath("datepicker/js/bootstrap-datepicker.js"); ?>"></script>
   
   <script type="text/javascript">
      setSession("auth","<?php echo getvalue("auth"); ?>");
      <?php
         if ($dbg) {
            if (getvalue("hUser") == "") {
               echo "alert('No User Assigned [getval]');";
            }
         }
      ?>
   </script>
   <?php
      //$rs = mysqli_query($conn,"SELECT `PatternLogo` FROM `company`");
      //if ($rs) {
         $patternLogo = "whitebg.jpg";
         if ($patternLogo != "") {
            echo '<style>';
            echo 'body {background-image:url("'.img($patternLogo).'");}';
            echo '</style>';
         }
      //}

   ?>



<?php
if (!$dbg) {
   echo
   '<script type = "text/javascript">
      history.pushState(null, null, \'pagename\');
      window.addEventListener(\'popstate\', function(event) {
                                             history.pushState(null, null, \'pagename\');
                                            });
   </script>
   ';
}
?>

