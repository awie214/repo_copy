<?php 
	include 'FnUpload.php';
	$count = 0;
	$file = fopen("csv/35/emp_info.csv", "r");
	while(!feof($file)) {
		$str = fgets($file);
		$row = explode(",", $str);
		if (count($row) > 1) {
			$count++;
			$employee_fld 		= "";
			$employee_val 		= "";
			$empinfo_fld 		= "";
			$empinfo_val 		= "";
			$AgencyId 		= clean($row[0]);
			$LastName 		= clean($row[1]);
			$FirstName 		= clean($row[2]);
			$Position 		= clean($row[3]);
			$Office 		= clean($row[4]);
			$Division 		= clean($row[5]);
			$Unit 			= clean($row[6]);
			$PosItem 		= clean($row[7]);
			$AssumptionDate = clean($row[8]);
			$SalaryGrade	= clean($row[9]);
			$StepIncrement	= clean($row[10]);
			$SalaryAmount	= clean($row[11]);
			$EmpStatus 		= clean($row[12]);



			if ($AgencyId != "") {
				$employee_fld .= "`AgencyId`,";
				$employee_val .= "'$AgencyId',";
			}
			if ($LastName != "") {
				$employee_fld .= "`LastName`,";
				$employee_val .= "'$LastName',";
			}
			if ($FirstName != "") {
				$employee_fld .= "`FirstName`,";
				$employee_val .= "'$FirstName',";
			}
			if ($SalaryAmount != "") {
				$empinfo_fld .= "`SalaryAmount`,";
				$empinfo_val .= "'$SalaryAmount',";
			}

			if ($Position != "") {
				$PositionRefId = saveFM("Position","Name, ","'$Position', ",$Position);
				$empinfo_fld .= "`PositionRefId`,";
				$empinfo_val .= "'$PositionRefId',";
			}

			if ($Office != "") {
				$OfficeRefId = saveFM("Office","Name, ","'$Office', ",$Office);
				$empinfo_fld .= "`OfficeRefId`,";
				$empinfo_val .= "'$OfficeRefId',";
			}

			if ($Division != "") {
				$DivisionRefId = saveFM("Division","Name, ","'$Division', ",$Division);
				$empinfo_fld .= "`DivisionRefId`,";
				$empinfo_val .= "'$DivisionRefId',";
			}

			if ($SalaryGrade != "") {
				$SalaryGradeRefId = saveFM("SalaryGrade","Name, ","'$SalaryGrade', ",$SalaryGrade);
				$empinfo_fld .= "`SalaryGradeRefId`,";
				$empinfo_val .= "'$SalaryGradeRefId',";
			}

			if ($StepIncrement != "") {
				$StepIncrementRefId = saveFM("StepIncrement","Name, ","'$StepIncrement', ",$StepIncrement);
				$empinfo_fld .= "`StepIncrementRefId`,";
				$empinfo_val .= "'$StepIncrementRefId',";
			}

			$emprefid = FindFirst("employees","WHERE AgencyId = '$AgencyId'","RefId",$conn);
			if ($emprefid) {
				$empinfo_fld .= "`EmployeesRefId`,";
				$empinfo_val .= "'$emprefid',";
				$save_empinfo = save("empinformation",$empinfo_fld,$empinfo_val);
				if (is_numeric($save_empinfo)) {
					$count++;
					echo "$count. $emprefid added employee info.<br>";
				}
			} else {
				echo "Cannot Find $AgencyId.<br>";
			}

		}
	}
?>