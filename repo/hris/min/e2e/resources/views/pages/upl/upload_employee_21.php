<?php
	include 'FnUpload.php';
	include '../conn.e2e.php';
	$file = fopen("csv/employee_21.csv", "r");
	mysqli_query($conn,"TRUNCATE employees");
	mysqli_query($conn,"TRUNCATE empinformation");
	mysqli_query($conn,"TRUNCATE pms_employees");
	mysqli_query($conn,"TRUNCATE pms_employee_information");
	mysqli_query($conn,"TRUNCATE pms_salaryinfo");
	while(!feof($file)) {
		$str = fgets($file);
		$employee_fld 			= "CompanyRefId, BranchRefId, ";
		$employee_val 			= "21, 1,";
		$empinfo_fld 			= "CompanyRefId, BranchRefId, ";
		$empinfo_val 			= "21, 1,";
		$pms_employee_fld 		= "company_id, branch_id, ";
		$pms_employee_val 		= "21, 1,";
		$pms_empinfo_fld 		= "";
		$pms_empinfo_val 		= "";
		$pms_salaryinfo_fld 	= "";
		$pms_salaryinfo_val 	= "";
		$str_arr 				= explode(",", $str);
		$employee_number 		= trim($str_arr[0]);
		$lastname 				= trim($str_arr[1]);
		$firstname 				= trim($str_arr[2]);
		$middlename 			= trim($str_arr[3]);
		$extname 				= trim($str_arr[4]);
		$position 				= trim($str_arr[5]);
		$division 				= trim($str_arr[6]);
		$office 				= trim($str_arr[7]);
		$plantilla 				= trim($str_arr[8]);
		$salarygrade 			= trim($str_arr[9]);
		$stepincrement 			= trim($str_arr[10]);
		$basic1 				= trim($str_arr[11]);
		$basic2 				= trim($str_arr[12]);
		$empstatus 				= trim($str_arr[13]);
		$basic1 				= floatval($basic1);
		$basic2 				= floatval($basic2);
		$salaryamount 			= floatval($basic1) + floatval($basic2);


		$employee_fld 			.= "LastName, FirstName, MiddleName, ExtName, AgencyId, ";
		$employee_val 			.= "'$lastname', '$firstname', '$middlename', '$extname', '$employee_number', ";
		$pms_employee_fld 		.= "lastname, firstname, middlename, extension_name, employee_number, active, ";
		$pms_employee_val 		.= "'$lastname', '$firstname', '$middlename', '$extname', '$employee_number', 1, ";
		if ($empstatus != "") {
			$empstatus 				= FindFirst("empstatus","WHERE Name = '$empstatus'","RefId",$conn);
			$empinfo_fld 			.= "EmpStatusRefId, ";
			$empinfo_val 			.= "'$empstatus', ";
			$pms_empinfo_fld 		.= "employee_status_id, ";
			$pms_empinfo_val 		.= "'$empstatus', ";
			$pms_salaryinfo_fld 	.= "employee_status_id, ";
			$pms_salaryinfo_val 	.= "'$empstatus', ";
		}
		if ($position != "") {
			$position 		= FindFirst("position","WHERE Name = '$position'","RefId",$conn);
			$empinfo_fld 			.= "PositionRefId, ";
			$empinfo_val 			.= "'$position', ";
			$pms_empinfo_fld 		.= "position_id, ";
			$pms_empinfo_val 		.= "'$position', ";
			$pms_salaryinfo_fld 	.= "position_id, ";
			$pms_salaryinfo_val 	.= "'$position', ";
		}
		if ($division != "") {
			$division 		= FindFirst("division","WHERE Name = '$division'","RefId",$conn);
			$empinfo_fld 			.= "DivisionRefId, ";
			$empinfo_val 			.= "'$division', ";
			$pms_empinfo_fld 		.= "division_id, ";
			$pms_empinfo_val 		.= "'$division', ";
			//$pms_salaryinfo_fld 	.= "division_id, ";
			//$pms_salaryinfo_val 	.= "'$division', ";
		}
		if ($plantilla != "") {
			$plantilla 				= FindFirst("positionitem","WHERE Name = '$plantilla'","RefId",$conn);
			$empinfo_fld 			.= "PositionItemRefId, ";
			$empinfo_val 			.= "'$plantilla', ";
			$pms_empinfo_fld 		.= "position_item_id, ";
			$pms_empinfo_val 		.= "'$plantilla', ";
			$pms_salaryinfo_fld 	.= "positionitem_id, ";
			$pms_salaryinfo_val 	.= "'$plantilla', ";
		}
		if ($salarygrade != "") {
			$salarygrade 	= FindFirst("salarygrade","WHERE Name = '$salarygrade'","RefId",$conn);
			$empinfo_fld 			.= "SalaryGradeRefId, ";
			$empinfo_val 			.= "'$salarygrade', ";
			//$pms_empinfo_fld 		.= "salarygrade_id, ";
			//$pms_empinfo_val 		.= "'$salarygrade', ";
			$pms_salaryinfo_fld 	.= "salarygrade_id, ";
			$pms_salaryinfo_val 	.= "'$salarygrade', ";
		}
		if ($stepincrement != "") {
			$stepincrement 	= FindFirst("stepincrement","WHERE Name = '$stepincrement'","RefId",$conn);
			$empinfo_fld 			.= "StepIncrementRefId, ";
			$empinfo_val 			.= "'$stepincrement', ";
			$pms_salaryinfo_fld 	.= "step_inc, ";
			$pms_salaryinfo_val 	.= "'$stepincrement', ";
		}
		$empinfo_fld 			.= "SalaryAmount, SalaryAmountTwo,  ";
		$empinfo_val 			.= "'$basic1', '$basic2',";
		$pms_salaryinfo_fld 	.= "basic_amount_one, basic_amount_two, ";
		$pms_salaryinfo_val		.= "'$basic1', '$basic2',";
		$emprefid = save("employees",$employee_fld,$employee_val);
		if (is_numeric($emprefid)) {
			echo "$employee_number added to employees.<br>";
			$empinfo_fld .= "EmployeesRefId, ";
			$empinfo_val .= "'$emprefid',";
			$save_empinfo = save("empinformation",$empinfo_fld,$empinfo_val);
			if (is_numeric($save_empinfo)) {
				echo "$employee_number added to empinfo.<br>";
				$save_pms_employee = savePMS("pms_employees",$pms_employee_fld,$pms_employee_val);
				if (is_numeric($save_pms_employee)) {
					echo "$employee_number added to pms employees.<br>";
					$pms_empinfo_fld 	.= "employee_id, ";
					$pms_empinfo_val 	.= "'$save_pms_employee', ";
					$pms_salaryinfo_fld .= "employee_id, ";
					$pms_salaryinfo_val .= "'$save_pms_employee', ";
					$save_pms_empinfo 	= savePMS("pms_employee_information",$pms_empinfo_fld,$pms_empinfo_val);
					if (is_numeric($save_pms_empinfo)) {
						echo "$employee_number added to pms empinfo.<br>";
					} else {
						echo "error is saving in pms empinfo for $lastname.<br>";
						return false;	
					}
					$save_pms_salaryinfo 	= savePMS("pms_salaryinfo",$pms_salaryinfo_fld,$pms_salaryinfo_val);
					if (is_numeric($save_pms_salaryinfo)) {
						echo "$employee_number added to pms salaryinfo.<br>";
					} else {
						echo "error is saving in pms salaryinfo for $lastname.<br>";
						return false;	
					}
				} else {
					echo "error is saving in pms employee for $lastname.<br>";
					return false;
				}
			} else {
				echo "error is saving in empinfo for $lastname.<br>";
				return false;
			}
		} else {
			echo "error is saving $lastname.<br>";
			return false;
		}
		echo "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-<br>";
		/*if ($position != "") {
			$position = saveFM("position","`Name`, ","'$position', ",$position);
			$plantilla_fld .= "PositionRefId, ";
			$plantilla_val .= "'$position', ";
		}
		if ($division != "") {
			$division = saveFM("division","`Name`, ","'$division', ",$division);
			$plantilla_fld .= "DivisionRefId, ";
			$plantilla_val .= "'$division', ";
		}
		if ($salarygrade != "") {
			$salarygrade = saveFM("salarygrade","`Name`, ","'$salarygrade', ",$salarygrade);
			$plantilla_fld .= "SalaryGradeRefId, ";
			$plantilla_val .= "'$salarygrade', ";
		}
		if ($stepincrement != "") {
			$stepincrement = saveFM("stepincrement","`Name`, ","'$stepincrement', ",$stepincrement);
			$plantilla_fld .= "StepIncrementRefId, ";
			$plantilla_val .= "'$stepincrement', ";
		}
		if ($plantilla != "") {
			$plantilla_fld .= "Name, SalaryAmount, ";
			$plantilla_val .= "'$plantilla', '$salaryamount', ";
			$save_plantilla = save("positionitem",$plantilla_fld,$plantilla_val);
			if (is_numeric($save_plantilla)) {
				echo "Successfully Added to Plantilla<br>";
			} else {
				echo "Error in adding in Plantilla<br>";
				return false;
			}
		}*/
		// echo "lastname: $lastname -> firstname: $firstname -> middlename: $middlename -> extname: $extname<br>";
		// echo "position: $position -> division: $division -> office: $office -> plantilla: $plantilla<br>";
		// echo "salarygrade: $salarygrade -> stepincrement: $stepincrement -> basic1: $basic1 -> basic2: $basic2 -> empstatus: $empstatus<br>";
		// echo "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-<br>";
	}
?>