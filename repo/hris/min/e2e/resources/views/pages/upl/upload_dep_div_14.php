
<?php
	//Employee ID,Office,Department,Position,Emp Status
	include 'FnUpload.php';
	$file = fopen("14/dep_div_perma.csv", "r");
	while(!feof($file)) {
		$row 		= explode(",", fgets($file));
		$emp_no 	= trim($row[0]);
		$office 	= trim($row[1]);
		$department = trim($row[2]);
		$position 	= trim($row[3]);
		$empstatus  = trim($row[4]);
		if ($office != "") {
			$office_id  	= saveFM("office","`Name`, ","'$office', ",$office);	
		} else {
			$office_id = 0;
		}
		if ($department != "") {
			$division_id  	= saveFM("division","`Name`, ","'$department', ",$department);	
		} else {
			$division_id 	= 0;
		}
		if ($position != "") {
			$position_id  	= saveFM("position","`Name`, ","'$position', ",$position);	
		} else {
			$position_id = 0;
		}
		if ($empstatus != "") {
			$empstatus_id  	= saveFM("empstatus","`Name`, ","'$empstatus', ",$empstatus);	
		} else {
			$empstatus_id = 0;
		}
		
		/*
		"DivisionRefId":"division_id",
		"DepartmentRefId":"department_id",
		"OfficeRefId":"office_id",
		"UnitsRefId":"unit_id",
		"EmpStatusRefId":"employee_status_id",
		"PositionItemRefId":"position_item_id",
		"PositionRefId":"position_id",
		*/
		$fldnval = "DivisionRefId = '$division_id', OfficeRefId = '$office_id', EmpStatusRefId = '$empstatus_id', PositionRefId = '$position_id', ";
		$fldnval2 = "division_id = '$division_id', office_id = '$office_id', employee_status_id = '$empstatus_id', position_id = '$position_id'";

		$emprefid = FindFirst("employees","WHERE AgencyId = '$emp_no'","RefId",$conn);
		if (is_numeric($emprefid)) {
			$empinfo_id = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","RefId",$conn);
			$pms_empid = FindFirst("pms_employees","WHERE employee_number = '$emp_no'","id",$conn);
			if (is_numeric($pms_empid)) {
				$update_salaryinfo = "UPDATE pms_employee_information SET $fldnval2 WHERE employee_id = '$pms_empid'";
				$result_pms = mysqli_query($conn,$update_salaryinfo);
				if ($result_pms) {
					echo "PMS Info Updated for $emp_no.<br>";
				}
			}
			if (is_numeric($empinfo_id)) {
				$result = update("empinformation",$fldnval,$empinfo_id);
				if ($result == "") {
					echo "Emp info Updated for $emp_no.<br>";
				}
			}	
		}
	}
?>