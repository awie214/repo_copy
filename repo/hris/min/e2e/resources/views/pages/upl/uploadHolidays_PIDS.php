<!DOCTYPE html>
<html>
<body onload="alert('Done');">

<?php
require "constant.e2e.php";
require "conn.e2e.php";
require_once pathClass."0620functions.e2e.php";

            $maintable = "holidays";
            $conn->query("TRUNCATE TABLE `holiday`;");

            $t = time();
            $date_today    = date("Y-m-d",$t);
            $curr_time     = date("H:i:s",$t);
            $trackingA_fld = "`LastUpdateDate`, `LastUpdateTime`, `LastUpdateBy`, `Data`";
            $trackingA_val = "'$date_today', '$curr_time', 'PHP', 'M'";

            $sql = "SELECT * FROM `$maintable`";
            $rs = mysqli_query($pids_conn,$sql) or die(mysqli_error($pids_conn));
            echo "Number Records : ".mysqli_num_rows($rs)."<br>";
            if ($rs) {
               if (mysqli_num_rows($rs) > 0) {
                  while ($pids = mysqli_fetch_array($rs)) {
                     $refid = $pids["id"];
                     $Name = remquote($pids["reason"]);
                     $StartDate = $pids["date"];
                     $EndDate = $pids["date"];
                     if ($pids["ishalfday"] == 1) {
                        $StartTime = "00:01:00";
                        $EndTime = "12:00:00";
                     } else {
                        $StartTime = "00:01:00";
                        $EndTime = "23:59:00";
                     }
                     $isLegal = 0;
                     $isApplyEveryYr = 0;

                     $flds   = "`CompanyRefId`,`BranchRefId`,`RefId`,`Name`,`Remarks`,`StartDate`,`EndDate`,`StartTime`,`EndTime`,".$trackingA_fld;
                     $values = "1000,1,'$refid','$Name','Migrated','$StartDate','$EndDate','$StartTime','$EndTime',".$trackingA_val;
                     $sql = "INSERT INTO `holiday` ($flds) VALUES ($values)";
                     if ($conn->query($sql) === TRUE) {
                        echo "Migrated $maintable -->$refid<br>";
                     }
                     else {
                        echo "ERROR Migration $maintable -->$refid<br>";
                     }

                  }
               }
            }
            mysqli_close($conn);
?>
</body>
</html>