<!DOCTYPE html>
<html>
<body onload="alert('Done');">

<?php
require "constant.e2e.php";
require "conn.e2e.php";
require_once pathClass."0620functions.e2e.php";

            $conn->query("TRUNCATE TABLE `employeesotherinfo`;");


            $t = time();
            $date_today    = date("Y-m-d",$t);
            $curr_time     = date("H:i:s",$t);
            $trackingA_fld = "`LastUpdateDate`, `LastUpdateTime`, `LastUpdateBy`, `Data`";
            $trackingA_val = "'$date_today', '$curr_time', 'PHP', 'M'";
            $i = 1;
            $sql = "SELECT * FROM `employee_hobbies`";
            $rs = mysqli_query($pids_conn,$sql) or die(mysqli_error($pids_conn));
            echo "Number Records : ".mysqli_num_rows($rs)."<br>";
            if ($rs) {
               if (mysqli_num_rows($rs) > 0) {
                  while ($pids = mysqli_fetch_array($rs)) {
                     $refid = $pids["employee_personal_information_sheet_id"];
                     $empRefId = $refid;
                     $wSkills = $pids['hobbies'];
                     $flds = "";
                     $values = "";
                     $wAfflliates = "";
                     $wRecognition = "";
                     
                     $flds .= "`CompanyRefId`,`BranchRefId`,`EmployeesRefId`,`Skills`,`Remarks`,`Affiliates`,`Recognition`,".$trackingA_fld;
                     $values .= "1000,1,'$empRefId','$wSkills','Migrated','$wAfflliates','$wRecognition',".$trackingA_val;
                     $sql = "INSERT INTO `employeesotherinfo` ($flds) VALUES ($values)";
                     if ($conn->query($sql) === TRUE) {
                        echo $i++." Migrated Skills -->$empRefId<br>";
                     }
                     else {
                        echo "ERROR Migration Skills -->$empRefId<br>";
                     }
                     
                  }
               }
            }
            $sql = "SELECT * FROM employee_member_organizations";
            $rs = mysqli_query($pids_conn,$sql) or die(mysqli_error($pids_conn));
            echo "Number Records : ".mysqli_num_rows($rs)."<br>";
            if ($rs){
               if (mysqli_num_rows($rs) > 0) {
                  while ($pids = mysqli_fetch_array($rs)) {
                     $refid = $pids["employee_personal_information_sheet_id"];
                     $empRefId = $refid;
                     
                     $wAfflliates = remquote($pids['member_organization']);
                     
                     $isExist = FindFirst("employeesotherinfo","WHERE `EmployeesRefId` = $empRefId AND `Affiliates` = '' AND `Skills` != '' ","RefId");
                     if ($isExist){
                        $sql = "UPDATE employeesotherinfo SET `Affiliates` = '$wAfflliates' WHERE `EmployeesRefId` = $empRefId AND `Affiliates` = '' AND `Skills` != '' LIMIT 1";
                        if ($conn->query($sql) === TRUE) {
                           echo $i++." Updated Affiliates -->$empRefId<br>";
                        }
                     }else {
                        $wSkills = "";
                        $wRecognition = "";
                        $flds = "`CompanyRefId`,`BranchRefId`,`EmployeesRefId`,`Skills`,`Remarks`,`Affiliates`,`Recognition`,".$trackingA_fld;
                        $values = "1000,1,'$empRefId','$wSkills','Migrated','$wAfflliates','$wRecognition',".$trackingA_val;
                        $sql = "INSERT INTO `employeesotherinfo` ($flds) VALUES ($values)";
                        if ($conn->query($sql) === TRUE) {
                           echo $i++." Insert Affiliates -->$empRefId<br>";
                        }
                        else {
                           echo "ERROR Insert Affiliates -->$empRefId<br>";
                        }
                     }


                     
                  }
               }
            }
            
            $sql = "SELECT * FROM employee_non_academic_distinctions";
            $rs = mysqli_query($pids_conn,$sql) or die(mysqli_error($pids_conn));
            echo "Number Records : ".mysqli_num_rows($rs)."<br>";
            if ($rs){
               if (mysqli_num_rows($rs) > 0) {
                  while ($pids = mysqli_fetch_array($rs)) {
                     $refid = $pids["employee_personal_information_sheet_id"];
                     $empRefId = $refid;
                     
                     $wRecognition = remquote($pids['title']);
                     
                     $isExist = FindFirst("employeesotherinfo","WHERE `EmployeesRefId` = $empRefId AND `Recognition` = '' AND `Skills` != '' AND `Affiliates` != '' ","RefId");
                     if ($isExist){
                        $sql = "UPDATE employeesotherinfo SET `Recognition` = '$wRecognition' WHERE `EmployeesRefId` = $empRefId AND `Recognition` = '' AND `Skills` != '' AND `Affiliates` != '' LIMIT 1";
                        if ($conn->query($sql) === TRUE) {
                           echo $i++." Updated Recognition -->$empRefId<br>";
                        }
                     }else {
                        $wSkills = "";
                        $wAfflliates = "";
                        $flds = "`CompanyRefId`,`BranchRefId`,`EmployeesRefId`,`Skills`,`Remarks`,`Affiliates`,`Recognition`,".$trackingA_fld;
                        $values = "1000,1,'$empRefId','$wSkills','Migrated','$wAfflliates','$wRecognition',".$trackingA_val;
                        $sql = "INSERT INTO `employeesotherinfo` ($flds) VALUES ($values)";
                        if ($conn->query($sql) === TRUE) {
                           echo $i++." Insert Recognition -->$empRefId<br>";
                        }
                        else {
                           echo "ERROR Insert Recognition -->$empRefId<br>";
                        }
                     }


                     
                  }
               }
            }
            
            
            mysqli_close($conn);
            


?>

</body>
</html>