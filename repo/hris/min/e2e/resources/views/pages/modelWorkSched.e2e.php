<?php
   $mainTable = "workschedule";
   require_once "constant.e2e.php";
   require_once "inc_model.e2e.php";
   require_once "incUtilitiesJS.e2e.php";;
?>
<script>
   $(document).ready(function () {
      $("#schedTypeFix").click(function () {
         $("#schedType").val("Fi");
         $(".WholeDay--").val("08:00");
         $(".HalfDay--").val("04:00");
      });
      $("#schedTypeFlexi").click(function () {
         $("#schedType").val("Fl");
         $(".WholeDay--").val("08:00");
         $(".HalfDay--").val("04:00");
      });
      $("#schedTypeCompress").click(function () {
         $("#schedType").val("Co");
         $(".WholeDay--").val("10:00");
         $(".HalfDay--").val("05:00");
      });
      $(".chkbox--").each(function () {
         $(this).click(function () {
            if ($(this).is(":checked")){
               $(this).val(1);
            }else {
               $(this).val(0);
            }
         });
      });
      $("#AutoLB").click(function () {
         if ($(this).is(":checked")) {
            $("[name='sint_AutoLB']").val(1);
         } else {
            $("[name='sint_AutoLB']").val(0);
         }
      });
   });

</script>
<div class="row" id="EntryScrn">
   <div class="col-xs-12" id="div_CONTENT">
      <?php if ($ScrnMode != 1) { ?>
         <div class="row">
            <ul class="nav nav-pills">
               <li class="active" style="font-size:12pt;font-weight:600;margin-left:10px;">
                  <a>REFID : <span class="badge" style="font-size:12pt;font-weight:600;" id="idRefid">
                  <?php echo $refid; ?>
                  </span></a>
               </li>
            </ul>
         </div>
      <?php } ?>
      <div class="padd10 margin-top10">
            <div class="row margin-top">
               <div class="col-xs-2 txt-right">
                  <span class="label">Schedule Name:</span>
               </div>
               <div class="col-xs-3">
                  <input type="text" class="form-input saveFields-- mandatory" name="char_Name" autofocus>
               </div>
               <div class="col-xs-3">
                  <select class="form-input saveFields--" name="char_ScheduleType">
                     <option value="">SELECT WORKSCHEDULE</option>
                     <option value="Fi">Fix</option>
                     <option value="Fl">Flexi</option>
                     <option value="Co">Compress</option>
                  </select>
               </div>

               <!--
               <div class="col-xs-1"></div>
               <div class="col-xs-1">
                  <input type="radio" class="saveFields--" name="type" id="schedTypeFix" value="Fi" checked>&nbsp;Fix
               </div>
               <div class="col-xs-1">
                  <input type="radio" class="saveFields--" name="type" id="schedTypeFlexi" value="Fl">&nbsp;Flexi
               </div>
               <div class="col-xs-2">
                  <input type="radio" class="saveFields--" name="type" id="schedTypeCompress" value="Co">&nbsp;Compress Work
               </div>
               -->
               <div class="col-xs-2">
                  <input type="checkbox" class="saveFields--" name="sint_AutoLB" id="sint_AutoLB">&nbsp;Auto Lunch Break
               </div>
               <!--<input type="hidden" class="saveFields--" name="char_ScheduleType" id="schedType" value="Fi">-->

               <!--<input type="hidden" class="saveFields--" name="sint_AutoLB" id="sint_AutoLB" value="0">-->
            </div>
            <?php spacer (30);?>
            <div class="row padd5" style="border-bottom:1px solid gray">
               <div class="col-xs-1"></div>
               <div class="col-xs-1 txt-center"><span class="label">AM (In)</span></div>
               <div class="col-xs-1 txt-center"><span class="label">MID (Out)</span></div>
               <div class="col-xs-1 txt-center"><span class="label">MID (In)</span></div>
               <div class="col-xs-1 txt-center"><span class="label">PM (Out)</span></div>
               <div class="col-xs-1 txt-center"><span class="label">Strict MID</span></div>
               <div class="col-xs-1 txt-center"><span class="label">Flexi MID Break</span></div>
               <div class="col-xs-1 txt-center"><span class="label">Flexi</span></div>
               <div class="col-xs-1 txt-center"><span class="label">Flexi Time</span></div>
               <div class="col-xs-1 txt-center"><span class="label">Restday</span></div>
               <div class="col-xs-1 txt-center"><span class="label">Whole Day</span></div>
               <div class="col-xs-1 txt-center"><span class="label">Half Day</span></div>
            </div>
            <div class="row margin-top padd5" style="border-bottom:1px solid gray">
               <div class="col-xs-1 txt-right"><span class="label">Sunday</span></div>
               <div class="col-xs-1"><?php fTimePicker_WS("sint_SundayIn",8)?></div>
               <div class="col-xs-1"><?php fTimePicker_WS("sint_SundayLBOut",12)?></div>
               <div class="col-xs-1"><?php fTimePicker_WS("sint_SundayLBIn",13)?></div>
               <div class="col-xs-1"><?php fTimePicker_WS("sint_SundayOut",17)?></div>
               <div class="col-xs-1"><input type="checkbox" class="form-input chkbox-- saveFields--" name="sint_SundayisStrictLB" value="0"></div>
               <div class="col-xs-1"><input type="checkbox" class="form-input chkbox-- saveFields--" name="sint_SundayisFlexiLB" value="0"></div>
               <div class="col-xs-1"><input type="checkbox" class="form-input chkbox-- saveFields--" name="sint_SundayisFlexi" value="0"></div>
               <!--<div class="col-xs-1"><input type="text" class="form-input saveFields--" name="sint_SundayFlexiTime"></div>-->
               <div class="col-xs-1"><?php fTimePicker_WS("sint_SundayFlexiTime",8)?></div>
               <div class="col-xs-1"><input type="checkbox" class="form-input chkbox-- saveFields--" name="sint_SundayisRestDay" value="1" checked></div>
               <div class="col-xs-1"><input type="text" class="form-disp WholeDay--" name="txtSun8Hrs" value="08:00" disabled></div>
               <div class="col-xs-1"><input type="text" class="form-disp HalfDay--" name="txtSun4Hrs" value="04:00" disabled></div>
            </div>
            <div class="row margin-top padd5" style="border-bottom:1px solid gray">
               <div class="col-xs-1 txt-right"><span class="label">Monday</span></div>
               <div class="col-xs-1"><?php fTimePicker_WS("sint_MondayIn",8)?></div>
               <div class="col-xs-1"><?php fTimePicker_WS("sint_MondayLBOut",12)?></div>
               <div class="col-xs-1"><?php fTimePicker_WS("sint_MondayLBIn",13)?></div>
               <div class="col-xs-1"><?php fTimePicker_WS("sint_MondayOut",17)?></div>
               <div class="col-xs-1"><input type="checkbox" class="form-input chkbox-- saveFields--" name="sint_MondayisStrictLB" value="0"></div>
               <div class="col-xs-1"><input type="checkbox" class="form-input chkbox-- saveFields--" name="sint_MondayisFlexiLB" value="0"></div>
               <div class="col-xs-1"><input type="checkbox" class="form-input chkbox-- saveFields--" name="sint_MondayisFlexi" value="0"></div>
               <!--<div class="col-xs-1"><input type="text" class="form-input saveFields--" name="sint_MondayFlexiTime"></div>-->
               <div class="col-xs-1"><?php fTimePicker_WS("sint_MondayFlexiTime",8)?></div>
               <div class="col-xs-1"><input type="checkbox" class="form-input chkbox-- saveFields--" name="sint_MondayisRestDay" value="0"></div>
               <div class="col-xs-1"><input type="text" class="form-disp WholeDay--" name="txtMon8Hrs" value="08:00" disabled></div>
               <div class="col-xs-1"><input type="text" class="form-disp HalfDay--" name="txtMon4Hrs" value="04:00" disabled></div>
            </div>
            <div class="row margin-top padd5" style="border-bottom:1px solid gray">
               <div class="col-xs-1 txt-right"><span class="label">Tuesday</span></div>
               <div class="col-xs-1"><?php fTimePicker_WS("sint_TuesdayIn",8)?></div>
               <div class="col-xs-1"><?php fTimePicker_WS("sint_TuesdayLBOut",12)?></div>
               <div class="col-xs-1"><?php fTimePicker_WS("sint_TuesdayLBIn",13)?></div>
               <div class="col-xs-1"><?php fTimePicker_WS("sint_TuesdayOut",17)?></div>
               <div class="col-xs-1"><input type="checkbox" class="form-input chkbox-- saveFields--" name="sint_TuesdayisStrictLB" value="0"></div>
               <div class="col-xs-1"><input type="checkbox" class="form-input chkbox-- saveFields--" name="sint_TuesdayisFlexiLB" value="0"></div>
               <div class="col-xs-1"><input type="checkbox" class="form-input chkbox-- saveFields--" name="sint_TuesdayisFlexi" value="0"></div>
               <!--<div class="col-xs-1"><input type="text" class="form-input saveFields--" name="sint_TuesdayFlexiTime"></div>-->
               <div class="col-xs-1"><?php fTimePicker_WS("sint_TuesdayFlexiTime",8)?></div>
               <div class="col-xs-1"><input type="checkbox" class="form-input chkbox-- saveFields--" name="sint_TuesdayisRestDay" value="0"></div>
               <div class="col-xs-1"><input type="text" class="form-disp WholeDay--" name="txtTue8Hrs" value="08:00" disabled></div>
               <div class="col-xs-1"><input type="text" class="form-disp HalfDay--" name="txtTue4Hrs" value="04:00" disabled></div>
            </div>
            <div class="row margin-top padd5" style="border-bottom:1px solid gray">
               <div class="col-xs-1 txt-right"><span class="label">Wednesday</span></div>
               <div class="col-xs-1"><?php fTimePicker_WS("sint_WednesdayIn",8)?></div>
               <div class="col-xs-1"><?php fTimePicker_WS("sint_WednesdayLBOut",12)?></div>
               <div class="col-xs-1"><?php fTimePicker_WS("sint_WednesdayLBIn",13)?></div>
               <div class="col-xs-1"><?php fTimePicker_WS("sint_WednesdayOut",17)?></div>
               <div class="col-xs-1"><input type="checkbox" class="form-input chkbox-- saveFields--" name="sint_WednesdayisStrictLB" value="0"></div>
               <div class="col-xs-1"><input type="checkbox" class="form-input chkbox-- saveFields--" name="sint_WednesdayisFlexiLB" value="0"></div>
               <div class="col-xs-1"><input type="checkbox" class="form-input chkbox-- saveFields--" name="sint_WednesdayisFlexi" value="0"></div>
               <!--<div class="col-xs-1"><input type="text" class="form-input saveFields--" name="sint_WednesdayFlexiTime"></div>-->
               <div class="col-xs-1"><?php fTimePicker_WS("sint_WednesdayFlexiTime",8)?></div>
               <div class="col-xs-1"><input type="checkbox" class="form-input chkbox-- saveFields--" name="sint_WednesdayisRestDay" value="0"></div>
               <div class="col-xs-1"><input type="text" class="form-disp WholeDay--" name="txtWed8Hrs" value="08:00" disabled></div>
               <div class="col-xs-1"><input type="text" class="form-disp HalfDay--" name="txtWed4Hrs" value="04:00" disabled></div>
            </div>
            <div class="row margin-top padd5" style="border-bottom:1px solid gray">
               <div class="col-xs-1 txt-right"><span class="label">Thursday</span></div>
               <div class="col-xs-1"><?php fTimePicker_WS("sint_ThursdayIn",8)?></div>
               <div class="col-xs-1"><?php fTimePicker_WS("sint_ThursdayLBOut",12)?></div>
               <div class="col-xs-1"><?php fTimePicker_WS("sint_ThursdayLBIn",13)?></div>
               <div class="col-xs-1"><?php fTimePicker_WS("sint_ThursdayOut",17)?></div>
               <div class="col-xs-1"><input type="checkbox" class="form-input chkbox-- saveFields--" name="sint_ThursdayisStrictLB" value="0"></div>
               <div class="col-xs-1"><input type="checkbox" class="form-input chkbox-- saveFields--" name="sint_ThursdayisFlexiLB" value="0"></div>
               <div class="col-xs-1"><input type="checkbox" class="form-input chkbox-- saveFields--" name="sint_ThursdayisFlexi" value="0"></div>
               <!--<div class="col-xs-1"><input type="text" class="form-input saveFields--" name="sint_ThursdayFlexiTime"></div>-->
               <div class="col-xs-1"><?php fTimePicker_WS("sint_ThursdayFlexiTime",8)?></div>
               <div class="col-xs-1"><input type="checkbox" class="form-input chkbox-- saveFields--" name="sint_ThursdayisRestDay" value="0"></div>
               <div class="col-xs-1"><input type="text" class="form-disp WholeDay--" name="txtThu8Hrs" value="08:00" disabled></div>
               <div class="col-xs-1"><input type="text" class="form-disp HalfDay--" name="txtThu4Hrs" value="04:00" disabled></div>
            </div>
            <div class="row margin-top padd5" style="border-bottom:1px solid gray">
               <div class="col-xs-1 txt-right"><span class="label">Friday</span></div>
               <div class="col-xs-1"><?php fTimePicker_WS("sint_FridayIn",8)?></div>
               <div class="col-xs-1"><?php fTimePicker_WS("sint_FridayLBOut",12)?></div>
               <div class="col-xs-1"><?php fTimePicker_WS("sint_FridayLBIn",13)?></div>
               <div class="col-xs-1"><?php fTimePicker_WS("sint_FridayOut",17)?></div>
               <div class="col-xs-1"><input type="checkbox" class="form-input chkbox-- saveFields--" name="sint_FridayisStrictLB" value="0"></div>
               <div class="col-xs-1"><input type="checkbox" class="form-input chkbox-- saveFields--" name="sint_FridayisFlexiLB" value="0"></div>
               <div class="col-xs-1"><input type="checkbox" class="form-input chkbox-- saveFields--" name="sint_FridayisFlexi" value="0"></div>
               <!--<div class="col-xs-1"><input type="text" class="form-input saveFields--" name="sint_FridayFlexiTime"></div>-->
               <div class="col-xs-1"><?php fTimePicker_WS("sint_FridayFlexiTime",8)?></div>
               <div class="col-xs-1"><input type="checkbox" class="form-input chkbox-- saveFields--" name="sint_FridayisRestDay" value="0"></div>
               <div class="col-xs-1"><input type="text" class="form-disp WholeDay--" name="txtFri8Hrs" value="08:00" disabled></div>
               <div class="col-xs-1"><input type="text" class="form-disp HalfDay--" name="txtFri4Hrs" value="04:00" disabled></div>
            </div>
            <div class="row margin-top padd5" style="border-bottom:1px solid gray">
               <div class="col-xs-1 txt-right"><span class="label">Saturday</span></div>
               <div class="col-xs-1"><?php fTimePicker_WS("sint_SaturdayIn",8)?></div>
               <div class="col-xs-1"><?php fTimePicker_WS("sint_SaturdayLBOut",12)?></div>
               <div class="col-xs-1"><?php fTimePicker_WS("sint_SaturdayLBIn",13)?></div>
               <div class="col-xs-1"><?php fTimePicker_WS("sint_SaturdayOut",17)?></div>
               <div class="col-xs-1"><input type="checkbox" class="form-input chkbox-- saveFields--" name="sint_SaturdayisStrictLB" value="0"></div>
               <div class="col-xs-1"><input type="checkbox" class="form-input chkbox-- saveFields--" name="sint_SaturdayisFlexiLB" value="0"></div>
               <div class="col-xs-1"><input type="checkbox" class="form-input chkbox-- saveFields--" name="sint_SaturdayisFlexi" value="0"></div>
               <!--<div class="col-xs-1"><input type="text" class="form-input saveFields--" name="sint_SaturdayFlexiTime"></div>-->
               <div class="col-xs-1"><?php fTimePicker_WS("sint_SaturdayFlexiTime",8)?></div>
               <div class="col-xs-1"><input type="checkbox" class="form-input chkbox-- saveFields--" name="sint_SaturdayisRestDay" value="1" checked></div>
               <div class="col-xs-1"><input type="text" class="form-disp WholeDay--" name="txtSat8Hrs" value="08:00" disabled></div>
               <div class="col-xs-1"><input type="text" class="form-disp HalfDay--" name="txtSat4Hrs" value="04:00" disabled></div>
            </div>
            <?php spacer(20);?>
            <div class="row" style="border-bottom:1px solid gray">
               <div class="col-xs-2"></div>
               <div class="col-xs-2 txt-center"><span class="label">O.T (In)</span></div>
               <!--<div class="col-xs-1 txt-center"><span class="label">O.T Start Time</span></div>-->
               <div class="col-xs-2 txt-center"><span class="label">Minimum O.T</span></div>
               <div class="col-xs-2 txt-center"><span class="label">O.T Needs Aprroval</span></div>
               <div class="col-xs-2 txt-center"><span class="label">Advance O.T</span></div>
               <div class="col-xs-2 txt-center"><span class="label">Advance O.T Start Time</span></div>
            </div>
            <div class="row margin-top padd5" style="border-bottom:1px solid gray">
               <div class="col-xs-2 txt-right"><span class="label">Sunday</span></div>
               <!--<div class="col-xs-1"><input type="text" class="form-input saveFields--" name="sint_"></div>-->
               <div class="col-xs-2"><?php fTimePicker_WS("sint_SundayOTIn",8)?></div>
               <div class="col-xs-2"><input type="text" class="form-input saveFields--" name="sint_SundayOTMinHrs"></div>
               <div class="col-xs-2"><input type="checkbox" class="form-input chkbox-- saveFields--" name="sint_SundayOTNeedApproval" value="0"></div>
               <div class="col-xs-2"><input type="checkbox" class="form-input chkbox-- saveFields--" name="sint_SundayOTAdvance" value="0"></div>
               <div class="col-xs-2"><?php fTimePicker_WS("sint_SundayOTAdvanceIn",6)?></div>
            </div>
            <div class="row margin-top padd5" style="border-bottom:1px solid gray">
               <div class="col-xs-2 txt-right"><span class="label">Monday</span></div>
               <!--<div class="col-xs-1"><input type="text" class="form-input saveFields--" name="sint_"></div>-->
               <div class="col-xs-2"><?php fTimePicker_WS("sint_MondayOTIn",8)?></div>
               <div class="col-xs-2"><input type="text" class="form-input saveFields--" name="sint_MondayOTMinHrs"></div>
               <div class="col-xs-2"><input type="checkbox" class="form-input chkbox-- saveFields--" name="sint_MondayOTNeedApproval" value="0"></div>
               <div class="col-xs-2"><input type="checkbox" class="form-input chkbox-- saveFields--" name="sint_MondayOTAdvance" value="0"></div>
               <div class="col-xs-2"><?php fTimePicker_WS("sint_MondayOTAdvanceIn",6)?></div>
            </div>
            <div class="row margin-top padd5" style="border-bottom:1px solid gray">
               <div class="col-xs-2 txt-right"><span class="label">Tuesday</span></div>
               <!--<div class="col-xs-1"><input type="text" class="form-input saveFields--" name="sint_"></div>-->
               <div class="col-xs-2"><?php fTimePicker_WS("sint_TuesdayOTIn",8)?></div>
               <div class="col-xs-2"><input type="text" class="form-input saveFields--" name="sint_TuesdayOTMinHrs"></div>
               <div class="col-xs-2"><input type="checkbox" class="form-input chkbox-- saveFields--" name="sint_TuesdayOTNeedApproval" value="0"></div>
               <div class="col-xs-2"><input type="checkbox" class="form-input chkbox-- saveFields--" name="sint_TuesdayOTAdvance" value="0"></div>
               <div class="col-xs-2"><?php fTimePicker_WS("sint_TuesdayOTAdvanceIn",6)?></div>
            </div>
            <div class="row margin-top padd5" style="border-bottom:1px solid gray">
               <div class="col-xs-2 txt-right"><span class="label">Wednesday</span></div>
              <!--<div class="col-xs-1"><input type="text" class="form-input saveFields--" name="sint_"></div>-->
               <div class="col-xs-2"><?php fTimePicker_WS("sint_WednesdayOTIn",8)?></div>
               <div class="col-xs-2"><input type="text" class="form-input saveFields--" name="sint_WednesdayOTMinHrs"></div>
               <div class="col-xs-2"><input type="checkbox" class="form-input chkbox-- saveFields--" name="sint_WednesdayOTNeedApproval" value="0"></div>
               <div class="col-xs-2"><input type="checkbox" class="form-input chkbox-- saveFields--" name="sint_WednesdayOTAdvance" value="0"></div>
               <div class="col-xs-2"><?php fTimePicker_WS("sint_WednesdayOTAdvanceIn",6)?></div>
            </div>
            <div class="row margin-top padd5" style="border-bottom:1px solid gray">
               <div class="col-xs-2 txt-right"><span class="label">Thursday</span></div>
               <!--<div class="col-xs-1"><input type="text" class="form-input saveFields--" name="sint_"></div>-->
               <div class="col-xs-2"><?php fTimePicker_WS("sint_ThursdayOTIn",8)?></div>
               <div class="col-xs-2"><input type="text" class="form-input saveFields--" name="sint_ThursdayOTMinHrs"></div>
               <div class="col-xs-2"><input type="checkbox" class="form-input chkbox-- saveFields--" name="sint_ThursdayOTNeedApproval" value="0"></div>
               <div class="col-xs-2"><input type="checkbox" class="form-input chkbox-- saveFields--" name="sint_ThursdayOTAdvance" value="0"></div>
               <div class="col-xs-2"><?php fTimePicker_WS("sint_ThursdayOTAdvanceIn",6)?></div>
            </div>
            <div class="row margin-top padd5" style="border-bottom:1px solid gray">
               <div class="col-xs-2 txt-right"><span class="label">Friday</span></div>
            <!--<div class="col-xs-1"><input type="text" class="form-input saveFields--" name="sint_"></div>-->
               <div class="col-xs-2"><?php fTimePicker_WS("sint_FridayOTIn",8)?></div>
               <div class="col-xs-2"><input type="text" class="form-input saveFields--" name="sint_FridayOTMinHrs"></div>
               <div class="col-xs-2"><input type="checkbox" class="form-input chkbox-- saveFields--" name="sint_FridayOTNeedApproval" value="0"></div>
               <div class="col-xs-2"><input type="checkbox" class="form-input chkbox-- saveFields--" name="sint_FridayOTAdvance" value="0"></div>
               <div class="col-xs-2"><?php fTimePicker_WS("sint_FridayOTAdvanceIn",6)?></div>
            </div>
            <div class="row margin-top padd5" style="border-bottom:1px solid gray">
               <div class="col-xs-2 txt-right"><span class="label">Saturday</span></div>
              <!--<div class="col-xs-1"><input type="text" class="form-input saveFields--" name="sint_"></div>-->
               <div class="col-xs-2"><?php fTimePicker_WS("sint_SaturdayOTIn",8)?></div>
               <div class="col-xs-2"><input type="text" class="form-input saveFields--" name="sint_SaturdayOTMinHrs"></div>
               <div class="col-xs-2"><input type="checkbox" class="form-input chkbox-- saveFields--" name="sint_SaturdayOTNeedApproval" value="0"></div>
               <div class="col-xs-2"><input type="checkbox" class="form-input chkbox-- saveFields--" name="sint_SaturdayOTAdvance" value="0"></div>
               <div class="col-xs-2"><?php fTimePicker_WS("sint_SaturdayOTAdvanceIn",6)?></div>
            </div>
      </div>
   </div>
</div>
