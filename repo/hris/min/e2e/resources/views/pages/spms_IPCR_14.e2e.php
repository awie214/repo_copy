<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_AfterTrn"); ?>"></script>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_IPCR_14"); ?>"></script>
      <style type="text/css">
         .td-input {
            padding: 2px;
         }
         textarea {resize: none;}
      </style>
   </head>
   <body>
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"spms"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar("INDIVIDUAL PERFORMANCE COMMITMENT AND REVIEW [IPCR]"); ?>
            <div class="container-fluid margin-top10">
               <div class="row" id="spmsList">
                  <?php
                     if (getvalue("hUserGroup") != "COMPEMP") {
                  ?>
                  <div class="col-xs-3">
                     <?php
                        employeeSelector();
                     ?>
                  </div>
                  <div class="col-xs-9">
                  <?php
                     } else {
                        echo '<div class="col-xs-12">';   
                     }
                  ?>
                     <div class="panel-top">
                        LIST OF PERFORMANCE MANAGEMENT OF 
                        <span id="selectedEmployees">&nbsp;</span>
                     </div>
                     <div class="panel-mid">
                        <div id="spGridTable">
                        </div>
                     </div>
                     <div class="panel-bottom">
                        <button type="button"
                             class="btn-cls4-sea trnbtn"
                             id="btnINSERTSPMS" name="btnINSERTSPMS">
                           <i class="fa fa-file" aria-hidden="true"></i>
                           &nbsp;Insert New
                        </button>
                     </div>
                  </div>
               </div>
               <div class="row" id="insert_spms">
                  <div class="col-xs-12">
                     <div class="panel-top">ADD NEW</div>
                     <div class="panel-mid" id="contentIPCR">
                        <div class="row">
                           <div class="col-xs-4">
                              <label>Semester</label>
                              <select class="form-input" name="sint_Semester" id="sint_Semester">
                                 <option value="">Select Semester</option>
                                 <option value="1">1st Half</option>
                                 <option value="2">2nd Half</option>
                              </select>
                           </div>
                           <div class="col-xs-4">
                              <label>Year</label>
                              <select class="form-input" name="sint_Year" id="sint_Year">
                                 <?php
                                    $past_year = date("Y",time()) - 2;
                                    $curr_year = date("Y",time());
                                    $future_year = date("Y",time()) + 2;
                                    for ($i=$past_year; $i <= $future_year ; $i++) { 
                                       if ($i == $curr_year) {
                                          echo '<option value="'.$i.'" selected>'.$i.'</option>';   
                                       } else {
                                          echo '<option value="'.$i.'">'.$i.'</option>';
                                       }
                                       
                                    }
                                 ?>
                              </select>
                           </div>
                        </div>
                        <div class="row margin-top">
                           <div class="col-xs-4">
                              <label>Supervisor Rating:</label>
                              <input type="text" class="form-input" name="Supervisor_Rating" id="Supervisor_Rating">
                           </div>
                           <div class="col-xs-4">
                              <label>Final Rating:</label>
                              <input type="text" class="form-input" name="Final_Rating" id="Final_Rating">
                           </div>
                        </div>
                        <br>
                        <div id="canvas">
                           <?php
                              for ($x=1; $x <=1; $x++) { 
                           ?>
                           <div id="EntryIPCR_<?php echo $x; ?>" class="entry201">
                              <div class="row margin-top">
                                 <div class="col-xs-6">
                                    <label>#<?php echo $x; ?> </label>
                                    <input type="hidden" name="refid_<?php echo $x; ?>" id="refid_<?php echo $x; ?>">
                                 </div>
                              </div>
                              <?php bar(); ?>
                              <div class="row margin-top">
                                 <div class="col-xs-4">
                                    <label>Select Type:</label>
                                    <select class="form-input" name="type_<?php echo $x; ?>" id="type_<?php echo $x; ?>">
                                       <option value="Core Function">Core Function</option>
                                       <option value="Strategic Function">Strategic Function</option>
                                       <option value="Support Function">Support Function</option>
                                       <option value="Other">Other</option>
                                    </select>
                                 </div>
                              </div>
                              <div class="row margin-top">
                                 <div class="col-xs-12">
                                    <label>OUTPUT</label>
                                    <textarea class="form-input" rows="4" name="output_<?php echo $x; ?>" id="output_<?php echo $x; ?>"></textarea>
                                 </div>
                              </div>
                              <div class="row margin-top">
                                 <div class="col-xs-12">
                                    <label>SUCCESS INDICATORS (TARGETS+MEASURES)</label>
                                    <textarea class="form-input" rows="4" name="si_<?php echo $x; ?>" id="si_<?php echo $x; ?>"></textarea>
                                 </div>
                              </div>
                              <div class="row margin-top">
                                 <div class="col-xs-3">
                                    <label>Quality:</label>
                                    <select class="form-input" name="quality_<?php echo $x; ?>" id="quality_<?php echo $x; ?>">
                                       <option value="0">0</option>
                                       <option value="1">1</option>
                                       <option value="2">2</option>
                                       <option value="3">3</option>
                                       <option value="4">4</option>
                                       <option value="5">5</option>
                                    </select>
                                 </div>
                                 <div class="col-xs-3">
                                    <label>Effectiveness:</label>
                                    <select class="form-input" name="effectiveness_<?php echo $x; ?>" id="effectiveness_<?php echo $x; ?>">
                                       <option value="0">0</option>
                                       <option value="1">1</option>
                                       <option value="2">2</option>
                                       <option value="3">3</option>
                                       <option value="4">4</option>
                                       <option value="5">5</option>
                                    </select>
                                 </div>
                                 <div class="col-xs-3">
                                    <label>Timeliness:</label>
                                    <select class="form-input" name="timeliness_<?php echo $x; ?>" id="timeliness_<?php echo $x; ?>">
                                       <option value="0">0</option>
                                       <option value="1">1</option>
                                       <option value="2">2</option>
                                       <option value="3">3</option>
                                       <option value="4">4</option>
                                       <option value="5">5</option>
                                    </select>
                                 </div>
                                 <div class="col-xs-3">
                                    <label>Average:</label>
                                    <input type="text" class="form-input" name="average_<?php echo $x; ?>" id="average_<?php echo $x; ?>">
                                 </div>
                              </div>
                              <div class="row margin-top">
                                 <div class="col-xs-12">
                                    <label>Actual Accomplishments</label>
                                    <textarea class="form-input" rows="4" name="accomplishment_<?php echo $x; ?>" id="accomplishment_<?php echo $x; ?>"></textarea>
                                 </div>
                              </div>
                              <br>
                              <div class="row margin-top">
                                 <div class="col-xs-12">
                                    <label>Quality:</label>
                                 </div>
                              </div>
                              <?php bar(); ?>
                              <div class="row margin-top">
                                 <div class="col-xs-2">
                                    <label>Rating 1:</label>
                                    <textarea class="form-input" rows="2" name="q1_<?php echo $x; ?>" id="q1_<?php echo $x; ?>"></textarea>
                                 </div>
                                 <div class="col-xs-2">
                                    <label>Rating 2:</label>
                                    <textarea class="form-input" rows="2" name="q2_<?php echo $x; ?>" id="q2_<?php echo $x; ?>"></textarea>
                                 </div>
                                 <div class="col-xs-2">
                                    <label>Rating 3:</label>
                                    <textarea class="form-input" rows="2" name="q3_<?php echo $x; ?>" id="q3_<?php echo $x; ?>"></textarea>
                                 </div>
                                 <div class="col-xs-2">
                                    <label>Rating 4:</label>
                                    <textarea class="form-input" rows="2" name="q4_<?php echo $x; ?>" id="q4_<?php echo $x; ?>"></textarea>
                                 </div>
                                 <div class="col-xs-2">
                                    <label>Rating 5:</label>
                                    <textarea class="form-input" rows="2" name="q5_<?php echo $x; ?>" id="q5_<?php echo $x; ?>"></textarea>
                                 </div>
                              </div>
                              <div class="row margin-top">
                                 <div class="col-xs-12">
                                    <label>Effectiveness</label>
                                 </div>
                              </div>
                              <?php bar(); ?>
                              <div class="row margin-top">
                                 <div class="col-xs-2">
                                    <label>Rating 1:</label>
                                    <textarea class="form-input" rows="2" name="e1_<?php echo $x; ?>" id="e1_<?php echo $x; ?>"></textarea>
                                 </div>
                                 <div class="col-xs-2">
                                    <label>Rating 2:</label>
                                    <textarea class="form-input" rows="2" name="e2_<?php echo $x; ?>" id="e2_<?php echo $x; ?>"></textarea>
                                 </div>
                                 <div class="col-xs-2">
                                    <label>Rating 3:</label>
                                    <textarea class="form-input" rows="2" name="e3_<?php echo $x; ?>" id="e3_<?php echo $x; ?>"></textarea>
                                 </div>
                                 <div class="col-xs-2">
                                    <label>Rating 4:</label>
                                    <textarea class="form-input" rows="2" name="e4_<?php echo $x; ?>" id="e4_<?php echo $x; ?>"></textarea>
                                 </div>
                                 <div class="col-xs-2">
                                    <label>Rating 5:</label>
                                    <textarea class="form-input" rows="2" name="e5_<?php echo $x; ?>" id="e5_<?php echo $x; ?>"></textarea>
                                 </div>
                              </div>
                              <div class="row margin-top">
                                 <div class="col-xs-12">
                                    <label>Timeliness:</label>
                                 </div>
                              </div>
                              <?php bar(); ?>
                              <div class="row margin-top">
                                 <div class="col-xs-2">
                                    <label>Rating 1:</label>
                                    <textarea class="form-input" rows="2" name="t1_<?php echo $x; ?>" id="t1_<?php echo $x; ?>"></textarea>
                                 </div>
                                 <div class="col-xs-2">
                                    <label>Rating 2:</label>
                                    <textarea class="form-input" rows="2" name="t2_<?php echo $x; ?>" id="t2_<?php echo $x; ?>"></textarea>
                                 </div>
                                 <div class="col-xs-2">
                                    <label>Rating 3:</label>
                                    <textarea class="form-input" rows="2" name="t3_<?php echo $x; ?>" id="t3_<?php echo $x; ?>"></textarea>
                                 </div>
                                 <div class="col-xs-2">
                                    <label>Rating 4:</label>
                                    <textarea class="form-input" rows="2" name="t4_<?php echo $x; ?>" id="t4_<?php echo $x; ?>"></textarea>
                                 </div>
                                 <div class="col-xs-2">
                                    <label>Rating 5:</label>
                                    <textarea class="form-input" rows="2" name="t5_<?php echo $x; ?>" id="t5_<?php echo $x; ?>"></textarea>
                                 </div>
                              </div>
                           </div>
                           <br>
                           <?php
                              }
                           ?>   
                        </div>
                        
                     </div>
                     <div class="panel-bottom">
                        <button type="button" id="addIPCR" class="btn-cls4-tree">
                           <i class="fa fa-plus"></i>&nbsp;ADD ROW
                        </button>
                        <input type="hidden" name="sint_IPCRRefId" id="sint_IPCRRefId" value="0">
                        <input type="hidden" name="sint_EmployeesRefId" id="sint_EmployeesRefId" value="0">
                        <input type="hidden" name="count" id="count">
                        <input type="hidden" name="fn" id="fn" value="saveIPCR">
                        <button type="button" id="btnSAVEIPCR" class="btn-cls4-sea">
                           <i class="fa fa-save"></i>&nbsp;SAVE
                        </button>
                        <button type="button" id="btnCANCELIPCR" class="btn-cls4-red">
                           <i class="fa fa-times"></i>&nbsp;CANCEL
                        </button>
                        <button type="button" id="btnEDITIPCR" class="btn-cls4-sea">
                           <i class="fa fa-save"></i>&nbsp;UPDATE
                        </button>
                        <button type="button" id="btnBACKIPCR" class="btn-cls4-red">
                           <i class="fa fa-backward"></i>&nbsp;BACK
                        </button>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="prnModal" role="dialog">
               <div class="modal-dialog" style="height:90%;width:80%">
                  <div class="mypanel" style="height:100%;">
                     <div class="panel-top bgSilver">
                        <a href="#" data-toggle="tooltip" data-placement="top" id="btnPRINTNOW">
                           <i class="fa fa-print" aria-hidden="true"></i>
                        </a>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                  </div>
               </div>
            </div>
            <?php
               footer();
               $table = "spms_ipcr";
               include_once ("varHidden.e2e.php");
            ?>
         </div>
      </form>
   </body>
   <script type="text/javascript">
      $(document).ready(function () {
         $("#addIPCR").click(function() {
            AddRows("EntryIPCR_","contentIPCR","refid_");
         });
      });
      function AddRows(parentId,gparentId,refid) {
         // $.notify(parentId + "1");
         // $.notify(gparentId + "2");
         // $.notify(refid + "3");
         var rowHTML    = "";
         var newAppends = "";
         var appends    = "";
         var rowLength  = $("[id*='"+parentId+"']").length;
         appends        = $("#"+parentId+rowLength).html();
         
         newAppends = appends.split("_"+rowLength).join("_"+(rowLength+1));
         newAppends = newAppends.split("#"+rowLength).join("#"+(rowLength+1));
         newAppends = newAppends.split('idx="' + rowLength + '"').join('idx="'+ (rowLength + 1) +'"');
         newAppends = newAppends.split('unclick="CancelAddRow"').join('onClick=removeRow("'+parentId+(rowLength+1)+'");');
         newAppends = newAppends.split('focus="validateGrade"').join('onChange=validateGrade2($(this).attr(\'partner\')); onFocus=validateGrade2($(this).attr(\'partner\'));');
         newAppends = newAppends.split('click="HighestGrade_validation"').join('onClick=HighestGrade_validation($(this).attr(\'name\'));');
         rowHTML += '<div id="'+parentId+(rowLength+1)+'" class="entry201">';
         rowHTML += newAppends;
         rowHTML += '</div>';
         $("#"+gparentId).append(rowHTML);
         $("[name='"+refid+(rowLength + 1)+"']").val("");
         if (rowHTML.indexOf("date--") > 0) {
            $("#" + parentId+(rowLength+1) + " input[class*='date--']").each(function () {
               $(this).keypress(fnDateClass($(this).attr("name")));
            });
         }
         if (rowHTML.indexOf("alpha--") > 0) {
            $("#" + parentId+(rowLength+1) + " input[class*='alpha--']").each(function () {
               var e = arguments[0];
               fnAlphaClass($(this).attr("name"),e);
            });
         }
         if (rowHTML.indexOf("number--") > 0) {
            $("#" + parentId+(rowLength+1) + " input[class*='number--']").each(function () {
               var e = arguments[0];
               fnNumberClass($(this).attr("name"),e);
            });
         }
         if (rowHTML.indexOf("valDate--") > 0) {
            $("#" + parentId+(rowLength+1) + " input[class*='valDate--']").each(function () {
               $(this).keypress(fnValDate($(this).attr("name")));
            });
         }
         if (rowHTML.indexOf('rowid="') > 0) {
            $("#" + parentId+(rowLength+1) + " input[class*='enabler--']").each(function () {
               $(this).attr("refid","");
               $("[name='"+$(this).attr("rowid")+"']").val("");
            });
         }
         if (rowHTML.indexOf('presentbox--') > 0) {
            $("#" + parentId+(rowLength+1) + " input[class*='presentbox--']").each(function () {
               $(this).attr("checked",false);
               $(this).click(function () {
                  sint_Present_Click($(this).attr("id"));
               });
            });
         }
         $("#refid_"+(rowLength+1)).val("");
         $("#output_"+(rowLength+1)).val("");
         $("#si_"+(rowLength+1)).val("");
         $("#accomplishment_"+(rowLength+1)).val("");
         $("#remarks_"+(rowLength+1)).val("");
         $("#q1_"+(rowLength+1)).val("");
         $("#e2_"+(rowLength+1)).val("");
         $("#t3_"+(rowLength+1)).val("");
         $("#a4_"+(rowLength+1)).val("");
         // After Add Row
         //$("#"+parentId+(rowLength+1)).append(btnCan);
         $("#" +parentId+(rowLength+1)+ " .saveFields--").prop("disabled",false);
         $("#" +parentId+(rowLength+1)+ "> .enabler--").prop("checked",true);
         showBtnUpd();
      }
   </script>
</html>