<?php
   session_start();
   require "conn.e2e.php";
   require_once $_SESSION["Classes"]."0620functions.e2e.php";
   doGridTable($_SESSION["module_table"],
               $_SESSION["module_gridTableHdr_arr"],
               $_SESSION["module_gridTableFld_arr"],
               $_SESSION["module_sql"],
               $_SESSION["list_ShowAction"],
               $_SESSION["module_gridTable_ID"]);
   $conn = null;
?>
<script language="JavaScript">
   $('#gridTable').DataTable();
   var table = $('#gridTable').DataTable();
   table
      .order([ 0, 'desc' ])
      .draw();
</script>