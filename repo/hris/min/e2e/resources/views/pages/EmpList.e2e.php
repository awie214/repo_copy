<?php
   session_start();
   require_once "constant.e2e.php";
   require_once "conn.e2e.php";
   require_once pathClass.'0620functions.e2e.php';
   $Group      = getvalue("hUserGroup");
?>
<script>
   $(document).ready(function(){
      $(".selEmp").each(function () {
         $(this).click(function () {
            if ($(this).is(":checked")) {
               $(this).prop("disabled",true);
               //$(".srch--").prop("disabled",true);
               //$("[name*='srch']").prop("disabled",true);
               var hEmpSelected_Array = $("[name='hEmpSelected']").val().split(":");
               if (hEmpSelected_Array.indexOf($(this).attr("id")) < 0) {
                  $('#EmpSelected').append('<option value="'+$(this).attr("id")+'">'+$(this).val()+'</option>');
                  if ($("#totalCount").val() == $(".selEmp:checked").length) {
                     $("#checkAll").prop("checked",true);
                  } else {
                     $("#checkAll").prop("checked",false);
                  }
                  updateEmpSelected();
               } else {
                  $.notify("Selected Employees already in the list!!!","info");
                  return false;
               }
            }
         });
         <?php
            if ($Group == "AA") {
               echo '$("[class*=\'srch--\']").prop("disabled",true);';
            }
         ?>
      });
      $("#clearEmpSelected").click(function () {
         //$(".srch--, .selEmp").prop("disabled",false);
         //$("[name*='srch']").prop("disabled",false);
         $(".selEmp").prop("checked",false);
         $('#EmpSelected').empty();
         updateEmpSelected();
      });
      $("#EmpSelected").dblclick(function() {
         var idx = $(this).val();
         $("#EmpSelected option[value='"+idx+"']").remove();
         $("input[id='"+idx+"']").prop("checked",false);
         $("input[id='"+idx+"']").prop("disabled",false);
         $("#checkAll").prop("checked",false)
         updateEmpSelected();
      });
   });
   function updateEmpSelected() {
      var arr = "";
      $("#EmpSelected > option").each(function () {
         if (arr == "") {
            arr = $(this).val();
         } else {
            arr = arr + ":" + $(this).val();
         }
      });
      $("[name='hEmpSelected']").val(arr);
      $("#EmpSelected").focus();
      $("#EmpSelected").select();
   }
   $("#checkAll").click(function () {
      $(".selEmp").prop("checked",$(this).is(":checked"));
      $(".selEmp").prop("disabled",$(this).is(":checked"));
      $('#EmpSelected').empty();
      if ($(this).is(":checked") == true) {
         $(".selEmp").each(function () {
            //$(".srch--").prop("disabled",true);
            //$("[name*='srch']").prop("disabled",true);
            $('#EmpSelected').append('<option value="'+$(this).attr("id")+'">'+$(this).val()+'</option>');
            updateEmpSelected();
         });
      }
   });
</script>

   <table class="mytable" id="mytable" style="width:100%">
      <tr>
         <th class="table-hdr"><input type="checkbox" id="checkAll">&nbsp;</th>
         <th class="table-hdr">Employees Name</th>
      </tr>
      <?php
         $LastName   = getvalue("LastName");
         $FirstName  = getvalue("FirstName");
         $Sex        = getvalue("Sex");
         $CompanyId  = getvalue("hCompanyID");
         $BranchId   = getvalue("hBranchID");
         
         $emprefid   = getvalue("emprefid");
         if ($Group == "AA") {
            $division   = FindFirst("usermanagement","WHERE EmployeesRefId = '$emprefid'","DivisionRefId");
            $whereClause = " INNER JOIN empinformation WHERE empinformation.DivisionRefId = '$division'";
            $whereClause .= " AND employees.RefId = empinformation.EmployeesRefId";
            $whereClause .= " AND (employees.Inactive != 1 OR employees.Inactive IS NULL)";
            $whereClause .= " ORDER BY employees.LastName";
            $rs_emp = SelectEach("employees",$whereClause);
            if ($rs_emp) {
               while ($row = mysqli_fetch_array($rs_emp)) {
                  echo
                  '<tr class="table-dtl">
                     <td class="dataStyle txt-center">
                        <input type="checkbox" class="selEmp" id="'.$row["EmployeesRefId"].'" value="['.($row["EmployeesRefId"].'] '.$row["LastName"].', '.$row["FirstName"]).'" name="chk_'.$row["EmployeesRefId"].'">
                     </td>
                     <td class="dataStyle">'.$row["LastName"].', '.$row["FirstName"].'</td>
                  </tr>';
               }
            }   
         } else {
            $whereClause = "WHERE CompanyRefId = ".$CompanyId;
            $whereClause .= " AND BranchRefId = ".$BranchId;
            if (!empty($LastName)) $whereClause .= " AND LastName LIKE '$LastName%'";
            if (!empty($FirstName)) $whereClause .= " AND FirstName LIKE '$FirstName%'";
            if (!empty($Sex)) $whereClause .= " AND Sex = '$Sex'";
            $whereClause .= " AND (Inactive != 1 OR Inactive IS NULL) ORDER BY LastName";
            //echo $_SESSION["cid"]."====".$dbname."====".$whereClause;
            $rs_emp = SelectEach("employees",$whereClause);
            if ($rs_emp) {
               while ($row = mysqli_fetch_array($rs_emp)) {
                  echo
                  '<tr class="table-dtl">
                     <td class="dataStyle txt-center">
                        <input type="checkbox" class="selEmp" id="'.$row["RefId"].'" value="['.($row["RefId"].'] '.$row["LastName"].', '.$row["FirstName"]).'" name="chk_'.$row["RefId"].'">
                     </td>
                     <td class="dataStyle">'.$row["LastName"].', '.$row["FirstName"].'</td>
                  </tr>';
               }
            }   
         }
         
      ?>
   </table>
   <input type="hidden" id="totalCount" value="<?php echo mysqli_num_rows($rs_emp); ?>">

