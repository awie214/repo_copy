<?php
   require_once 'constant.e2e.php';
   require_once pathClass.'0620functions.e2e.php';
   /*RUNNING*/
   $diag = 0;
   $empRefId = getvalue("empRefId");
   $MasterFile = true;
   $rsPersonInfo = FindFirst("employees","WHERE RefId = $empRefId","*");
   $rsEmpFamily = FindFirst("employeesfamily","WHERE EmployeesRefId = $empRefId","*");
   $rsEmpChild = SelectEach("employeeschild","WHERE EmployeesRefId = $empRefId ORDER BY BirthDate");
   $rsEmpEducElem = FindFirst("employeeseduc","WHERE LevelType = 1 AND EmployeesRefId = $empRefId","*");
   $rsEmpEducSeco = FindFirst("employeeseduc","WHERE LevelType = 2 AND EmployeesRefId = $empRefId","*");
   $rsEmpEducVoca = FindFirst("employeeseduc","WHERE LevelType = 3 AND EmployeesRefId = $empRefId","*");
   $rsEmpEducColl = FindFirst("employeeseduc","WHERE LevelType = 4 AND EmployeesRefId = $empRefId","*");
   $rsEmpEducGrad = FindFirst("employeeseduc","WHERE LevelType = 5 AND EmployeesRefId = $empRefId","*");
   $rsEmpEligibility = SelectEach("employeeselegibility","WHERE EmployeesRefId = $empRefId ORDER BY ExamDate DESC");
   $rsEmpWorkExp = SelectEach("employeesworkexperience","WHERE EmployeesRefId = $empRefId ORDER BY WorkStartDate DESC");
   $rsEmpVoluntary = SelectEach("employeesvoluntary","WHERE EmployeesRefId = $empRefId ORDER BY RefId");
   $rsEmpTraining = SelectEach("employeestraining","WHERE EmployeesRefId = $empRefId ORDER BY RefId");
   $rsEmpOtherInfo = SelectEach("employeesotherinfo","WHERE EmployeesRefId = $empRefId ORDER BY RefId");
   $rsEmpReference = SelectEach("employeesreference","WHERE EmployeesRefId = $empRefId ORDER BY RefId");
   $rsPDSQ = FindFirst("employeespdsq","WHERE EmployeesRefId = $empRefId","*");

   function setValue($rs,$fields) {
      if (empty($rs[$fields])) {
         echo "N/A";
      } else {
         echo '<span style="color:blue;font-weight:600;">'.strtoupper($rs[$fields]).'</span>';
      }
   }
   function dispValue($str) {
      if ($str == "") {
         echo "N/A";
      } else {
         echo '<span style="color:blue;font-weight:600;">'.strtoupper($str).'</span>';
      }
   }

   function paging() {
      echo
      '<div class="noPrint">
         <a href="#page1">Page 1</a> | <a href="#page2">Page 2</a> | <a href="#page3">Page 3</a> | <a href="#page4">Page 4</a>
      </div>';
   }
?>
<!DOCTYPE>
<html>
   <head>
      <script type="text/javascript" src="<?php echo path("jquery/jquery.js") ?>"></script>
      <style type="text/css">
         #PDSbody
         {
            font-family:Arial Narrow;
            font-size:11pt;
         }
         td {padding-bottom:1px;padding-top:1px;}
         table {width:100%;border-collapse:collapse;}
         .tabTitle {font-family:Arial Narrow;font-size:11pt;font-weight:bold;}
         .tabTitle_td {border-top:2px solid #000;border-bottom:2px solid #000;}
         .bgGray {background:#bfbfbf;color:white;}
         .bgGrayLabel {background:#bfbfbf;color:black;vertical-align:top;}
         .answer{font-family:calibri;font-size:11pt;font-weight:600;color:blue;}
         .SignDate {font-family:Arial Narrow;font-size:10pt;font-weight:bold;color:black;}
         .b-left{border-left:1px solid #000}
         .b-right{border-right:1px solid #000}
         .b-top{border-top:1px solid #000}
         .b-bottom{border-bottom:1px solid #000}
         .bgBlueLabel{background:#b3d1ff;color:black;vertical-align:top;}
         .pagex-- {max-height:13.5in;width:8.5in;border:1px solid #ddd;}
         @media print {
            .ruler, .noPrint, .noPrint *{display: none !important;}
            .nextpage  {page-break-after:always !important;}
            .lastpage  {page-break-after:avoid !important;}
            .page-- {max-height:14in !important;width:8.5in !important;}
            #PDSbody
            {
               font-family:Arial Narrow !important;
               font-size:8pt;
               color:black;
            }
            .answer{font-family:Arial Narrow;font-size:9pt !important;font-weight:600;}
         }
      </style>
   </head>
   <body>
      <div id="PDSbody">
            <div class="page-- nextpage" id="page1">
               <?php
                  require_once("pds_Page1.e2e.php");
               ?>
            </div>
            <br class="noPrint">
            <div class="page-- nextpage" id="page2">
               <?php
                  require_once("pds_Page2.e2e.php");
               ?>
            </div>
            <br class="noPrint">
            <div id="page3" class="page-- nextpage">
               <?php
                  require_once("pds_Page3.e2e.php");
               ?>
            </div>
            <br class="noPrint">
            <div id="page4" class="page-- lastpage">
               <?php
                  require_once("pds_Page4.e2e.php");
               ?>
            </div>
      </div>
      <script type="text/javascript">
         <?php
            echo '$(document).ready(function() {';
               if ($rsEmpChild) {
                  $idx=34;
                  while ($row = mysqli_fetch_assoc($rsEmpChild)) {
                     if ($idx != 33) {
                        echo '$("#page1_I'.$idx.'").html("'.$row["FullName"].'");';
                        echo '$("#page1_M'.$idx.'").html("'.$row["BirthDate"].'");';
                     }
                     $idx++;
                  }
               }

               if ($rsEmpEducElem) {
                  echo '$("#page1_C51").html("'.getRecord("schools",$rsEmpEducElem["SchoolsRefId"],"Name").'");';
                  echo '$("#page1_F51").html("'.getRecord("course",$rsEmpEducElem["CourseRefId"],"Name").'");';
                  echo '$("#page1_J51").html("'.$rsEmpEducElem["DateFrom"].'");';
                  echo '$("#page1_K51").html("'.$rsEmpEducElem["DateTo"].'");';
                  echo '$("#page1_L51").html("'.$rsEmpEducElem["HighestGrade"].'");';
                  echo '$("#page1_M51").html("'.$rsEmpEducElem["YearGraduated"].'");';
                  echo '$("#page1_N51").html("'.$rsEmpEducElem["Honors"].'");';
               }

               if ($rsEmpEducSeco) {
                  echo '$("#page1_C53").html("'.getRecord("schools",$rsEmpEducSeco["SchoolsRefId"],"Name").'");';
                  echo '$("#page1_F53").html("'.getRecord("course",$rsEmpEducSeco["CourseRefId"],"Name").'");';
                  echo '$("#page1_J53").html("'.$rsEmpEducSeco["DateFrom"].'");';
                  echo '$("#page1_K53").html("'.$rsEmpEducSeco["DateTo"].'");';
                  echo '$("#page1_L53").html("'.$rsEmpEducSeco["HighestGrade"].'");';
                  echo '$("#page1_M53").html("'.$rsEmpEducSeco["YearGraduated"].'");';
                  echo '$("#page1_N53").html("'.$rsEmpEducSeco["Honors"].'");';
               }

               if ($rsEmpEducVoca) {
                  echo '$("#page1_C55").html("'.getRecord("schools",$rsEmpEducVoca["SchoolsRefId"],"Name").'");';
                  echo '$("#page1_F55").html("'.getRecord("course",$rsEmpEducVoca["CourseRefId"],"Name").'");';
                  echo '$("#page1_J55").html("'.$rsEmpEducVoca["DateFrom"].'");';
                  echo '$("#page1_K55").html("'.$rsEmpEducVoca["DateTo"].'");';
                  echo '$("#page1_L55").html("'.$rsEmpEducVoca["HighestGrade"].'");';
                  echo '$("#page1_M55").html("'.$rsEmpEducVoca["YearGraduated"].'");';
                  echo '$("#page1_N55").html("'.$rsEmpEducVoca["Honors"].'");';
               }
               if ($rsEmpEducColl) {
                  echo '$("#page1_C57").html("'.getRecord("schools",$rsEmpEducColl["SchoolsRefId"],"Name").'");';
                  echo '$("#page1_F57").html("'.getRecord("course",$rsEmpEducColl["CourseRefId"],"Name").'");';
                  echo '$("#page1_J57").html("'.$rsEmpEducColl["DateFrom"].'");';
                  echo '$("#page1_K57").html("'.$rsEmpEducColl["DateTo"].'");';
                  echo '$("#page1_L57").html("'.$rsEmpEducColl["HighestGrade"].'");';
                  echo '$("#page1_M57").html("'.$rsEmpEducColl["YearGraduated"].'");';
                  echo '$("#page1_N57").html("'.$rsEmpEducColl["Honors"].'");';
               }
               if ($rsEmpEducGrad) {
                  echo '$("#page1_C59").html("'.getRecord("schools",$rsEmpEducGrad["SchoolsRefId"],"Name").'");';
                  echo '$("#page1_F59").html("'.getRecord("course",$rsEmpEducGrad["CourseRefId"],"Name").'");';
                  echo '$("#page1_J59").html("'.$rsEmpEducGrad["DateFrom"].'");';
                  echo '$("#page1_K59").html("'.$rsEmpEducGrad["DateTo"].'");';
                  echo '$("#page1_L59").html("'.$rsEmpEducGrad["HighestGrade"].'");';
                  echo '$("#page1_M59").html("'.$rsEmpEducGrad["YearGraduated"].'");';
                  echo '$("#page1_N59").html("'.$rsEmpEducGrad["Honors"].'");';
               }

               // Eligibility
               echo '$(".ELIGIBILITY").html("N/A");';
               if ($rsEmpEligibility) {
                  $j = 0;
                  while ($row = mysqli_fetch_array($rsEmpEligibility)) {
                     $j++;
                     echo '
                     $("#page2_A'.($j + 3).'").html("'.getRecord("CareerService",$row["CareerServiceRefId"],"Name").'");
                     $("#page2_F'.($j + 3).'").html("'.$row["Rating"].'");
                     $("#page2_G'.($j + 3).'").html("'.$row["ExamDate"].'");
                     $("#page2_I'.($j + 3).'").html("'.$row["ExamPlace"].'");
                     $("#page2_L'.($j + 3).'").html("'.$row["LicenseNo"].'");
                     $("#page2_M'.($j + 3).'").html("'.$row["LicenseReleasedDate"].'");
                     ';
                  }
               }

               // Work Experience
               echo '$(".WORKEXPERIENCE").html("N/A");';
               if ($rsEmpWorkExp) {
                  $j = 0;
                  while ($row = mysqli_fetch_array($rsEmpWorkExp)) {
                     $j++;
                     echo '
                     $("#page2_O'.($j + 5).'").html("'.$row["WorkStartDate"].'");
                     $("#page2_Q'.($j + 5).'").html("'.$row["WorkEndDate"].'");
                     $("#page2_R'.($j + 5).'").html("'.getRecord("Position",$row["PositionRefId"],"Name").'");
                     $("#page2_U'.($j + 5).'").html("'.getRecord("Agency",$row["AgencyRefId"],"Name").'");
                     $("#page2_X'.($j + 5).'").html("'.$row["SalaryAmount"].'");
                     $("#page2_Y'.($j + 5).'").html("'.getRecord("SalaryGrade",$row["SalaryGradeRefId"],"Name").'");
                     $("#page2_Z'.($j + 5).'").html("'.getRecord("ApptStatus",$row["ApptStatusRefId"],"Name").'");';
                     if ($row["isGovtService"])
                        echo '$("#page2_AA'.($j + 5).'").html("YES");';
                     else
                        echo '$("#page2_AA'.($j + 5).'").html("NO");';
                  }
               }

               // VOLUNTARY
               echo '$(".VOLUNTARY").html("N/A");';
               if ($rsEmpVoluntary) {
                  $j = 0;
                  while ($row = mysqli_fetch_array($rsEmpVoluntary)) {
                     $j++;
                     echo '
                     $("#page3_A'.($j + 4).'").html("'.getRecord("Organization",$row["OrganizationRefId"],"Name").'");
                     $("#page3_E'.($j + 4).'").html("'.$row["StartDate"].'");
                     $("#page3_F'.($j + 4).'").html("'.$row["EndDate"].'");
                     $("#page3_G'.($j + 4).'").html("'.$row["NumofHrs"].'");
                     $("#page3_H'.($j + 4).'").html("'.$row["WorksNature"].'");';
                  }
               }

               echo '$(".TRAININGPROG").html("N/A");';
               if ($rsEmpTraining) {
                  $j = 0;
                  while ($row = mysqli_fetch_array($rsEmpTraining)) {
                     $j++;


                     echo '
                     $("#page3_L'.($j + 6).'").html("'.getRecord("Seminars",$row["SeminarsRefId"],"Name").'");
                     $("#page3_Q'.($j + 6).'").html("'.$row["StartDate"].'");
                     $("#page3_R'.($j + 6).'").html("'.$row["EndDate"].'");
                     $("#page3_S'.($j + 6).'").html("'.$row["NumofHrs"].'");
                     $("#page3_T'.($j + 6).'").html("'.$row["Description"].'");
                     $("#page3_U'.($j + 6).'").html("'.getRecord("Sponsor",$row["SponsorRefId"],"Name").'");
                     ';
                  }
               }

               echo '$(".OTHERINFO").html("N/A");';
               if ($rsEmpOtherInfo) {
                  $j = 0;
                  while ($row = mysqli_fetch_array($rsEmpOtherInfo)) {
                     $j++;
                     echo '
                     $("#page3_X'.($j + 6).'").html("'.$row["Skills"].'");
                     $("#page3_Y'.($j + 6).'").html("'.$row["Recognition"].'");
                     $("#page3_Z'.($j + 6).'").html("'.$row["Affiliates"].'");';
                  }
               }

               echo '$(".REFERENCES").html("N/A");';
               if ($rsEmpReference) {
                  $j = 0;
                  while ($row = mysqli_fetch_array($rsEmpReference)) {
                     $j++;
                     echo '
                     $("#page4_A'.($j + 53).'").html("'.$row["Name"].'");
                     $("#page4_E'.($j + 53).'").html("'.$row["Address"].'");
                     $("#page4_F'.($j + 53).'").html("'.$row["ContactNo"].'");';
                  }
               }
            echo '});';
         ?>
      </script>
   </body>
</html>

