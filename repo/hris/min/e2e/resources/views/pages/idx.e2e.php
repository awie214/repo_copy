<?php
   session_start();

   unset($_SESSION['cid']);
   include 'conn.e2e.php';
   include 'constant.e2e.php';
   $_SESSION["path"] = $path;
   $_SESSION["Classes"] = $pathClass;
   require_once pathClass."0620functions.e2e.php";
   require_once pathClass."SysFunctions.e2e.php";
   require_once pathClass."Forms.e2e.php";
   $filesContent = file_get_contents(json.'file.json');
   $files        = json_decode($filesContent, true);
   $_SESSION["jsonFile"] = $files;
   $sys = new SysFunctions();
   $form = new SysForm();

   $rs = mysqli_query($conn,"SELECT * FROM `company`");
   $today = f_encode(date("Ymd",time()));
   $mynewfile = path."css/~~az".$today.".css";
   foreach(glob(pathPublic.'css/~~az*.css') as $file) {
      if ($mynewfile != $file) {
         unlink($file);
      }
   }
   copy(pathPublic.'css/arc/tip.css',pathPublic.'css/~~az1'.$today.".css");
   if ($rs) {
      $row       = mysqli_fetch_assoc($rs);
      $BigLogo   = $row["BigLogo"];
      $SmallLogo = $row["SmallLogo"];
      $Logo      = $row["Logo"];
      $cid       = $row["RefId"];
      $color1    = $row["Color1"];
      $color2    = $row["Color2"];
      $settingsContent = file_get_contents(json."Settings_".$row["RefId"].".json");
      $settings = json_decode($settingsContent, true);
      $_SESSION["settings"] = $settings;
      $dirTarget = path."css";
      $myfile = fopen(path."css/arc/az.css", 'r') or die("Unable to open file!");
      $newFile = fopen($mynewfile, 'w');
      while(!feof($myfile)) {
         $str = fgets($myfile);
         $str = str_replace("var(--armyBlue1)","#".$color1,$str);
         $str = str_replace("var(--armyBlue2)","#".$color2,$str);
         fwrite($newFile,$str);
      }
      fclose($newFile);
      fclose($myfile);

   } else {
      balloonMsg("No Company Profile !!!","error");
      return false;
   }

   foreach(glob(pathPublic.'js/*.e2e.*.js') as $file) {
      unlink($file);
   }
   $lgMin = false;
   $header = 1;
   $sys->destroy_css("CSS_PIS");
   $css = ["armyBlue1","armyBlue2","bgPIS"];

   $_SESSION["0620utilities"]    = jsPath("js1_0620utilities");
   $_SESSION["0620functions"]    = jsPath("js2_0620functions");
   $_SESSION["0620SystemRoute"]  = jsPath("js3_0620SystemRoute");
   $_SESSION["0620sys"]          = jsPath("js4_0620sys");
   $_SESSION["load"]             = jsPath("js0_load");
   $_SESSION["toolTip"]          = jsPath("js5_toolTip");
   $_SESSION["jquery_utilities"] = jsPath("js6_jquery_utilities");
   $_SESSION["login"]            = jsPath("js7_login");
   $_SESSION["tip"]              = path.'css/~~az1'.$today.".css";
   $_SESSION["cssFilePath"]      = $mynewfile;
   $_SESSION["cidx"]             = $cid;
   $_SESSION["PayGroup"]         = ["Monthly","Semi-Monthly"];

   function doSignIn() {
      echo  
      '<div class="signInDiv">
         <div class="form-group">
            <input class="form-control mandatory--" type="text" id="inputUser" name="txtUser" placeholder="Username">
         </div>
         <div class="form-group">
            <input class="form-control mandatory--" type="password" id="inputPW" name="token" placeholder="Password">
         </div>
         <div class="txt-left">
            <button type="button" name="btnLogin" class="btn-cls4-sea" style="width:100%;border: 1px solid #999999;">
               Sign Me In
            </button>
            <button type="button" name="btnBack" id="btnBack" class="btn-cls4-red" style="width:100%;border: 1px solid #999999;">
               Back
            </button>
         </div>
         <br>
         <label class="text-center"> Supported Browser for <b>HRIS</b> <img src="../../../public/modules/Chrome.png" alt="Google Chrome" title="Google Chrome" style="width:35px;></label>
      </div>';
      /*<div class="txt-center margin-top">
         <hr>
         <span style="font-family:tahoma;font-size:10px;font-weight:600;">WELCOME TO HRIS</span>
      </div>*/
   }
   
?>
<html>
   <head>
      <title>HRIS</title>
      <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link href="<?php echo $sys->publicPath("css/font-awesome/css/font-awesome.min.css"); ?>" rel="stylesheet" type="text/css">
      <script type="text/javascript" src="<?php echo $path; ?>jquery/jquery.js"></script>
      <link rel="stylesheet" href="<?php echo $path; ?>bs/css/bootstrap.min.css" crossorigin="anonymous">
      <link rel="stylesheet" href="<?php echo $path; ?>bs/css/bootstrap-theme.min.css" crossorigin="anonymous">
      <script src="<?php echo $path; ?>bs/js/bootstrap.min.js" crossorigin="anonymous"></script>
      <link href="<?php echo $sys->publicPath("datatables/jquery.dataTables.min.css"); ?>" rel="stylesheet">
      <script type="text/javascript" src="<?php echo $sys->publicPath("datatables/jquery.dataTables.min.js"); ?>"></script>
      <script type="text/javascript" src="<?php echo $_SESSION["0620utilities"] ?>"></script>
      <script type="text/javascript" src="<?php echo $_SESSION["0620functions"] ?>"></script>
      <script type="text/javascript" src="<?php echo $_SESSION["0620sys"] ?>"></script>
      <script type="text/javascript" src="<?php echo $_SESSION["load"] ?>"></script>
      <script type="text/javascript" src="<?php echo $_SESSION["login"] ?>"></script>
      <script type="text/javascript" src="<?php echo $path; ?>js/jsSHAver2/src/sha.js"></script>
      <script type="text/javascript" src="<?php echo $path; ?>js/notify.js"></script>
      <script type="text/javascript" src="<?php echo jsCtrl("ctrl_idx"); ?>"></script>
      <link rel="stylesheet" href="<?php echo $_SESSION["cssFilePath"]; ?>">
      <link rel="stylesheet" href="<?php echo $_SESSION["tip"]; ?>">
      <link href="<?php echo $sys->publicPath("datepicker/css/datepicker.css"); ?>" rel="stylesheet">
      <script type="text/javascript" src="<?php echo $sys->publicPath("datepicker/js/bootstrap-datepicker.js"); ?>"></script>
      <style>
         .signInDiv {
            padding:20px;
            max-height:250px;
            max-width:320px;
            /*background-color:#d9d9d9;*/
            /*border: 1px solid #999999;*/
            color: black;
         }
         .form-control {
            border: 1px solid #999999;
            border-radius: 0;
         }
         .midBody {
            height: calc(100vh - 85px);
         }
         .sysNameHolder {
            height: 60px;
            text-align: center;
            align-items: center;
            margin:0;
         }
         .sysName {
            font-family:Arial;
            font-size:16pt;
            font-weight:500;
            display:inline-block;
            vertical-align:middle;
            line-height:60px;
            margin:0;
         }
         .outer {
             display: table;
             position: absolute;
             height: calc(100% - 130px);
             width: 100%;
             background:transparent;
         }
         .middle {
             display: table-cell;
             vertical-align: middle;
             background:transparent;
         }
         .inner {
             margin-left: auto;
             margin-right: auto;
             height:100%;
             background:#ffffff;
             background:url("<?php echo img($cid.'/'.$BigLogo); ?>") no-repeat center;
         }
         .tdBG {
            width:100%;
            height:100%;
            background-image: url("<?php echo img($cid.'/'.$Logo); ?>");
            background-repeat:no-repeat;
            background-size:100% 100%;
         }
         .center {
            display: block;
            position:absolute;
            top:0;
            bottom:0;
            left:0;
            right:0;
            margin:auto;
         }
         #imgBG {
            width:100%;
            height:100%;
         }
         .btest {
            border: 1px solid black;
         }
         .module_name {
            font-weight: 600;
         }
         .pad10 {
            padding: 20px;
         }
         .module_title {
            border-radius: 10px;
         }
         .module_title:hover {
            box-shadow: 3px 3px 3px 3px gray;
            transition: 0.5s;
            background: #dcdee2;
         }
      </style>
      <?php
         if (isset($_GET["module"]) && isset($_GET["account"])) {
            $sys = $_GET["module"];
            switch ($sys) {
               case 'pis':
                  $sys_header = "PERSONAL INFORMATION SYSTEM";
                  break;
               case 'ams':
                  $sys_header = "ATTENDANCE MANAGEMENT SYSTEM";
                  break;
               case 'eservice':
                  $sys_header = "Human Resource And Payroll Management System";
                  break;
               case 'spms':
                  $sys_header = "STRATEGIC PERFORMANCE MANAGEMENT SYSTEM";
                  break;
               case 'ldms':
                  $sys_header = "LEARNING AND DEVELOPMENT MANAGEMENT SYSTEM";
                  break;
               default:
                  $sys_header = "HUMAN RESOURCE INFORMATION SYSTEM";
                  break;
            }
            echo '
            <script>
               $(document).ready(function(){
                  $("#hAccount").val("'.$_GET["account"].'");
                  $("#hModule").val("'.$_GET["module"].'");
                  $("#module_div, #module_div_2").hide();
                  $("#login_div").show();   
                  $(".sysName").html("'.$sys_header.'");
               });
            </script>
            ';
            if ($cid == 14 && $_GET["account"] == "employee") {
               echo '
               <script>
                  $(document).ready(function(){
                     $("#btnBack").hide();
                  });
               </script>
               ';  
            }
         }
         if ($cid == 1000 || $cid == 35) {
            echo '
            <script>
               $(document).ready(function(){
                  $("#module_div, #module_div_2, #btnBack").hide();
                  $("#login_div").show();   
               });
            </script>
            ';  
         }
         
      ?>
      <script type="text/javascript">
         $(document).ready(function(){
            $("#aLogin").click(function () {
               $("#div_login").show();
               $("#div_logo").hide();
               $("#inputUser").focus();
            });
         });
         sessionStorage.clear();
      </script>
   </head>
   <body style="padding:0;" class="wmlogo">
      <div class="container-fluid">
      <form name="formlogin" method="post" action="login.e2e.php">
         <div style="padding:0;">
            <div class="row">
               <div class="col-xs-12">
                  <div class="sysNameHolder sysBG">
                     <span class="sysName">
                        <?php
                           echo $settings["Title"];
                        ?>
                     </span>
                  </div>
               </div>
            </div>
            <div class="midBody wmlogo" style="padding:0;">
               <?php
                  if (getvalue("login_error") == "yes") {
                     balloonMsg(getvalue("msg"),"warn");
                  }
                  if (getvalue("cpwsuccess") == "yes") {
                     balloonMsg("User Password Succesfully Changed...","success");
                  }
                  if (getvalue("sessExpired") == "yes"){
                     balloonMsg("Session Has Been Expired... ".getvalue("msg"),"info");
                  }
                  if (getvalue("pageAuth") == "no"){
                     balloonMsg("Page Not Auth...","info");
                  }
                  if (getvalue("wrongLoginInfo") == "yes"){
                     balloonMsg("No Specific Login Info ... ","error");
                  }
                  if (getvalue("inactive") == "yes") {
                    balloonMsg("Your Login info is inactive ... ","error");
                  }
                  $signin = "block";
                  $imgID = ""; 
               ?>
               <div id="module_div">
                  <div class="row">
                     <div class="col-xs-12">
                        <div class="container-fluid">
                           <div class="row">
                              <div class="col-xs-12">
                                 <table style="width: 100%; height: 90%;">
                                    <tr>
                                       <td style="width: 8.33%;"></td>
                                       <td style="width: 8.33%;"></td>
                                       <td style="width: 8.33%;"></td>
                                       <td style="width: 8.33%;"></td>
                                       <td style="width: 8.33%;"></td>
                                       <td style="width: 8.33%;"></td>
                                       <td style="width: 8.33%;"></td>
                                       <td style="width: 8.33%;"></td>
                                       <td style="width: 8.33%;"></td>
                                       <td style="width: 8.33%;"></td>
                                       <td style="width: 8.33%;"></td>
                                       <td style="width: 8.33%;"></td>
                                    </tr>
                                    <tr>
                                       <td rowspan="4" colspan="6" class="text-center">
                                          <img src="<?php echo path."images/".$cid."/".$Logo; ?>" id="<?php echo $imgID; ?>" style="width:80%;">
                                       </td>
                                       <?php
                                          if ($cid == 14) {
                                             echo '<td colspan="6"></td>';
                                          } else {
                                       ?>
                                       <td></td>
                                       <td class="text-center pad10 module_title" 
                                           id="user_employee" 
                                           module="eservice" 
                                           colspan="4">
                                          <img src="<?php echo $path; ?>modules/008-laptop.png" 
                                               style="width: 15%; margin-bottom:10px;">
                                          <br>
                                          <span class="module_name">
                                             USER PORTAL SERVICES
                                          </span>
                                       </td>
                                       <td></td>
                                       <?php } ?>
                                       
                                    </tr>
                                    <tr>
                                       <td class="text-center pad10 module_title" id="user_admin" module="pis" colspan="3">
                                          <img src="<?php echo $path; ?>modules/001-screen.png" style="width: 15%; margin-bottom:10px;">
                                          <br>
                                          <span class="module_name">
                                             PERSONAL INFORMATION
                                             <br>
                                             SYSTEM
                                          </span>
                                       </td>
                                       <td class="text-center pad10 module_title" id="user_admin" module="ams" colspan="3">
                                          <img src="<?php echo $path; ?>modules/007-time.png" style="width: 15%; margin-bottom:10px;">
                                          <br>
                                          <span class="module_name">
                                             ATTENDANCE MANAGEMENT
                                             <br>
                                             SYSTEM
                                          </span>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td class="text-center pad10 module_title" id="user_admin" module="pms" colspan="3">
                                          <img src="<?php echo $path; ?>modules/003-atm.png" style="width: 15%; margin-bottom:10px;">
                                          <br>
                                          <span class="module_name">
                                             PAYROLL MANAGEMENT
                                             <br>
                                             SYSTEM
                                          </span>
                                       </td>
                                       <td class="text-center pad10 module_title" id="user_admin" module="rms" colspan="3">
                                          <img src="<?php echo $path; ?>modules/006-interview.png" style="width: 15%; margin-bottom:10px;">
                                          <br>
                                          <span class="module_name">
                                             RECRUITMENT MANAGEMENT
                                             <br>
                                             SYSTEM
                                          </span>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td class="text-center pad10 module_title" id="user_admin" module="spms" colspan="3">
                                          <img src="<?php echo $path; ?>modules/004-performance.png" style="width: 15%; margin-bottom:10px;">
                                          <br>
                                          <span class="module_name">
                                             STRATEGIC PERFORMANCE
                                             <br>
                                             MANAGEMENT SYSTEM
                                          </span>
                                       </td>
                                       <td class="text-center pad10 module_title" id="user_admin" module="ldms" colspan="3">
                                          <img src="<?php echo $path; ?>modules/005-teacher.png" style="width: 15%; margin-bottom:10px;">
                                          <br>
                                          <span class="module_name">
                                             LEARNING AND DEVELOPMENT
                                             <br>
                                             MANAGEMENT SYSTEM
                                          </span>
                                       </td>
                                    </tr>
                                 </table>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div id="login_div">
                  <div class="row" style="height:100%;margin:0;padding:0;" id="div_login">
                     <div class="col-xs-12" style="height:100%;margin:0;padding:0;">
                        <div class="login scrns" id="login" style="position:relative;width:100%;height:100%;padding:0;vertical-align:middle;">
                           <table style="width:100%;height:calc(100vh - 60px);">
                              <tr>
                                 <td width="5%"></td>
                                 <td width="50%" valign="middle" align="center">
                                    <img src="<?php echo path."images/".$cid."/".$Logo; ?>" id="<?php echo $imgID; ?>" style="width:80%;">
                                 </td>
                                 <td width="40%" align="center" valign="middle">
                                    <?php doSignIn(); ?>
                                 </td>
                                 <td width="5%">&nbsp;</td>
                              <tr>
                           </table>    
                        </div>
                        <div id="EntryScrn"></div>
                        <?php
                           if (getvalue("expired") == "yes") {
                              $msg = "";
                              $msg .= "<p>sess : ".getvalue("hSess")."</p>";
                              $msg .= "<p>user : ".getvalue("hUser")."</p>";
                              $msg .= "<p>mid : ".getvalue("id")."</p>";
                              $msg .= "<p>tbl : ".getvalue("hTable")."</p>";
                              msg("warn",$msg);
                           }
                        ?>
                     </div>
                  </div>
               </div>

               <?php
                  footer();
                  //include_once ("varHidden.e2e.php");
               ?>
            </div>
         <input type="hidden" name="hToken" value="">
         <input type="hidden" id="hmode"  name="hmode"  value="">
         <input type="hidden" id="hRefId" name="hRefId"  value="">
         <input type="hidden" id="hAccount" name="hAccount"  value="">
         <input type="hidden" id="hModule" name="hModule"  value="">
      </form>
      </div>
   </body>
</html>