<div class="modal fade" id="selectSchool" role="dialog">
   <div class="modal-dialog modal-lg" style="width: 60%;">
      <div class="mypanel">
         <div class="panel-top">
            LIST OF SCHOOL
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="panel-mid">
            <div class="row">
               <div class="col-sm-12" id="tbl_school">

               </div>
            </div>
         </div>
         <div class="panel-bot">
            &nbsp;
         </div>
      </div>
   </div>
</div>
<?php 
   $templ->splitSettings();
   include "pds_modal.e2e.php"; 
?>
<div id="DataEntry">
   <div class="row margin-top">
      <div class="col-sm-12">
         <?php
            /*Non User Side*/
            if ($GLOBALS["UserCode"]!="COMPEMP") {
         ?>
               <div class="btn-group btn-group-md">
                  <button type="button" class="btn-cls4-sea trnbtn"
                          name="btnSAVE" id="btnSAVE">
                     <i class="fa fa-floppy-o" aria-hidden="true"></i>
                        &nbsp;&nbsp;SAVE
                  </button>
                  <button type="button" class="btn-cls4-red trnbtn"
                              name="btnCANCEL" id="btnCANCEL">
                        <i class="fa fa-undo" aria-hidden="true"></i>
                        &nbsp;&nbsp;CANCEL
                  </button>
                  
               </div>
         <?php
            } else {
               $EmployeesRefId = getvalue("hEmpRefId");
               $completion = FindFirst("employees","WHERE RefId = '$EmployeesRefId'","Completed");
         ?>
               <button type="button" class="btn-cls4-sea"
                       name="btnUPDATE" id="btnUPDATE">
                  <i class="fa fa-floppy-o" aria-hidden="true"></i>
                     &nbsp;&nbsp;SAVE
               </button>
               <button type="button" class="btn-cls4-lemon"
                       name="btnPREVIEW" id="btnPREVIEW">
                  <i class="fa fa-file-text" aria-hidden="true"></i>
                     &nbsp;&nbsp;FILE PREVIEW
               </button>
               <?php createButton("Print Preview","btnPrintMF","btn-cls4-lemon","fa-print",""); ?>
               <button type="button" class="btn-cls4-tree"
                       name="btnSplitSetting" id="aSplitSettings">
                  <i class="fa fa-file-text" aria-hidden="true"></i>
                     &nbsp;&nbsp;Split Settings
               </button>
               <?php
                  if (getvalue("hCompanyID") == 2) {
                     echo '
                        <button type="button" class="btn-cls4-sea">
                           <a href="forms/2/PCCUSERGUIDEFINAL.pdf" style="color:white !important; text-decoration:none !important;" download>
                              DOWNLOAD - PIS MANUAL
                           </a>
                        </button>
                     ';
                  }
               ?>
               <button type="button" class="btn-cls4-sea"
                       name="btn_complete" id="btn_complete">
                  <i class="fa fa-check" aria-hidden="true"></i>
                     &nbsp;COMPLETED
               </button>

         <?php
            }
         ?>
         <input type="hidden" name="fn" value="Update201">
      </div>
   </div>
   <div class="row" style="margin-top:5px;">
      <div class="col-sm-12">
         <input type="hidden" name="hTabIdx" value="1">
         <div class="btn-group btn-group-sm">
            <?php
               $idx = 0;
               $active = "";
               for ($j=0;$j<count($tabTitle);$j++) {
                  $idx = $j + 1;
                  if ($idx == 1) $active = "elActive";
                            else $active = "";
                  echo
                  '<button type="button" idx="'.$idx.'" name="btnTab_'.$idx.'" class="btn btn-default '.$active.'">'.$tabTitle[$j].'</button>'."\n";
               }
            ?>
         </div>
         <div class="row">
            <div class="col-sm-12">
               <?php
                  spacer(5);
                  $idx = 0;
                  for ($j=0;$j<count($tabTitle);$j++) {
                     $idx = $j + 1;
                     fTabs($idx,$tabTitle[$j],$tabFile[$j]);
                  }
               ?>
            </div>
         </div>
      </div>
   </div>
   <div class="modal fade" id="filereview" role="dialog">
      <div class="modal-dialog modal-lg" style="width: 98%;">
         <div class="mypanel">
            <div class="panel-top">
               FILE REVIEW
               <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="panel-mid">
               <?php include 'filereview.e2e.php'; ?>
            </div>
            <div class="panel-bot">
               <div class="row text-right" style="padding: 5px;" id="btn201">
                  <div class="col-sm-12">
                     <button type="button" class="btn-cls4-red class" data-dismiss="modal">
                        <i class="fa fa-times" aria-hidden="true"></i>
                        &nbsp;Cancel
                     </button>
                     <button type="button" class="btn-cls4-sea" id="btnSAVE201">
                        <i class="fa fa-floppy-o" aria-hidden="true"></i>
                        &nbsp;Save
                     </button>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript">
   $(document).ready(function () {
      var completion = <?php echo intval($completion); ?>;
      if (completion == 1) {
         $("#btn_complete, [class*='enabler--']").prop("disabled",true);
         $("[class*='addRow']").hide();
      }
   });
</script>