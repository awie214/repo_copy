<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <style type="text/css">
         th {
            text-align: center;
            vertical-align: top;
            background: #d9d9d9;
         }
         .td-input {
            padding: 2px;
         }
      </style>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post">
         <?php $sys->SysHdr($sys,"ldms"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar("INDIVIDUAL DEVELOPMENT PLAN"); ?>
            <div class="container-fluid margin-top">
               <div class="mypanel">
                  <div class="row">
                     <div class="col-xs-3">
                        <?php
                           employeeSelector();
                        ?>
                     </div>
                     <div class="col-xs-9" style="padding: 5px;">
                        <div class="row">
                           <div class="col-xs-12">
                              <button type="button" id="ldms_edit" name="ldms_edit" class="btn-cls4-sea">
                                 <i class="fa fa-edit"></i>&nbsp;EDIT
                              </button>
                              <button type="button" id="ldms_print" name="ldms_print" class="btn-cls4-lemon">
                                 <i class="fa fa-print"></i>&nbsp;PRINT
                              </button>
                           </div>
                        </div>
                        <div class="row margin-top">
                           <div class="col-xs-12">
                              <div class="panel-top margin-top">
                                 INDIVIDUAL DEVELOPMENT PLAN
                              </div>
                              <div class="panel-mid" style="padding: 10px;">
                                 <div class="row">
                                    <div class="col-xs-2">
                                       <label>Current Position:</label>
                                    </div>
                                    <div class="col-xs-4">
                                       <input class="form-input" type="text" name="current_position" id="current_position" readonly>
                                    </div>
                                    <div class="col-xs-2">
                                       <label>Two-Year Period:</label>
                                    </div>
                                    <div class="col-xs-4">
                                       <input class="form-input" type="text" name="year_period" id="year_period" readonly>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-2">
                                       <label>Salary Grade:</label>
                                    </div>
                                    <div class="col-xs-4">
                                       <input class="form-input" type="text" name="salary_grade" id="salary_grade" readonly>
                                    </div>
                                    <div class="col-xs-2">
                                       <label>Division:</label>
                                    </div>
                                    <div class="col-xs-4">
                                       <input class="form-input" type="text" name="current_division" id="current_division" readonly>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-2">
                                       <label>Years in Position:</label>
                                    </div>
                                    <div class="col-xs-4">
                                       <input class="form-input" type="text" name="position_years" id="position_years" readonly>
                                    </div>
                                    <div class="col-xs-2">
                                       <label>Office:</label>
                                    </div>
                                    <div class="col-xs-4">
                                       <input class="form-input" type="text" name="current_office" id="current_office" readonly>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-2">
                                       <label>Years in Agency:</label>
                                    </div>
                                    <div class="col-xs-4">
                                       <input class="form-input" type="text" name="agency_years" id="agency_years" readonly>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-12">
                                       <?php bar(); ?>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-6">
                                       <div class="row">
                                          <div class="col-xs-12">
                                             No further development is desired or required for this year/s
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-4">
                                             <input type="checkbox" name="year_one">&nbsp;<b>Year 1</b>
                                          </div>
                                          <div class="col-xs-4">
                                             <input type="checkbox" name="year_two">&nbsp;<b>Year 2</b>
                                          </div>
                                          <div class="col-xs-4">
                                             <input type="checkbox" name="year_both">&nbsp;<b>Both Years</b>
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-4">
                                             Supervisor's Name
                                          </div>
                                          <div class="col-xs-8">
                                             <input type="text" name="supervisor" id="supervisor" class="form-input">
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-xs-6">
                                       <div class="row">
                                          <div class="col-xs-12">
                                             Purpose
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-12">
                                             <input type="checkbox" name="purpose_1" id="purpose_1">&nbsp;
                                             To meet the comptencies of current position
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-12">
                                             <input type="checkbox" name="purpose_2" id="purpose_2">&nbsp;
                                             To increase the level of competencies of current position
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-12">
                                             <input type="checkbox" name="purpose_3" id="purpose_3">&nbsp;
                                             To meet the competencies of the next higher position
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-12">
                                             <input type="checkbox" name="purpose_4" id="purpose_4">&nbsp;
                                             To acquire new competencies accross different functions/position
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-12">
                                             <input type="checkbox" name="purpose_5" id="purpose_5">&nbsp;
                                             Others, please specify
                                             <br>
                                             <input type="text" name="other_exp" id="other_exp" class="form-input margin-top">
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <?php
                                    spacer(30);
                                 ?>
                                 <div class="row margin-top">
                                    <div class="col-xs-12">
                                       <button type="button" id="btn_competency" class="btn-cls4-sea">
                                          COMPETENCY ASSESSMENT AND DEVELOPMENT PLAN
                                       </button>
                                    </div>
                                 </div>
                                 <br>
                                 <div class="row margin-top" id="competency_div">
                                    <div class="col-xs-12">
                                       <table width="100%" border="1">
                                          <thead>
                                             <tr>
                                                <th>
                                                   Target Competency<br>(1)
                                                </th>
                                                <th>
                                                   Priority<br>IDP<br>(2)
                                                </th>
                                                <th>Code</th>
                                                <th>
                                                   Specific Behavioral<br>
                                                   Indicators NOT<br>
                                                   Consistently<br>
                                                   Demonstrated
                                                   (3)
                                                </th>
                                                <th>
                                                   Development<br>
                                                   Activity<br>
                                                   (4)
                                                </th>
                                                <th>
                                                   Support<br>
                                                   Needed(5)
                                                </th>
                                                <th>
                                                   Trainer/<br>
                                                   Provider<br>
                                                   (6)
                                                </th>
                                                <th>
                                                   Schedule or<br>
                                                   Completion<br>
                                                   Date<br>
                                                   (7)
                                                </th>
                                             </tr>
                                          </thead>
                                          <tbody>
                                             <?php
                                                for ($i=1; $i <= 5; $i++) { 
                                                   echo '<tr id="tbl_row_'.$i.'">';
                                                   echo '
                                                      <td class="td-input">
                                                         <input type="text" 
                                                                class="form-input" 
                                                                name="target_'.$i.'" 
                                                                id="target_'.$i.'">
                                                      </td>
                                                      <td class="td-input">
                                                         <input type="text" 
                                                                class="form-input" 
                                                                name="priority_'.$i.'" 
                                                                id="priority_'.$i.'">
                                                      </td>
                                                      <td class="td-input">
                                                         <input type="text" 
                                                                class="form-input" 
                                                                name="code_'.$i.'" 
                                                                id="code_'.$i.'">
                                                      </td>
                                                      <td class="td-input">
                                                         <input type="text" 
                                                                class="form-input" 
                                                                name="behavioral_'.$i.'" 
                                                                id="behavioral_'.$i.'">
                                                      </td>
                                                      <td class="td-input">
                                                         <input type="text" 
                                                                class="form-input" 
                                                                name="development_'.$i.'" 
                                                                id="development_'.$i.'">
                                                      </td>
                                                      <td class="td-input">
                                                         <input type="text" 
                                                                class="form-input" 
                                                                name="support_'.$i.'" 
                                                                id="support_'.$i.'">
                                                      </td>
                                                      <td class="td-input">
                                                         <input type="text" 
                                                                class="form-input" 
                                                                name="trainer_'.$i.'" 
                                                                id="trainer_'.$i.'">
                                                      </td>
                                                      <td class="td-input">
                                                         <input type="text" 
                                                                class="form-input" 
                                                                name="schedule_'.$i.'" 
                                                                id="schedule_'.$i.'">
                                                      </td>
                                                   ';
                                                   echo '</tr>';
                                                }
                                             ?>
                                             <tr>
                                                <td colspan="8" class="td-input">
                                                   <button type="button" class="btn-cls4-tree" id="add_row">
                                                      Add row
                                                   </button>
                                                   <button type="button" class="btn-cls4-red" id="delete_row">
                                                      Delete last row
                                                   </button>
                                                </td>
                                             </tr>
                                          </tbody>
                                       </table>
                                    </div>
                                 </div>
                              </div>
                              <div class="panel-bottom">
                                 <input type="hidden" name="emprefid" id="emprefid">
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="prnModal" role="dialog">
               <div class="modal-dialog" style="height:90%;width:80%">
                  <div class="mypanel" style="height:100%;">
                     <div class="panel-top bgSilver">
                        <a href="#" data-toggle="tooltip" data-placement="top" id="btnPRINTNOW">
                           <i class="fa fa-print" aria-hidden="true"></i>
                        </a>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                  </div>
               </div>
            </div>
            <?php
               footer();
               doHidden("paramTitle",getvalue("paramTitle"),"");
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
   <script type="text/javascript">
      $(document).ready(function () {
         $("#ldms_print").click(function () {
            var refid = $("#emprefid").val();
            if (refid == "") {
               $.notify("No Employee Selected");
               return false;
            }
            $("#rptContent").attr("src","blank.htm");
            var rptFile = "rpt_ldms_idp";
            var url = "ReportCaller.e2e.php?file=" + rptFile;
            url += "&refid=" + refid;
            url += "&" + $("[name='hgParam']").val();
            $("#prnModal").modal();
            $("#rptContent").attr("src",url);
         });
         $("#add_row").click(function () {
            var last = "";
            var str = "";
            $("[id*='tbl_row_']").each(function () {
               last = $(this).attr("id").split("_")[2];
            });
            var idx = parseInt(last) + 1;
            
            str += '<tr id="tbl_row_' + idx + '">';
            str += '<td class="td-input">';
            str += '<input type="text" class="form-input" name="target_' + idx + '" id="target_' + idx + '">';
            str += '</td>';
            str += '<td class="td-input">';
            str += '<input type="text" class="form-input" name="priority_' + idx + '" id="priority_' + idx + '">';
            str += '</td>';
            str += '<td class="td-input">';
            str += '<input type="text" class="form-input" name="code_' + idx + '" id="code_' + idx + '">';
            str += '</td>';
            str += '<td align="center">';
            str += '<input type="text" class="form-input" name="behavioral_' + idx + '" id="behavioral_' + idx + '">';
            str += '</td>';
            str += '<td align="center">';
            str += '<input type="text" class="form-input" name="development_' + idx + '" id="development_' + idx + '">';
            str += '</td>';
            str += '<td align="center">';
            str += '<input type="text" class="form-input" name="support_' + idx + '" id="support_' + idx + '">';
            str += '</td>';
            str += '<td align="center">';
            str += '<input type="text" class="form-input" name="trainer_' + idx + '" id="trainer_' + idx + '">';
            str += '</td>';
            str += '<td class="td-input">';
            str += '<input type="text" class="form-input" name="schedule_' + idx + '" id="schedule_' + idx + '">';
            str += '</td>';
            str += '</tr>';
            $("#tbl_row_" + last +":last").after(str);
         });
         $("[id*='delete_row']").each(function () {
            $(this).click(function () {
               var last = "";
               $("[id*='tbl_row_']").each(function () {
                  last = $(this).attr("id").split("_")[2];
               });
               $("#tbl_row_" + last).remove();
            });
         });
         $("#competency_div").hide();
         $("#btn_competency").click(function () {
            $("#competency_div").toggle();
         });
      });

      function selectMe(emprefid) {
         $("#emprefid").val(emprefid);
      }
      // function selectMe(emprefid) {
      //    $.get("employee_information.e2e.php",
      //    {
      //       emprefid:emprefid
      //    },
      //    function(data,status) {
      //       if(status == "error")return false;
      //       if(status == "success"){
      //          var data = JSON.parse(data);
      //          try {
                  
      //          }
      //          catch (e)
      //          {
      //             if (e instanceof SyntaxError) {
      //                 alert(e.message);
      //             }
      //          }
      //       }
      //    });
      // }
   </script>
</html>



