
<?php
/*
------------------------------------------------------------------------
    File        : pds_Page4.php
    Purpose     :
    Syntax      : PHP / HTML
    Description :
    Author(s)   : Erwin Mendoza / emendoza0620@gmail.com
    Created     :
    Notes       :
------------------------------------------------------------------------
Developer   Date     Changes
--------------------------------------------------------------------------
--------------------------------------------------------------------------*/
?>
      <table border=1 cellspacing=0 cellpadding=1>
            <tr align="center" class="ruler">
               <td width="20px"></td>
               <td width="139px"></td>
               <td width="180px"></td>
               <td width="14px"></td>
               <td width="215px"></td>
               <td width="17px"></td>
               <td width="55px"></td>
               <td width="11px"></td>
               <td width="15px"></td>
               <td width="148px"></td>
               <td width="23px"></td>
            </tr>
            <tr>
               <td class="bgGrayLabel" align="center" rowspan="7">34.</td>
               <td class="bgGrayLabel" colspan="4" rowspan="3" valign="top">
                  Are you related by consanguinity or affinity to the appointing or recommending authority, or to the
                  <br>
                  chief of bureau or office or to the person who has immediate supervision over you in the Office,
                  <br>
                  Bureau or Department where you will be appointed,
               </td>
               <td colspan="6" rowspan="3"></td>
            </tr>
            <tr></tr>
            <tr></tr>
            <tr>
               <td class="bgGrayLabel" colspan="4" rowspan="2" valign="top">
                  a. within the third degree?
               </td>
               <td></td>
               <td colspan="4">
                  <?php
                     if ($MasterFile) {
                        if ($rsPDSQ["Q1a"]) {
                           chkBox("YES",1);
                           echo "&nbsp;";
                           chkBox("NO",0);
                        } else {
                           chkBox("YES",0);
                           echo "&nbsp;";
                           chkBox("NO",1);
                        }
                     } else {
                        if (putValue("F4") == "YES") {
                           chkBox("YES",1);
                           echo "&nbsp;";
                           chkBox("NO",0);
                        } else {
                           chkBox("YES",0);
                           echo "&nbsp;";
                           chkBox("NO",1);
                        }
                     }
                  ?>
               </td>
               <td></td>
            </tr>
            <tr>
               <td></td>
               <td colspan="5" class="pdsq_exp">
                  If YES, give details:
                  <br>
                  <span class="answer"><?php
                     if ($MasterFile) {
                        echo $rsPDSQ["Q1aexp"];
                     } else {
                        echo putValue("G5");
                     } ?>
                  </span>
                  
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel" colspan="4" rowspan="2">
                  b. within the fourth degree (for Local Government Unit - Career Employees)?
               </td>
               <td></td>
               <td colspan="4" >
                  <?php
                     if ($MasterFile) {
                        if ($rsPDSQ["Q1b"]) {
                           chkBox("YES",1);
                           echo "&nbsp;";
                           chkBox("NO",0);
                        } else {
                           chkBox("YES",0);
                           echo "&nbsp;";
                           chkBox("NO",1);
                        }
                     } else {
                        if (putValue("F8") == "YES") {
                           chkBox("YES",1);
                           echo "&nbsp;";
                           chkBox("NO",0);
                        } else {
                           chkBox("YES",0);
                           echo "&nbsp;";
                           chkBox("NO",1);
                        }
                     }
                  ?>
               </td>
               <td></td>
            </tr>
            <tr>
               <td></td>
               <td colspan="5" class="pdsq_exp">
                  If YES, give details:
                  <br>
                  <span class="answer">
                     <?php
                        if ($MasterFile) {
                           echo $rsPDSQ["Q1bexp"];
                        } else {
                           echo putValue("G9");
                        }
                     ?>
                  </span>
                  
               </td>

            </tr>
            <tr>
               <td class="bgGrayLabel" align="center" rowspan="6">35.</td>
               <td colspan="4" rowspan="2" class="bgGrayLabel">
                  a. Have you ever been found guilty of any administration offense?
               </td>
               <td></td>
               <td colspan="4" >
                  <?php
                     if ($MasterFile) {
                        if ($rsPDSQ["Q2a"]) {
                           chkBox("YES",1);
                           echo "&nbsp;";
                           chkBox("NO",0);
                        } else {
                           chkBox("YES",0);
                           echo "&nbsp;";
                           chkBox("NO",1);
                        }
                     } else {
                        if (putValue("F12") == "YES") {
                           chkBox("YES",1);
                           echo "&nbsp;";
                           chkBox("NO",0);
                        } else {
                           chkBox("YES",0);
                           echo "&nbsp;";
                           chkBox("NO",1);
                        }
                     }
                  ?>
               </td>
               <td></td>
            </tr>
            <tr>
               <td></td>
               <td colspan="5" class="pdsq_exp">
                  If YES, give details:
                  <br>
                  <span class="answer">
                  <?php
                     if ($MasterFile) {
                        echo $rsPDSQ["Q2aexp"];
                     } else {
                        echo putValue("G13");
                     }
                  ?>
                  </span>
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel" colspan="4" rowspan="4">
                  b. Have you been criminally charged before any court?
               </td>
               <td></td>
               <td colspan="4" >
                  <?php
                     if ($MasterFile) {
                        if ($rsPDSQ["Q2b"]) {
                           chkBox("YES",1);
                           echo "&nbsp;";
                           chkBox("NO",0);
                        } else {
                           chkBox("YES",0);
                           echo "&nbsp;";
                           chkBox("NO",1);
                        }
                     } else {
                        if (putValue("F16") == "YES") {
                           chkBox("YES",1);
                           echo "&nbsp;";
                           chkBox("NO",0);
                        } else {
                           chkBox("YES",0);
                           echo "&nbsp;";
                           chkBox("NO",1);
                        }
                     }
                  ?>
               </td>
               <td></td>
            </tr>
            <tr>
               <td></td>
               <td colspan="5" class="pdsq_exp">
                  If YES, give details:
                  <br>
                  <span class="answer">
                  <?php
                     if ($MasterFile) {
                        echo $rsPDSQ["Q2bexp"];
                     } else {
                        echo putValue("F16");
                     }
                  ?>
                  </span>
               </td>
            </tr>
            <tr>
               <td></td>
               <td></td>
               <td colspan="4">
                  Date Filled:<span class="answer"><?php if ($MasterFile) echo dispVal($rsPDSQ["Q2DateFiled"]); ?></span>
               </td>
            </tr>
            <tr>
               <td></td>
               <td></td>
               <td colspan="4">
                  Status of Case/s:<u><span class="answer"><?php if ($MasterFile) echo dispVal($rsPDSQ["Q2CaseStatus"]); ?></span></u>
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel" align="center" rowspan="2">36.</td>
               <td class="bgGrayLabel" colspan="4" rowspan="2">
                  Have you ever been convicted of any crime or violation of any law, decree, ordinance, or regulation by
                  any court or tribunal?
               </td>
               <td></td>
               <td colspan="4" >
                  <?php
                     if ($MasterFile) {
                        if ($rsPDSQ["Q3a"]) {
                           chkBox("YES",1);
                           echo "&nbsp;";
                           chkBox("NO",0);
                        } else {
                           chkBox("YES",0);
                           echo "&nbsp;";
                           chkBox("NO",1);
                        }
                     } else {
                        if (putValue("F24") == "YES") {
                           chkBox("YES",1);
                           echo "&nbsp;";
                           chkBox("NO",0);
                        } else {
                           chkBox("YES",0);
                           echo "&nbsp;";
                           chkBox("NO",1);
                        }
                     }
                  ?>
               </td>
               <td></td>
            </tr>
            <tr>
               <td></td>
               <td colspan="5" class="pdsq_exp">
                  If YES, give details:
                  <br>
                  <span class="answer"><?php if ($MasterFile) echo $rsPDSQ["Q3aexp"]; else echo putValue("G25"); ?></span>
                  
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel" align="center" rowspan="2">37.</td>
               <td class="bgGrayLabel" colspan="4" rowspan="2" >
                  Have you ever been separated from the service in any of the following modes: resignation, retirement,
                  dropped from
                  <br>
                  the rolls, dismissal, termination, end of term, finished contract or phased out (abolition) in the
                  public or private sector?
               </td>
               <td></td>
               <td colspan="4" >
                  <?php
                        if ($MasterFile) {
                           if ($rsPDSQ["Q4a"]) {
                              chkBox("YES",1);
                              echo "&nbsp;";
                              chkBox("NO",0);
                           } else {
                              chkBox("YES",0);
                              echo "&nbsp;";
                              chkBox("NO",1);
                           }
                        } else {
                           if (putValue("F28") == "YES") {
                              chkBox("YES",1);
                              echo "&nbsp;";
                              chkBox("NO",0);
                           } else {
                              chkBox("YES",0);
                              echo "&nbsp;";
                              chkBox("NO",1);
                           }
                        }
                  ?>
               </td>
               <td></td>
            </tr>
            <tr>
               <td></td>
               <td colspan="5" class="pdsq_exp">
                  If YES, give details:
                  <br>
                  <span class="answer"><?php if ($MasterFile) echo $rsPDSQ["Q4aexp"]; else echo putValue("G29"); ?></span>
                  
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel" align="center" rowspan="2">38.</td>
               <td class="bgGrayLabel" colspan="4" rowspan="2" >
                  a. Have you ever been candidate in a national or local election held within the last year (except
                  Barangay Election)?
               </td>
               <td></td>
               <td colspan="4" >
                  <?php
                        if ($MasterFile) {
                           if ($rsPDSQ["Q5a"]) {
                              chkBox("YES",1);
                              echo "&nbsp;";
                              chkBox("NO",0);
                           } else {
                              chkBox("YES",0);
                              echo "&nbsp;";
                              chkBox("NO",1);
                           }
                        } else {
                           if (putValue("F33") == "YES") {
                              chkBox("YES",1);
                              echo "&nbsp;";
                              chkBox("NO",0);
                           } else {
                              chkBox("YES",0);
                              echo "&nbsp;";
                              chkBox("NO",1);
                           }
                        }

                  ?>
               </td>
               <td></td>
            </tr>
            <tr>
               <td></td>
               <td colspan="5" class="pdsq_exp">
                  If YES, give details:
                  <br>
                  <span class="answer"><?php if ($MasterFile) echo $rsPDSQ["Q5aexp"]; else echo putValue("G34"); ?></span>
                  
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel" align="center" rowspan="2"></td>
               <td class="bgGrayLabel" colspan="4" rowspan="2" >
                  b. Have you resigned from the government service during the three (3)-month period before the last
                  <br>
                  election to promote/actively campaign for a national or local candidate?
               </td>
               <td></td>
               <td colspan="4" >
                  <?php
                        if ($MasterFile) {
                           if ($rsPDSQ["Q5b"]) {
                              chkBox("YES",1);
                              echo "&nbsp;";
                              chkBox("NO",0);
                           } else {
                              chkBox("YES",0);
                              echo "&nbsp;";
                              chkBox("NO",1);
                           }
                        } else {
                           if (putValue("F36") == "YES") {
                              chkBox("YES",1);
                              echo "&nbsp;";
                              chkBox("NO",0);
                           } else {
                              chkBox("YES",0);
                              echo "&nbsp;";
                              chkBox("NO",1);
                           }
                        }
                  ?>
               </td>
               <td></td>
            </tr>
            <tr>
               <td></td>
               <td colspan="5" class="pdsq_exp">
                  If YES, give details:
                  <br>
                  <span class="answer"><?php if ($MasterFile) echo $rsPDSQ["Q5bexp"]; else echo putValue("G37"); ?></span>
                  
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel" align="center" rowspan="2">39.</td>
               <td class="bgGrayLabel" colspan="4" rowspan="2" >
                  Have you acquired the status of an immigrant or permanent resident of another country?
               </td>
               <td></td>
               <td colspan="4" >
                  <?php
                        if ($MasterFile) {
                           if ($rsPDSQ["Q6a"]) {
                              chkBox("YES",1);
                              echo "&nbsp;";
                              chkBox("NO",0);
                           } else {
                              chkBox("YES",0);
                              echo "&nbsp;";
                              chkBox("NO",1);
                           }
                        } else {
                           if (putValue("F39") == "YES") {
                              chkBox("YES",1);
                              echo "&nbsp;";
                              chkBox("NO",0);
                           } else {
                              chkBox("YES",0);
                              echo "&nbsp;";
                              chkBox("NO",1);
                           }
                        }
                  ?>
               </td>
               <td></td>
            </tr>
            <tr>
               <td></td>
               <td colspan="5" class="pdsq_exp">
                  If YES, give details:
                  <br>
                  <span class="answer"><?php if ($MasterFile) echo $rsPDSQ["Q6aexp"]; else echo putValue("G40"); ?></span>
                  
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel" align="center" rowspan="8">40.</td>
               <td class="bgGrayLabel" colspan="4" rowspan="2">
                  Pursuant to: (a) Indigenous People's Act (RA 8371); (b) Magna Carta for Disabled Persons (RA 7277);
                  and (c) Solo
                  <br>
                  Parents Welfare Act of 2000 (RA 8972), please answer the following items:
               </td>
               <td colspan="6" rowspan="2"></td>
            </tr>
            <tr></tr>
            <tr>
               <td colspan="4" class="bgGrayLabel" rowspan="2">
                  a. Are you a member of any indigenous group?
               </td>
               <td></td>
               <td colspan="4" >
                  <?php
                        if ($MasterFile) {
                           if ($rsPDSQ["Q7a"]) {
                              chkBox("YES",1);
                              echo "&nbsp;";
                              chkBox("NO",0);
                           } else {
                              chkBox("YES",0);
                              echo "&nbsp;";
                              chkBox("NO",1);
                           }
                        } else {
                           if (putValue("F43") == "YES") {
                              chkBox("YES",1);
                              echo "&nbsp;";
                              chkBox("NO",0);
                           } else {
                              chkBox("YES",0);
                              echo "&nbsp;";
                              chkBox("NO",1);
                           }
                        }
                  ?>
               </td>
               <td></td>
            </tr>
            <tr>
               <td></td>
               <td colspan="5" class="pdsq_exp">
                  If YES, give details:
                  <br>
                  <span class="answer"><?php if ($MasterFile) echo $rsPDSQ["Q7aexp"]; else  echo putValue("G44"); ?></span>
                  
               </td>
            </tr>
            <tr>
               <td colspan="4" class="bgGrayLabel" rowspan="2">
                  b. Are you a person with disability?
               </td>
               <td></td>
               <td colspan="4" >
                  <?php
                     if ($MasterFile) {
                        if ($rsPDSQ["Q7b"]) {
                           chkBox("YES",1);
                           echo "&nbsp;";
                           chkBox("NO",0);
                        } else {
                           chkBox("YES",0);
                           echo "&nbsp;";
                           chkBox("NO",1);
                        }
                     } else {
                        if (putValue("F46") == "YES") {
                           chkBox("YES",1);
                              echo "&nbsp;";
                              chkBox("NO",0);
                           } else {
                              chkBox("YES",0);
                              echo "&nbsp;";
                              chkBox("NO",1);
                        }
                     }
                  ?>
               </td>
               <td></td>
            </tr>
            <tr>
               <td></td>
               <td colspan="5">
                  If YES, please specify ID no:
                  <br>
                  <span class="answer"><?php if ($MasterFile) echo $rsPDSQ["Q7bexp"]; else echo putValue("G47"); ?></span>
                  
               </td>
            </tr>
            <tr>
               <td colspan="4" class="bgGrayLabel" rowspan="2">
                  c. Are you a solo parent?
               </td>
               <td></td>
               <td colspan="4" >
                  <?php
                     if ($MasterFile) {
                        if ($rsPDSQ["Q7c"]) {
                           chkBox("YES",1);
                              echo "&nbsp;";
                              chkBox("NO",0);
                           } else {
                              chkBox("YES",0);
                              echo "&nbsp;";
                              chkBox("NO",1);
                        }
                     } else {
                        if (putValue("F49") == "YES") {
                           chkBox("YES",1);
                              echo "&nbsp;";
                              chkBox("NO",0);
                           } else {
                              chkBox("YES",0);
                              echo "&nbsp;";
                              chkBox("NO",1);
                        }
                     }
                  ?>
               </td>
               <td></td>
            </tr>
            <tr>
               <td></td>
               <td colspan="5">
                  If YES, please specifiy ID no:
                  <br>
                  <span class="answer"><?php if ($MasterFile) echo $rsPDSQ["Q7cexp"]; else putValue("G50"); ?></span>

               </td>
            </tr>
         <!-- </table> -->
<!-- ####################################################################################################################################### -->
         <!-- <table>
            <tr align="center" class="ruler">
               <td width="20px"></td>
               <td width="139px"></td>
               <td width="180px"></td>
               <td width="14px"></td>
               <td width="215px"></td>
               <td width="17px"></td>
               <td width="55px"></td>
               <td width="11px"></td>
               <td width="15px"></td>
               <td width="148px"></td>
               <td width="23px"></td>
            </tr> -->
            <tr>
               <td class="bgGrayLabel" align="center">41.</td>
               <td class="bgGrayLabel" colspan="7">
                  REFERENCES <i style="color:#FF0000">(Person not related by consanguinity or affinity to applicant / appointee)</i>
               </td>
               <td colspan="3" rowspan="4" align="center" valign="middle" style="padding:10px;">
                  <?php
                     // $EmployeesPic = $rsPersonInfo["PicFilename"];
                     // if ($EmployeesPic == "") $EmployeesPic = "nopic.png";
                     // $fileExist = file_exists(img($rsPersonInfo["CompanyRefId"]."/EmployeesPhoto/".$EmployeesPic));
                     // if ($fileExist) {
                     //    $userPic = img($rsPersonInfo["CompanyRefId"]."/EmployeesPhoto/".$EmployeesPic);
                     // } else {
                     //    $userPic = img("nopic.png");
                     // }
                     // $userPic = img("nopic.png");
                     // echo '<img src="'.$userPic.'" style="height:4.5cm;width:3.5cm;border:2px solid #000">';
                  ?>
                  <div style="height:3.5cm;width:2.5cm;border:2px solid #000; padding-left: 10px; padding-right: 10px; font-size: 6pt;">
                     <br><br>
                     ID picture taken within the last 6 months 3.5 cm. X 4.5 cm (passport size)
                     <br><br>
                     With full and handwritten name tag and signature over printed name
                     <br><br>
                     Computer generated or photocopied picture is not acceptable
                  </div>
                  <div>PHOTO</div>
                  <div style="border:1px solid #000;width:3.5cm;margin-top: 10px;">
                     <div style="height:2.5cm;">&nbsp;</div>
                     <div class="bgBlueLabel" align="center" style="border-top:1px solid #000;">Right Thumbmark</div>
                  </div>

               </td>
            </tr>
            <!--
            <tr>
               <td colspan=8></td>
            </tr>
            <tr>
               <td colspan=8></td>
            </tr>
            <tr>
               <td colspan=8></td>
            </tr>
            -->
            <tr>
               <td colspan=8 valign="top">

                  <table border=1 style="height:100%;width:100%;">
                     <tr>
                        <th class="bgGrayLabel" align="center" style="width:2in;">NAME</th>
                        <th class="bgGrayLabel" align="center" style="width:*in;">ADDRESS</th>
                        <th class="bgGrayLabel" align="center" style="width:1.5in;">TEL NO.</th>
                     </tr>
                     <?php
                        for ($h=1;$h<=3;$h++) {
                           $l = 53 + $h;
                     ?>
                           <tr class="trHeight">
                              <td style="text-transform: uppercase;">
                                 <span class="answer REFERENCES ucase--" id="page4_A<?php echo $l ?>"><?php if (!$MasterFile) echo putValue("A".$l); ?></span>
                              </td>
                              <td style="text-transform: uppercase;">
                                 <span class="answer REFERENCES" id="page4_E<?php echo $l ?>"><?php if (!$MasterFile) echo putValue("E".$l); ?></span>
                              </td>
                              <td>
                                 <span class="answer REFERENCES" id="page4_F<?php echo $l ?>"><?php if (!$MasterFile) echo putValue("F".$l); ?></span>
                              </td>
                           </tr>
                     <?php
                        }
                     ?>
                  </table>
               </td>
            </tr>


            <tr>
               <td class="bgGrayLabel" align="center">42.</td>
               <td class="bgGrayLabel" colspan="7">
                  I declare under the oath that I have personally accomplished this Personal Data Sheet which is a true,
                  correct and complete statement pursuant to the provisions of pertinent laws, rules and regualations of
                  the Republic of the Philippines. I authorize the agency head/ authorized representative to verify/
                  validate the contents stated herein. I agree that any misrepresentation made in this document and its
                  attachment shall cause the filing of administrative/criminal case/s against me.
               </td>
            </tr>

            <tr>
               <td colspan="4" valign="top" style="padding:10px;">
                  <table border=1 style="border: 2px solid #000;">
                     <tr class="trHeight">
                        <td colspan="2" class="bgBlueLabel">
                           Government Issued ID (i.e. Passport, GSIS, SSS, PRC, Driver's License, etc.)
                           <strong><i>PLEASE INDICATE ID Number and Date of Issuance</i></strong>
                        </td>
                     </tr>
                     <tr class="trHeight">
                        <td width="50%">Government Issued ID:</td>
                        <td width="50%">
                           <span class="answer">
                              <?php
                                 if ($MasterFile) echo setValue($rsPersonInfo,"GovtIssuedID");
                              ?>
                           </span>
                        </td>
                     </tr>
                     <tr class="trHeight">
                        <td>ID/License/Passport No. :</td>
                        <td>
                           <span class="answer">
                              <?php
                                 if ($MasterFile) echo setValue($rsPersonInfo,"GovtIssuedIDNo");
                              ?>
                           </span>
                        </td>
                     </tr>
                     <tr class="trHeight">
                        <td>Date/Place of Issuance:</td>
                        <td>
                           <span class="answer">
                              <?php
                                 if ($MasterFile) {
                                    echo dispVal($rsPersonInfo["GovtIssuedIDDate"]);
                                    echo " / ";
                                    echo setValue($rsPersonInfo,"GovtIssuedIDPlace");
                                 }
                                    
                              ?>
                           </span>
                        </td>
                     </tr>
                  </table>
               </td>
               <td colspan="4" valign="top" style="padding:10px;">
                  <table height="100%" border=1 style="border: 2px solid #000;">
                     <tr style="height: 42px;"><td>&nbsp;</td></tr>
                     <tr style="height: 14px;"><td class="bgBlueLabel" align="center"><i>Signature (Sign inside the box)</i></td></tr>
                     <tr style="height: 42px;"><td>&nbsp;</td></tr>
                     <tr style="height: 14px;"><td class="bgBlueLabel" align="center">Date Accomplished</td></tr>
                  </table>
               </td>
            </tr>

            <!-- ============================================================================================= -->
            <tr style="border-bottom: none !important;">
               <td colspan="11" align="center">
                  SUBSCRIBED AND SWORN to before me this ___________________ , affiant exhibiting his/her validly issued
                  government ID as indicated above.
               </td>
            </tr>
            <tr>
               <td colspan="11">
                  <table height="100%">
                     <tr style="height:<?php echo $pds["PAOBoxSign"]; ?>;">
                        <td width="30%">&nbsp;</td>
                        <td width="*%" style="border:2px solid #000;">&nbsp;</td>
                        <td width="30%">&nbsp;</td>
                     </tr>
                     <tr style="height:20px;">
                        <td>&nbsp;</td>
                        <td align="center" class="bgBlueLabel" style="border:2px solid #000;">Person Administering Oath</td>
                        <td>&nbsp;</td>
                     </tr>
                  </table>
               </td>
            </tr>
         </tbody>
      </table>
      <div class="table_footer" id="LastPage" style="text-align:right;">
         <i><strong>CS FORM 212 (Revised 2017),
            <span class="page-number"></span> 
            <span class="page-total"></span>
         </strong></i>
      </div>   