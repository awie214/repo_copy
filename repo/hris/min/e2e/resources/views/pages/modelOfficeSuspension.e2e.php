<?php
   require_once "constant.e2e.php";
   require_once "inc_model.e2e.php";
   require_once "incUtilitiesJS.e2e.php";
?>
   <div class="container" id="EntryScrn">
      <div class="row" id="EntryScrn">
         <div class="col-xs-6">
            <?php if ($ScrnMode != 1) { ?>
               <div class="row">
                  <ul class="nav nav-pills">
                     <li class="active" style="font-size:12pt;font-weight:600;">
                        <a>REFID : <span class="badge" style="font-size:12pt;font-weight:600;" id="idRefid">
                        <?php echo $refid; ?>
                        </span></a>
                     </li>
                  </ul>
               </div>
            <?php } ?>
            <div class="row margin-top">
               <div class="col-xs-12">
                  <div class="form-group">
                     <label class="control-label" for="inputs">Suspension Name:</label><br>
                     <input class="form-input saveFields-- uCase-- mandatory" type="text" name="char_Name">
                  </div>
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-6">
                  <div class="form-group">
                     <label class="control-label" for="inputs">Start Date:</label><br>
                     <input class="form-input saveFields-- mandatory date--" type="text" name="date_StartDate">
                     <?php timePicker("sint_StartTime","Start Time"); ?>
                  </div>
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-6">
                  <div class="form-group">
                     <label class="control-label" for="inputs">End Date:</label><br>
                     <input class="form-input saveFields-- mandatory date--" type="text" name="date_EndDate">
                  </div>
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-6">
                  <label>Is Whole Day?</label>
                  <select class="form-input saveFields--" name="sint_Whole" id="sint_Whole">
                     <option value="0">NO</option>
                     <option value="1">YES</option>
                  </select>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12">
                  <div class="form-group">
                     <label class="control-label" for="inputs">REMARKS:</label>
                     <textarea class="form-input saveFields--" rows="5" name="char_Remarks" <?php echo $disabled; ?>
                     placeholder="remarks"><?php echo $remarks; ?></textarea>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <script type="text/javascript">
      $(document).ready(function () {
         $("#sint_Whole").change(function () {
            var value = $(this).val();
            if (value == 1) {
               $("[name='sint_StartTime']").val("");
            }
         });
      });
   </script>