<style type="text/css">
	@media print {
		.btnPrint { display: none !important; }
	}
	.btnPrint {
		padding: 10px;
		background: #0047b3;
		border: 1px solid black;
		border-radius: 5px;
		color: white;
		font-weight: 600;
		box-shadow: 1px 1px 1px gray;
	}
	.btnPrint:hover {
		box-shadow: 3px 3px 3px gray;
		cursor: pointer;
		transition: 0.3s;
	}
</style>
<button type="button" onclick="self.print()" class="btnPrint">PRINT DTR</button>
<?php
	include_once 'conn.e2e.php';
	function HoursFormat($timeInMin) {
        if ($timeInMin > 0) {
           	$hr = explode(".",$timeInMin / 60)[0];
           	if ($hr <= 9) {
              	$hr = "0".$hr;
           	}
           	$mod_min = ($timeInMin % 60);
           	if ($mod_min <= 9) {
              	$mod_min = "0".$mod_min;
           	}
           	return $hr.":".$mod_min;   
           	//return convertToHoursMins($timeInMin);
        } else {
        	if ($GLOBALS["cid"] == 21) {
        		return ":";
        	} else {
        		return "&nbsp;";
        	}
           
        }
    }
    function AMPMFormat($time){
    	if ($time > 0) {
    		$military_time 	= convertToHoursMins($time);
	    	$time_arr 		= explode(":", $military_time);
	    	$hr 			= $time_arr[0];
	    	$minute 		= $time_arr[1];
	    	$stamp 			= "";
	    	if (intval($hr) > 11) {
	    		if ($hr == "12") {
	    			return $hr.":".$minute." pm";	
	    		} else {
	    			$hr 	= intval($hr);
	    			$new_hr = $hr - 12;
	    			if ($new_hr <= 9) $new_hr = "0".$new_hr;
	    			return $new_hr.":".$minute." pm";	
	    		}
	    	} else {
	    		return $hr.":".$minute." am";
	    	}	
    	} else {
    		return "&nbsp;";
    	}
    }
    
    /*==============================================================================================================*/
	//SETTING VARIABLE TO NULL
	/*==============================================================================================================*/
	$map["[[]]"] = "";
    $map["[[SmallLogo]]"] 		= "";
    $map["[[CompanyAddress]]"] 	= "";
    $map["[[EmployeesName]]"] 	= "";
    $map["[[SuperVisorName]]"]	= "";
    $map["[[StartMonth]]"]		= "";
   	$map["[[EndMonth]]"]		= "";
    $map["[[Division]]"] 		= "";
    $map["[[Position]]"] 		= "";
    $map["[[Period]]"] 			= "";
    $map["[[Office]]"] 			= "";
    $map["[[WorkSchedule]]"] 	= "";
	$map["[[totrg]]"] 			= "";
	$map["[[lcvl]]"] 			= "";
	$map["[[lcsl]]"] 			= "";
	$map["[[lcfl]]"] 			= "";
	$map["[[lcspl]]"] 			= "";
	$map["[[pvl]]"] 			= "";
	$map["[[psl]]"] 			= "";
	$map["[[pfl]]"] 			= "";
	$map["[[p]]"] 				= "";
	$map["[[tott]]"] 			= "";
	$map["[[otp]]"] 			= "";
	$map["[[otps]]"] 			= "";
	$map["[[totul]]"] 			= "";
	$map["[[cocbal]]"] 			= "";
	$map["[[cocbals]]"]			= "";
	$map["[[tottt]]"] 			= "";
	$map["[[tottu]]"] 			= "";
	$map["[[tota]]"] 			= "";
	$map["[[totcc]]"] 			= "";
	$map["[[toteh]]"] 			= "";
	$map["[[AgencyId]]"]	 	= "";
	$map["[[WorkingDays]]"]	 	= "";
	$map["[[deducteq]]"]	 	= "";
	$map["[[ODU]]"]	 			= "";
	
	

	$map["[[utthr]]"]	 		= "";
	$map["[[uttmin]]"]	 		= "";
	$map["[[tdymin]]"]	 		= "";
	$map["[[utmin]]"]	 		= "";
	$map["[[eqdayut]]"]	 		= "";
	$map["[[eqdaytardy]]"]		= "";
	$map["[[totpresent]]"]		= "";
	$map["[[totleave]]"] 		= "";
	$map["[[totob]]"] 			= "";
	$map["[[totcto]]"] 			= "";

	/*==============================================================================================================*/
    for($a=1;$a<=31;$a++) {
    	if ($a < 10) $a = "0".$a;
    	
    	$map["[[new_date".$a."]]"] 	= "";
    	$map["[[WDay".$a."]]"] 	= "";
    	$map["[[numdate".$a."]]"] 	= "";
    	$map["[[Day".$a."]]"] 	= "";
    	$map["[[uthr".$a."]]"] 	= "";
    	$map["[[utmin".$a."]]"] = "&nbsp;";
    	$map["[[din".$a."]]"] 	= "";
		$map["[[lout".$a."]]"] 	= "";
		$map["[[lin".$a."]]"] 	= "";
		$map["[[dout".$a."]]"] 	= "";
		$map["[[tdy".$a."]]"] 	= "";
		$map["[[ut".$a."]]"] 	= "";
		$map["[[ot".$a."]]"] 	= "";
		$map["[[reg".$a."]]"] 	= "";
		$map["[[exh".$a."]]"] 	= "";
		$map["[[rem".$a."]]"] 	= "&nbsp;";
		$map["[[ot".$a."]]"]	= "";
		$map["[[ob".$a."]]"]	= "";
		$map["[[otin".$a."]]"]  = "";
		$map["[[otout".$a."]]"] = "";
		$map["[[date".$a."]]"] 	= "";
		$map["[[Num".$a."]]"] 	= "";
    }
    /*==============================================================================================================*/
    for ($b=1; $b<=6; $b++) { 
    	$map["[[rgwk".$b."]]"] 	= "";
		$map["[[twk".$b."]]"] 	= "";
		$map["[[ulwk".$b."]]"] 	= "";
		$map["[[ttwk".$b."]]"] 	= "";
		$map["[[tuwk".$b."]]"] 	= "";
		$map["[[awk".$b."]]"] 	= "";
		$map["[[cowk".$b."]]"] 	= "";
		$map["[[ehwk".$b."]]"] 	= "";
		$map["[[ot".$b."]]"]	= "";
    }
    /*==============================================================================================================*/
    for ($c=1; $c <= 31; $c++) { 
    	$map["[[d".$c."]]"]     = "";
    }
    /*==============================================================================================================*/
    $css_str					= "";
    $CompanyLogo 				= "";
    $CompanyAddress 			= "";
    $EmployeesName 				= "";
    $Position 					= "";
    $Division 					= "";
    $Period 					= "";
    $SuperVisorName 			= "";
    $TimeIn 					= "";
    $LunchOut 					= "";
    $LunchIn 					= "";
    $TimeOut 					= "";
    $RegHour 					= "";
    $ExcessHour 				= "";
    $TardyEQ					= "";
	$UndertimeEQ				= "";
	$AbsentEQ					= "";
	$First_Monday 				= "";
    $WorkSchedule 				= 0;
    $day_count_diff				= 0;
	$PresentDays				= 0;
	$VLCount 					= 0;
	$SLCount 					= 0;
	$TotalRegHour 				= 0;
	$TotalTardyHour 			= 0;
	$TotalUndertimeHour			= 0;
	$TotalExcessHour			= 0;
	$TotalAbsentCount			= 0;
	$TotalCOCHour 				= 0;
	$TotalTardyCount 			= 0;
	$TotalOTHour 				= 0;
	$TotalUndertimeCount		= 0;
	$TotalDeduct				= 0;
	$SPL_count 					= 0;
	$Used_COC 					= 0;
	$OT_Special 				= 0;
	$COC_Special 				= 0;
	$Leave_count 				= 0;
	$OB_count 					= 0;
	$CTO_count 					= 0;
    $arr 						= array();
    $RegHour_PerWeek 			= Array(0,0,0,0,0,0,0);
    $TardyHour_PerWeek 			= Array(0,0,0,0,0,0,0);
    $UndertimeHour_PerWeek 		= Array(0,0,0,0,0,0,0);
    $COCHours_PerWeek 			= Array(0,0,0,0,0,0,0);
    $ExcessHour_PerWeek 		= Array(0,0,0,0,0,0,0);
    $OTHours_PerWeek 			= Array(0,0,0,0,0,0,0);
    $UndertimeCount_PerWeek 	= Array(0,0,0,0,0,0,0);
    $AbsentCount_PerWeek 		= Array(0,0,0,0,0,0,0);
    $OTCount_PerWeek 			= Array(0,0,0,0,0,0,0);
    $TardyCount_PerWeek 		= Array(0,0,0,0,0,0,0);
    $CutOff_PerWeek 			= Array(0,0,0,0,0,0,0);
    $Friday_Holiday_PerWeek 	= Array(0,0,0,0,0,0,0);
    $WorkDayConversion 			= file_get_contents(json."WorkDayConversion.json");
	$WorkDayEQ   				= json_decode($WorkDayConversion, true);
    /*==============================================================================================================*/
    /*==============================================================================================================*/
	//SETTING OF DATE RANGE
	/*==============================================================================================================*/
	$new_week 					= "";
	$week_count 				= 0;
	$userID 					= 0;
	$NewMonth 					= intval(getvalue("hNewMonth"));
	$NewYear 					= intval(getvalue("hNewYear"));
	if ($NewMonth <= 9) {
		$NewMonth = "0".$NewMonth;
	}
	$curr_date					= date("Y-m-d",time());
	$num_of_days 				= cal_days_in_month(CAL_GREGORIAN,$NewMonth,$NewYear);
	$exact_end_days 			= $num_of_days;
	$month_start 				= $NewYear."-".$NewMonth."-01";
	$month_end 					= $NewYear."-".$NewMonth."-".$num_of_days;
	if (strtotime($curr_date) <= strtotime($month_end)) {
		$month_end = $curr_date;
	} else {
		$month_end = $month_end;
	}
	$map["[[StartMonth]]"]		= date("F d, Y",strtotime($month_start));
	$map["[[EndMonth]]"]		= date("F d, Y",strtotime($month_end));
	$num_of_days 				= date("d",strtotime($month_end));
	$Period 					= $NewMonth."/01/".$NewYear." - ".$NewMonth."/".$num_of_days."/".$NewYear;
	for($d=1;$d<=$num_of_days;$d++) {
		$y = $NewYear."-".$NewMonth."-".$d;
		$day = date("D",strtotime($y));	
		if ($day == "Sat" || $day == "Sun") {
			$day = "<span style='color:red;'>".$day."</span>";
			$css_str .= "."."row_".$d.", ";
		}
		$map["[[d$d]]"] = $day;

		if ($d <= 9) $d = "0".$d;
		$y = $NewYear."-".$NewMonth."-".$d;
		$week = date("W",strtotime($y));
		if ($week != $new_week) {
			$week_count++;
		}
		$dtr = [
	   		"AttendanceDate" => "$y",
	   		"AttendanceTime" => "",
	   		"UTC" => "",
	   		"TimeIn" => "",
	   		"LunchOut" => "",
	   		"LunchIn" => "",
	   		"TimeOut" => "",
	   		"OBOut" => "",
	   		"OBIn" => "",
	   		"Day" => "$d",
	   		"Week" => "$week_count",
	   		"KEntry" => "",
	   		"Holiday" => "",
	   		"CTO"=>"",
	   		"Leave"=> "",
	   		"FL"=>"",
	   		"OffSus"=>"",
	   		"OffSusName"=>"",
	   		"HasOT"=>"",
	   		"OBCode"=>"",
	   		"OBName"=>"",
	   		"OBTimeIn"=>"",
	   		"OBTimeOut"=>"",
	   		"OTTime"=>"",
	   		"ChangesRem"=>""
	   	];
	   	$arr["ARR"][$y] = $dtr;
		$new_week = $week;
	}
	/*==============================================================================================================*/
	
    if (getvalue("hucode") != "COMPEMP") {
    	$emprefid = getvalue("emprefid");	
    } else {
    	$emprefid = getvalue("hEmpRefId");
    }
    /*==============================================================================================================*/
    /*==============================================================================================================*/
	//START OF EMPLOYEE QUERY
	/*==============================================================================================================*/
	if ($emprefid == "") {
		$where = "";
	} else {
		$where = "WHERE RefId = $emprefid";
	}
    //$row = FindFirst("employees",,"*");
    $rs = SelectEach("employees",$where);
    if ($rs) {
    	while ($row = mysqli_fetch_assoc($rs)) {
    		$emprefid 					= $row["RefId"];
	    	$EmployeesName				= $row["LastName"].", ".$row["FirstName"]." ".$row["ExtName"]." ".$row["MiddleName"];	
			$AgencyId					= $row["AgencyId"];
			$biometricsID 				= $row["BiometricsID"];
			$CompanyID 					= $row["CompanyRefId"];
			$BranchID					= $row["BranchRefId"];
			$KEntryContent 				= file_get_contents(json."Settings_".$CompanyID.".json");
	      	$KEntry_json   				= json_decode($KEntryContent, true);
	      	$OTMinTime 					= $KEntry_json["OTMinTime"];
	      	$COCMinTime 				= $KEntry_json["COCMinTime"];
	      	$IsAuto 					= FindFirst("ams_employees","WHERE EmployeesRefId = '$emprefid'","IsAutoDTR");
	      	$GLOBALS["cid"] 			= $CompanyID;
	      	if ($CompanyID == 35) {
	      		$EmployeesName				= $row["FirstName"]." ".substr($row["MiddleName"], 0,1)." ".$row["LastName"]." ".$row["ExtName"];	
	      	}
			/*==============================================================================================================*/      	
	      	$Default_qry				= "WHERE CompanyRefId = ".$CompanyID." AND BranchRefId = ".$BranchID;
	      	$Default_qry 				= "WHERE RefId > 0";
	      	/*==============================================================================================================*/
			$Company_row 				= FindFirst("company","WHERE RefId = $CompanyID","*");
			if ($Company_row) {
				if ($CompanyID != "2") {
					$CompanyLogo 			= "<img src='../../../public/images/".$Company_row["RefId"]."/".$Company_row["SmallLogo"]."'
												style='height:60px;'>";
				} else {
					$CompanyLogo 			= "<img src='../../../public/images/".$Company_row["RefId"]."/PCCLOGO_report.png' style='height:80px;'>";
				}
				
				$CompanyAddress 		= $Company_row["Address"];
			}
			/*==============================================================================================================*/
			$where_empinfo 				= $Default_qry." AND EmployeesRefId = $emprefid";
			$EmpInfo_row				= FindFirst("empinformation",$where_empinfo,"*");
			if ($EmpInfo_row) {
				$Division				= getRecord("division",$EmpInfo_row["DivisionRefId"],"Name");
				$Department				= getRecord("Department",$EmpInfo_row["DepartmentRefId"],"Name");
				$Position 				= getRecord("position",$EmpInfo_row["PositionRefId"],"Name");
				$Office 				= getRecord("office",$EmpInfo_row["OfficeRefId"],"Name");
				$WorkSchedule 			= getRecord("workschedule",$EmpInfo_row["WorkScheduleRefId"],"Name");
				$WorkScheduleRefId      = $EmpInfo_row["WorkScheduleRefId"];
			}
			/*==============================================================================================================*/
			if ($WorkScheduleRefId > 0) {
				/*==========================================================================================================*/
				//GETTING OF ATTENDANCE IN EMPLOYEE ATTENDANCE
				/*==========================================================================================================*/
				$where_empAtt 			= $Default_qry;
				$where_empAtt 			.= " AND EmployeesRefId = '".$emprefid."'";
				$where_empAtt       	.= " AND AttendanceDate BETWEEN '".$month_start."' AND '".$month_end."'";
				$rs_empAtt 				= SelectEach("employeesattendance",$where_empAtt);
				if ($rs_empAtt) {
					while ($row = mysqli_fetch_assoc($rs_empAtt)) {
						$AttendanceDate 	= $row["AttendanceDate"];
						$AttendanceTime 	= $row["AttendanceTime"];
						$UTC 				= $row["CheckTime"];
						$KEntry         	= $row["KindOfEntry"];
						$fld 				= "";
						$val 				= "";
						switch ($KEntry) {
							case 1:
								$fld 		= "TimeIn";
								$val     	= get_today_minute($UTC);
								break;
							case 2:
								$fld 		= "LunchOut";
								$val     	= get_today_minute($UTC);
								break;
							case 3:
								$fld 		= "LunchIn";
								$val     	= get_today_minute($UTC);
								break;
							case 4:
								$fld 		= "TimeOut";
								$val     	= get_today_minute($UTC);
								break;
							/*case 7:
								$fld 		= "OBOut";
								$val     	= get_today_minute($UTC);
								break;
							case 8:
								$fld 		= "OBIn";
								$val     	= get_today_minute($UTC);
								break;*/
						}
						//echo $AttendanceDate."->".date("Y-m-d h:i A",$AttendanceTime)."<br>";
						$arr["ARR"][$AttendanceDate][$fld] = $val;
						$arr["ARR"][$AttendanceDate]["UTC"] = $UTC;
					}
				}
				/*==========================================================================================================*/
				/*==========================================================================================================*/
				//GETTING ATTENDANCE IN BIOMETRICS
				/*==========================================================================================================*/
				switch ($CompanyID) {
					case '2':
						include 'inc_biometrics_2.e2e.php';
						break;
					case '28':
						include 'inc_biometrics_28.e2e.php';
						break;
					case '14':
						include 'inc_biometrics_14.e2e.php';
						break;
					case '21':
						include 'inc_biometrics_21.e2e.php';
						break;
					default:
						include 'inc_bio_default.e2e.php';
						break;
				}
				/*==========================================================================================================*/
				/*==========================================================================================================*/
				//CHECKING HOLIDAY TABLE
				/*==========================================================================================================*/
				$ChangesRem = SelectEach("dtr_remarks","WHERE EmployeesRefId = '$emprefid'");
				if ($ChangesRem) {
					while ($ChangesRem_row = mysqli_fetch_assoc($ChangesRem)) {
						$ChangeDate = $ChangesRem_row["AttendanceDate"];
						$ChangeDetail = $ChangesRem_row["Remarks"];
						if(isset($arr["ARR"][$ChangeDate])) {
	                        $arr["ARR"][$ChangeDate]["ChangesRem"] = $ChangeDetail;
                    	}   
					}
				}

				/*==========================================================================================================*/
				/*==========================================================================================================*/
				//CHECKING HOLIDAY TABLE
				/*==========================================================================================================*/
				$holiday = SelectEach("holiday","");
			    if ($holiday) {
			        while ($row = mysqli_fetch_assoc($holiday)) {
			            $StartDate        = $row["StartDate"];
			            $EndDate          = $row["EndDate"];
			            $holiday_date_diff   = dateDifference($StartDate,$EndDate);
			            $Name             = $row["Name"];
			            $EveryYr          = $row["isApplyEveryYr"];
			            $Legal            = $row["isLegal"];
			            $temp_arr         = explode("-", $StartDate);
			            $temp_date        = $NewYear."-".$temp_arr[1]."-".$temp_arr[2];
			            for ($H=0; $H <= $holiday_date_diff; $H++) { 
		                  	$holiday_NewDate = date('Y-m-d', strtotime($temp_date. ' + '.$H.' days'));
	                  		if ($EveryYr == 1) {
		                     	if(isset($arr["ARR"][$holiday_NewDate])) {
			                        $arr["ARR"][$holiday_NewDate]["Holiday"] = $Name;
		                    	}   
		                  	} else {
		                     	if(isset($arr["ARR"][$StartDate])) {
		                        	$arr["ARR"][$StartDate]["Holiday"] = $Name;
		                     	}
		                  	}
		               	}
			        }
			    }
			    /*==========================================================================================================*/
				/*==========================================================================================================*/
				//CHECKING OFFICE SUSPENSION TABLE
				/*==========================================================================================================*/
				$officesuspension = SelectEach("officesuspension","");
		      	if ($officesuspension) {
		         	while ($row = mysqli_fetch_assoc($officesuspension)) {
			            $StartDate        = $row["StartDate"];
		            	$EndDate          = $row["EndDate"];
		            	$OffSus_date_diff = dateDifference($StartDate,$EndDate);
		            	$Name             = $row["Name"];
		            	$OffSusTime       = $row["StartTime"];
		            	if ($OffSusTime == "") $OffSusTime = 420;
		            	$temp_arr         = explode("-", $StartDate);
		            	for ($OS=0; $OS <= $OffSus_date_diff; $OS++) { 
	                  		$OffSus_NewDate = date('Y-m-d', strtotime($StartDate. ' + '.$OS.' days'));
	                  		if(isset($arr["ARR"][$OffSus_NewDate])) {
	                     		$arr["ARR"][$OffSus_NewDate]["OffSusName"] 	= $Name;
		                    	$arr["ARR"][$OffSus_NewDate]["OffSus"] 		= $OffSusTime;
	                  		}   
	               		}
		         	}
		      	}
		      	/*==========================================================================================================*/
				/*==========================================================================================================*/
				//GETTING EMPLOYEE LEAVE
				/*==========================================================================================================*/
				$where_leave 		= $Default_qry;
				$where_leave		.= " AND EmployeesRefId = ".$emprefid;
				// $where_leave		.= " AND ApplicationDateFrom BETWEEN '".$month_start."'";
				// $where_leave  		.= " AND '".$month_end."'";
				$where_leave 		.= " AND Status = 'Approved'";
				$rs_leave			= SelectEach("employeesleave",$where_leave);
				if ($rs_leave) {
					while ($row = mysqli_fetch_assoc($rs_leave)) {
						$diff = dateDifference($row["ApplicationDateFrom"],$row["ApplicationDateTo"]);
						for ($i=0; $i <= $diff ; $i++) { 
							$temp_date = date('Y-m-d', strtotime($row["ApplicationDateFrom"] . ' +'.$i.' day'));
							if(isset($arr["ARR"][$temp_date])) {
		                     	$arr["ARR"][$temp_date]["Leave"] = $row["LeavesRefId"];
		                     	if ($row["isForceLeave"] == 1) {
		                     		$arr["ARR"][$temp_date]["FL"] = 1;
		                     	}
		                  	}
						}
					}
				}
				/*==========================================================================================================*/
				/*==========================================================================================================*/
				//GETTING OFFICE AUTHORITY NAME
				/*==========================================================================================================*/
				$where_OB 			= $Default_qry;
				$where_OB			.= " AND EmployeesRefId = ".$emprefid;
				//$where_OB			.= " AND ApplicationDateFrom BETWEEN '".$month_start."'";
				$where_OB  			.= " AND '".$month_end."'";
				$where_OB 			.= " AND Status = 'Approved'";
				$rs_OB				= SelectEach("employeesauthority",$where_OB);
				if ($rs_OB) {
					while ($row = mysqli_fetch_assoc($rs_OB)) {
						$diff 		= dateDifference($row["ApplicationDateFrom"],$row["ApplicationDateTo"]);
						$OBName 	= getRecord("absences",$row["AbsencesRefId"],"Name");
						$OBCode		= getRecord("absences",$row["AbsencesRefId"],"Code");
						$whole 		= $row["Whole"];
						$time_in 	= $row["FromTime"];
						$time_out 	= $row["ToTime"];
						$OBCode		= $OBCode."-".date("Y",strtotime($row["FiledDate"]))."".date("m",strtotime($row["FiledDate"]))."-".str_pad($row["RefId"], 3, '0',STR_PAD_LEFT);
						for ($i=0; $i <= $diff ; $i++) { 
							$temp_date = date('Y-m-d', strtotime($row["ApplicationDateFrom"] . ' +'.$i.' day'));
							if(isset($arr["ARR"][$temp_date])) {
		                     	$arr["ARR"][$temp_date]["OBName"] = $OBName;
		                     	$arr["ARR"][$temp_date]["OBCode"] = $OBCode;
		                     	if ($whole == 1) {
		                     		$arr["ARR"][$temp_date]["OBTimeIn"] = "Whole";
		                     		$arr["ARR"][$temp_date]["OBTimeOut"] = "Whole";	
		                     	} else {
		                     		$arr["ARR"][$temp_date]["OBTimeIn"] = $time_in;
		                     		$arr["ARR"][$temp_date]["OBTimeOut"] = $time_out;	
		                     	}
		                     	
		                  	}
						}
					}
				}
				/*==========================================================================================================*/
				/*==========================================================================================================*/
				//GETTING EMPLOYEE CTO AVAILMENT
				/*==========================================================================================================*/
				$where_cto 		= $Default_qry;
				$where_cto		.= " AND EmployeesRefId = ".$emprefid;
				//$where_cto		.= " AND ApplicationDateFrom BETWEEN '".$month_start."' AND '".$month_end."'";
				$where_cto 		.= " AND Status = 'Approved'";
				$rs_cto			= SelectEach("employeescto",$where_cto);
				if ($rs_cto) {
					while ($row = mysqli_fetch_assoc($rs_cto)) {
						$diff = dateDifference($row["ApplicationDateFrom"],$row["ApplicationDateTo"]);
						for ($i=0; $i <= $diff; $i++) { 
							$temp_date = date('Y-m-d', strtotime($row["ApplicationDateFrom"] . ' +'.$i.' day'));
							if(isset($arr["ARR"][$temp_date])) {
		                     	$arr["ARR"][$temp_date]["CTO"] = $row["Hours"]*60;
		                  	}
						}
					}
				}
				/*==========================================================================================================*/
				/*==========================================================================================================*/
				//GETTING EMPLOYEE OVERTIME REQUEST
				/*==========================================================================================================*/
				$where_OT    = $Default_qry;
				$where_OT   .= " AND EmployeesRefId = ".$emprefid;
				//$where_OT   .= " AND StartDate BETWEEN '".$month_start."' AND '".$month_end."'";
				$where_OT   .= " AND Status = 'Approved'";
				$rs_OT 		 = SelectEach("overtime_request",$where_OT);
				if ($rs_OT) {
					while ($row = mysqli_fetch_assoc($rs_OT)) {
						$StartDate 		= $row["StartDate"];
						$EndDate 		= $row["EndDate"];
						$WithPay        = $row["WithPay"];
						$OTFrom			= $row["FromTime"];
						$OTTo 			= $row["ToTime"];
						$OTClass 		= $row["OTClass"];
						$OTTime 		= "";
						if ($OTClass == 2) {
							if ($OTTo != "") {
								if ($OTTo > $OTFrom) {
									$OTTime = $OTTo - $OTFrom;
									for ($y=60; $y <= $OTTime ; $y+=60) {
										if ($y == 180) {
											$dummy = $OTTime - 180;
											if ($dummy > 60) {
												$OTTime -= 60;	
											} else {
												$OTTime -= $dummy;
											}
										}
									}
								} else {
									$OTTime = (1440 - $OTFrom) + $OTTo;
									for ($x=60; $x < $OTTime ; $x+=60) { 
										if ($x == 180) {
											$dummy = $OTTime - 180;
											if ($dummy > 60) {
												$OTTime -= 60;	
											} else {
												$OTTime -= $dummy;
											}
										}
									}
								}	
							}
						}

						$OT_DateDiff 	= dateDifference($StartDate,$EndDate);
						for ($O=0; $O <= $OT_DateDiff; $O++) { 
		               		$OT_NewDate = date('Y-m-d', strtotime($StartDate. ' + '.$O.' days'));
		               		if ($WithPay == 1) {
		               			$Pay = 1;
		               		} else {
		               			$Pay = 0;
		               		}
		               		if(isset($arr["ARR"][$OT_NewDate])) {
		                     	$arr["ARR"][$OT_NewDate]["HasOT"] = $Pay;
		                     	$arr["ARR"][$OT_NewDate]["OTTime"] = $OTTime;
		                  	}
		               	}
					}
				}


				/*==============================================================================================================*/
				/*==============================================================================================================*/
				/*==============================================================================================================*/
				/*==============================================================================================================*/
				/*==============================================================================================================*/
				/*==============================================================================================================*/
				/*==============================================================================================================*/
				/*==============================================================================================================*/
				//START OF COMPUTATION AND SHOOTING
				/*==============================================================================================================*/
				$Emp_WorkSched 		= FindFirst("workschedule"," WHERE RefId = ".$WorkScheduleRefId,"*");

				if (intval($IsAuto) == 1) {
					$Monday_in 		= $Emp_WorkSched["MondayIn"];
					$Monday_lout 	= $Emp_WorkSched["MondayLBOut"];
					$Monday_lin 	= $Emp_WorkSched["MondayLBIn"];
					$Monday_out 	= $Emp_WorkSched["MondayOut"];
					for ($j=1; $j <= $num_of_days ; $j++) { 
						if ($j < 10) $j = "0".$j;
						$AutoDate = $NewYear."-".$NewMonth."-".$j;
						$Auto_day = date("D",strtotime($AutoDate));
						switch ($Auto_day) {
							case 'Mon':
								$Auto_day = "Monday";
								break;
							case 'Tue':
								$Auto_day = "Tuesday";
								break;
							case 'Wed':
								$Auto_day = "Wednesday";
								break;
							case 'Thu':
								$Auto_day = "Thursday";
								break;
							case 'Fri':
								$Auto_day = "Friday";
								break;
							case 'Sat':
								$Auto_day = "Saturday";
								break;
							case 'Sun':
								$Auto_day = "Sunday";
								break;
						}
						$auto_RestDay = $Auto_day."isRestDay";
						if ($Emp_WorkSched[$auto_RestDay] != 1) {
							$arr["ARR"][$AutoDate]["TimeIn"] = $Monday_in;
							$arr["ARR"][$AutoDate]["LunchOut"] = $Monday_lout;
							$arr["ARR"][$AutoDate]["LunchIn"] = $Monday_lin;
							$arr["ARR"][$AutoDate]["TimeOut"] = $Monday_out;	
						}
					}
				}
				foreach ($arr as $xvalue) {
					foreach ($xvalue as $xkey => $yvalue) {
						$xday_name = date("D",strtotime($yvalue['AttendanceDate']));
						
						if ($xday_name == "Fri") {
							if ($yvalue["Holiday"] != "") {
								$Friday_Holiday_PerWeek[$yvalue["Week"]] = 1;
							}
						}
					}
				}
				foreach ($arr as $nvalue) {
					foreach ($nvalue as $zvalue) {
						if (date("D",strtotime($zvalue['AttendanceDate'])) == "Mon") {
							$First_Monday = date("Y-m-d",strtotime($zvalue['AttendanceDate']));
							break;
						}
					}
				}
				foreach ($arr as $value) {
					foreach ($value as $key => $row) {
						$Remarks 					= "";
					    $TardyHour 					= "";
					    $UndertimeHour 				= "";
					    $arr_TI						= "";
						$arr_OBO					= "";
						$arr_TO						= "";
						$arr_OBI					= "";
						$arr_Holiday				= "";
						$arr_leave 					= "";
						$arr_COC					= "";
						$arr_OffSus					= "";
						$arr_HasOT 					= "";
						$arr_OBName 				= "";
						$new_exc 					= 0;
						$invisible_time 			= 0;
						$MidUnderTime 				= 0;
						$day_name 					= date("D",strtotime($row['AttendanceDate']));
						if ($day_name != "") {
							switch ($day_name) {
								case 'Mon':
									$day_name = "Monday";
									break;
								case 'Tue':
									$day_name = "Tuesday";
									break;
								case 'Wed':
									$day_name = "Wednesday";
									break;
								case 'Thu':
									$day_name = "Thursday";
									break;
								case 'Fri':
									$day_name = "Friday";
									break;
								case 'Sat':
									$day_name = "Saturday";
									break;
								case 'Sun':
									$day_name = "Sunday";
									break;
							}
						}	
						$day 						= $row["Day"]; 
						$day_in             	   	= $day_name."In";
						$day_out            	   	= $day_name."Out";
						$day_flexi          	   	= $day_name."FlexiTime";
						$day_LBOut					= $day_name."LBOut";
						$day_LBIn					= $day_name."LBIn";
						$day_RestDay				= $day_name."isRestDay";
						$day_isflexi          	   	= $day_name."isFlexi";


						$data_flexi             	= $Emp_WorkSched[$day_flexi];
						$data_isflexi             	= $Emp_WorkSched[$day_isflexi];
						$data_timein            	= $Emp_WorkSched[$day_in];
						$data_timeout 				= $Emp_WorkSched[$day_out];
						$data_LunchOut 				= $Emp_WorkSched[$day_LBOut];
						$data_LunchIn 				= $Emp_WorkSched[$day_LBIn];
						$data_RestDay      			= $Emp_WorkSched[$day_RestDay];
						$data_LunchTime 			= $data_LunchIn - $data_LunchOut;
						$day_work_hours_count   	= ($data_timeout - $data_timein) - ($data_LunchIn - $data_LunchOut);
						$Week 						= $row["Week"];	
						$AutoLB						= $Emp_WorkSched["AutoLB"];
						$PerDayHours                = $day_work_hours_count / 60;
						
						
						

						$arr_TI						= $row["TimeIn"];
						$arr_OBO					= $row["OBOut"];
						$arr_TO						= $row["TimeOut"];
						$arr_OBI					= $row["OBIn"];
						$arr_Holiday				= $row["Holiday"];
						$arr_leave 					= $row["Leave"];
						$arr_COC					= $row["CTO"];
						$arr_OffSus					= $row["OffSus"];
						$arr_HasOT 					= $row["HasOT"];
						$arr_OBName					= $row["OBName"];
						$arr_OT						= $row["OTTime"];
						$arr_OBTimeIn 				= $row["OBTimeIn"];
						$arr_OBTimeOUt 				= $row["OBTimeOut"];


						if ($CompanyID == 35) {
							if ($First_Monday != "") {
								if ($row['AttendanceDate'] == $First_Monday) {
									$data_flexi 	= "510";
								}
							}
						}


						$core_time 					= $data_flexi + ($data_timeout - $data_timein);

						if ($Emp_WorkSched["ScheduleType"] == "Fi") {
							$data_flexi = $data_timein;
						} else {
							$data_flexi = $data_flexi;
						}

						
						if ($CompanyID == 1000) {
							if ($Friday_Holiday_PerWeek[$Week] == 1) {
								$data_LunchTime = 60;
								$day_work_hours_count = 480;
								$data_timeout = 960;
							}							
						}

						if ($arr_COC != "") {
							$Used_COC = $Used_COC + $arr_COC;
						}


						if ($arr_TI >= $data_LunchIn && $data_RestDay != 1) {
							$arr_TI = $arr_TI - ($data_LunchIn - $data_LunchOut);
						}
						if ($arr_leave != "") {
							$arr_TI = $data_timein;
							$arr_TO = $data_timeout;
						}

						if ($arr_OT != "") {
							if ($arr_TI != "") {
								if ($arr_TI < $data_timein) $arr_TI = $data_timein;
								$arr_OT = ($arr_TO - $arr_TI) - 60;
							}
						}


						if ($arr_Holiday != "" && $arr_TI == "" && $arr_OT == "") {
							$arr_TI = $data_timein;
							$arr_TO = $data_timeout;
						}
						if ($arr_COC != "") {
							if ($arr_TI != "") {
								if ($arr_TI < $data_flexi) {
									if ($arr_COC >= $day_work_hours_count) {
										$arr_TI = $data_timein;
										$arr_TO = $data_timeout;
									} else {
										if ($arr_TI == "") {
											$arr_TI = $data_timein;
											$Lunch_Out = $data_LunchOut;
										} else {
											$arr_TI = $arr_TI - $arr_COC;
										}
										if ($arr_TO == "") {
											//$arr_TO = $data_timein + $arr_COC;
											$arr_TO = $data_LunchIn + $arr_COC;
										} else {
											$arr_TO = $arr_TO + $arr_COC;
											
										}
									}
								} else {
									$arr_TI = $arr_TI - $arr_COC;
								}	
							} else {
								if ($arr_COC == $day_work_hours_count) {
									$arr_TI = $data_timein;
									$arr_TO = $data_timeout;	
								} else {
									$arr_TI 	= $data_timein;
									$arr_TO 	= $arr_TI + $arr_COC;
									$Lunch_Out 	= $data_LunchOut;
									$Lunch_In 	= $data_LunchIn;
								}
							}
						}


						if ($arr_TI != "" && $data_RestDay != 1) {
							$PresentDays++;
						}
						/*===================================================================================================*/
						/*===================================================================================================*/
						//AUTO LUNCH BREAK
						/*===================================================================================================*/
						if ($AutoLB == 1) {
							if ($arr_TI != "") {
								$Lunch_Out = $data_LunchOut;
								$Lunch_In = $data_LunchIn;
							} else {
								$Lunch_Out = "";
								$Lunch_In = "";
							}
						} else {
							$Lunch_Out = $row["LunchOut"];
							$Lunch_In = $row["LunchIn"];
						}
						/*===================================================================================================*/
						/*===================================================================================================*/
						//MAXIMUM TIME FROM FOR EMPLOYEE TO ENTER WITHOUT LATE
						/*===================================================================================================*/
						

						if ($arr_OBName != "") {
							if (is_numeric($arr_OBTimeIn)) {
								if ($arr_TI != "") {
									if ($arr_TI >= $arr_OBTimeIn) {
										$arr_TI = $arr_OBTimeIn;
									}	
								} else {
									$arr_TI = $data_timein;
								}
								if ($arr_TO != "") {
									if ($arr_TO <= $arr_OBTimeOUt) {
										$arr_TO = $arr_OBTimeOUt;
									}	
								} else {
									$arr_TO = $data_timeout;
								}
								
								// $arr_TI = $arr_OBTimeIn - $data_LunchTime;
								// $arr_TO = $arr_OBTimeOUt;
							} else {
								if ($TardyHour != "") {
									$arr_TO = $data_timeout;
								} else {
									$arr_TI = $data_timein;
									$arr_TO = $data_timeout;
								}	
							}
						}
						/*===================================================================================================*/
						/*===================================================================================================*/
						//GETTING LATE
						/*===================================================================================================*/
						if ($data_RestDay != 1) {
							if ($arr_OT != "" && $arr_OT != "0") {
								$arr_TO = $arr_TO + $arr_OT;
							}
							if ($arr_TI != "") {
								if (($arr_TI + $data_LunchTime) > $data_LunchIn) {

									if ($arr_TO > $core_time) {
										$AfternoonIN 				= $core_time - $arr_TI;
									} else {
										$AfternoonIN 				= intval($arr_TO) - intval($arr_TI);	
									}
									if ($arr_TI > $data_LunchIn) {

										$TardyHour 						= $day_work_hours_count - $AfternoonIN;
									} else {
										$TardyHour 						= $day_work_hours_count - $AfternoonIN + $data_LunchTime;
									}
									
									if ($CompanyID == "28") {
										$TardyHour 					= $arr_TI - $data_flexi;
										$TardyHour 					= $TardyHour + 15;	
									}
									$TardyHour_PerWeek[$Week] 	+= $TardyHour;
									$TardyCount_PerWeek[$Week]++;

								} else {
									if ($data_isflexi != 1) {
										if ($arr_TI >= $data_timein) {
											$TardyHour 					= "";
										} else {
											$TardyHour 					= ($arr_TI - $data_flexi);
											$TardyHour_PerWeek[$Week] 	= $TardyHour_PerWeek[$Week] + $TardyHour;
											$TardyCount_PerWeek[$Week]++;
										}
									} else {
										if ($arr_TI <= $data_flexi) {
											$TardyHour = "";
										} else {
											$TardyHour 					= ($arr_TI - $data_flexi);
											$TardyHour_PerWeek[$Week] 	= $TardyHour_PerWeek[$Week] + $TardyHour;
											$TardyCount_PerWeek[$Week]++;
										}
									}	
								}
							}	
						}
						if ($arr_OffSus != "") {
							if ($arr_TI != "") {
								if ($arr_OffSus == "420") {
									$arr_TI = $data_timein;
									$arr_TO = $data_timeout;
									$TardyHour = 0;
									if ($CompanyID == 1000) {
										if ($Friday_Holiday_PerWeek[$Week] == 1) {
											$arr_TI = $data_timein;
											$arr_TO = "960";
										}
									}
								} else {
									if ($TardyHour != "") {
										$arr_TO = $core_time;
									} else {
										
										if ($arr_TO != "") {
											if ($arr_TO <= $arr_OffSus) {

												if (($arr_TO - $arr_TI) <= $day_work_hours_count) {
													if ($arr_TO <= $arr_OffSus) {
														$MidUnderTime = intval($arr_OffSus) - intval($arr_TO);	
														$arr_TO = $data_timeout - $MidUnderTime;
													}
												}	
											} else {
												if ($arr_TI < $data_timein) $arr_TI = $data_timein;
												$arr_TO = $arr_TI + $day_work_hours_count + $data_LunchTime;
											}
										}
									}
									if ($CompanyID == 1000) {
										if ($Friday_Holiday_PerWeek[$Week] == 1) {
											$arr_TI = $data_timein;
											$arr_TO = "960";
										}							
									}	
								}
							} else {
								if ($arr_OffSus == "420") {
									$arr_TI = $data_timein;
									$arr_TO = $data_timeout;
									$TardyHour = 0;
								}
							}
						}


						/*===================================================================================================*/
						/*===================================================================================================*/
						//GETTING THE WORKING AND EXCESS HOURS OF THE EMPLOYEE
						/*===================================================================================================*/
						if ($arr_TO != "" && $arr_TI != "" && $data_RestDay != 1) {
							if ($arr_TI <= $data_timein) {
								$arr_TI = $data_timein;
							}
							if ($TardyHour != "" && $TardyHour != "0") {
								if ($arr_TO > $core_time) {
									$arr_TO = $core_time;
								}
							}

							$consume_time 	= (($arr_TO - $arr_TI) - $data_LunchTime);
							$excess_time 	= $consume_time - $day_work_hours_count; 	
						} else {
							$excess_time = 0;
							$consume_time = 0;
						}
						/*===================================================================================================*/
						/*===================================================================================================*/
						//GETTING THE UNDERTIME AND EXCESS HOURS OF THE EMPLOYEE
						/*===================================================================================================*/
						if ($consume_time >= $day_work_hours_count) {
							if ($day_work_hours_count != "") {
								$RegHour_PerWeek[$Week] = $RegHour_PerWeek[$Week] + $day_work_hours_count;
							}
							$consume_time = $day_work_hours_count;
						} else {
							if ($arr_TO != "" && $arr_TI != "" && $data_RestDay != 1) {
								$excess_time = 0;
								if ($TardyHour != "") {
									$UndertimeHour = $core_time - $arr_TO;
								} else {
									$UndertimeHour = $day_work_hours_count - $consume_time;	
								}
								if ($CompanyID == 2 || 
									$CompanyID == 28) {
									if ($arr_TO >= $data_timeout) {
										$UndertimeHour = 0;
									} else {
										$UndertimeHour = $data_timeout - $arr_TO;	
									}	
								} else if ($CompanyID == 1000) {
									if ($arr_TO >= "960") {
										$UndertimeHour = 0;
									} else {
										$UndertimeHour = "960" - $arr_TO;	
									}
								}
								if ($UndertimeHour != 0) {
									$UndertimeHour_PerWeek[$Week] 	= $UndertimeHour_PerWeek[$Week] + $UndertimeHour;
									$UndertimeCount_PerWeek[$Week]++;	
								}
							} else {
								$excess_time = 0;
							}
							if ($consume_time != 0) {
								$RegHour_PerWeek[$Week] = $RegHour_PerWeek[$Week] + $consume_time;
							}
						}
						
						if ($arr_TI != "") {
							if ($UndertimeHour == "") {
								$full_time = $arr_TI + $day_work_hours_count + (intval($data_LunchTime));
								if ($arr_TO >= $full_time) {
									if ($arr_TO > $core_time) {
										$exc = $arr_TO - $full_time;
										$new_exc = $core_time - $full_time;
										$invisible_time = $arr_TO - $core_time;
										$excess_time = $new_exc;
									}	
								}
							}
						}
						if ($arr_TO != "" && $data_RestDay != 1) {
							$CutOff_PerWeek[$Week] 		= $CutOff_PerWeek[$Week] + $day_work_hours_count;
						}
						
						/*===================================================================================================*/
						/*===================================================================================================*/
						/*===================================================================================================*/
						/*===================================================================================================*/
						/*===================================================================================================*/
						/*===================================================================================================*/
						//MAPPING
						/*===================================================================================================*/
						$map["[[Day".$day."]]"] 		= date("d",strtotime($row['AttendanceDate']));
						$map["[[Num".$day."]]"] 		= date("d D",strtotime($row["AttendanceDate"]));
						$map["[[date".$day."]]"] 		= date("F d,Y",strtotime($row["AttendanceDate"]));
						$map["[[WDay".$day."]]"] 		= $day_name;
						$map["[[new_date".$day."]]"] 	= date("m/d/Y",strtotime($row["AttendanceDate"]));
						if ($data_RestDay == 1) {
							if ($CompanyID != "1000") {
								$map["[[rem".$day."]]"]    	= "<span style='color:red;'>RestDay</span>";	
							} else {
								$map["[[rem".$day."]]"]    	= "";
							}
							if ($CompanyID ==  "28") {
								$map["[[din".$day."]]"]    	= HoursFormat($row["TimeIn"]);	
								$map["[[dout".$day."]]"]   	= HoursFormat($row["TimeOut"]);
								if (intval($Lunch_Out) > intval($Lunch_In)) {
									$map["[[lout".$day."]]"]   	= HoursFormat($Lunch_In);
									$map["[[lin".$day."]]"]    	= HoursFormat($Lunch_Out);	
								} else {
									$map["[[lout".$day."]]"]   	= HoursFormat($Lunch_Out);
									$map["[[lin".$day."]]"]    	= HoursFormat($Lunch_In);	
								}
							}
						} else {
							if ($CompanyID ==  "1000") {
								$map["[[din".$day."]]"]    	= AMPMFormat($row["TimeIn"]);	
								$map["[[dout".$day."]]"]   	= AMPMFormat($row["TimeOut"]);
								$map["[[lout".$day."]]"]   	= AMPMFormat($Lunch_Out);
								$map["[[lin".$day."]]"]    	= AMPMFormat($Lunch_In);
							} else {
								$map["[[din".$day."]]"]    	= HoursFormat($row["TimeIn"]);	
								$map["[[dout".$day."]]"]   	= HoursFormat($row["TimeOut"]);
								if (intval($Lunch_Out) > intval($Lunch_In)) {
									$map["[[lout".$day."]]"]   	= HoursFormat($Lunch_In);
									$map["[[lin".$day."]]"]    	= HoursFormat($Lunch_Out);	
								} else {
									$map["[[lout".$day."]]"]   	= HoursFormat($Lunch_Out);
									$map["[[lin".$day."]]"]    	= HoursFormat($Lunch_In);	
								}
								
							}
							
							$map["[[reg".$day."]]"]   	= HoursFormat($consume_time);
							$map["[[tdy".$day."]]"]     = HoursFormat($TardyHour);
							$map["[[ut".$day."]]"]    	= HoursFormat($UndertimeHour);	
							$map["[[rem".$day."]]"]    	= "<span style='color:black;'>".$Remarks."</span>";

							if ($UndertimeHour != "") {
								$map["[[uthr".$day."]]"] = explode(":", HoursFormat($UndertimeHour))[0];
								$map["[[utmin".$day."]]"] = explode(":",HoursFormat($UndertimeHour))[1];
							}
						}
						if ($TardyHour == "") {
							if ($arr_Holiday == "" && $data_RestDay != 1) {
								if ($arr_HasOT == "1") {
									$excess_time = $excess_time + $invisible_time;
									if ($excess_time >= $OTMinTime) {
										if ($excess_time >= $day_work_hours_count) {
											for ($v=60; $v <= $excess_time ; $v+=60) { 
												if ($v == 180) {
													$dummy = $excess_time - 180;
													if ($dummy > 60) {
														$excess_time -= 60;	
													} else {
														$excess_time -= $dummy;
													}
												}
											}
										}
										if ($CompanyID == "1000") {
											if ($Friday_Holiday_PerWeek[$Week] == 1) {
												$excess_time = $excess_time - $data_LunchTime;
											}
										}
										$OTHours_PerWeek[$Week] 			= $OTHours_PerWeek[$Week] + $excess_time;
										$map["[[ot".$day."]]"]				= HoursFormat($excess_time);
										$map["[[rem".$day."]]"]  			= "OT Pay";
									} else {
										$map["[[ot".$day."]]"] 				= "";
										//$excess_time 						= 0;
										$ExcessHour_PerWeek[$Week] 			= $ExcessHour_PerWeek[$Week] + $excess_time;
									}
								} else if ($arr_HasOT == "0") {
									$excess_time = $excess_time + $invisible_time;
									if ($excess_time >= $COCMinTime) {
										if ($excess_time >= $day_work_hours_count) {
											for ($w=60; $w <= $excess_time ; $w+=60) { 
												if ($w == 180) {
													$dummy = $excess_time - 180;
													if ($dummy > 60) {
														$excess_time -= 60;	
													} else {
														$excess_time -= $dummy;
													}
												}
											}	
										}
										if ($CompanyID == "1000") {
											if ($Friday_Holiday_PerWeek[$Week] == 1) {
												$excess_time = $excess_time - $data_LunchTime;
											}
										}
										$COCHours_PerWeek[$Week] 			= $COCHours_PerWeek[$Week] + $excess_time;
										$map["[[ot".$day."]]"]   	   		= HoursFormat($excess_time);
										if ($CompanyID != "1000") {
											$map["[[rem".$day."]]"] 		= "COC";
										}
									} else {
										$map["[[ot".$day."]]"] 				= "";
										$ExcessHour_PerWeek[$Week] 			= $ExcessHour_PerWeek[$Week] + $excess_time;
									}
								} else {
									$map["[[exh".$day."]]"]   	   			= HoursFormat($excess_time);
									$ExcessHour_PerWeek[$Week] 				= $ExcessHour_PerWeek[$Week] + $excess_time;
								}	
							}
						} else {
							$map["[[exh".$day."]]"]   	   				= HoursFormat($excess_time);
							$ExcessHour_PerWeek[$Week] 					= intval($ExcessHour_PerWeek[$Week]) + intval($excess_time);
						}
						if ($arr_TI == "" && $data_RestDay != 1) {
							$AbsentCount_PerWeek[$Week]++;
							$map["[[rem".$day."]]"]    = "<span style='color:black;'>Absent</span>";
						}
						if ($arr_TI == "" && $arr_TO != "" && $data_RestDay != 1) {
							if ($CompanyID != 28) {
								$map["[[rem".$day."]]"]    = "<span style='color:black;'>Incomplete</span>";	
							}
						}
						if ($arr_TI != "" && $arr_TO == "" && $data_RestDay != 1) {
							if ($CompanyID != 28) {
								$map["[[rem".$day."]]"]    = "<span style='color:black;'>Incomplete</span>";	
							}
						}
						if ($row["Leave"] != "") {
							$Leave_count++;
							if ($data_RestDay != 1) {
								$leave = getRecord("leaves",$row["Leave"],"Name");
				              	if ($row["FL"] == 1) {
				              		$map["[[rem".$day."]]"]    = "<span style='color:black;'>FL</span>";
				              	} else {
				              		$map["[[rem".$day."]]"]    = "<span style='color:black;'>".$leave."</span>";	
				              	}
				              	if ($leave == "SPL") {
				              		$SPL_count++;
				              	}
				              	$map["[[lin".$day."]]"]    	= "";
								$map["[[lout".$day."]]"]   	= "";
								$map["[[reg".$day."]]"]    	= "";
								$map["[[din".$day."]]"]    	= "";
								$map["[[dout".$day."]]"]   	= "";
							}
			            }

						if ($arr_OBName != "") {
							if ($row["TimeIn"] == "" && $row["TimeOut"] == "") {
								$map["[[lin".$day."]]"]    = "";
								$map["[[lout".$day."]]"]   = "";
							}
							$OB_count++;
							if ($CompanyID == "35") {
								$map["[[rem".$day."]]"]    = "<span style='color:black;'>".$row["OBCode"]."</span>";
							} else {
								$map["[[rem".$day."]]"]    = "<span style='color:black;'>".$arr_OBName."</span>";
							}
							
						}
						if ($arr_Holiday != "") {
							$map["[[rem".$day."]]"]    	= "<span style='color:black;'>".$arr_Holiday."</span>";
							$map["[[din".$day."]]"]    	= "";	
							$map["[[dout".$day."]]"]   	= "";
							$map["[[lout".$day."]]"]   	= "";
							$map["[[lin".$day."]]"]    	= "";
							$map["[[reg".$day."]]"]    	= "";
						}
						if ($arr_COC != "") {
							if ($arr_COC >= $day_work_hours_count) {
								$map["[[lin".$day."]]"]    = "";
								$map["[[lout".$day."]]"]   = "";
							}
							$CTO_count++;
							$map["[[reg".$day."]]"]   	= HoursFormat($consume_time);
							$map["[[rem".$day."]]"]    	= "<span style='color:black;'>CTO (".($row["CTO"] / 60)." Hours)</span>";
							//$map["[[reg".$day."]]"]    = "";

						} else {
							$map["[[reg".$day."]]"]   	= HoursFormat($consume_time);
						}

						if ($arr_OffSus != "") {
							if ($row["TimeIn"] == "" && $row["TimeOut"] == "") {
								$map["[[lin".$day."]]"]    = "";
								$map["[[lout".$day."]]"]   = "";
							}
							if ($arr_TI != "") {
								$map["[[rem".$day."]]"]    = "<span style='color:black;'>".$row["OffSusName"]."</span>";
							}
							
						}
						if ($arr_TI >= $data_LunchOut) {
							$map["[[lin".$day."]]"]	= "";
							$map["[[lout".$day."]]"]	= "";
						}
						if ($data_RestDay == 1) {
							if ($CompanyID == "1000") {
								$map["[[din".$day."]]"]    		= AMPMFormat($row["TimeIn"]);	
								$map["[[dout".$day."]]"]   		= AMPMFormat($row["TimeOut"]);	
							}
							if ($arr_HasOT == "0" || $arr_HasOT == 1) {
								if ($arr_OT != "") {
									if ($CompanyID == "1000") {
										$map["[[din".$day."]]"]    		= AMPMFormat($row["TimeIn"]);	
										$map["[[dout".$day."]]"]   		= AMPMFormat($row["TimeOut"]);	
									} else {
										$map["[[din".$day."]]"]    		= HoursFormat($row["TimeIn"]);	
										$map["[[dout".$day."]]"]   		= HoursFormat($row["TimeOut"]);	
									}
									$map["[[ot".$day."]]"] 	   			= HoursFormat($arr_OT);
									if ($CompanyID == "1000") {
										if ($arr_HasOT == "1") {
											$OTHours_PerWeek[$Week] 	= $OTHours_PerWeek[$Week] + $arr_OT;
										} else {
											$COCHours_PerWeek[$Week]   	= $COCHours_PerWeek[$Week] + $arr_OT;
										}
									} else {
										if ($arr_HasOT == "1") {
											$map["[[rem".$day."]]"]    	= "<span style='color:black;'>OT PAY</span>";	
											//$OTHours_PerWeek[$Week] 	= $OTHours_PerWeek[$Week] + ($arr_OT * 1.5);
											$OT_Special 				= $OT_Special + ($arr_OT * 1.5);
										} else {
											$map["[[rem".$day."]]"]    	= "<span style='color:black;'>COC</span>";
											$COCHours_PerWeek[$Week]   	= $COCHours_PerWeek[$Week] + ($arr_OT * 1.5);
										}	
									}
								} else {
									if ($arr_TI != "" && $row["Leave"] == "" && $arr_OBName == "" && $arr_COC == "") {
										if ($CompanyID == "1000") {
											if ($arr_TI <= $data_timein) {
												$arr_TI = $data_timein;
											}
											if ($arr_TO >= $data_LunchOut && $arr_TO <= $data_LunchIn) {
												$arr_TO = $data_LunchOut;
											} else {
												if ($arr_TI <= $data_LunchIn) {
													$arr_TO = $arr_TO - 60;	
												}
											}
											$weekend_consume_time = (intval($arr_TO) - intval($arr_TI));
											for ($y=60; $y <= $weekend_consume_time ; $y+=60) {
												if ($y == 180) {
													$dummy = $weekend_consume_time - 180;
													if ($dummy > 60) {
														$weekend_consume_time -= 60;	
													} else {
														$weekend_consume_time -= $dummy;
													}
												}
											}
										} else {
											$weekend_consume_time = (intval($arr_TO) - intval($arr_TI));
											if ($weekend_consume_time >= $day_work_hours_count) {
												$weekend_consume_time = $weekend_consume_time - 60;
											} else {
												$weekend_consume_time = $weekend_consume_time + 60;
											}
										}
										$weekend_excess_time = $weekend_consume_time - $day_work_hours_count;
										if ($weekend_consume_time > $day_work_hours_count) {
											for ($z=60; $z <= $weekend_excess_time ; $z+=60) { 
												if ($z == 180) {
													$new_dummy = $weekend_consume_time - 180;
													if ($new_dummy > 60) {
														$weekend_consume_time -= 60;	
													} else {
														$weekend_consume_time -= $new_dummy;
													}
												}
											}
										}
										if ($CompanyID == "1000") {
											$map["[[din".$day."]]"]    		= AMPMFormat($row["TimeIn"]);	
											$map["[[dout".$day."]]"]   		= AMPMFormat($row["TimeOut"]);	
										} else {
											$map["[[din".$day."]]"]    		= HoursFormat($row["TimeIn"]);	
											$map["[[dout".$day."]]"]   		= HoursFormat($row["TimeOut"]);	
										}
										$map["[[ot".$day."]]"] 	   	= HoursFormat($weekend_consume_time);
										if ($CompanyID == "1000") {
											if ($arr_HasOT == "1") {
												$OTHours_PerWeek[$Week] 	= $OTHours_PerWeek[$Week] + $weekend_consume_time;
											} else {
												$COCHours_PerWeek[$Week]   	= $COCHours_PerWeek[$Week] + $weekend_consume_time;
											}
										} else {
											if ($arr_HasOT == "1") {
												//$OTHours_PerWeek[$Week] 	= $OTHours_PerWeek[$Week] + ($weekend_consume_time * 1.5);
												$OT_Special 				= $OT_Special + ($weekend_consume_time * 1.5);
											} else {
												$COCHours_PerWeek[$Week]   	= $COCHours_PerWeek[$Week] + ($weekend_consume_time * 1.5);
											}	
										}
									} else {
										if ($CompanyID != "1000") {
											$map["[[rem".$day."]]"]    	= "<span style='color:red;'>RestDay</span>";	
										} else {
											$map["[[rem".$day."]]"]    	= "";
										}	
									}
								}
							}
						} else {
							if ($arr_Holiday != "") {
								if ($arr_HasOT == "0" or $arr_HasOT == 1) {
									if ($arr_OT == "") {
										if ($CompanyID == "1000") {
											if ($arr_TI <= $data_timein) {
												$arr_TI = $data_timein;
											}
											if ($arr_TO >= $data_LunchOut && $arr_TO <= $data_LunchIn) {
												$arr_TO = $data_LunchOut;
											} else {
												$arr_TO = $arr_TO - 60;
											}	
										}	
										$arr_OT = (intval($arr_TO) - intval($arr_TI));
										if ($arr_OT <= $data_LunchTime) {
											$arr_OT = $arr_OT - 60;
										}

										if ($arr_OT >= $day_work_hours_count) {
											for ($z=60; $z <= $arr_OT ; $z+=60) { 
												if ($z == 180) {
													$new_dummy = $arr_OT - 180;
													if ($new_dummy > 60) {
														$arr_OT -= 60;	
													} else {
														$arr_OT -= $new_dummy;
													}
												}
											}
										}	
									}
									if ($CompanyID == "1000") {
										$map["[[din".$day."]]"]    		= AMPMFormat($row["TimeIn"]);	
										$map["[[dout".$day."]]"]   		= AMPMFormat($row["TimeOut"]);	
										$map["[[lin".$day."]]"]    		= AMPMFormat($row["LunchIn"]);	
										$map["[[lout".$day."]]"]   		= AMPMFormat($row["LunchOut"]);	
									} else {
										$map["[[din".$day."]]"]    		= HoursFormat($row["TimeIn"]);	
										$map["[[dout".$day."]]"]   		= HoursFormat($row["TimeOut"]);	
									}
									$map["[[ot".$day."]]"] 	   			= HoursFormat($arr_OT);
									if ($CompanyID == "1000") {
										if ($arr_HasOT == "1") {
											$OTHours_PerWeek[$Week] 	= $OTHours_PerWeek[$Week] + $arr_OT;
										} else {
											$COCHours_PerWeek[$Week]   	= $COCHours_PerWeek[$Week] + $arr_OT;
										}
									} else {
										if ($arr_HasOT == "1") {
											$map["[[rem".$day."]]"]    	= "<span style='color:black;'>OT PAY</span>";	
											$OTHours_PerWeek[$Week] 	= $OTHours_PerWeek[$Week] + ($arr_OT * 1.5);
										} else {
											$map["[[rem".$day."]]"]    	= "<span style='color:black;'>COC</span>";
											$COCHours_PerWeek[$Week]   	= $COCHours_PerWeek[$Week] + ($arr_OT * 1.5);
										}	
									}	
								}
							}
						}
						if ($arr_HasOT != "0" && $arr_HasOT != "1" && $arr_Holiday == "") {
							switch ($CompanyID) {
								case '1000':
									$lunch = 0;
									if ($arr_TI != "") {
										if ($arr_OBName == "") {
											if ($Lunch_Out == "" && $Lunch_In == "") {
												$lunch = $data_LunchTime;
											}	
										}
									}
									$map["[[reg".$day."]]"]   	= HoursFormat($consume_time + $excess_time + $lunch);
									break;
								case '2':
									$map["[[reg".$day."]]"]   	= HoursFormat($consume_time + $excess_time);
									break;
								default:
									$map["[[reg".$day."]]"]   	= HoursFormat($consume_time);
									break;
							}
						}
						if ($row["ChangesRem"] != "") {
							$map["[[rem".$day."]]"] = $row["ChangesRem"];	
						}
						if ($IsAuto == 1) {
							$map["[[din".$day."]]"]    		= "";	
							$map["[[dout".$day."]]"]   		= "";	
							$map["[[lin".$day."]]"]    		= "";	
							$map["[[lout".$day."]]"]   		= "";	
						}
					}
				}
				for ($i=1; $i <= 6; $i++) { 
					if ($ExcessHour_PerWeek[$i] < 0) {
						$ExcessHour_PerWeek[$i] = 0;
					}
					if ($CompanyID == "1000") {	
						$RegHour_PerWeek[$i] = $RegHour_PerWeek[$i] + $ExcessHour_PerWeek[$i];
						if ($CutOff_PerWeek[$i] >= ($RegHour_PerWeek[$i] + $TardyHour_PerWeek[$i])) {
							$UndertimeHour_PerWeek[$i] = $CutOff_PerWeek[$i] - ($RegHour_PerWeek[$i] + $TardyHour_PerWeek[$i]);

						}
					}

					$map["[[rgwk$i]]"]     	= HoursFormat($RegHour_PerWeek[$i]);
					// if ($CompanyID == "1000") {	
					// 	$Offset = $RegHour_PerWeek[$i];
					// 	if ($Offset > $CutOff_PerWeek[$i]) {
					// 		$UndertimeHour_PerWeek[$i] = 0;
					// 	} else {
					// 		if ($ExcessHour_PerWeek[$i] > $UndertimeHour_PerWeek[$i]) {
					// 			$new_excess = $ExcessHour_PerWeek[$i] - $UndertimeHour_PerWeek[$i];
					// 			// if ($TardyHour_PerWeek[$i] > $new_excess) {
					// 			// 	$TardyHour_PerWeek[$i] = $TardyHour_PerWeek[$i] - $new_excess;
					// 			// }
					// 			$UndertimeHour_PerWeek[$i] = 0;
					// 		} else {
					// 			$UndertimeHour_PerWeek[$i] = $UndertimeHour_PerWeek[$i] - $ExcessHour_PerWeek[$i];
					// 		}
					// 	}
					// }
					$map["[[twk$i]]"]      	= HoursFormat($TardyHour_PerWeek[$i]);	
					$map["[[ulwk$i]]"]     	= HoursFormat($UndertimeHour_PerWeek[$i]);	
					$map["[[ehwk$i]]"]     	= HoursFormat($ExcessHour_PerWeek[$i]);
					$map["[[ttwk$i]]"]     	= $TardyCount_PerWeek[$i];
					$map["[[tuwk$i]]"]     	= $UndertimeCount_PerWeek[$i];
					$map["[[awk$i]]"]      	= $AbsentCount_PerWeek[$i];
					$map["[[cowk$i]]"]     	= HoursFormat($COCHours_PerWeek[$i]);
					if ($CompanyID == "1000") {
						$map["[[ot$i]]"]			= HoursFormat($OTHours_PerWeek[$i] + $COCHours_PerWeek[$i]);
					} else {
						$map["[[ot$i]]"]			= HoursFormat($OTHours_PerWeek[$i]);
					}

				}
				for ($x=1; $x <= 6; $x++) { 
					$TotalRegHour 			= $TotalRegHour + $RegHour_PerWeek[$x];
					$TotalTardyHour 		= $TotalTardyHour + $TardyHour_PerWeek[$x];
					$TotalUndertimeHour 	= $TotalUndertimeHour + $UndertimeHour_PerWeek[$x];
					$TotalExcessHour    	= $TotalExcessHour + $ExcessHour_PerWeek[$x];
					$TotalCOCHour       	= $TotalCOCHour + $COCHours_PerWeek[$x];
					$TotalTardyCount    	= $TotalTardyCount + $TardyCount_PerWeek[$x];
					$TotalUndertimeCount 	= $TotalUndertimeCount + $UndertimeCount_PerWeek[$x];
					$TotalAbsentCount  		= $TotalAbsentCount + $AbsentCount_PerWeek[$x];
					$TotalOTHour       		= $TotalOTHour + $OTHours_PerWeek[$x];
				}
			    if ($TotalCOCHour >= 2400) {
			    	$TotalCOCHour = 2400;
			    }
	          	$map["[[totrg]]"] 	= HoursFormat($TotalRegHour);
	          	$map["[[tott]]"] 	= HoursFormat($TotalTardyHour);
	          	$map["[[totul]]"]   = HoursFormat($TotalUndertimeHour);
	          	$map["[[toteh]]"]   = HoursFormat($TotalExcessHour);
	          	$map["[[totcc]]"]  	= HoursFormat($TotalCOCHour - $Used_COC);
	          	$map["[[tottt]]"]   = $TotalTardyCount;
	          	$map["[[tottu]]"]   = $TotalUndertimeCount;
	          	$map["[[tota]]"]   	= $TotalAbsentCount;
	          	$map["[[otps]]"]	= HoursFormat($OT_Special);
	          	if ($CompanyID == 1000) {
	          		$map["[[otp]]"]		= HoursFormat($TotalOTHour + ($TotalCOCHour - $Used_COC));
	          	} else {
	          		$map["[[otp]]"]		= HoursFormat($TotalOTHour);	
	          	}
	          	if (intval($TotalUndertimeHour) > 0) {
	          		$map["[[utthr]]"] 		= explode(":", HoursFormat($TotalUndertimeHour))[0];	
	          		$map["[[uttmin]]"]		= explode(":", HoursFormat($TotalUndertimeHour))[1];
	          		$map["[[eqdayut]]"]   	= getEquivalent($TotalUndertimeHour,"workinghrsconversion");
	          	}
	          	if (intval($TotalTardyHour) > 0) {
	          		$map["[[eqdaytardy]]"] = getEquivalent($TotalTardyHour,"workinghrsconversion");
	          	}
	          	

	          	/*===================================================================================================*/
				/*===================================================================================================*/
				//GETTING THE EARNINGS
				/*===================================================================================================*/
				$TardyEQ 			= getEquivalent($TotalTardyHour,"workinghrsconversion");
			   	$UndertimeEQ		= getEquivalent($TotalUndertimeHour,"workinghrsconversion");
			   	/*if ($AbsentEQ != "") {
			   		if ($TotalAbsentCount > 0) {
				   		for ($i=1; $i <= $TotalAbsentCount; $i++) { 
							$AbsentEQ = $WorkDayEQ["WorkDayConversion"][$PerDayHours] + $AbsentEQ;
						}
				   	}
			   	} else {
			   		$AbsentEQ = 0;
			   	}*/
			   	$AbsentEQ = 0;
			   	
			   	if ($exact_end_days == $num_of_days) {
					$max_day = 30;
				} else {
					switch ($exact_end_days) {
						case '30':
							$max_day = $num_of_days - 1;
							break;
						case '28':
							$max_day = $num_of_days + 2;
							break;
						case '29':
							$max_day = $num_of_days + 1;
							break;
						default:
							$max_day = $num_of_days;
							break;
					}
				}
				
				$curr_vl 			= 0;
				$curr_fl 			= 0;
				$curr_sl 			= 0;
				$curr_ot 			= 0;
				$curr_spl 			= 0;
				$Total_Days 		= $max_day - $TotalAbsentCount;
				$TotalDeduct 		= $TardyEQ + $UndertimeEQ + $AbsentEQ;
				
				$credit_detail    = FindLast("employeescreditbalance","WHERE EmployeesRefId = '$emprefid' AND NameCredits = 'VL' AND EffectivityYear = '".$NewYear."'","*");
			    if ($credit_detail) {
			        $year          	= $credit_detail["EffectivityYear"];
			        $balance_date  	= $credit_detail["BegBalAsOfDate"]; 
			        $from          	= date("m",strtotime($balance_date." + 1 Day"));
			        $to 			= $NewMonth;
			        $credit_arr    	= computeCredit($emprefid,$from,$to,$year);
			        $curr_vl        = $credit_arr["VL"];
			        $curr_sl        = $credit_arr["SL"];
			        $curr_spl 		= $credit_arr["SPL"];
			        $curr_fl 		= $credit_arr["FL"];
			        $curr_ot		= $credit_arr["OT"];
			    }
	          	$day_eq_vl 			= FindFirst("slvlearneddaily","WHERE NoOfDays =".$Total_Days,"VLEarned");
		    	$day_eq_sl 			= FindFirst("slvlearneddaily","WHERE NoOfDays =".$Total_Days,"SLEarned");
		    	$map["[[pvl]]"] 		= floatval($curr_vl);
				$map["[[psl]]"]			= floatval($curr_sl);
				$map["[[pfl]]"]			= floatval($curr_fl);
				$map["[[p]]"]			= floatval($curr_spl);
				$map["[[lcvl]]"] 		= $day_eq_vl;
				$map["[[lcsl]]"]		= $day_eq_sl;
				$map["[[lcspl]]"]   	= $SPL_count;
				$map["[[cocbal]]"]  	= HoursFormat($TotalCOCHour + $curr_ot);
				$map["[[totleave]]"] 	= $Leave_count;
				$map["[[totob]]"] 		= $OB_count;
				$map["[[totcto]]"] 		= $CTO_count;
				$map["[[totpresent]]"] 	= $PresentDays;
				$map["[[deducteq]]"] 	= $TotalDeduct;

				/*==============================================================================================================*/
				//DEFAULT MAPPING
				/*==============================================================================================================*/

			    $map["[[SmallLogo]]"]			= $CompanyLogo;
				$map["[[CompanyAddress]]"]		= $CompanyAddress;
				$map["[[EmployeesName]]"]		= $EmployeesName;
				$map["[[SuperVisorName]]"]		= $SuperVisorName;
				$map["[[Division]]"]			= $Division;
				$map["[[Position]]"]			= $Position;
				$map["[[Period]]"]				= $Period;
				$map["[[Office]]"]				= $Office;
				$map["[[WorkSchedule]]"]		= $WorkSchedule;
				$map["[[AgencyId]]"]			= $AgencyId;
				$map["[[ODU]]"] 				= $Department;
				if ($CompanyID == 28) {
					echo '
					<style type="text/css">
						'.$css_str.'.day_32 {
							background:#a6a6a6;
						}
					</style>
					';
				}
			} else {
				echo "No WorkSchedule Set<Br>";
			}
		}
    }






    
 

	/*==============================================================================================================*/
	//DEBUGGING
	/*==============================================================================================================*/

    $dbg = false;

	if ($dbg) {
		echo '
			<table border=1 style="width:100%;">
				<tr>
					<td>AttendanceDate</td>
					<td>AttendanceTime</td>
					<td>UTC</td>
					<td>TimeIn</td>
					<td>LunchOut</td>
					<td>LunchIn</td>
					<td>TimeOut</td>
					<td>OBOut</td>
					<td>OBIn</td>
					<td>Day</td>
					<td>Week</td>
					<td>KEntry</td>
					<td>Holiday</td>
					<td>Leave</td>
					<td>CTO</td>
					<td>OFFSUS</td>
					<td>HasOT</td>
					<td>OBNAME</td>
					<td>OBIN</td>
					<td>OBOUT</td>
					<td>OTTIME</td>
				</tr>
		';
		foreach ($arr as $value) {
			foreach ($value as $key => $new_val) {
				echo '
					<tr>
						<td>'.$new_val["AttendanceDate"].'</td>
						<td>'.$new_val["AttendanceTime"].'</td>
						<td>'.$new_val["UTC"].'</td>
						<td>'.$new_val["TimeIn"].'</td>
						<td>'.$new_val["LunchOut"].'</td>
						<td>'.$new_val["LunchIn"].'</td>
						<td>'.$new_val["TimeOut"].'</td>
						<td>'.$new_val["OBOut"].'</td>
						<td>'.$new_val["OBIn"].'</td>
						<td>'.$new_val["Day"].'</td>
						<td>'.$new_val["Week"].'</td>
						<td>'.$new_val["KEntry"].'</td>
						<td>'.$new_val["Holiday"].'</td>
						<td>'.$new_val["Leave"].'</td>
						<td>'.$new_val["CTO"].'</td>
						<td>'.$new_val["OffSus"].'</td>
						<td>'.$new_val["HasOT"].'</td>
						<td>'.$new_val["OBName"].'</td>
						<td>'.$new_val["OBTimeIn"].'</td>
						<td>'.$new_val["OBTimeOut"].'</td>
						<td>'.$new_val["OTTime"].'</td>
					</tr>
				';
				
			}
			
		}
		echo '</table>';

	}
?>