<div class="modal fade border0" id="DataEntry" role="dialog">
   <div class="modal-dialog border0">
      <div class="mypanel margin-top">
         <div class="panel-top" id="dialogTitle">&nbsp;</div>
         <div class="panel-mid">
            <div class="row">
               <div class="col-sm-12">
                  <?php
                     for ($i=0;$i<count($objs);$i++) {
                        echo
                        '<div>
                           <div class="form-group" id="FieldEntry">
                              <label class="control-label" for="'.$objs[$i].'">'.$label[$i].'</label><br>'."\n";
                              switch ($inputType[$i]) {
                                 case "Text";
                                 case "Number";
                                    echo '<input type="'.$inputType[$i].'" class="form-input saveFields-- '.$class[$i].'" value="'.$defvalue[$i].'" name="'.$objs[$i].'" id="'.$objs[$i].'">';
                                 break;
                                 case "Drop";
                                    $items = explode(",",$defvalue[$i]);
                                    echo
                                    '<select class="form-input saveFields-- '.$class[$i].'" name="'.$objs[$i].'" id="'.$objs[$i].'">';
                                    if (explode("|",$items[0])[0] == "LookUp") {
                                       $rs = SelectEach(strtolower(explode("|",$items[0])[1]),"ORDER BY Name");
                                       if ($rs) {
                                          while ($row = mysqli_fetch_assoc($rs)) {
                                             echo "\n".'<option value="'.$row["RefId"].'">'.$row["Name"].'</option>';
                                          }
                                       }
                                    } else {
                                       foreach ($items as $key) {
                                          $key_Arr = explode("|",$key);
                                          echo "\n".'<option value="'.$key_Arr[0].'">'.$key_Arr[1].'</option>';
                                       }
                                    }
                                    echo
                                    '</select>';
                                 break;
                              }
                           echo
                           '</div>
                        </div>'."\n";
                     }

                  ?>
               </div>
            </div>
         </div>
         <div class="panel-bottom">
            <div class="row">
               <div class="col-sm-4">
                  <button type="button" class="trnButton" style="width:70px;" name="bSave" id="bSave">Save</button>
                  <button type="button" class="cancelButton" style="width:70px;" name="bCancel" id="bCancel">Cancel</button>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>