<?php
   require_once "constant.e2e.php";
   require_once pathClass.'0620functions.e2e.php';
   $EmpRefId = getvalue("hEmpRefId");
   $PersonalInfo = FindFirst("employees","WHERE RefId = ".$EmpRefId,"*");
?>

<div class="row">
   <div class="col-xs-12" id="PInfo">
      <input type="checkbox" for="<?php echo $iFile; ?>" class="enabler--" id="chkPersonalInfo" name="chkPersonalInfo" refid="" fldName="">
      <label for="chkPersonalInfo" class="btn-cls2-sea">EDIT PERSONAL INFORMATION</label>
      <div class="mypanel">
         <div class="panel-top" for="midPERSONALINFORMATION"></div>
         <div class="panel-mid-litebg" id="midPERSONALINFORMATION">
            <div class="row">
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" for="inputs">LAST NAME</label>
                     <input class="form-input saveFields-- uCase-- mandatory--" type="text" id="" name="char_LastName" autofocus>
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" for="inputs">FIRST NAME</label>
                     <input class="form-input saveFields-- mandatory-- uCase--" type="text" id="" name="char_FirstName">
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" for="inputs">MIDDLE NAME</label>
                     <input class="form-input saveFields-- mandatory-- uCase--" type="text" id="" name="char_MiddleName">
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" for="inputs">EXT. NAME</label>
                     <input class="form-input saveFields-- uCase--" type="text" id="" name="char_ExtName">
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" for="inputs">DATE OF BIRTH</label><br>
                     <input class="form-input date-- saveFields-- valDate-- mandatory--" type="text" id="BirthDate" name="date_BirthDate">
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" for="inputs">PLACE OF BIRTH</label>
                     <input class="form-input saveFields-- mandatory-- uCase--" type="text" id="" name="char_BirthPlace">
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" for="inputs">SEX</label><br>
                     <input type="radio" class="form-input"
                        style="width:20px;"
                        name="char_SexOpt"
                        id="sexMale"/>
                     <label class="label" for="sexMale">MALE</label>
                     <input type="radio" class="form-input"
                        style="margin-left:30px;width:20px;"
                        name="char_SexOpt"
                        id="sexFemale"/>
                     <label class="label" for="sexFemale">FEMALE</label>
                     <input type="hidden" class="saveFields--" name="char_Sex">
                  </div>
               </div>
               <div class="col-xs-3">
                  <div>
                     <div class="form-group">
                        <input type="hidden" class="saveFields--" name="sint_isFilipino" value="1">
                        <label class="control-label" for="inputs">CITIZENSHIP</label><br>
                        <input type="radio" class="form-input" style="width:20px;" name="txtCitizen" id="isFilipino" checked/>
                        <label class="label" for="isFilipino">Filipino</label>
                        <input type="radio" class="form-input" style="margin-left:20px;width:20px;" name="txtCitizen" id="isDual"/>
                        <label class="label" for="isDual">Dual Citizenship</label>
                     </div>
                  </div>
                  <div class="mypanel" id="CountryCitizen">
                     <div class="panel-top">For Dual Citizenship</div>
                     <div class="panel-mid bgSilver">
                        <input type="hidden" class="saveFields--" name="sint_isByBirthOrNatural" value=0>
                        <div class="row">
                           <div class="col-xs-3 txt-right">
                              <input type="radio" class="form-input" style="width:20px;" name="radDual" id="isByBirth" />
                           </div>
                           <div class="col-xs-9">
                              <label class="label" for="isByBirth">By Birth</label>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-xs-3 txt-right">
                              <input type="radio" class="form-input" style="width:20px;" name="radDual" id="isNaturalization"/>
                           </div>
                           <div class="col-xs-9">
                              <label class="label" for="isNaturalization">By Naturalization</label>
                           </div>
                        </div>
                        <div class="row margin-top">
                           <div class="col-xs-12">
                              <?php
                                 createSelect("Country",
                                  "sint_CountryCitizenRefId",
                                  "",100,"Name","Select Country","");
                              ?>
                           </div>
                        </div>
                     </div>
                     <div class="panel-bottom"></div>
                  </div>
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" for="inputs">CIVIL STATUS</label><br>
                     <?php createSelectCivilStatus("char_CivilStatus","",""); ?>
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="row">
                     <div class="col-xs-6">
                        <div class="form-group">
                           <label class="control-label" for="inputs">HEIGHT (m)</label>
                           <input class="form-input number-- saveFields-- mandatory--" type="text" id="" name="deci_Height">
                        </div>
                     </div>
                     <div class="col-xs-6">
                        <div class="form-group">
                           <label class="control-label" for="inputs">WEIGHT (kg)</label>
                           <input class="form-input number-- saveFields-- mandatory--" type="text" id="" name="deci_Weight">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group" id="blood">
                     <label class="control-label" for="inputs">BLOOD TYPE</label><br>
                     <?php
                        createSelect("BloodType",
                                     "sint_BloodTypeRefId",
                                     "",100,"Name","Select Blood","");

                     ?>
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" for="inputs">AGENCY EMPLOYEE NO.</label><br>
                     <input class="form-input number-- saveFields-- text-center" type="text" id="" group="1" name="char_AgencyId">
                  </div>
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" for="inputs">GOVT. ISSUED ID</label><br>
                     <input class="form-input saveFields-- text-center uCase--" type="text" id="" name="char_GovtIssuedID">
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" for="inputs">GOVT. ISSUED ID NO.</label><br>
                     <input class="form-input saveFields-- text-center" type="text" id="" name="char_GovtIssuedIDNo">
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" for="inputs">GOVT. ISSUED ID PLACE</label><br>
                     <input class="form-input saveFields-- text-center" type="text" id="" name="char_GovtIssuedIDPlace">
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" for="inputs">DATE ISSUED</label><br>
                     <input class="form-input saveFields-- text-center date--" type="text" id="" name="char_GovtIssuedIDDate">
                  </div>
               </div>
            </div>
         </div>
         <div class="panel-bottom"></div>
      </div>

      <div class="mypanel">
         <div class="panel-top margin-top" for="midADDRESS">ADDRESS</div>
         <div class="panel-mid-litebg" id="midADDRESS">
            <h4 style="color:blue;font-weight:600;">Residential Address</h4>
            <?php bar(); ?>
            <div class="row">
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label">HOUSE NO.</label>
                     <input class="form-input saveFields-- uCase-- alphanum--" type="text" name="char_ResiHouseNo">
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label">STREET</label>
                     <input class="form-input saveFields-- uCase-- alphanum--" type="text" name="char_ResiStreet">
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label">BRGY.</label>
                     <input class="form-input saveFields-- uCase-- alphanum--" type="text" name="char_ResiBrgy">
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label">VILLAGE/SUBDIVISION</label>
                     <input class="form-input saveFields-- uCase-- alphanum--" type="text" name="char_ResiSubd">
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" for="inputs">COUNTRY</label><br>
                     <?php
                        createSelect("Country",
                                     "sint_ResiCountryRefId",
                                     209,100,"Name","Select Country","");
                     ?>
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" for="inputs">PROVINCE</label><br>
                     <?php
                        createSelect("Province",
                                     "sint_ResiAddProvinceRefId",
                                     0,100,"Name","Select Province","");
                     ?>
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" for="inputs">CITY</label><br>
                     <?php
                        if ($PersonalInfo) {
                           $sql = "WHERE ProvinceRefId = ".setRefIdValue($PersonalInfo["ResiAddProvinceRefId"]);
                           createSelect2("City",
                                     "sint_ResiAddCityRefId",
                                     setRefIdValue($PersonalInfo["ResiAddCityRefId"]),100,"Name","Select City","",$sql,"");
                        } else {
                           createSelect2("City",
                                     "sint_ResiAddCityRefId",
                                     0,100,"Name","Select City","","WHERE ProvinceRefId = -1","");
                        }

                        
                     ?>
                  </div>
               </div>

               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" for="inputs">ZIPCODE</label>
                     <input class="form-disp" type="text" id="char_ResiAddZipCode" name="char_ResiAddZipCode" disabled>
                  </div>
               </div>
            </div>

            <h4 style="color:blue;font-weight:600;">Permanent Address</h4>
            <div class="form-group">
               <input class="form-input" type="checkbox" name="chkSameAsAbove" style="width:30px;">
               <label class="control-label" for="sameAsAbove" style="color:red;">Same As Above</label>
            </div>

            <?php bar(); ?>
            <div class="row">
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label">HOUSE NO.</label>
                     <input class="form-input saveFields-- uCase-- alphanum--" type="text" name="char_PermanentHouseNo">
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label">STREET</label>
                     <input class="form-input saveFields-- alphanum-- uCase--" type="text" name="char_PermanentStreet">
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label">BRGY.</label>
                     <input class="form-input saveFields-- uCase-- alphanum--" type="text" name="char_PermanentBrgy">
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label">VILLAGE/SUBDIVISION</label>
                     <input class="form-input saveFields-- uCase-- alphanum--" type="text" name="char_PermanentSubd">
                  </div>
               </div>
            </div>

            <div class="row">
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" for="inputs">COUNTRY</label><br>
                     <?php
                        createSelect("Country",
                                     "sint_PermanentCountryRefId",
                                     209,100,"Name","Select Country","");
                     ?>
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" for="inputs">PROVINCE</label><br>
                     <?php
                        createSelect("Province",
                                     "sint_PermanentAddProvinceRefId",
                                     0,100,"Name","Select Province","");
                     ?>
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" for="inputs">CITY</label><br>
                     <?php
                        if ($PersonalInfo) {
                           $permaProvince = 0;
                           if ($PersonalInfo["PermanentAddProvinceRefId"] > 0) {
                              $permaProvince = $PersonalInfo["PermanentAddProvinceRefId"];
                           }
                           createSelect2("City","sint_PermanentAddCityRefId",$PersonalInfo["PermanentAddCityRefId"],100,"Name",
                                       "Select City","","WHERE ProvinceRefId = ".$permaProvince,false);
                        } else {
                           createSelect2("City",
                                       "sint_PermanentAddCityRefId",
                                       "",
                                       100,
                                       "Name",
                                       "Select City",
                                       "",
                                       "WHERE ProvinceRefId = -1",
                                       false);
                        }
                     ?>
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" for="inputs">ZIPCODE</label>
                     <input class="form-disp" type="text" id="char_PermanentAddZipCode" name="char_PermanentAddZipCode" disabled>
                  </div>
               </div>
            </div>
         </div>
         <div class="panel-bottom"></div>
      </div>

      <div class="mypanel">
         <div class="panel-top margin-top" for="midContact">CONTACT</div>
         <div class="panel-mid-litebg" id="midContact">
            <div class="row">
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" for="inputs">TELEPHONE NO.</label><br>
                     <input class="form-input number-- saveFields--" type="text" id="" name="char_TelNo" maxlength="11">
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" for="inputs">MOBILE NO</label>
                     <input class="form-input number-- saveFields--" type="text" id="" name="char_MobileNo" maxlength="11">
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" for="inputs">E-MAIL ADDRESS (if any)</label>
                     <input class="form-input saveFields-- mandatory--" type="email" id="" name="char_EmailAdd">
                  </div>
               </div>
            </div>
         </div>
         <div class="panel-bottom"></div>
      </div>

      <div class="mypanel">
         <div class="panel-top margin-top" for="midID">ID</div>
         <div class="panel-mid-litebg" id="midID">
            <div class="row">
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" for="inputs">GSIS ID NO.</label><br>
                     <input class="form-input number-- saveFields--" type="text" id="" name="char_GSIS">
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" for="inputs">PAGIBIG ID NO.</label>
                     <input class="form-input number-- saveFields--" type="text" id="" name="char_PAGIBIG">
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" for="inputs">PHILHEALTH NO.</label>
                     <input class="form-input number-- saveFields--" type="text" id="" name="char_PHIC">
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" for="inputs">TIN NO.</label><br>
                     <input class="form-input number-- saveFields--" type="text" id="" name="char_TIN">
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" for="inputs">SSS NO.</label>
                     <input class="form-input number-- saveFields--" type="text" id="" name="char_SSS">
                  </div>
               </div>
            </div>
         </div>
         <div class="panel-bottom"></div>
      </div>
   </div>
</div>
<script type="text/javascript">
   $("#PInfo .newDataLibrary").hide();
   $("#PInfo .createSelect--").css("width","100%");
   $("#PInfo .saveFields--").attr("tabname","Personal");
   if ("#hCompanyID" == 2) {
      $("[name='char_CivilStatus'], [name='sint_BloodTypeRefId'], [name='sint_ResiCountryRefId'], [name='sint_ResiAddProvinceRefId']").addClass("mandatory--");   
   }
</script>