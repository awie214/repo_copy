<?php
	require_once 'constant.e2e.php';
   require_once pathClass.'0620functions.e2e.php';
   require_once pathClass.'0620RptFunctions.e2e.php';
   require_once pathClass.'DTRFunction.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $whereClause .= " ORDER BY LastName";
   $table = "employees";
   $rsEmployees_week1 = SelectEach($table,$whereClause);
   $rsEmployees_week2 = SelectEach($table,$whereClause);
   $rsEmployees_week3 = SelectEach($table,$whereClause);
   $rsEmployees_week4 = SelectEach($table,$whereClause);

   $month 	= getvalue("txtAttendanceMonth");
   $year    = getvalue("txtAttendanceYear");
   $period  = monthName($month,1)." 01-".cal_days_in_month(CAL_GREGORIAN,$month,$year).", ".$year;

   $cos = FindFirst("empstatus","WHERE Code = 'COS'","RefId");
   function chkDay($day,$month,$year) {
   	$date = $year."-".$month."-".$day;
   	$curr_day = date("D",strtotime($date));
   	if ($curr_day == "Sat" || $curr_day == "Sun") {
   		return $curr_day;
   	} else {
   		return $day;
   	}
   }
   function rmvValue($value){
   	if ($value > 0) {
   		return $value;
   	} else {
   		return "&nbsp;";
   	}
   }
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<?php include_once $files["inc"]["pageHEAD"]; ?>
   <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
   <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   <style type="text/css">
   	th, td {
   		font-size: 7pt;
   	}
   	body {
   		background:#ffffff;
		   color:#000;
		   font-size:10pt;
		   font-family:Arial;
		   margin: 0;
   	}
   	@media print {
   		td {
   			font-size: 6pt;
   		}
   	}
   </style>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12">
				<div class="row">
					<div class="col-xs-12 text-center">
						METROPOLITAN WATERWORKS AND SEWERAGE SYSTEM REGULATORY OFFICE
						<br>
						Katipunan Road, Balara, Quezon City
						<br>
						<b>SUMMARY TIME REPORT</b>
						<br>
						For the period <b><?php echo $period; ?></b>
					</div>
				</div>
				<?php
					include_once 'inc/STR_week1.php';
					//echo '<div class="noPrint">'.spacer(100).'</div>';
					include_once 'inc/STR_week2.php';
					//echo '<div class="noPrint">'.spacer(100).'</div>';
					include_once 'inc/STR_week3.php';
					//echo '<div class="noPrint">'.spacer(100).'</div>';
					include_once 'inc/STR_week4.php';
				?>
			</div>
		</div>
	</div>
</body>
</html>