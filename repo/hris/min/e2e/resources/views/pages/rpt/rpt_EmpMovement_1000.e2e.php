<?php
   $emprefid      = getvalue("ServiceRecordEmpRefId");
   $whereClause   = "WHERE EmployeesRefId = $emprefid ORDER BY EffectivityDate";
   $rs            = SelectEach("employeesmovement",$whereClause);
   $record_count  = mysqli_num_rows($rs);
   $row_emp       = FindFirst("employees","WHERE RefId = '$emprefid'","*");
   $arr           = array();
   $count         = "1";
   $page_count    = 0;
   if ($rs) {
      while ($row = mysqli_fetch_assoc($rs)) {
         $count++;
         $EffectivityDate  = date("m/d/Y",strtotime($row["EffectivityDate"]));
         $ExpiryDate       = strval($row["ExpiryDate"]);
         $PositionRefId    = getRecord("position",$row["PositionRefId"],"Name");
         $EmpStatusRefId   = getRecord("empstatus",$row["EmpStatusRefId"],"Name");
         $SalaryAmount     = $row["SalaryAmount"];
         $AgencyRefId      = getRecord("agency",$row["AgencyRefId"],"Name");
         $LWOP             = intval($row["LWOP"]);
         $Remarks          = strval($row["Remarks"]);
         $Cause            = strval($row["Cause"]);
         if ($LWOP == 0) {
            $LWOP = "";
         }
         if ($Remarks == "") {
            $Remarks = $Cause;
         }
         if ($ExpiryDate != "") {
            $ExpiryDate  = date("m/d/Y",strtotime($row["ExpiryDate"]));
         } else {
            $ExpiryDate  = "Present";
         }
         $arr[] = [
            "EffectivityDate"=>$EffectivityDate,
            "ExpiryDate"=>strval($ExpiryDate),
            "PositionRefId"=>$PositionRefId,
            "EmpStatusRefId"=>$EmpStatusRefId,
            "SalaryAmount"=>number_format(($SalaryAmount * 12),2),
            "AgencyRefId"=>$AgencyRefId,
            "LWOP"=>$LWOP,
            "Remarks"=>strval($Remarks)
         ];
      }
   }
   if ($record_count <= 12) {
      $page_count = 1;
   } else {
      for ($i=1; $i <= $record_count; $i++) { 
         $trigger = $i % 12;
         if ($trigger == 0) $page_count++;
      }
      $check = $page_count * 12;
      if ($record_count > $check) $page_count++;
   }
   $MiddleInitial = substr($row_emp["MiddleName"], 0,1);
   $FullName      = $row_emp["LastName"].", ".$row_emp["FirstName"]." ".$MiddleInitial;
   if ($row_emp["Sex"] == "F") {
      if ($row_emp["CivilStatus"] == "Ma") {
         $MaidenName = $row_emp["MiddleName"];
      } else {
         $MaidenName = "";
      }
   } else {
      $MaidenName = "";
   }
   // echo "<pre>";
   // echo json_encode($arr,JSON_PRETTY_PRINT);
   // echo "</pre>";


?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
      <style type="text/css">
         @media print {
            body {
               margin-top: 0;
               font-size: 8pt;
            }
            thead {
               font-size: 8pt;  
               font-weight: 400;
            }
            tbody {
               font-size: 8pt !important;
            }
            .header_table {
               font-size: 8pt;    
            }
         }
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <div class="row">
            <div class="col-xs-12">
               <div class="row">
                  <div class="col-xs-12 nextpage">
                  <?php
                     for ($i=1; $i <= $page_count ; $i++) { 
                  ?>
                  <div style="page-break-after: always !important;">
                     <div class="row">
                        <div class="col-xs-12 text-center">
                           <div class="row">
                              <div class="col-xs-3 txt-left">
                                 &nbsp;
                              </div>
                              <div class="col-xs-6 txt-center">
                                 <b>Republic of the Philippines</b>
                                 <br>
                                 <b>PHILIPPINE INSTITUTE FOR DEVELOPMENT STUDIES</b>
                                 <br>
                                 NEDA Sa Makati Building<br>
                                 106 Amorsolo St., Legaspi Village, Makati City
                              </div>
                              <div class="col-xs-3 text-right">
                                 Page <?php echo $i; ?> of <?php echo $page_count; ?>
                              </div>
                           </div>
                           <div class="txt-center">
                              <div class="fontB10 txt-center">S E R V I C E  R E C O R D</div>
                           </div>
                           <br>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-5" style="border-bottom: 1px solid black;">
                           <?php echo rptDefaultValue($FullName); ?>
                        </div>
                        <div class="col-xs-1"></div>
                        <div class="col-xs-6">
                           If married woman, give also full maiden name
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-5">
                           Surname, Given Name, Middle Name
                        </div>
                        <div class="col-xs-1">
                        </div>
                        <div class="col-xs-6" style="border-bottom: 1px solid black;">
                           <?php echo $MaidenName; ?>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-1">BIRTH:</div>
                        <div class="col-xs-2 text-center">
                           <u>
                              <?php echo rptDefaultValue(date("d M Y",strtotime($row_emp["BirthDate"]))); ?>
                           </u>
                           <br>
                           Date
                        </div>
                        <div class="col-xs-3 text-center">
                           <u>
                              <?php echo rptDefaultValue($row_emp["BirthPlace"]); ?>
                           </u>
                           <br>
                           Place
                        </div>
                        <div class="col-xs-6">
                           (Date herein should be checked from birth or baptismal certificate or some other reliable documents)
                        </div>
                     </div>
                     <br>
                     <div class="row">
                        <div class="col-xs-12">
                           <p>
                              This is to certify that the employee named herein above actually rendered services in this Office as shown by the service record below, each line of which is supported by appointment and other papers actually issued by this Office and approved by the authorities otherwise indicated.
                           </p>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-12">
                           <table style="width: 100%;">
                              <thead>
                                 <tr class="colHEADER">
                                    <th colspan="2">SERVICES<br>(Inclusive Dates)</th>
                                    <th colspan="3">RECORD OF APPOINTMENT</th>
                                    <th rowspan="2" style="width: 15%;">Station/Place<br>of Assignment</th>
                                    <th rowspan="2" style="width: 10%;">Leave of<br>Absence<br>Without Pay</th>
                                    <th rowspan="2" style="width: 15%;">Remarks<br>Separation<br>(Reference)</th>
                                 </tr>
                                 <tr class="colHEADER">
                                    <th style="width: 10%;">FROM</th>
                                    <th style="width: 10%;">TO</th>
                                    <th style="width: 20%;">DESIGNATION</th>
                                    <th style="width: 10%;">STATUS</th>
                                    <th style="width: 10%;">RATE/YEAR</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <?php
                                    $idx        = ($i * 13) - 12 - $i;
                                    $counter    = ($i * 12) - 1;
                                    $checking   = 0;
                                    for ($a=$idx; $a <= $counter; $a++) { 
                                       if (isset($arr[$a])) {
                                 ?>   
                                 <tr>
                                    <td class="text-center" valign="top" style="height: 70px;">
                                       <?php 
                                          echo $arr[$a]["EffectivityDate"];
                                       ?>
                                    </td>
                                    <td class="text-center" valign="top">
                                       <?php 
                                          echo $arr[$a]["ExpiryDate"];
                                       ?>
                                    </td>
                                    <td valign="top">
                                       <?php 
                                          echo $arr[$a]["PositionRefId"];
                                       ?>
                                    </td>
                                    <td class="text-center" valign="top">
                                       <?php 
                                          echo $arr[$a]["EmpStatusRefId"];
                                       ?>
                                    </td>
                                    <td class="text-right" valign="top">
                                       <?php
                                          echo $arr[$a]["SalaryAmount"];
                                       ?>
                                    </td>
                                    <td valign="top">
                                       <?php 
                                          echo $arr[$a]["AgencyRefId"];
                                       ?>
                                    </td>
                                    <td class="text-center" valign="top">
                                       <?php
                                          echo $arr[$a]["LWOP"];
                                       ?>
                                    </td>
                                    <td valign="top">
                                       <?php 
                                          echo $arr[$a]["Remarks"];
                                       ?>
                                    </td>
                                 </tr>
                                 <?php
                                       }
                                       if ($a == $record_count) {
                                          $checking = $record_count;
                                          echo '
                                             <tr>
                                                <td class="text-center" colspan="8"><b>* * *NOTHING FOLLOWS* * *</b></td>
                                             </tr>
                                          ';
                                       }
                                    }
                                 ?>
                              </tbody>
                           </table>
                        </div>
                     </div>
                     <?php
                        if ($checking == $record_count) {

                     ?>
                     <div class="row margin-top">
                        <div class="col-xs-12">
                           <br>
                           Issued in compliance with Executive Order No. 54 dated August 10, 1954, and in accordance with Circular No. 58, dated August 10,1954 of the System.
                        </div>
                     </div>
                     <br><br>
                     <div class="row margin-top">
                        <div class="col-xs-6">
                           DATE: <b><?php echo date("m/d/Y",time()); ?></b>
                        </div>
                        <div class="col-xs-3">
                           CERTIFIED CORRECT:
                        </div>
                        <div class="col-xs-3 text-center">
                           <u>
                              ANDREA S AGCAOILI
                           </u>
                           <br>
                           DEPARTMENT MANAGER III
                        </div>
                     </div>   
                  </div>
                  
                  <?php
                        }
                     }
                  ?>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>