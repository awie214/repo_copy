<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include_once 'incRptQryString.e2e.php';
   $rsEmployees = SelectEach("employees",$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) { echo "DBG >> ".$whereClause; }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>

      <?php
         while ($row = mysqli_fetch_assoc($rsEmployees)) {
            $EmployeesRefId = $row["RefId"];
            $CompanyRefId   = $row["CompanyRefId"];
            $BranchRefId    = $row["BranchRefId"];
            $where  = "WHERE CompanyRefId = $CompanyRefId";
            $where .= " AND BranchRefId = $BranchRefId";
            $where .= " AND EmployeesRefId = $EmployeesRefId";
               $empinfo_row = FindFirst("empinformation",$where,"*");
               if ($empinfo_row) {
                  $Position      = getRecord("position",$empinfo_row["PositionRefId"],"Name");
                  $Division      = getRecord("Division",$empinfo_row["DivisionRefId"],"Name");
               } else {
                  $Position = "";
                  $Division = "";
               }

      ?>

      <div class="container-fluid rptBody">
         <table>
            <thead>
               <tr>
                  <th colspan="8" align="center" style="text-align:center;">
                     <?php
                        rptHeader(getvalue("RptName"));
                     ?>
                     <?php spacer(30); ?>
                  </th>
               </tr>  
            </thead>
            <tbody>
               <tr>
                  <td>Date: <?php echo date("d F Y",time()); ?></td>
                  <td>Time of Departure:</td>
                  <td>Expected Time of Return:</td>
               </tr>
               <tr>
                  <td align="center">Printed Name and Signature of Official/Employee:<br><br>_________________________________<br>
                  <b><?php echo $row["LastName"].", ".$row["FirstName"]." ".$row["MiddleName"]; ?></b>
                  </td>
                  <td valign="top">Position Title:<br>
                     <?php echo $Position;?>
                  </td>
                  <td valign="top">Office/Division:<br>
                     <?php echo $Division;?>
                  </td>
               </tr>
               <tr>
                  <td colspan="2">Destination and Purpose of Official Business</td>
                  <td>Recommending Approval:</td>
               </tr>
               <tr>
                  <td colspan="2">
                     <div class="row">
                        <div class="col-xs-12">
                           Destination:   
                           <?php spacer(40); ?>
                        </div>
                     </div>
                  </td>
                  <td></td>
               </tr>
               <tr>
                  <td colspan="2">Prupose:</td>
                  <td class="text-center">Immediate Supervisor</td>
               </tr>
               <tr>
                  <td colspan="2" rowspan="2">
                     <?php spacer(40); ?>
                  </td>
                  <td>
                     Approved:
                     <?php spacer(40); ?>
                  </td>
               </tr>
               <tr>
                  <td class="text-center">Next Higher Official</td>
               </tr>
               <tr>
                  <td colspan="2">
                     <div class="row">
                        <div class="col-xs-12">
                           <div class="row">
                              <div class="col-xs-12">
                                 Recorded in Security Logbook:
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-12">
                                 Time Left Office: __________________________________
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-12">
                                 Time Returned: __________________________________
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-xs-12 text-center">
                                 <?php spacer(20); ?>          
                                 Security Officer
                              </div>
                           </div>
                        </div>
                     </div>
                  </td>
                  <td valign="top">
                     <div class="row">
                        <div class="col-xs-12">
                           Recorded by HRDD:
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-12">
                           ____ Leave Card
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-12">
                           ____ Others:
                        </div>
                     </div>
                  </td>
               </tr>
            </tbody>
         </table>
      </div>
      <?php
         }
      ?>
   </body>
</html>
