<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
?>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
   </head>
   <body>
      <div class="container-fluid rptBody">
         <div class="row">
            <div class="col-xs-12">
               <div class="row">
                  <div class="col-xs-12">
                     <?php
                        rptHeader(getvalue("RptName"));
                     ?>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <span>Agency Name: &nbsp;</span>
                     <br>
                     <span>Agency BP Number: &nbsp;</span>
                     <br>
                     <span>
                        FORM C. List of Employees with salary adjustments for confirmation as to correct amount of monthly salary and effectivity date to be supplied below.
                     </span>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <table style="width: 100%;">
                        <thead>
                           <tr class="colHEADER">
                              <th>Member BP Number</th>
                              <th>Employee Name</th>
                              <th>Date of Transfer</th>
                              <th>Salary</th>
                              <th>Position</th>
                              <th>Employment Status</th>
                              <th>FROM</th>
                              <th>TO</th>
                           </tr>   
                        </thead>
                        <tbody>
                           <?php
                              while ($row_emp = mysqli_fetch_assoc($rsEmployees)) {
                                 $FullName   = $row_emp["LastName"].", ".$row_emp["FirstName"]." ".$row_emp["MiddleName"];
                                 $emp_info = FindFirst("empinformation","WHERE EmployeesRefId = ".$row_emp["RefId"],"*");
                                 if ($emp_info) {
                                    $Position = rptDefaultValue($emp_info["PositionRefId"],"position");
                                    $ApptStatus = rptDefaultValue($emp_info["ApptStatusRefId"],"apptstatus");
                                    $SalaryAmount = number_format($emp_info["SalaryAmount"],2);
                                 } else {
                                    $Position = "";
                                    $ApptStatus = "";
                                    $SalaryAmount = "0.00";
                                 }
                                 echo '<tr>';
                                    echo '
                                       <td>&nbsp;</td>
                                       <td>'.$FullName.'</td>
                                       <td>&nbsp;</td>
                                       <td class="text-right">P '.$SalaryAmount.'</td>
                                       <td>'.$Position.'</td>
                                       <td>'.$ApptStatus.'</td>
                                       <td>&nbsp;</td>
                                       <td>&nbsp;</td>
                                    ';
                                 echo '</tr>';
                              }
                           ?>
                        </tbody>
                     </table>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <span>Issue No. 01, Rev No. 0, (16 August 2016),FM-GSIS-OPS-UMR-03</span>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>