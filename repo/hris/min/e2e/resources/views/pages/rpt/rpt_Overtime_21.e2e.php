<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $refid      = getvalue("refid");
   $row        = FindFirst("overtime_request","WHERE RefId = '$refid'","*");
   $FiledDate  = date("F d, Y",strtotime($row["FiledDate"]));
   $emprefid   = $row["EmployeesRefId"];
   $StartDate  = $row["StartDate"];
   $EndDate    = $row["EndDate"];
   $emp_row    = FindFirst("employees","WHERE RefId = '$emprefid'","`FirstName`,`LastName`,`MiddleName`,`ExtName`");
   if ($emp_row) {
      $LastName = $emp_row["LastName"];
      $FirstName = $emp_row["FirstName"];
      $MiddleName = $emp_row["MiddleName"];
      $ExtName = $emp_row["ExtName"];
      $FullName = $LastName.", ".$FirstName." $ExtName ".$MiddleName;
   } else {
      $FullName = "";
   }

   if($StartDate == $EndDate) {
      $date = date("F d, Y",strtotime($StartDate));
   } else {
      $date = date("d",strtotime($StartDate))." - ".date("d F Y",strtotime($EndDate));
   }

   $empinfo_row = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","*");
   if ($empinfo_row) {
      $Office = getRecord("office",$empinfo_row["OfficeRefId"],"Name");
      $Division = getRecord("Division",$empinfo_row["DivisionRefId"],"Name");
      $Position = getRecord("position",$empinfo_row["PositionRefId"],"Name");
   } else {
      $Office = "";
      $Position = "";
      $Division = "";
   }
   $stat = $row["WithPay"];
   if (intval($stat) > 0) {
      $stat = "OT Pay";
   } else {
      $stat = "CTO";
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <div class="row" style="page-break-after: always;">
            <div class="col-xs-12">
               <div class="row">
                  <div class="col-xs-12">
                     <?php
                        rptHeader("AUTHORITY TO RENDER OVERTIME (OT) SERVICES");
                     ?>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-8">
                     <label>Division:&nbsp;<u><?php echo $Division; ?></u> </label>
                  </div>
                  <div class="col-xs-4">
                     <label>Date of Filing:&nbsp;<u><?php echo $FiledDate; ?></u></label>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-6 text-center">
                     <label>EMPLOYEE/S TO RENDER OT SERVICES</label>
                  </div>
                  <div class="col-xs-3 text-center">
                     <label>OT PERIOD</label>
                  </div>
                  <div class="col-xs-3 text-center">
                     <label>TIME</label>
                  </div>
               </div>
               <div class="row">
                  <div class="col-xs-6 text-center">
                     <u><?php echo $FullName; ?></u>
                  </div>
                  <div class="col-xs-3 text-center">
                     <u><?php echo $date; ?></u>
                  </div>
                  <div class="col-xs-3 text-center">
                     ____________________
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-6 text-center">
                     _____________________________
                  </div>
                  <div class="col-xs-3 text-center">
                     ____________________
                  </div>
                  <div class="col-xs-3 text-center">
                     ____________________
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-6 text-center">
                     _____________________________
                  </div>
                  <div class="col-xs-3 text-center">
                     ____________________
                  </div>
                  <div class="col-xs-3 text-center">
                     ____________________
                  </div>
               </div>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <label>PURPOSE/EXPECTED OUTPUT:</label>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12" style="padding: 5px;">
                     <u>
                        <?php echo convertBR($row["Justification"]); ?>
                     </u>
                     <!-- <div class="row">
                        <div class="col-xs-12" style="border-bottom: 1px solid black;">&nbsp;</div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-12" style="border-bottom: 1px solid black;">&nbsp;</div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-12" style="border-bottom: 1px solid black;">&nbsp;</div>
                     </div> -->
                  </div>
               </div>
               <br><br>
               <div class="row margin-top">
                  <div class="col-xs-6">
                     <label>ENTITLED REMUNERATION:</label>
                  </div>
                  <div class="col-xs-2">
                     <input type="checkbox" name="" <?php if ($stat == "CTO") echo "checked";?> disabled>&nbsp;COC
                  </div>
                  <div class="col-xs-2">
                     <input type="checkbox" name="" <?php if ($stat == "OT Pay") echo "checked";?> disabled>&nbsp;OT Pay
                  </div>
                  <div class="col-xs-2">
                     <input type="checkbox" name="" disabled>&nbsp;None 
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-6">
                     <i><b>Note:</b>  Only personnel not receiving honoraria due to assignment to any government special projects are entitled to CTO/OT Pay.</i>
                  </div>
               </div>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-4 text-center">
                     <label>Requested by:</label>
                  </div>
                  <div class="col-xs-4 text-center">
                     <label>Recommending Approval</label>
                  </div>
                  <div class="col-xs-4 text-center">
                     <label>Approved by:</label>
                  </div>
               </div>  
               <div class="row margin-top">
                  <div class="col-xs-4 text-center">
                     _____________________________
                  </div>
                  <div class="col-xs-4 text-center">
                     ____________________
                  </div>
                  <div class="col-xs-4 text-center">
                     ____________________
                  </div>
               </div> 
               <br><br><br><br><br><br>
            </div>
         </div>
         <div class="row" style="page-break-after: always;" id="Content">
            <div class="col-xs-12">
               <div class="row">
                  <div class="col-xs-12">
                     <?php
                        rptHeader("CERTIFICATE OF OVERTIME SERVICE RENDERED");
                     ?>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12" style="text-align: justify;">
                     This is to certify that I have rendered the OT services specified in my approved Authority to Render Overtime Services dated __________________ .
                  </div>
               </div>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-12 text-center">
                     <b>DETAILS OF OT SERVICES RENDERED</b>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-1"></div>
                  <div class="col-xs-11">
                     OT PERIOD <u><?php echo $date; ?></u>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-1"></div>
                  <div class="col-xs-11">
                     ACCOMPLISHMENTS:
                     <br>
                     <u>
                        <?php echo convertBR($row["Justification"]); ?>
                     </u>
                     <!-- <br>
                     __________________________________________________________________
                     <br>
                     __________________________________________________________________
                     <br>
                     __________________________________________________________________ -->
                  </div>
               </div>
               <br><br><br><br>
               <div class="row margin-top">
                  <div class="col-xs-6">
                     Prepared by:
                  </div>
                  <div class="col-xs-6">
                     Certified Correct by:
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-6">
                     ________________________________
                  </div>
                  <div class="col-xs-6">
                     ________________________________
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-6">
                     Print Name and Signature
                  </div>
                  <div class="col-xs-6">
                     Division Head
                  </div>
               </div>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     Attachment:<i><b>Approved Authority to Render Overtime Services</b></i>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>