<?php
   //session_start();
   //include 'colors.e2e.php';
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   
   include "incRptSortBy.e2e.php";
   $rs = SelectEach($table,$whereClause);
   if ($rs) $rowcount = mysqli_num_rows($rs);

   if ($dbg) {
      echo $whereClause;
   }
?>
<html>
   <head>
      <?php include "pageHEAD.e2e.php"; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
   </head>
   <body>
      <div class="container-fluid rptBody">
         <div class="row">
            <label>Agency Name:&nbsp;<?php //echo $row['']; ?></label>
         </div>
         <div class="row margin-top">
            <label>Agency BP Number:&nbsp;<?php //echo $row['']; ?></label>
         </div>
         <div class="row margin-top">
            <label>FORM D. List of Employees with no premium remittance for 2 concecutive months.</label>
         </div>
         <?php
            $errmsg = "";
            rptHeader("FOR AGENCY REMITTANCE ADVICE");
            if ($rs && $errmsg == "")
            {
         ?>
         <table border="1" width="80%">
            <tr>
               <td nowrap class="center--">Ref Id</td>
               <td nowrap class="center--">Member BP Number</td>
               <td nowrap class="center--">Employee Name</td>
               <td nowrap class="center--">Reason<sup>1</sup></td>
               <td nowrap class="center--">Effectivity Date</td>
               <td nowrap class="center--">Remarks<sup>2</sup></td>
            </tr>
            <?php
               while ($row = mysqli_fetch_assoc($rs)) {
            ?>
               <tr>
                  <td nowrap class="center--"><?php echo $row['RefId'];?></td>
                  <td nowrap class="center--">BP NUMBER</td>
                  <td nowrap style="padding-left:15px;"><?php echo $row['LastName'].",".$row['FirstName']." ".$row['MiddleName']." ".$row['ExtName']; ?></td>
                  <td nowrap class="center--">Reason</td>
                  <td nowrap class="center--">Effectivity Date</td>
                  <td nowrap class="center--">Remarks</td>
               </tr>   
            <?php
               }
               echo "RECORD COUNT : ".mysqli_num_rows($rs);
            }else {
               echo '<div>NO RECORD QUERIED base on your criteria!!!</div>';
               echo '<div>'.$errmsg.'</div>';
            }
            ?>
         </table>
         <p class="MsoNormal margin-top" style='margin-bottom:0cm;margin-bottom:.0001pt;
         text-align:center;line-height:normal'><span style='font-size:10.0pt;
         font-family:"Arial",sans-serif'>Issue No. 01, Rev No. 0, (16 August 2016),
         FM-GSIS-OPS-UMR-01</span></p>
      </div>
      <?php rptFooter();?>
   </body>
</html>