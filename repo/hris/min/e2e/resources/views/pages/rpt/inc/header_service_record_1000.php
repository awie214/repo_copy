               <thead>
                  <tr>
                     <td colspan="8" class="header_table">
                        <div class="row">
                           <div class="col-xs-12 text-center">
                              <div class="row">
                                 <div class="col-xs-3 txt-left">
                                 </div>
                                 <div class="col-xs-6 txt-center">
                                    <b>Republic of the Philippines</b>
                                    <br>
                                    <b>PHILIPPINE INSTITUTE FOR DEVELOPMENT STUDIES</b>
                                    <br>
                                    NEDA Sa Makati Building<br>
                                    106 Amorsolo St., Legaspi Village, Makati City
                                 </div>
                                 <div class="col-xs-3 text-right">
                                    Page __ of <?php echo $page_count; ?>
                                 </div>
                              </div>
                              <div class="txt-center">
                                 <div class="fontB10 txt-center">S E R V I C E  R E C O R D</div>
                              </div>
                              <br>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-xs-5" style="border-bottom: 1px solid black;">
                              <?php echo rptDefaultValue($FullName); ?>
                           </div>
                           <div class="col-xs-1"></div>
                           <div class="col-xs-6">
                              If married woman, give also full maiden name
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-xs-5">
                              Surname, Given Name, Middle Name
                           </div>
                           <div class="col-xs-1">
                           </div>
                           <div class="col-xs-6" style="border-bottom: 1px solid black;">
                              <?php echo $MaidenName; ?>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-xs-1">BIRTH:</div>
                           <div class="col-xs-2 text-center">
                              <u>
                                 <?php echo rptDefaultValue(date("d M Y",strtotime($row_emp["BirthDate"]))); ?>
                              </u>
                              <br>
                              Date
                           </div>
                           <div class="col-xs-3 text-center">
                              <u>
                                 <?php echo rptDefaultValue($row_emp["BirthPlace"]); ?>
                              </u>
                              <br>
                              Place
                           </div>
                           <div class="col-xs-6">
                              (Date herein should be checked from birth or baptismal certificate or some other reliable documents)
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-xs-12">
                              <p>
                                 This is to certify that the employee named herein above actually rendered services in this Office as shown by the service record below, each line of which is supported by appointment and other papers actually issued by this Office and approved by the authorities otherwise indicated.
                              </p>
                           </div>
                        </div>
                     </td>
                  </tr>
                  <tr class="colHEADER">
                     <th colspan="2">SERVICES<br>(Inclusive Dates)</th>
                     <th colspan="3">RECORD OF APPOINTMENT</th>
                     <th rowspan="2" style="width: 15%;">Station/Place<br>of Assignment</th>
                     <th rowspan="2" style="width: 10%;">Leave of<br>Absence<br>Without Pay</th>
                     <th rowspan="2" style="width: 15%;">Remarks<br>Separation<br>(Reference)</th>
                  </tr>
                  <tr class="colHEADER">
                     <th style="width: 10%;">FROM</th>
                     <th style="width: 10%;">TO</th>
                     <th style="width: 20%;">DESIGNATION</th>
                     <th style="width: 10%;">STATUS</th>
                     <th style="width: 10%;">RATE/YEAR</th>
                  </tr>
               </thead>