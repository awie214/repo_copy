<?php
	$where = " ORDER BY HiredDate";
   if (isset($_GET["loyalty_years"])) {
      $loyalty_years = $_GET["loyalty_years"];
   } else {
      $loyalty_years = "";
   }
?>
<!DOCTYPE html>
<html>
   	<head>
      	<?php include_once $files["inc"]["pageHEAD"]; ?>
      	<link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      	<style>
         	td {vertical-align:top;}
      	</style>
   	</head>
   	<body>
      	<div class="container-fluid rptBody">
      		<div class="row">
      			<div class="col-xs-12">
      				<?php
			            rptHeader("EMPLOYEES LOYALTY","HRDMS-R-020");
			         ?>
      				<table style="width: 100%" border="1">
      					<thead>
      						<tr class="colHEADER">
      							<th>#</th>
                           <th>Fullname<sup>1</sup></th>
                           <th>Sex</th>
	      						<th>Employee No.</th>
                           <th>Present Position Title</th>
	      						<th>Assumption Date of<br>Continous Gov't<br>Service</th>
                           <th>No. of Years</th>
      						</tr>
      					</thead>
      					<tbody>
      						<?php
      							$count = 0;
	                          $newly_hired_rs = SelectEach("empinformation",$where);
	                           if ($newly_hired_rs) {
	                              while ($newly_hired_row = mysqli_fetch_assoc($newly_hired_rs)) {
                                    $emprefid      = $newly_hired_row["EmployeesRefId"];
                                    $hired_date    = $newly_hired_row["HiredDate"];
                                    $Salary        = $newly_hired_row["SalaryAmount"];
                                    $Salary        = intval($Salary);
                                    $years         = computeAge($hired_date);
                                    $Position      = getRecord("position",$newly_hired_row["PositionRefId"],"Name");
                                    if ($years > 0) {
                                       if ($years > 1) {
                                          $years = $years." Years"; 
                                       } else {
                                          $years = $years." Year"; 
                                       }
                                    } else {
                                       $years = "";
                                    }
                                    if ($Salary > 0) {
                                       $Salary = "P ".number_format(($Salary * 12),2);
                                    } else {
                                       $Salary = "";
                                    }
                                    $fld           = "`LastName`,`FirstName`,`MiddleName`,`AgencyId`, `Sex`";
                                    $row_emp = FindFirst("employees","WHERE RefId = '$emprefid'",$fld);
                                    if ($loyalty_years != "") {
                                       if (intval($loyalty_years) == computeAge($hired_date)) {
                                          
                                          if ($row_emp) {
                                             $count++;
                                             $AgencyId = $row_emp["AgencyId"];
                                             $FullName = $row_emp["LastName"].", ".$row_emp["FirstName"]." ".$row_emp["MiddleName"];
                                             echo '<tr>';
                                                echo '
                                                   <td class="text-center">'.$count.'</td>
                                                   <td>'.$FullName.'</td>
                                                   <td class="text-center">'.$row_emp["Sex"].'</td>
                                                   <td class="text-center">'.$AgencyId.'</td>
                                                   <td>'.$Position.'</td>
                                                   <td class="text-center">'.date("d M Y",strtotime($hired_date)).'</td>
                                                   <td class="text-center">'.$years.'</td>
                                                ';
                                             echo '</tr>';
                                          }      
                                       }
                                    } else {
                                       if ($row_emp) {
                                          $count++;
                                          $AgencyId = $row_emp["AgencyId"];
                                          $FullName = $row_emp["LastName"].", ".$row_emp["FirstName"]." ".$row_emp["MiddleName"];
                                          echo '<tr>';
                                             echo '
                                                <td class="text-center">'.$count.'</td>
                                                <td>'.$FullName.'</td>
                                                <td class="text-center">'.$row_emp["Sex"].'</td>
                                                <td class="text-center">'.$AgencyId.'</td>
                                                <td>'.$Position.'</td>
                                                <td class="text-center">'.date("d M Y",strtotime($hired_date)).'</td>
                                                <td class="text-center">'.$years.'</td>
                                             ';
                                          echo '</tr>';
                                       }   
                                    }
	                                 
	                              }
	                           }
      						?>
      					</tbody>
      				</table>
                  <br><br>
                  <label>Total Count = <?php echo $count; ?> employees</label>
      			</div>
      		</div>
      	</div>
    </body>
</html>