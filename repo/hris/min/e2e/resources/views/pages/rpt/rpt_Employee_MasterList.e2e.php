<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <!-- <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>"> -->
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
      <style type="text/css">
         @media print {
            table {
               font-size: 6pt !important;
            }
            .noPrint {
               display: none;
            }
         }
         th {
            text-align: center; 
            background: gray;
            border: 1px solid black;
         }
         td {
            padding: 5px;
            vertical-align: top;
         }
      </style>
      <script type="text/javascript">
         $(document).ready(function () {
            $("#download").click(function () {
               self.location = "rpt/rpt_excel_masterlist.e2e.php";
            });
         });
      </script>
   </head>
   <body>
      <button class="btn-cls4-sea noPrint" id="download" style="margin-bottom: 20px;">
         DOWNLOAD TO EXCEL
      </button>
      <table width="1000%" border="1">
         <thead>
            <tr>
               <th>#</th>
               <th>DIVISION</th>
               <th>SECTION</th>
               <th>POSITION TITLE</th>
               <th>ITEM NUMBER </th>
               <th>
                  DATE POSITION WAS CREATED
                  <br>
                  (If not able to indicate past creations just include since 2013 or 2014 up-to-date)
               </th>
               <th>SG</th>
               <th>MONTHLY SALARY OR COST OF SERVICES (Per SG)</th>
               <th>DESIGNATION (AS APPROPRIATE)</th>
               <th>DATE OF DESIGNATION</th>
               <th>SPECIAL ORDER NO.</th>
               <th>Email Address</th>
               <th>STATUS OF EMPLOYMENT</th>
               <th>MODE OF ACCESSION</th>
               <th>DATE FILLED UP</th>
               <th>INCUMBENT</th>
               <th>LAST NAME</th>
               <th>FIRST NAME</th>
               <th>MIDDLE NAME</th>
               <th>EXT.</th>
               <th>DATE OF ORIGINAL APPOINTMENT</th>
               <th>DATE OF LAST PROMOTION</th>
               <th>ENTRY DATE IN PCW (First day in service)</th>
               <th>ELIGIBILITY (Specify RA1080 if SW,CPA, etc.)</th>
               <th>HIGHEST EDUCATION COMPLETED</th>
               <th>DEGREE AND COURSE (Specify)</th>
               <th>MASTERS OR DOCTORAL DEGREE (Specify)</th>
               <th>DATE OF BIRTH (MM/DD/YYYY)</th>
               <th>AGE</th>
               <th>SEX</th>
               <th>CIVIL STATUS</th>
               <th>BIR TIN NO.</th>
               <th>RESIDENTIAL ADDRESS</th>
               <th>PERMANENT ADDRESS</th>
               <th>INDICATE WHETHER "SOLO PARENT"</th>
               <th>INDICATE WHETHER "SENIOR CITIZEN"</th>
               <th>INDICATE WHETHER "PWD"</th>
               <th>INDICATE IF MEMBER OF ANY INDIGENOUS GROUP</th>
               <th>CITIZESHIP</th>
               <th>CONTACT NOS.</th>
               <th>EMAIL ADDRESS</th>
               <th>FORMER INCUMBENT</th>
               <th>MODE OF SEPARATION</th>
               <th>DATE VACATED</th>
               <th>REMARKS / STATUS OF VACANT POSITION</th>
            </tr>
         </thead>
         <tbody>
            <?php
               if ($rsEmployees) {
                  $count = 0;
                  while ($row = mysqli_fetch_assoc($rsEmployees)) {
                     $count++;
                     $emprefid                     = $row["RefId"];
                     $LastName                     = $row["LastName"];
                     $FirstName                    = $row["FirstName"];
                     $MiddleName                   = $row["MiddleName"];
                     $ExtName                      = $row["ExtName"];
                     $Sex                          = $row["Sex"];
                     $CivilStatus                  = $row["CivilStatus"];
                     $TIN                          = $row["TIN"];
                     $BirthDate                    = $row["BirthDate"];
                     $EmailAdd                     = $row["EmailAdd"];
                     $ContactNo                    = $row["ContactNo"];
                     $ResiAddCityRefId             = getRecord("city",$row["ResiAddCityRefId"],"Name");
                     $ResiAddProvinceRefId         = getRecord("province",$row["ResiAddProvinceRefId"],"Name");
                     $ResiHouseNo                  = $row["ResiHouseNo"];
                     $ResiStreet                   = $row["ResiStreet"];
                     $ResiSubd                     = $row["ResiSubd"];
                     $ResiBrgy                     = $row["ResiBrgy"];
                     $PermanentHouseNo             = $row["PermanentHouseNo"];
                     $PermanentStreet              = $row["PermanentStreet"];
                     $PermanentSubd                = $row["PermanentSubd"];
                     $PermanentBrgy                = $row["PermanentBrgy"];
                     $PermanentAddCityRefId        = getRecord("city",$row["PermanentAddCityRefId"],"Name");
                     $PermanentAddProvinceRefId    = getRecord("province",$row["PermanentAddProvinceRefId"],"Name");

                     switch ($CivilStatus) {
                        case 'Ma':
                           $CivilStatus = "Married";
                           break;
                        case 'Si':
                           $CivilStatus = "Single";
                           break;
                     }

                     $where                        = "WHERE EmployeesRefId = '$emprefid'";
                     $empinfo                      = FindFirst("empinformation",$where,"*");
                     if ($empinfo) {
                        $HiredDate                 = $empinfo["HiredDate"];
                        $AgencyRefId               = getRecord("agency",$empinfo["AgencyRefId"],"Name");
                        $PositionItemRefId         = getRecord("positionitem",$empinfo["PositionItemRefId"],"Name");
                        $PositionRefId             = getRecord("position",$empinfo["PositionRefId"],"Name");
                        $SalaryGradeRefId          = getRecord("salarygrade",$empinfo["SalaryGradeRefId"],"Name");
                        $EmpStatusRefId            = getRecord("empstatus",$empinfo["EmpStatusRefId"],"Name");
                        $OfficeRefId               = getRecord("office",$empinfo["OfficeRefId"],"Name");
                        $DepartmentRefId           = getRecord("department",$empinfo["DepartmentRefId"],"Name");
                        $DivisionRefId             = getRecord("division",$empinfo["DivisionRefId"],"Name");
                        $SalaryAmount              = number_format(floatval($empinfo["SalaryAmount"]),2);
                     }

            ?>
            <tr>
               <td class="text-center">
                  <?php echo $count; ?>
               </td>
               <td>
                  <?php echo $DivisionRefId; ?>
               </td>
               <td>
                  
               </td>
               <td>
                  <?php echo $PositionRefId; ?>
               </td>
               <td class="text-center">
                  <?php echo $PositionItemRefId; ?>
               </td>
               <td>
                  &nbsp;
               </td>
               <td class="text-center">
                  <?php echo $SalaryGradeRefId; ?>
               </td>
               <td class="text-center">
                  <?php echo $SalaryAmount; ?>
               </td>
               <td>
                  <?php echo $PositionRefId; ?>
               </td>
               <td>&nbsp;</td>
               <td>&nbsp;</td>
               <td>
                  <?php echo $EmailAdd; ?>
               </td>
               <td>
                  <?php echo $EmpStatusRefId; ?>
               </td>
               <td>&nbsp;</td>
               <td>&nbsp;</td>
               <td>&nbsp;</td>
               <td>
                  <?php echo $LastName; ?>
               </td>
               <td>
                  <?php echo $FirstName; ?>
               </td>
               <td>
                  <?php echo $MiddleName; ?>
               </td>
               <td>
                  <?php echo $ExtName; ?>
               </td>
               <td>&nbsp;</td>
               <td>&nbsp;</td>
               <td>&nbsp;</td>
               <td>
                  <?php
                     $eligibility_rs = SelectEach("employeeselegibility",$where);
                     if ($eligibility_rs) {
                        while ($eligibility_row = mysqli_fetch_assoc($eligibility_rs)) {
                           echo "<li>".getRecord("careerservice",$eligibility_row["CareerServiceRefId"],"Name")."</li>";
                        }
                     }
                  ?>
               </td>
               <td>
                  
               </td>
               <td></td>
               <td>
                  <?php
                     $masters_rs = SelectEach("employeeseduc",$where." AND LevelType = '5'");
                     if ($masters_rs) {
                        while ($masters_row = mysqli_fetch_assoc($masters_rs)) {
                           echo "<li>".getRecord("course",$masters_row["CourseRefId"],"Name")."</li>";
                        }
                     }
                  ?>
               </td>
               <td class="text-center">
                  <?php echo date("m/d/Y",strtotime($BirthDate)); ?>
               </td>
               <td class="text-center">
                  <?php echo computeAge($BirthDate,$param = "today"); ?>
               </td>
               <td class="text-center">
                  <?php echo $Sex; ?>
               </td>
               <td class="text-center">
                  <?php echo $CivilStatus; ?>
               </td>
               <td>
                  <?php echo $TIN; ?>
               </td>
               <td>
                  <?php echo $ResiBrgy." ".$ResiAddCityRefId." ".$ResiAddProvinceRefId; ?>
               </td>
               <td>
                  <?php echo $PermanentBrgy." ".$PermanentAddCityRefId." ".$PermanentAddProvinceRefId; ?>
               </td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td>FILIPINO</td>
               <td>
                  <?php echo $ContactNo; ?>
               </td>
               <td>
                  <?php echo $EmailAdd; ?>
               </td>
               <td>&nbsp;</td>
               <td>&nbsp;</td>
               <td>&nbsp;</td>
               <td></td>
            </tr>
            <?php
                  }
               } 
            ?>
         </tbody>
      </table>
   </body>
</html>