<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   //include 'incRptParam.e2e.php';
   //include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause = " WHERE (Inactive != 1 OR Inactive IS NULL)";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            $count = 0;
            if ($rsEmployees) {
         ?>
         <?php
            rptHeader(getvalue("RptName"));
         ?>
         <center>
         <table style="width: 60%;">
            <thead>
               <tr class="colHEADER">
                  <th style="width: 20%;">Civil Status</th>
                  <th style="width: 20%;">Male</th>
                  <th style="width: 20%;">Female</th>
               </tr>
            </thead>
            <tbody>
               <?php
                  $M_Si = 0;
                  $M_Ma = 0;
                  $M_An = 0;
                  $M_Wi = 0;
                  $M_Se = 0;
                  $M_Ot = 0;
                  
                  $F_Si = 0;
                  $F_Ma = 0;
                  $F_An = 0;
                  $F_Wi = 0;
                  $F_Se = 0;
                  $F_Ot = 0;
                  $F_ = 0;
                  $_ = 0;
                  $M_ = 0;
                  $M = 0;
                  $F = 0;
                  $NoGender = 0;
                  while ($row_emp = mysqli_fetch_assoc($rsEmployees)) {
                     $count++;
                     if ($row_emp["Sex"] == "" && $row_emp["CivilStatus"] == "") {
                        $_++;
                     } else {
                        ${$row_emp["Sex"]."_".$row_emp["CivilStatus"]}++;
                     }
                     if ($row_emp["Sex"] != "") {
                        ${$row_emp["Sex"]}++;
                     } else {
                        $NoGender++;
                     }
                  }
               ?>
               <tr>
                  <td>Annulled</td>
                  <td class="text-center">
                     <?php echo $M_An; ?>
                  </td>
                  <td class="text-center">
                     <?php echo $F_An; ?>
                  </td>
               </tr>
               <tr>
                  <td>Married</td>
                  <td class="text-center">
                     <?php echo $M_Ma; ?>
                  </td>
                  <td class="text-center">
                     <?php echo $F_Ma; ?>
                  </td>
               </tr>
               <tr>
                  <td>Others</td>
                  <td class="text-center">
                     <?php echo $M_Ot; ?>
                  </td>
                  <td class="text-center">
                     <?php echo $F_Ot; ?>
                  </td>
               </tr>
               <tr>
                  <td>Separated</td>
                  <td class="text-center">
                     <?php echo $M_Se; ?>
                  </td>
                  <td class="text-center">
                     <?php echo $F_Se; ?>
                  </td>
               </tr>
               <tr>
                  <td>Single</td>
                  <td class="text-center">
                     <?php echo $M_Si; ?>
                  </td>
                  <td class="text-center">
                     <?php echo $F_Si; ?>
                  </td>
               </tr>
               <tr>
                  <td>Widowed</td>
                  <td class="text-center">
                     <?php echo $M_Wi; ?>
                  </td>
                  <td class="text-center">
                     <?php echo $F_Wi; ?>
                  </td>
               </tr>
               <tr>
                  <td>No Civil Status</td>
                  <td class="text-center">
                     <?php echo $M_; ?>
                  </td>
                  <td class="text-center">
                     <?php echo $F_; ?>
                  </td>
               </tr>
               <tr>
                  <td>No Civil Status And Gender</td>
                  <td colspan="2" class="text-center">
                     <?php echo $_; ?>
                  </td>
               </tr>
               <tr>
                  <td>Total: <?php echo mysqli_num_rows($rsEmployees); ?></td>
                  <td class="text-center">
                     <?php echo $M; ?>
                  </td>
                  <td class="text-center">
                     <?php echo $F; ?>
                  </td>
               </tr>
            </tbody>
         </table>
         </center>
         <?php
            }
         ?>
      </div>
   </body>
</html>