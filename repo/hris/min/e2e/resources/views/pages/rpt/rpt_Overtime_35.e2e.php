<?php
   $file = "Request for Authority To Render Overtime Work";
   
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   $refid = getvalue("refid");
   $row = FindFirst("overtime_request","WHERE RefId = '$refid'","*");
   $FiledDate = date("d F Y",strtotime($row["FiledDate"]));
   if ($row) {
      $HoursReq         = $row["HoursReq"];
      $Accomplishment   = $row["Accomplishment"];
      $Justification    = $row["Justification"];
      $emprefid         = $row["EmployeesRefId"];
      $StartDate        = $row["StartDate"];
      $EndDate          = $row["EndDate"];
      $Signatory1       = $row["Signatory1"];
      $Signatory2       = $row["Signatory2"];
      $Signatory3       = $row["Signatory3"];

      $s1_row           = FindFirst("signatories","WHERE RefId = '$Signatory1'","*");
      $s2_row           = FindFirst("signatories","WHERE RefId = '$Signatory2'","*");

      $s1_name          = $s1_row["Name"];
      $s1_position      = getRecord("position",$s1_row["PositionRefId"],"Name");

      $s2_name          = $s2_row["Name"];
      $s2_position      = getRecord("position",$s2_row["PositionRefId"],"Name");

      if ($StartDate == $EndDate) {
         $covered_date = date("d F Y",strtotime($StartDate));
      } else {
         $covered_date = date("d F Y",strtotime($StartDate))." to ".date("F d, Y",strtotime($EndDate));;
      }

      $emp_row          = FindFirst("employees","WHERE RefId = '$emprefid'","`FirstName`,`LastName`,`MiddleName`,`ExtName`");
      if ($emp_row) {
         $LastName = $emp_row["LastName"];
         $FirstName = $emp_row["FirstName"];
         $MiddleName = $emp_row["MiddleName"];
         $ExtName = $emp_row["ExtName"];
         $FullName = $LastName.", ".$FirstName." $ExtName ".$MiddleName;
         $MiddleInitial = substr($MiddleName, 0, 1);
         $Preparedby = $FirstName." ".$MiddleInitial.". ".$LastName." ".$ExtName;
      } else {
         $FullName = $Preparedby = "";
      }
      $empinfo_row = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","*");
      if ($empinfo_row) {
         $Office     = getRecord("office",$empinfo_row["OfficeRefId"],"Name");
         $Position   = getRecord("position",$empinfo_row["PositionRefId"],"Name");
         $Division   = getRecord("Division",$empinfo_row["DivisionRefId"],"Name");
      } else {
         $Office     = "";
         $Position   = "";
         $Division   = "";
      }
      $stat = $row["WithPay"];
      if (intval($stat) > 0) {
         $stat = "OT Pay";
      } else {
         $stat = "CTO";
      }
   }
   
?>
<!DOCTYPE html>
<html>
   <head>
      <?php
         include_once 'pageHEAD.e2e.php';
      ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
      <style>
         /*.bottomline {border-bottom:2px solid black;}*/
         .bold {font-size:600;}
         @media print {
            .ot_footer {
               position: fixed;
               right: 0;
               bottom: 0;
               left: 0;
               padding: 3px;
               font-weight:600;
            }
         }
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            rptHeader("REQUEST FOR AUTHORITY TO RENDER OVERTIME","HRMDS-T-002");
         ?>
         <div class="row">
            <div class="col-xs-1"></div>
            <div class="col-xs-11">
               <br><br>
               Date requested: <u><?php echo $FiledDate; ?></u>
               <br>
               Division/Unit:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b><u><?php echo $Division; ?></u></b>
            </div>
         </div>
         <br>
         <table border="1">
            <thead>
               <tr>
                  <th style="width: 10%;">
                     Date of the<br>Overtime
                  </th>
                  <th style="width: 10%;">
                     No. of Hours<br>Required
                  </th>
                  <th style="width: 20%;">
                     Employee Name
                  </th>
                  <th style="width: 30%;">
                     Work to be<br>Accomplished
                  </th>
                  <th style="width: 30%;">
                     Reason/<br>Justification
                  </th>
               </tr>
            </thead>
            <tbody>
               <tr valign="top">
                  <td class="txt-center">
                     <?php echo $covered_date; ?>
                  </td>
                  <td class="txt-center">
                     <?php echo $HoursReq; ?>
                  </td>
                  <td>
                     <?php echo $FullName; ?>
                  </td>
                  <td>
                     <?php echo htmlentities($Accomplishment); ?>
                  </td>
                  <td>
                     <?php echo htmlentities($Justification); ?>
                  </td>
               </tr>
            </tbody>
         </table>
         <br><br><br>
         <div class="row">
            <div class="col-xs-4">
               Prepared by:
               <br><br><br>
               <b><?php echo $Preparedby; ?>
               <br>
               <?php echo $Position; ?>
            </div>
            <div class="col-xs-4">
               Recommended by:
               <br><br><br>
               <b>
                  <?php
                     echo $s1_name;
                  ?>
               </b>
               <br>
               <?php
                  echo $s1_position;
               ?>
            </div>
            <div class="col-xs-4">
               Approved by:
               <br><br><br>
               <b>
                  <?php
                     echo $s2_name;
                  ?>
               </b>
               <br>
               <?php
                  echo $s2_position;
               ?>
            </div>
         </div>
         <div class="ot_footer">
            <div class="row">
               <div class="col-xs-1"></div>
               <div class="col-xs-10" style="border: 1px solid black; padding: 5px;">
                  The only CONTROLLED copy of this document is the online version maintained in the HRIS. The user must ensure that this or any other copy of a controlled document is current and complete prior to use. The MASTER copy of this document is with the AFD-HRMD Section. This document is UNCONTROLLED when downloaded and printed.
                  <br>
                  <br>
                  Printed on <?php echo date("d F Y",time()); ?>  
               </div>
            </div>
         </div>
      </div>
   </body>
</html>