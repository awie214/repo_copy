<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
?>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
   </head>
   <body>
      <div class="container-fluid rptBody">
         <div class="row">
            <div class="col-xs-12">
               <div class="row">
                  <div class="col-xs-12">
                     <?php
                        rptHeader(getvalue("RptName"));
                     ?>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <span>Agency Name: &nbsp;</span>
                     <br>
                     <span>Agency BP Number: &nbsp;</span>
                     <br>
                     <span>
                        FORM E. List of Employees with changes / correction in their Personal Data.
                     </span>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <table style="width: 100%;">
                        <thead>
                           <tr class="colHEADER">
                              <th>Member BP Number</th>
                              <th>Employee Name</th>
                              <th>Mailing/Address/Zip Code</th>
                              <th>Cellular Phone No.</th>
                              <th>Email Address</th>
                              <th>Civil Status</th>
                              <th>Date of Birth</th>
                              <th>Place of Birth</th>
                              <th>Position / Title</th>
                              <th>Status of Employment</th>
                           </tr>   
                        </thead>
                        <tbody>
                           <?php
                              while ($row_emp = mysqli_fetch_assoc($rsEmployees)) {
                                 $FullName   = $row_emp["LastName"].", ".$row_emp["FirstName"]." ".$row_emp["MiddleName"];
                                 $ResiStreet = $row_emp["ResiStreet"];
                                 $ResiSubd   = $row_emp["ResiSubd"];
                                 $ResiBrgy   = $row_emp["ResiBrgy"];
                                 $MobileNo   = $row_emp["MobileNo"];
                                 $EmailAdd   = $row_emp["EmailAdd"];
                                 $BirthDate  = $row_emp["BirthDate"];
                                 $BirthPlace = $row_emp["BirthPlace"];
                                 $ResiAddCityRefId      = getRecord("city",$row_emp["ResiAddCityRefId"],"Name");
                                 $ResiAddProvinceRefId  = getRecord("province",$row_emp["ResiAddProvinceRefId"],"Name");
                                 $ResiAddress = "";
                                 if ($ResiStreet != "") $ResiAddress .= "$ResiStreet, ";
                                 if ($ResiSubd != "") $ResiAddress .= "$ResiSubd, ";
                                 if ($ResiBrgy != "") $ResiAddress .= "$ResiBrgy, ";
                                 if ($ResiAddCityRefId != "") $ResiAddress .= "$ResiAddCityRefId, ";
                                 if ($ResiAddProvinceRefId != "") $ResiAddress .= "$ResiAddProvinceRefId";
                                 $CivilStat = $row_emp['CivilStatus']; 
                                 switch ($CivilStat) {
                                    case "Si":
                                       $CivilStat = 'Single';
                                    break;
                                    case "Ma":
                                       $CivilStat = 'Married';
                                    break;
                                    case "An":
                                       $CivilStat = 'Annulled';
                                    break;
                                    case "Wi":
                                       $CivilStat = 'Widowed';
                                    break;
                                    case "Se":
                                       $CivilStat = 'Separated';
                                    break;
                                    case "Ot":
                                       $CivilStat = 'Others';
                                    break;
                                 }
                                 $Gender = $row_emp["Sex"];
                                 if ($Gender == "M") {
                                    $Gender = "Male";
                                 } else if ($Gender == "F") {
                                    $Gender = "Female";
                                 } else {
                                    $Gender = "";
                                 }
                                 if ($BirthDate != "") {
                                    $BirthDate = date("m/d/Y",strtotime($BirthDate));
                                 } else {
                                    $BirthDate = "";
                                 }
                                 $emp_info = FindFirst("empinformation","WHERE EmployeesRefId = ".$row_emp["RefId"],"*");
                                 if ($emp_info) {
                                    $Position = rptDefaultValue($emp_info["PositionRefId"],"position");
                                    $ApptStatus = rptDefaultValue($emp_info["ApptStatusRefId"],"apptstatus");
                                    $SalaryAmount = number_format($emp_info["SalaryAmount"],2);
                                 } else {
                                    $Position = "";
                                    $ApptStatus = "";
                                    $SalaryAmount = "0.00";
                                 }
                                 echo '<tr>';
                                    echo '
                                       <td>&nbsp;</td>
                                       <td>'.$FullName.'</td>
                                       <td>'.$ResiAddress.'</td>
                                       <td>'.$MobileNo.'</td>
                                       <td>'.$EmailAdd.'</td>
                                       <td>'.$CivilStat.'</td>
                                       <td class="text-center">'.$BirthDate.'</td>
                                       <td>'.$BirthPlace.'</td>
                                       <td>'.$Position.'</td>
                                       <td>'.$ApptStatus.'</td>                                       
                                    ';
                                 echo '</tr>';
                              }
                           ?>
                        </tbody>
                     </table>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <span>Issue No. 01, Rev No. 0, (16 August 2016), FM-GSIS-OPS-UMR-05</span>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>