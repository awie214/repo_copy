<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
      <style type="text/css">
         @media print {
            body {
               font-size: 9pt;
            }
            thead {
               font-size: 9pt;  
            }
            tbody {
               font-size: 8pt !important;
            }
         }
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <div class="col-xs-12">
            <div class="row">
               <div class="col-xs-12">
                  QUARTERLY REPORT ON SEPARATION
                  <br>
                  For the __________ Quarter of CY 2018
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-8">
                  Agency: PHILIPPINE INSTITUTE FOR DEVELOPMENT STUDIES
               </div>
               <div class="col-xs-4">
                  Region:  NCR
                  <br>
                  Date Submitted :
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-12">
                  <table width="100%">
                     <thead>
                        <tr class="colHEADER">
                           <th>NAME</th>
                           <th>DATE OF BIRTH</th>
                           <th>POSITION TILE</th>
                           <th>STATUS OF <br> APPOINTMENT</th>
                           <th>SALARY<br>GRADE</th>
                           <th>MODE OF<br>SEPARATION</th>
                           <th>EFFECTIVITY DATE<br>OF SEPARATION</th>
                           <th>REMARKS</th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                           for ($i=1; $i <= 10 ; $i++) { 
                              echo '
                                 <tr>
                                    <td>&nbsp;</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                 </tr>
                              ';
                           }
                        ?>
                     </tbody>
                  </table>
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-12">
                  I hereby certify that the above list is true and correct per records of this Institute.
               </div>
            </div>
            <br>
            <br>
            <br>
            <div class="row margin-top">
               <div class="col-xs-6"></div>
               <div class="col-xs-6 text-center">
                  <b>ANDREA S. AGCAOILI</b>
                  <br>
                  Department Manager III-Administrative and Finance Department
               </div>
            </div>
         </div>
      </div>
   </body>
</html>