<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            switch (getvalue("for")){
               case "consolLoan":
                  rptHeader("Loan Report Detail - CONSOL LOAN");
               break;
               case "policyLoan":
                  rptHeader ("Loan Report Detail - POLICY LOAN");
               break;
               case "pagibigLoan":
                  rptHeader ("Loan Report Detail - PAGIBIG LOAN");
               break;
               case "emergencyLoan":
                  rptHeader ("Loan Report Detail - EMERGENCY LOAN");
               break;
               case "landbankLoan":
                  rptHeader ("Loan Report Detail - LANDBANK LOAN");
               break;
            }
         ?>
         <p class="txt-center">For the Month of <u><?php echo monthName(date("m",time()),1) ?></u> 2017</p>

         <table border="1">
            <tr style="text-align:center;">
               <td><label>No.</label></td>
               <td><label>Employee ID</label></td>
               <td><label>Name of Employee</label></td>
               <td><label>Loan Name</label></td>
               <td><label>Loan Amount</label></td>
               <td><label>Monthly Amortization</label></td>
               <td><label>Total Payment</label></td>
               <td><label>Balance</label></td>
            </tr>
            <?php for ($j=1;$j<=10;$j++) {?>
               <tr style="text-align:center;">
                  <td><?php echo $j ?></td>
                  <td><?php echo '100-'.$j;?></td>
                  <td>Juan Dela Cruz</td>
                  <td><?php 
                     switch (getvalue("for")){
                        case "consolLoan":
                           echo 'CONSOL LOAN';
                        break;
                        case "policyLoan":
                           echo 'POLICY LOAN';
                        break;
                        case "pagibigLoan":
                           echo 'PAGIBIG LOAN';
                        break;
                        case "emergencyLoan":
                           echo 'EMERGENCY LOAN';
                        break;
                        case "landbankLoan":
                           echo 'LANDBANK LOAN';
                        break;
                     }
                  
                  ?></td>
                  <td>P 3000.00</td>
                  <td>P 9000.00</td>
                  <td>P 12000.00</td>
                  <td>P 15000.00</td>
               </tr>
            <?php }?>
         </table>
         <p>
            <div class="row">
               <div class="col-xs-2 txt-right">Prepared By:</div>
               <div class="col-xs-4"></div>
               <div class="col-xs-2 txt-right">Approved By:</div>
               <div class="col-xs-4"></div>
            </div>
            <div class="row">
               <div class="col-xs-2"></div>
               <div class="col-xs-4">________________________</div>
               <div class="col-xs-2"></div>
               <div class="col-xs-3">________________________</div>
               <div class="col-xs-1"></div>
            </div>
         </p>

      </div>
      <?php rptFooter(); ?>
   </body>
</html>