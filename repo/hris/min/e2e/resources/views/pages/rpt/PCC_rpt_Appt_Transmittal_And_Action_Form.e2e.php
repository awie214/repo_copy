<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include "pageHEAD.e2e.php"; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <table>
            <thead>
               <tr>
                  <th colspan="16" align="center" style="text-align:center;">
                     <?php
                        rptHeader(getRptName(getvalue("drpReportKind")));
                     ?>
                  </th>
               </tr>
               <tr>
                  <th colspan="7" class="text-left">Agency: ____________________________________________</th>
                  <th colspan="2">&nbsp;</th>
                  <th colspan="7" class="text-right">CSFCO In-change: ____________________________________________</th>
               </tr>   
               <tr>
                  <th colspan="7" class="text-left">
                     <div class="row">
                        INSTRUCTIONS:
                     </div>
                     <div class="row margin-top">
                        (1) Fill-out the data needed in the form completely and accurately.
                     </div>
                     <div class="row margin-top">
                        (2) Do not abbreviate entries in the form.
                     </div>
                     <div class="row margin-top">
                        (3) Accomplish the Checklist of Common Requirements and sign the certification.
                     </div> 
                     <div class="row margin-top">
                        (4) Submit the duty accomplished form in the electronic and printed copy (2 copies) to the CSC Field Office-In-Charge together with the original copies of the appointments and supporting documents.
                     </div>
                  </th>
                  <th colspan="2">&nbsp;</th>
                  <th colspan="7" class="text-right">
                     <div class="row" style="padding: 5px;">
                        <div class="col-xs-12">
                           <div class="row" style="border: 1px solid black;"> 
                              <div class="col-xs-12">
                                 <div class="row" style="background: gray; border-bottom: 1px solid black; text-align: left;"> 
                                    <i>For CSCRO/FO's Use:</i>
                                 </div>
                                 <div class="row" style="height: 100px; margin-bottom: 20px;"></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </th>
               </tr>
               <tr class="colHEADER">
                  <th rowspan="2">ID</th>
                  <th rowspan="2">NAME</th>
                  <th rowspan="2">POSITION TILE</th>
                  <th rowspan="2">SALARY / JOB</th>
                  <th rowspan="2">EMPLOYMENT STATUS</th>
                  <th colspan="2">
                     PERIOD OF APPOINTMENT
                     <br>
                     (for Temporary, Casual / Contractaul Appointment)
                  </th>
                  <th rowspan="2">NATURE OF APPOINTMENT</th>
                  <th rowspan="2">DATE OF ISSUANCE</th>
                  <th colspan="3">PUBLICATION</th>
                  <th colspan="3">CSC ACTION</th>
                  <th rowspan="2">Agency Receiving Officer</th>
               </tr>
               <tr class="colHEADER">
                  <th>DATE<br>FROM</th>
                  <th>DATE<br>TO</th>
                  <th>DATE<br>FROM</th>
                  <th>DATE<br>TO</th>
                  <th>
                     MODE
                     <br>
                     (CSC Bulletin of Vacant Positions)
                  </th>
                  <th>APPROVED<br>OR<br>DISAPPROVED</th>
                  <th>Date of Action</th>
                  <th>Date of Release</th>
               </tr>
            </thead>
            <tbody>
                  <?php for ($j=1;$j<=10;$j++) 
                  { ?>
                     <tr>
                        <td>&nbsp;</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                     </tr>
                  <?php 
                  } ?>
               <tr>
                  <td colspan="2">&nbsp;</td>
                  <td colspan="14">
                     <br>
                     <label>CERTIFICATION:</label>
                     <br>
                     This is to certify that the information contained in this form are true, correct and complete.
                     <br>
                     <div class="row">
                        <div class="col-xs-6">
                           _______________________________________
                           <br>
                           Highest Ranking HRMO
                        </div>
                     </div>
                     <div class="row margin-top"> 
                        <div class="col-xs-12">
                           DATE: _______________________________________
                        </div>
                     </div>
                  </td>
               </tr>
               <tr>
                  <td colspan="16">
                     <div class="row" style="border: 1px solid black; padding: 5px; height: 100px; margin-top: 20px;">
                        <div class="col-xs-12">
                           <label>REMARKS / COMMENTS / RECOMMENDATIONS: (eg. Reasons for Disapproval of Appointment)</label>
                        </div>
                     </div>
                  </td>
               </tr>
            </tbody>
            <tfoot>
               
               <tr>
                  <td colspan="16">
                     <?php rptFooter(); ?>
                  </td>
               </tr>   
            </tfoot>
         </table>   
      </div>
   </body>
</html>