<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include_once 'incRptQryString.e2e.php';
   $year  = getvalue("txtAttendanceYear");
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <div class="row">
            <?php
               $rs = SelectEach("employees",$whereClause);
               if (mysqli_num_rows($rs)) {
                  while ($row = mysqli_fetch_assoc($rs)) {
                     if ($p_filter_value == "0" || $p_filter_table == "") {
                        $emprefid = $row["RefId"];
                     } else {
                        $emprefid   = $row["EmployeesRefId"];
                     }
                     rptHeader(getRptName(getvalue("drpReportKind")));
                     $LastName       = $row["LastName"];
                     $FirstName      = $row["MiddleName"];
                     $MiddleName     = $row["FirstName"];
                     $FullName       = $row["LastName"].", ".$row["FirstName"]." ".$row["MiddleName"];
            ?>

            <p class="txt-right">Date:&nbsp;&nbsp;<u><?php echo date("F j, Y") ?> </u></p>
            <p class="txt-left">MEMORANDUM</p>
            <blockquote><p><?php echo ("$FullName");?></p></blockquote>
            <p>Division: <u> MSID </u></p>
            <p>&nbsp;&nbsp;&nbsp;&nbsp;Pursuant to Section 8, Rule XVII of the Omnibus Rules Implementing Book V of Executive Order 292 and April 14, 2016 other Pertinent Civil Service Law, as a amended by CSC Memonrandum Circular No. 23,s 19998. officers or employees who have incurred tardniness and undertime, regardless of the number of minutes per day,ten(10) times a monthfor at least two(2) consecutive months during the year or for at least(2) months in a semester shall be subject to disciplinary action.</p>
            <p>Our recordsshow that you incurred tardiness more than the prescribed limit as follows:</p>
            <table border="1">
               <tr>
                  <th>MONTH</th>
                  <th>No. Of Tardy</th>
                  <th>No. Of Undertime</th>
                  <th>Total</th>
               </tr>
               <?php for ($j=1;$j<=12;$j++) {
                     if ($j <= 9) $j = "0".$j;
                     $dtr = FindFirst("dtr_process","WHERE EmployeesRefId = '$emprefid' AND Month = '$j' AND Year = '$year'","*");
                     if ($dtr) {
                        $late = $dtr["Total_Tardy_Count"];
                        $UT   = $dtr["Total_Undertime_Count"];
                        $Total = $late + $UT;
                     } else {
                        $late = $UT = $Total = 0;
                     }
               ?>
                  <tr>
                     <td class="txt-center"><?php echo monthName($j,1);?></td>
                     <td class="txt-center"><?php echo $late;?></td>
                     <td class="txt-center"><?php echo $UT;?></td>
                     <td class="txt-center"><?php echo $Total;?></td>
                  </tr>
               <?php } ?>
            </table>
            <br>
            <p>&nbsp;&nbsp;&nbsp;&nbsp;Item C, Number 4, Section 52 of Rule IV of the Uniform Rules on Administrative Cases in the Civil Service (URACCS) imposses the following penalties on your administrative offense, to wit:<br>
            "4. Frequent unauthorized tardiness (Habitual Tardiness)
            </p>
            <p style="padding-left:1in">1st Offense - Reprimand</p>
            <p style="padding-left:1in">2nd Offense - Suspension 1 - 30 days</p>
            <p style="padding-left:1in">3rd Offense - Dismissal"</p>
            <p>In view thereof, you are strictly advisedto imporove on your attendance, otherwise repetition on the same shall be deal with accordingly</p>
            <br><br><br>
         </div>
         <div class="row">
            <div class="col-xs-9"></div>
            <div class="col-xs-3 txt-center">
               <p>(Name)</p>
               <p>___________________________</p>
               <p>(Position)</p>
               <p>(Divisition)</p>
            </div>
         </div>

      <?php
               }
            }
      ?>

      </div>
   </body>
</html>