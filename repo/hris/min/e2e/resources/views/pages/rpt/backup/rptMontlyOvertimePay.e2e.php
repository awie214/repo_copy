<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include "pageHEAD.e2e.php"; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            rptHeader(getRptName(getvalue("drpReportKind")));
         ?>
         <p class="txt-center">For the Month of <u><?php echo monthName(date("m",time()),1).", ".date("Y",time()) ?></u> </p>


         <table border="1">
            <tr>
               <th rowspan=2 colspan=3>Name of Employee</th>
               <th colspan=3>Actual No. of mins. rendered</th>
               <th rowspan=2 style="width:10%">COC</th>
               <th rowspan=2 style="width:30%" colspan=3>Remarks</th>
            </tr>
            <tr>
               <th style="width:10%">Reg. Days</th>
               <th style="width:10%">WE/H*</th>
               <th style="width:10%">Total</th>
            </tr>
            <?php for ($j=1;$j<=10;$j++) {?>
               <tr>
                  <td colspan=3>Dela Cruz, Juan</td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td colspan=3></td>
               <tr>
            <?php } ?>
         </table>
         <p>*WE/H - Weekends and Holidays</p>
         <p>
            <div class="row">
               <div class="col-xs-2 txt-right">Prepared By:</div>
               <div class="col-xs-4"></div>
               <div class="col-xs-2 txt-right">Reviewed By:</div>
               <div class="col-xs-4"></div>
            </div>
            <div class="row">
               <div class="col-xs-2"></div>
               <div class="col-xs-4">________________________</div>
               <div class="col-xs-2"></div>
               <div class="col-xs-3">________________________</div>
               <div class="col-xs-1"></div>
            </div><br>
            <div class="row txt-center">
               <div>Approved By:</div>
               <div>________________________</div>
               <div>Position</div>
            </div>
         </p>

      </div>
      <?php rptFooter(); ?>
   </body>
</html>