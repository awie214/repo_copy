<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <table>
            <thead>
               <tr>
                  <th colspan="16" align="center" style="text-align:center;">
                     <?php
                        rptHeader(getvalue("RptName"));
                     ?>
                     <p class="txt-center">For the Month of <u><?php echo monthName(date("m",time()),1).", ".date("Y",time()) ?></u></p>
                  </th>
                  
               </tr>
               <tr class="colHEADER" align="center">
                  <th rowspan="2">ID</th>
                  <th rowspan="2">EMPLOYEE NAME</th>
                  <th rowspan="2">POSITION</th>
                  <th rowspan="2">ASSUMPTION</th>
                  <th rowspan="2">Effective Date of Promotion</th>
                  <th rowspan="2">STATUS OF EMPLOYMENT</th>
                  <th rowspan="2">NATURE OF APPOINTMENT</th>
                  <th rowspan="2">SALARY</th>
                  <th colspan="3">YEAR-END BONUS</th>
                  <th colspan="3">CASH GIFT</th>
                  <th></th>
                  <th></th>
               </tr>
               <tr class="colHEADER" align="center">
                  <th>ENTITLEMENT</th>
                  <th>RECEIVED</th>
                  <th>BALANCE</th>
                  <th>ENTITLEMENT</th>
                  <th>RECEIVED</th>
                  <th>BALANCE</th>
                  <th>W/ TAX</th>
                  <th>TOTAL</th>
               </tr>

            </thead>
            <tbody>
                  <?php for ($j=1;$j<=60;$j++) 
                  { ?>
                     <tr>
                        <td>&nbsp;</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                     </tr>
                  <?php 
                  } ?>
                  <tr>
                     <td colspan="17">
                        <div class="row">
                           <div class="col-sm-5">
                              This is to certify that that above PCC 
                              personnel have rendered at least 4 mos.   
                              of service including leave of absence w/  
                              pay as of Oct 31, 2017 and that  
                              the 1st half Year-End Bonus has been   
                              deducted on the above payroll, per  
                              Sec. 6.1 and 6.2 of BC 2010-1.   
                           </div>
                           <div class="col-sm-3">
                              Approved for Payment:
                           </div>
                           <div class="col-sm-4">
                              Certified: Supporting documents complete and       
                              proper, and cash available in the amount of     
                              Php _________________________.      
                           </div>
                        </div>
                        <?php spacer(20); ?>
                        <div class="row">
                           <div class="col-sm-5">
                              <u>ANTONIA LYNNELY L. BAUTISTA</u>
                              <br>
                              Chief Admin Officer, HRDD
                           </div>
                           <div class="col-sm-3">
                              <u>GWEN GRECIA-DE VERA</u>
                              <br>
                              Executive Director
                           </div>
                           <div class="col-sm-4">
                              <u>CAROLYN V. AQUINO</u>
                              <br>
                              Accountant III, FPMO
                           </div>

                        </div>
                     </td>
                  </tr>
            </tbody>
            <tfoot>
               <tr>
                  <td colspan="9">
                     <?php rptFooter(); ?>
                  </td>
               </tr>   
            </tfoot>
         </table>
      </div>
   </body>
</html>