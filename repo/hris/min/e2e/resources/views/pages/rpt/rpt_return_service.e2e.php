<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $count = 0;
   $quarter = getvalue("quarter");
   $year    = getvalue("year");
   $where   = "WHERE Year(ServiceStartDate) = '$year'";
   switch ($quarter) {
      case '1':
         $quarter_label = "1st";
         $where .= " AND Month(ServiceStartDate) BETWEEN '01' AND '03'";
         break;
      case '2':
         $quarter_label = "2nd";
         $where .= " AND Month(ServiceStartDate) BETWEEN '04' AND '06'";
         break;
      case '3':
         $quarter_label = "3rd";
         $where .= " AND Month(ServiceStartDate) BETWEEN '07' AND '09'";
         break;
      case '4':
         $quarter_label = "4th";
         $where .= " AND Month(ServiceStartDate) BETWEEN '10' AND '12'";
         break;
      default:
         $where = "";
         $quarter_label = "";
         break;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <style type="text/css">
         
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            rptHeader("Status of Return Service Agreement");
         ?>
         <div class="row">
            <div class="col-xs-12 text-center">
               Quarter:<b><u><?php echo $quarter_label; ?></u></b>, Year: <b><u><?php echo $year; ?></u></b>
            </div>
         </div>
         <br><br>
         <div class="row">
            <div class="col-xs-12">
               <table style="width: 100%;">
                  <thead>
                     <tr class="colHEADER">
                        <th>Name</th>
                        <th>Title of<br>Training/Seminar/Scholarship</th>
                        <th>Start Date of Return Service</th>
                        <th>Date Return Service will be <br>Served</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php
                        $competency    = SelectEach("ldmsreturnobligation",$where);
                        if ($competency) {
                           $check_name                = "";
                           $row_count  = mysqli_num_rows($competency);
                           while ($competency_row = mysqli_fetch_assoc($competency)) {
                              $emprefid                  = $competency_row["EmployeesRefId"];
                              $row                       = FindFirst("employees","WHERE RefId = '$emprefid'","*");
                              if ($row) {
                                 $LastName      = $row["LastName"];
                                 $FirstName     = $row["FirstName"];
                                 $MiddleName    = $row["MiddleName"];
                                 $MiddleInitial = substr($MiddleName, 0,1);
                                 $FullName      = $FirstName." ".$MiddleInitial.". ".$LastName;
                              } else {
                                 $FullName      = "";
                              }
                              $LDMSLNDInterventionRefId  = $competency_row["LDMSLNDInterventionRefId"];
                              $Rating                    = $competency_row["Rating"];
                              $Equivalent                = $competency_row["Equivalent"];
                              $Name                      = $competency_row["Name"];
                              $ServiceStartDate          = $competency_row["ServiceStartDate"];
                              $InterventionStartDate     = $competency_row["InterventionStartDate"];
                              $InterventionEndDate       = $competency_row["InterventionEndDate"];
                              $ServedStartDate           = $competency_row["ServedStartDate"];
                              $ReturnService             = $competency_row["ReturnService"];
                              $where_intervention        = "WHERE RefId = '$LDMSLNDInterventionRefId'";
                              $comp_row            = FindFirst("ldmslndintervention",$where_intervention,"*");
                              $Name                = $comp_row["Name"];
                              echo '<tr>';
                                 if ($check_name == $FullName) {
                                    echo '<td>&nbsp;</td>';
                                 } else {
                                    echo '<td>'.$FullName.'</td>';
                                    $check_name = $FullName;
                                 }
                                 echo '<td>'.$Name.'</td>';
                                 echo '<td class="text-center">'.date("F d, Y",strtotime($ServiceStartDate)).'</td>';
                                 echo '<td class="text-center">'.date("F d, Y",strtotime($ServedStartDate)).'</td>';
                              echo '</tr>';
                           }
                        } else {
                           echo '<tr>';
                              echo '<td colspan="4">No Record Found</td>';
                           echo '</tr>';
                        }
                     ?>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </body>
</html>
