<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   //$whereClause = "LIMIT 10";
   $table = "employees";
   $month   = getvalue("txtAttendanceMonth");
   $year    = getvalue("txtAttendanceYear");
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);

   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            $errmsg = "";
            rptHeader(getRptName(getvalue("drpReportKind")));
            if ($rsEmployees && $errmsg == "")
            {
         ?>
         <p class="txt-center">For the Month of <u><?php echo monthName($month,1).", ".$year; ?></u> </p>

         <p>DIVISION:</p>
         <table border="1">
            <tr>
               <th>EMPLOYEE NAME</th>
               <th>EMPLOYEE ID</th>
               <th>VL Balance</th>
               <th>SL Balance</th>
            </tr>
            <?php
               while ($row = mysqli_fetch_assoc($rsEmployees) ) {
                  if ($p_filter_value == "0" || $p_filter_table == "") {
                     $emprefid = $row["RefId"];
                  } else {
                     $emprefid   = $row["EmployeesRefId"];
                  }
                  $leave = computeCredit($emprefid,"01",$month,$year);
            ?>
               <tr>
                  <td class="pad-left"><?php echo $row['LastName'].', '.$row['FirstName'].', '.$row['MiddleName'];?></td>
                  <td class="text-center"><?php echo $row['AgencyId']?></td>
                  <td class="text-center"><?php echo $leave["VL"];?></td>
                  <td class="text-center"><?php echo $leave["SL"];?></td>
               <tr>
            <?php
               }
            }else {
               echo '<div>NO RECORD QUERIED base on your criteria!!!</div>';
               echo '<div>'.$errmsg.'</div>';
            }
            ?>
         </table>
         <p>
            <div class="row">
               <div class="col-xs-2 txt-right">Prepared By:</div>
               <div class="col-xs-4"></div>
               <div class="col-xs-2 txt-right">Approved By:</div>
               <div class="col-xs-4"></div>
            </div>
            <div class="row">
               <div class="col-xs-2"></div>
               <div class="col-xs-4">________________________</div>
               <div class="col-xs-2"></div>
               <div class="col-xs-3">________________________</div>
               <div class="col-xs-1"></div>
            </div>
         </p>

      </div>
   </body>
</html>