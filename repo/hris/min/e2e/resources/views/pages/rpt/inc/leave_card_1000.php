<?php
	$day_rate = "1.25";
	$limit    = "0.750";
	echo '<table border="1" width="100%">';
		echo '
			<thead>
	            <tr align="center">
	                <th rowspan="2" style="width: 15%;">Period Covered</th>
	                <th rowspan="2" style="width: 15%;">Particular</th>
	                <th colspan="4">Vacation Leave</th>
	                <th colspan="4">Sick Leave</th>
	                <th rowspan="2">Remarks</th>
	            </tr>
	            <tr align="center">
	                <th>Earned</th>
	                <th>Absences<br>Undertime<br>With Pay</th>
	                <th>Balance</th>
	                <th>Absences<br>Undertime<br>W/O Pay</th>

	                <th>Earned</th>
	                <th>Absences<br>Undertime<br>With Pay</th>
	                <th>Balance</th>
	                <th>Absences<br>Undertime<br>W/O Pay</th>
	            </tr>
	        </thead>
	        <tbody>
		';
		$where_credits 	= "WHERE EmployeesRefId = '$emprefid' AND EffectivityYear = '$year'";
		$rsCredits 		= SelectEach("employeescreditbalance",$where_credits);
		if ($rsCredits) {
			while ($credits_row = mysqli_fetch_assoc($rsCredits)) {
				$balance 		= $credits_row["BeginningBalance"];
				$credit 		= $credits_row["NameCredits"];
				$credits_date 	= $credits_row["BegBalAsOfDate"];
				switch ($credit) {
					case 'VL':
						${"VL_".$emprefid} 	= $balance;
						$as_of 				= date("F d, Y",strtotime($credits_date));
						break;
					case 'SL':
						${"SL_".$emprefid} = $balance;
						break;
				}
			}
		} else {
			$as_of 				= date("F 01, Y",time());
			${"SL_".$emprefid} 	= 0;
			${"FL_".$emprefid} 	= 0;
			${"VL_".$emprefid} 	= 0;
			${"SPL_".$emprefid} = 0;
		}
		for ($a=1; $a <= intval($curr_month); $a++) { 
			if ($a <= 9) {
				$selected_month = "0".$a;	
			} else {
				$selected_month = $a;
			}
			$lwop_check = 0;
			$selected_date = date("Y-m-t",strtotime($year."-".$selected_month."-01"));
			if (${"VL_".$emprefid} < 0) {
				echo '<tr>';
					echo '<td>'.$arr_month[$a - 1].'</td>';
					echo '<td></td>';
					echo '<td></td>';
					echo '<td></td>';
					echo '<td class="text-center">('.number_format(abs(${"VL_".$emprefid}),3).')</td>';
					echo '<td></td>';
					echo '<td></td>';
					echo '<td></td>';
					echo '<td class="text-center">'.number_format(${"SL_".$emprefid},3).'</td>';
					echo '<td></td>';
					echo '<td></td>';
				echo '</tr>';	
			} else {
				echo '<tr>';
					echo '<td>'.$arr_month[$a - 1].'</td>';
					echo '<td></td>';
					echo '<td></td>';
					echo '<td></td>';
					echo '<td class="text-center">'.number_format(${"VL_".$emprefid},3).'</td>';
					echo '<td></td>';
					echo '<td></td>';
					echo '<td></td>';
					echo '<td class="text-center">'.number_format(${"SL_".$emprefid},3).'</td>';
					echo '<td></td>';
					echo '<td></td>';
				echo '</tr>';	
			}
			



			${"data_arr".$emprefid."_".$a} = array();
			$where = "where EmployeesRefId = ".$emprefid." AND Status = 'Approved'";
            $where .= " AND MONTH(ApplicationDateFrom) = '$selected_month'";
            $where .= " AND YEAR(ApplicationDateFrom) = '$year'";
            $where .= " ORDER BY ApplicationDateFrom";
            $rsLeave = SelectEach("employeesleave",$where);
            if ($rsLeave) {
            	while ($leave_row = mysqli_fetch_assoc($rsLeave)) {
            		$leave_from = $leave_row["ApplicationDateFrom"];
            		$leave_to 	= $leave_row["ApplicationDateTo"];
            		$dfrom   	= date("d",strtotime($leave_from));
                  	$dto     	= date("d",strtotime($leave_to));
                  	$type    	= getRecord("leaves",$leave_row["LeavesRefId"],"Code");
                  	$leave_day  = count_leave($emprefid,$leave_from,$leave_to);
                  	${"data_arr".$emprefid."_".$a}[strtotime($leave_to)] = [
                  		"From"=>$leave_from,
                  		"To"=>$leave_to,
                  		"Code"=>$type,
                  		"Count"=>$leave_day
                  	];
            	}
            }
            $rsCTO 	= SelectEach("employeescto",$where);
            if ($rsCTO) {
            	while ($cto_row = mysqli_fetch_assoc($rsCTO)) {
            		$cto_from 	= $cto_row["ApplicationDateFrom"];
            		$cto_to 	= $cto_row["ApplicationDateTo"];
            		$cto_dfrom  = date("d",strtotime($cto_from));
                  	$cto_dto    = date("d",strtotime($cto_to));
                  	$cto_day 	= count_leave($emprefid,$cto_from,$cto_to);
            		$cto_hrs 	= $cto_row["Hours"] * $cto_day;
            		${"data_arr".$emprefid."_".$a}[strtotime($cto_to)] = [
                  		"From"=>$cto_from,
                  		"To"=>$cto_to,
                  		"Code"=>"CTO",
                  		"Count"=>$cto_hrs
                  	];
            	}
            }
            asort(${"data_arr".$emprefid."_".$a});
			foreach (${"data_arr".$emprefid."_".$a} as $key => $data) {
				$From 	= $data["From"];
				$To 	= $data["To"];
				$Type 	= $data["Code"];
				$Count 	= $data["Count"];
				if ($From == $To) {
					$covered_date = date("M d, Y",strtotime($To));
					if ($Type == "CTO") {
						$particular = "( ".$Count." Hours)";
					} else {
						$particular = "( ".$Count." day)";
					}
					
				} else {
					$dummy_from    	= date("d",strtotime($From));
					$dummy_to      	= date("d",strtotime($To));
					$dummy_month   	= date("M",strtotime($From));
					$dummy_year    	= date("Y",strtotime($To));
					$covered_date  	= $dummy_month." ".$dummy_from."-".$dummy_to.", ".$year;
					if ($Type == "CTO") {
						$particular = "( ".$Count." Hours)";
					} else {
						$particular = "( ".$Count." days)";
					}
				}
				$particular = $Type." ".$particular;
				if ($Type == "VL" || $Type == "SL" || $Type == "MANDATORY") $Count = $Count * $day_rate;
				
				if ($Type == "VL" || $Type == "MANDATORY") {
					if (${"VL_".$emprefid} < 0) {
						$temp_borrow = abs(${"VL_".$emprefid}) + ".5";
						if ($temp_borrow <= $limit) {
							$Count 	= $Count - "0.5";
							${"VL_".$emprefid} -= "0.5";
							$temp_count = number_format("0.5",3);
						} else {
							$temp_count = "";
						}
						$vl_eq 				= FindLast("leavecreditsearnedwopay","WHERE NoOfDaysLeaveWOP <= '".abs($Count)."'","NoOfDaysLeaveWOP");
						$vl_wop    			= $vl_eq;
						$lwop_check 		+= $vl_eq;
						echo '<tr>';
							echo '<td>'.$covered_date.'</td>';
							echo '<td>'.$particular.'</td>';
							echo '<td></td>';
							echo '<td class="text-center">'.$temp_count.'</td>';
							echo '<td class="text-center">('.number_format(abs(${"VL_".$emprefid}),3).')</td>';
							echo '<td class="text-center">'.number_format(abs($vl_wop),3).'</td>';
							echo '<td></td>';
							echo '<td></td>';
							echo '<td></td>';
							echo '<td></td>';
							echo '<td>'.$particular.'</td>';
						echo '</tr>';				
						
					} else {
						$vl_check = ${"VL_".$emprefid} - $Count;
						if ($vl_check < 0) {
							if (abs($vl_check) < $limit) {
								$borrow_vl 			= $vl_check;
								$vl_wop 			= 0;
								${"VL_".$emprefid} 	= $borrow_vl;
							} else {
								$borrow_vl  		= 0;
								$vl_eq 				= FindLast("leavecreditsearnedwopay","WHERE NoOfDaysLeaveWOP <= '".abs($vl_check)."'","NoOfDaysLeaveWOP");
								$vl_wop    			= $vl_eq;
								$lwop_check 		+= $vl_eq;
								${"VL_".$emprefid} 	= (abs($vl_check) - $vl_eq) * -1;
							}
						} else {
							${"VL_".$emprefid} -= $Count;
							$borrow_vl  = 0;
							$vl_wop    	= 0;
						}
						if ($vl_wop == 0) {
							if ($borrow_vl < 0) {
								echo '<tr>';
									echo '<td>'.$covered_date.'</td>';
									echo '<td>'.$particular.'</td>';
									echo '<td></td>';
									echo '<td class="text-center">'.number_format($Count,3).'</td>';
									echo '<td class="text-center">('.number_format(abs(${"VL_".$emprefid}),3).')</td>';
									echo '<td></td>';
									echo '<td></td>';
									echo '<td></td>';
									echo '<td></td>';
									echo '<td></td>';
									echo '<td>'.$particular.'</td>';
								echo '</tr>';	
							} else {
								echo '<tr>';
									echo '<td>'.$covered_date.'</td>';
									echo '<td>'.$particular.'</td>';
									echo '<td></td>';
									echo '<td class="text-center">'.number_format($Count,3).'</td>';
									echo '<td class="text-center">'.number_format(${"VL_".$emprefid},3).'</td>';
									echo '<td></td>';
									echo '<td></td>';
									echo '<td></td>';
									echo '<td></td>';
									echo '<td></td>';
									echo '<td>'.$particular.'</td>';
								echo '</tr>';		
							}
							
						} else {
							if (${"VL_".$emprefid} < 0) {
								if (abs(${"VL_".$emprefid}) > $limit) {
									echo '<tr>';
										echo '<td>'.$covered_date.'</td>';
										echo '<td>'.$particular.'</td>';
										echo '<td></td>';
										echo '<td class="text-center"></td>';
										echo '<td class="text-center">('.number_format(abs(${"VL_".$emprefid}),3).')</td>';
										echo '<td class="text-center">'.number_format(abs($vl_wop),3).'</td>';
										echo '<td></td>';
										echo '<td></td>';
										echo '<td></td>';
										echo '<td></td>';
										echo '<td>'.$particular.'</td>';
									echo '</tr>';			
								} else {
									echo '<tr>';
										echo '<td>'.$covered_date.'</td>';
										echo '<td>'.$particular.'</td>';
										echo '<td></td>';
										echo '<td class="text-center">'.number_format($Count - $vl_wop,3).'</td>';
										echo '<td class="text-center">('.number_format(abs(${"VL_".$emprefid}),3).')</td>';
										echo '<td class="text-center">'.number_format(abs($vl_wop),3).'</td>';
										echo '<td></td>';
										echo '<td></td>';
										echo '<td></td>';
										echo '<td></td>';
										echo '<td>'.$particular.'</td>';
									echo '</tr>';			
								}
								
							} else {
								echo '<tr>';
									echo '<td>'.$covered_date.'</td>';
									echo '<td>'.$particular.'</td>';
									echo '<td></td>';
									echo '<td class="text-center">'.number_format($Count,3).'</td>';
									echo '<td class="text-center">'.number_format(${"VL_".$emprefid},3).'</td>';
									echo '<td class="text-center">'.number_format(abs($vl_wop),3).'</td>';
									echo '<td></td>';
									echo '<td></td>';
									echo '<td></td>';
									echo '<td></td>';
									echo '<td>'.$particular.'</td>';
								echo '</tr>';		
							}
							
						}
					}
					
					
				} else if ($Type == "SL") {
					echo '<tr>';
						echo '<td>'.$covered_date.'</td>';
						echo '<td>'.$particular.'</td>';
						echo '<td></td>';
						echo '<td></td>';
						echo '<td></td>';
						echo '<td></td>';
						echo '<td></td>';
						echo '<td class="text-center">'.number_format($Count,3).'</td>';
						${"SL_".$emprefid} -= $Count;
						echo '<td class="text-center">'.number_format(${"SL_".$emprefid},3).'</td>';
						echo '<td></td>';
						echo '<td>'.$particular.'</td>';
					echo '</tr>';
				} else {
					echo '<tr>';
						echo '<td>'.$covered_date.'</td>';
						echo '<td>'.$particular.'</td>';
						echo '<td></td>';
						echo '<td></td>';
						echo '<td></td>';
						echo '<td></td>';
						echo '<td></td>';
						echo '<td></td>';
						echo '<td></td>';
						echo '<td></td>';
						echo '<td>'.$particular.'</td>';
					echo '</tr>';
				}
			}
			$where_dtr  = "WHERE EmployeesRefId = $emprefid AND Month = '$selected_month' AND Year = '$year'";
			$arr_empDTR = FindFirst("dtr_process",$where_dtr,"*");
			if ($arr_empDTR) {
				$Tardy_Deduction_EQ 	= $arr_empDTR["Tardy_Deduction_EQ"];
				$Undertime_Deduction_EQ = $arr_empDTR["Undertime_Deduction_EQ"];
				$Total_Absent_EQ 		= $arr_empDTR["Total_Absent_EQ"];
				$Total_Tardy_Hr 		= $arr_empDTR["Total_Tardy_Hr"];
				$Total_Undertime_Hr 	= $arr_empDTR["Total_Undertime_Hr"];
				$Total_Absent_Count 	= $arr_empDTR["Total_Absent_Count"];
				$VL_Earned 				= $arr_empDTR["VL_Earned"];
				$SL_Earned 				= $arr_empDTR["SL_Earned"];
				$UT_Tardy 				= $Total_Undertime_Hr + $Total_Tardy_Hr;
				$Total_Deduction_EQ 	= $Undertime_Deduction_EQ + $Tardy_Deduction_EQ;
				if ($Total_Undertime_Hr > 0) {
					$UT_Tardy_Particular = $Total_Undertime_Hr;
				} else {
					$UT_Tardy_Particular = $Total_Tardy_Hr;
				}
				if ($lwop_check > 0) {
					$where_lwop = "WHERE NoOfDaysLeaveWOP >= '$lwop_check'";
					$VL_Earned 	= FindFirst("leavecreditsearnedwopay",$where_lwop,"LeaveCreditsEarned");
					$SL_Earned	= $VL_Earned;
				}
				$vl_check_lwop 	= ${"VL_".$emprefid} - $Total_Deduction_EQ;
				if ($vl_check_lwop < 0) {
					$vl_lwop_where 	= "WHERE NoOfDaysLeaveWOP <= '".abs($vl_check_lwop)."'";
					$vl_lwop_row 	= FindLast("leavecreditsearnedwopay",$vl_lwop_where,"*");
					$vl_lwop_eq 	= $vl_lwop_row["NoOfDaysLeaveWOP"];
					$vl_borrow 		= abs($vl_check_lwop) - $vl_lwop_eq;
				} else {
					$vl_borrow 		= 0;
					$vl_lwop_eq 	= 0;
				}
				if ($UT_Tardy > 0) {
					if ($vl_lwop_eq > 0) {
						$Deduction_notes	= "UT ".convertToHoursMins($Total_Deduction_EQ);
						${"VL_".$emprefid} -= $vl_borrow;
						echo '<tr>';
							echo '<td>'.date("F d, Y",strtotime($selected_date)).'</td>';
							echo '<td>'.$Deduction_notes.'</td>';
							echo '<td></td>';
							echo '<td class="text-center">'.number_format($vl_borrow,3).'</td>';
							if (${"VL_".$emprefid} < 0) {
								echo '<td class="text-center">('.number_format(abs(${"VL_".$emprefid}),3).' )</td>';
							} else {
								echo '<td class="text-center">'.number_format(${"VL_".$emprefid},3).'</td>';	
							}
							echo '<td class="text-center">'.number_format($vl_lwop_eq,3).'</td>';
							echo '<td></td>';
							echo '<td></td>';
							echo '<td></td>';
							echo '<td></td>';
							echo '<td></td>';
						echo '</tr>';	
					} else {
						$Deduction_notes	= "UT ".convertToHoursMins($UT_Tardy_Particular);
						echo '<tr>';
							echo '<td>'.date("F d, Y",strtotime($selected_date)).'</td>';
							echo '<td>'.$Deduction_notes.'</td>';
							echo '<td class="text-center"></td>';
							echo '<td class="text-center">'.number_format($Total_Deduction_EQ,3).'</td>';
							${"VL_".$emprefid} -= $Total_Deduction_EQ;
							echo '<td class="text-center">'.number_format(${"VL_".$emprefid},3).'</td>';
							echo '<td></td>';
							echo '<td class="text-center"></td>';
							echo '<td></td>';
							echo '<td class="text-center">'.number_format(${"SL_".$emprefid},3).'</td>';
							echo '<td></td>';
							echo '<td></td>';
						echo '</tr>';		
					}
				}
				echo '<tr>';
					echo '<td>'.date("F d, Y",strtotime($selected_date)).'</td>';
					echo '<td></td>';
					echo '<td class="text-center">'.number_format($VL_Earned,3).'</td>';
					echo '<td></td>';
					${"VL_".$emprefid} += $VL_Earned;
					echo '<td class="text-center">'.number_format(${"VL_".$emprefid},3).'</td>';
					echo '<td></td>';
					echo '<td class="text-center">'.number_format($SL_Earned,3).'</td>';
					echo '<td></td>';
					${"SL_".$emprefid} += $SL_Earned;
					echo '<td class="text-center">'.number_format(${"SL_".$emprefid},3).'</td>';
					echo '<td></td>';
					echo '<td></td>';
				echo '</tr>';
			}
			echo '<tr><td colspan="11" style="background: gray;">&nbsp;</td></tr>';
		}
		echo '</tbody></table>';
?>