<?php
   include_once 'pageHEAD.e2e.php';
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   $control_number                = getvalue("control_number");
   $where      = "WHERE ControlNumber = '$control_number'";
   $rs         = SelectEach("employeesauthority",$where);
   $rs2         = SelectEach("employeesauthority",$where);
   while ($dum_row = mysqli_fetch_assoc($rs)) {
      $Signatory1       = $dum_row["Signatory1"];
      $Signatory2       = $dum_row["Signatory2"];
      $date_start       = $dum_row["ApplicationDateFrom"];
      $date_end         = $dum_row["ApplicationDateTo"];
      $time_start       = $dum_row["FromTime"];
      $time_end         = $dum_row["ToTime"];
      $filed_date       = $dum_row["FiledDate"];
      $remarks          = $dum_row["Remarks"];
      $destination      = $dum_row["Destination"];
      $ActivityNature   = $dum_row["ActivityNature"];
      $Sponsor          = $dum_row["Sponsor"];
      $EventName        = $dum_row["EventName"];

      $s1_row           = FindFirst("signatories","WHERE RefId = '$Signatory1'","*");
      $s2_row           = FindFirst("signatories","WHERE RefId = '$Signatory2'","*");

      $s1_name          = $s1_row["Name"];
      $s1_position      = getRecord("position",$s1_row["PositionRefId"],"Name");

      $s2_name          = $s2_row["Name"];
      $s2_position      = getRecord("position",$s2_row["PositionRefId"],"Name");
   }
   if ($date_start == $date_end) {
      $date_range = date("d F Y",strtotime($date_start));   
   } else {
      $date_range = date("d F Y",strtotime($date_start))." to ".date("d F Y",strtotime($date_end));   
   }
   if ($time_start != "") {
      $time_range = HrsFormat($time_start)." - ".HrsFormat($time_end);
   } else {
      $time_range = "Whole Day";
   }
   $user      = getvalue("hEmpRefId");
   $user_row  = FindFirst("employees","WHERE RefId = '$user'","`FirstName`,`LastName`,`MiddleName`,`ExtName`");
   if ($user_row) {
      $user_LastName   = $user_row["LastName"];
      $user_FirstName  = $user_row["FirstName"];
   } else {
      $user_FirstName = $user_LastName = "";
   }
   $user_FullName = $user_FirstName." ".$user_LastName;
   
?>
<!DOCTYPE html>
<html>
<head>
	<style type="text/css">
		td, th {
			border: 2px solid black;
			vertical-align: top;
			padding: 5px;
			font-size: 9pt;
		}
      th {text-align: center;}
		.data {
			font-size: 10pt;
			text-transform: uppercase;
		}
	</style>
</head>
<body>
	<div class="container-fluid rptBody">
		<div style="page-break-after: always;">
	        <?php
	            rptHeader("Office Order","HRDMS-T-003");
	        ?>
	        <div class="row margin-top">
	        	<div class="col-xs-12">
	        		<b>OFFICE ORDER NO: <?php echo $control_number; ?></b>
               <br>
               Series of <?php echo date("Y",strtotime($filed_date)); ?>
               <br>
               <br>
               This office order authorizes the following personnel to attend/participate/conduct the specified activity
               <br>
               <br>
               <b>WHAT </b>
               <u><?php echo $EventName; ?></u>
               <br>
               <br>
               <b>NATURE OF ACTIVITY </b><?php echo $ActivityNature; ?>
               <br>
               <br>
               <b>SPONSORED/CONDUCTED/REQUESTED BY: </b><?php echo $Sponsor; ?>
               
	        	</div>
	        </div>
	        <br>
	        <div class="row margin-top">
	        	<div class="col-xs-12">
	        		<table width="100%" border="1">
                  <thead>
                     <tr>
                        <th style="width: 50%;">NAME OF PERSONNEL</th>
                        <th style="width: 50%;">
                           PURPOSE OF ATTENDANCE
                           <br>
                           <i>
                              (specify if participant, resource speaker, observer, facilitator, secretariat,documenter, TA provider, coordinator, etc)
                           </i>
                        </th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php
                        while ($row = mysqli_fetch_assoc($rs2)) {
                           $emprefid = $row["EmployeesRefId"];
                           $remarks  = $row["Remarks"];
                           $emp_row       = FindFirst("employees","WHERE RefId = $emprefid","*");
                           if ($emp_row) {
                              $LastName      = $emp_row["LastName"];
                              $FirstName     = $emp_row["FirstName"];
                              $MiddleName    = $emp_row["MiddleName"];   
                              $ExtName       = $emp_row["ExtName"];
                              $FullName      = $LastName.", ".$FirstName." ".$ExtName." ".$MiddleName;
                              $cid           = $emp_row["CompanyRefId"];
                           } else {
                              $FullName = $ExtName = $LastName = $FirstName = $MiddleName = "&nbsp;";
                              $cid           = 0;
                           }
                           echo '
                              <tr>
                                 <td>'.$FullName.'</td>
                                 <td>'.$remarks.'</td>
                              </tr>
                           ';
                        }
                     ?>
                  </tbody>
               </table>
	        	</div>
	        </div>
            <div class="row margin-top">
               <div class="col-xs-12">
                  <b>WHEN:</b> <u><?php echo $date_range; ?></u>
                  <br>
                  <b>WHERE:</b> <u><?php echo $destination; ?></u>
                  <br>
                  <b>TIME:</b> <u><?php echo $time_range; ?></u>
               </div>
            </div>
            <br>
            <br>
            <div class="row margin-top">
               <div class="col-xs-12">
                  <p>
                     This further allows the above-named personnel to charge the following expense/s, subject to the availability of ______________ funds and usual accounting and auditing requirements:
                  </p>
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-1">
               </div>
               <div class="col-xs-11">
                  <input type="checkbox" name="">&nbsp;Registration fee (Amount _________)
                  <br>
                  <input type="checkbox" name="">&nbsp;Transportation expense/allowance
                  <br>
                  <input type="checkbox" name="">&nbsp;Representation allowance
                  <br>
                  <input type="checkbox" name="">&nbsp;Others (please specify e.g. Food allowance, etc)
               </div>
            </div>
            <br>
            <br>
            <div class="row margin-top">
               <div class="col-xs-12">
                  <p>All Office Orders previously issued which are inconsistent with the above are deemed superseded by this Order.</p>
               </div>
            </div>
            <br>
            <br>
	        <div class="row margin-top">
               <div class="col-xs-6">
                Recommended by:
               <br><br><br>
               <b>
                  <?php echo $s1_name; ?>
               </b>
               <br>
               <?php echo $s1_position; ?>
            </div>
            <div class="col-xs-6">
               Approved by:
               <br><br><br>
               <b>
                  <?php echo $s2_name; ?>
               </b>
               <br>
               <?php echo $s2_position; ?>
            </div>
           </div>

           <?php spacer(40); ?>
            <div class="row margin-top">
               <div class="col-xs-1"></div>
               <div class="col-xs-10" style="border: 1px solid black; padding: 5px;">
                     The only CONTROLLED copy of this document is the online version maintained in the HRIS. The user must ensure that this or any other copy of a controlled document is current and complete prior to use. The MASTER copy of this document is with the AFD-HRMD Section. This document is UNCONTROLLED when downloaded and printed.
                     <br>
                     <br>
                     Printed on <?php echo date("F d,Y",time()); ?>
                     <br>
                     Printed by <?php echo $user_FullName; ?>
               </div>
            </div>
	    </div>
    </div>
</body>
</html>