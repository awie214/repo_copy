<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            rptHeader(getRptName(getvalue("drpReportKind")));
         ?>
         <p class="txt-center">YEAR : <u><?php echo date("Y",time()) ?></u></p>
         <p>Name of Employee : <u>DELA CRUZ, JUAN</u></p>

         <table border="1">
            <tr>
               <th rowspan="2">Date</th>
               <th colspan="3">Partculars</th>
               <th colspan="4">Vacation Leave</th>
               <th colspan="4">Sick Leave</th>
               <th rowspan="2">Remarks</th>
            </tr>
            <tr>
               <th>late</th>
               <th>ut</th>
               <th>abs</th>
               <th>used</th>
               <th>lwop</th>
               <th>earned</th>
               <th>balance</th>
               <th>used</th>
               <th>lwop</th>
               <th>earned</th>
               <th>balance</th>
            </tr>
            <tr>
               <th colspan="13">BEGINNING BALANCE AS OF</th>
            </tr>
            <?php for($j=1;$j<=12;$j++) {?>
            <tr>
               <td class="txt-center"><?php echo monthName($j,1) ?></td>
               <td class="txt-center"></td>
               <td class="txt-center"></td>
               <td class="txt-center"></td>
               <td class="txt-center"></td>
               <td class="txt-center"></td>
               <td class="txt-center"></td>
               <td class="txt-center"></td>
               <td class="txt-center"></td>
               <td class="txt-center"></td>
               <td class="txt-center"></td>
               <td class="txt-center"></td>
               <td class="txt-center"></td>
            </tr>
            <tr>
               <td class="txt-right" style="padding-right:5px;">Total</td>
               <td class="txt-center">0.00</td>
               <td class="txt-center">0.00</td>
               <td class="txt-center">0.00</td>

               <td class="txt-center"></td>
               <td class="txt-center"></td>
               <td class="txt-center"></td>
               <td class="txt-center"></td>

               <td class="txt-center"></td>
               <td class="txt-center"></td>
               <td class="txt-center"></td>
               <td class="txt-center"></td>

               <td class="txt-center"></td>
            </tr>
            <?php } ?>

         </table>
         <p>
            <div class="row">
               <div class="col-xs-2 txt-right">Prepared By:</div>
               <div class="col-xs-4"></div>
               <div class="col-xs-2 txt-right">Approved By:</div>
               <div class="col-xs-4"></div>
            </div>
            <div class="row">
               <div class="col-xs-2"></div>
               <div class="col-xs-4">________________________</div>
               <div class="col-xs-2"></div>
               <div class="col-xs-3">________________________</div>
               <div class="col-xs-1"></div>
            </div>
         </p>

      </div>
      <?php rptFooter(); ?>
   </body>
</html>