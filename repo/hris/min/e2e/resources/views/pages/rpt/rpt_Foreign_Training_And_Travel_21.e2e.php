<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            $count = 0;
            if ($rsEmployees) {
         ?>
         
         <table style="width: 100%;">
            <thead>
               <tr>
                  <td colspan="9">
                     <?php
                        rptHeader(getvalue("RptName"));
                     ?>
                  </td>
               </tr>
               <tr>
                  <td colspan="9">&nbsp;</td>
               </tr>
               <tr class="colHEADER">
                  <th>Employee Name</th>
                  <th>Position</th>
                  <th>Division</th>
                  <th>Title of Program</th>
                  <th>Duration</th>
                  <th>Country</th>
                  <th>Sponsor</th>
                  <th>Classification</th>
                  <th>No. Of Days</th>
               </tr>
            </thead>
            <tbody>
               <?php
                  while ($row_emp = mysqli_fetch_assoc($rsEmployees)) {
                     $count++;
                     $FullName   = $row_emp["LastName"].", ".$row_emp["FirstName"]." ".$row_emp["MiddleName"];
                     $emp_info   = FindFirst("empinformation","WHERE EmployeesRefId = ".$row_emp["RefId"],"*");
                     if ($emp_info) {
                        echo '
                           <tr>
                              <td>'.$FullName.'</td>
                              <td class="text-center">'.rptDefaultValue($emp_info["PositionRefId"],"position").'</td>
                              <td class="text-center">'.rptDefaultValue($emp_info["DivisionRefId"],"division").'</td>
                              <td class="text-center">----</td>
                              <td class="text-center">----</td>
                              <td class="text-center">----</td>
                              <td class="text-center">----</td>
                              <td class="text-center">----</td>
                              <td class="text-center">----</td>
                           </tr>
                        ';   
                     }
                     
                  }
               ?>
               
            </tbody>
         </table>
         <?php
            }
         ?>
      </div>
   </body>
</html>