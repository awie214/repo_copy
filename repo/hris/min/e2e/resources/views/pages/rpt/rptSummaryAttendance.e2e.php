<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   //$whereClause .= " LIMIT 10";
   $table = "employees";
   $rsEmployees = SelectEach($table,$whereClause);
   //echo mysqli_num_rows($rsEmployees);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   $from = getvalue("txtAttendanceDateFrom");
   $to   = getvalue("txtAttendanceDateTo");
   $month = getvalue("txtAttendanceMonth");
   $year  = getvalue("txtAttendanceYear");
   $from    = $year."-".$month."-01";
   $to      = $year."-".$month."-".cal_days_in_month(CAL_GREGORIAN,$month,$year);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            $errmsg = "";
            rptHeader(getRptName(getvalue("drpReportKind")));
            if ($rsEmployees && $errmsg == "")
            {
         ?>
         <!--<p class="txt-center">For the Month of <u><?php //echo monthName(date("m",time()),1) ?></u> 2017</p>-->
         <p class="txt-center">
            <?php
               echo "For the Date ";
               echo date("m/d/Y",strtotime($from))." - ".date("m/d/Y",strtotime($to));
            ?>
         </p>
         <table border="1" style="width: 100%;">
            <tr>
               <th>Division:</th>
               <th colspan="2">Tardy</th>
               <th colspan="2">Undertime</th>
               <th rowspan="2">Total Hours</th>
               <th rowspan="2">Total Hours Eqv.</th>
               <th rowspan="2">No. Of Abs.</th>
               <th rowspan="2">Total Total No. of Work Days</th>
               <th rowspan="2">Remarks</th>
            </tr>
            <tr>
               <th>Name of Employee</th>
               <th>No. of days Tardy</th>
               <th>Total Min/Hours Tardy</th>
               <th>No. of days UT</th>
               <th>Total Mins/Hours UT</th>
            </tr>
            <?php
               $count = 0;
               while ($row_emp = mysqli_fetch_assoc($rsEmployees) ) {
                  $emprefid       = $row_emp["RefId"];
                  $biometricsID   = $row_emp["BiometricsID"];
                  $CompanyID      = $row_emp["CompanyRefId"];
                  $BranchID       = $row_emp["BranchRefId"];
                  $Default_qry    = "WHERE CompanyRefId = ".$CompanyID." AND BranchRefId = ".$BranchID;
                  $where          = "WHERE EmployeesRefId = $emprefid AND Month = '$month' AND Year = '$year'";
                  $dtr_summary    = FindFirst("dtr_process",$where,"*");
                  if ($dtr_summary) {
                     $late           = $dtr_summary["Total_Tardy_Count"];
                     $late_count     = rpt_HoursFormat($dtr_summary["Total_Tardy_Hr"]);
                     $UT             = $dtr_summary["Total_Undertime_Count"];
                     $UT_count       = rpt_HoursFormat($dtr_summary["Total_Undertime_Hr"]);
                     $total_hours    = rpt_HoursFormat($dtr_summary["Total_Regular_Hr"]);
                     $tot_hours_eq   = $dtr_summary["Regular_Hr_EQ"];
                     $absent         = $dtr_summary["Total_Absent_Count"];
                     $Total_Days     = $dtr_summary["Total_Present_Count"];

                  /*$workschedrefid = FindFirst("empinformation","WHERE EmployeesRefId = ".$row_emp["RefId"],"WorkscheduleRefId");
                  if (is_numeric($workschedrefid)) {
                     $count++;
                     $curr_date   = date("Y-m-d",time());
                     $month_start = $from;
                     $month_end   = $to;
                     include 'mdbcn.e2e.php';
                     include 'SummaryOfAttendance.e2e.php';*/
            ?>
               <tr>
                  <td class="pad-left">
                     <?php 
                        echo $row_emp['LastName'].', '.$row_emp['FirstName'].', '.$row_emp['MiddleName'];
                     ?>
                  </td>
                  <td class="text-center">
                     <?php echo $late; ?>
                  </td>
                  <td class="text-center">
                     <?php echo $late_count; ?>
                  </td>
                  <td class="text-center">
                     <?php echo $UT; ?>
                  </td>
                  <td class="text-center">
                     <?php echo $UT_count; ?>
                  </td>
                  <td class="text-center">
                     <?php echo $total_hours; ?>
                  </td>
                  <td class="text-center">
                     <?php echo $tot_hours_eq; ?>
                  </td>
                  <td class="text-center">
                     <?php echo $absent; ?>
                  </td>
                  <td class="text-center">
                     <?php echo $Total_Days; ?>
                     
                  </td>
                  <td class="text-center">
                     
                  </td>
               <tr>
            <?php
                  }
               }
            }else {
               echo '<div>NO RECORD QUERIED base on your criteria!!!</div>';
               echo '<div>'.$errmsg.'</div>';
            }
            ?>
         </table>
         <qoute>Only Employees With Work Schedule are listed.</qoute>
         <p>
            <div class="row">
               <div class="col-xs-2 txt-right">Prepared By:</div>
               <div class="col-xs-4"></div>
               <div class="col-xs-2 txt-right">Approved By:</div>
               <div class="col-xs-4"></div>
            </div>
            <div class="row">
               <div class="col-xs-2"></div>
               <div class="col-xs-4">________________________</div>
               <div class="col-xs-2"></div>
               <div class="col-xs-3">________________________</div>
               <div class="col-xs-1"></div>
            </div>
         </p>

      </div>
   </body>
</html>