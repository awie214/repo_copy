<?php
	require_once 'constant.e2e.php';
   require_once pathClass.'0620functions.e2e.php';
   require_once pathClass.'0620RptFunctions.e2e.php';
   require_once pathClass.'DTRFunction.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
  

   for ($a=1; $a <=12 ; $a++) { 
   	if ($a <= 9) $a = "0".$a;
   	for ($b=1; $b <= 31; $b++) { 
   		if ($b <= 9) $b = "0".$b;
   		${"m".$a."_d".$b} = "&nbsp;";
   	}
   	${"m".$a."_VLEarned"} 			= "&nbsp;";
   	${"m".$a."_SLEarned"} 			= "&nbsp;";
   	${"m".$a."_VLUsed"} 				= "&nbsp;";
   	${"m".$a."_SLUsed"} 				= "&nbsp;";
   	${"m".$a."_VLNewBalance"} 		= "&nbsp;";
   	${"m".$a."_SLNewBalance"} 		= "&nbsp;";
   	${"m".$a."_VLWOP"} 				= "&nbsp;";
   	${"m".$a."_SLWOP"}			 	= "&nbsp;";
   	${"m".$a."_Undertime"} 			= "&nbsp;";
   	${"m".$a."_VLDate"} 				= "&nbsp;";
   	${"m".$a."_SLDate"} 				= "&nbsp;";
   }
   $months = ["January","February","March","April","May","June","July","August","September","October","November","December"];
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<?php include_once $files["inc"]["pageHEAD"]; ?>
   <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
   <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   <style type="text/css">
   	.bg-green {background: green; padding: 1px; }
   	.bg-red {background: red; padding: 1px; }
   	.bg-purple {background: violet; padding: 1px; }
   	td {
   		padding: 3px;
   	}
   	@media print {
   		.card-page {
   			width: 1500px;
   		}
   	}
   	@page {
   		size: landscape;
   	}
   </style>
</head>
<body>
	<div class="container-fluid rptBody">
		<div class="row">
			<div class="col-xs-12">
				<?php
					if ($rsEmployees) {
						while ($row = mysqli_fetch_assoc($rsEmployees)) {
							$RefId 		= $row["RefId"];
							$LastName 	= $row["LastName"];
							$FirstName 	= $row["FirstName"];
							$MiddleName = $row["MiddleName"];
							$ExtName 	= $row["ExtName"];
							$VL_Balance = 0;
							$SL_Balance = 0;
							$NewVL_Balance = 0;
							$NewSL_Balance = 0;
							$PrevVL_Balance = 0;
							$PrevSL_Balance = 0;
							$FullName 	= $LastName.", ".$FirstName." ".$ExtName." ".$MiddleName;
							$empinfo 	= FindFirst("empinformation","WHERE EmployeesRefId = '$RefId'","*");
							if ($empinfo) {
								$Position 	= getRecord("position",$empinfo["PositionRefId"],"Name");
								$Office 		= getRecord("Office",$empinfo["OfficeRefId"],"Name");
							} else {
								$Position = $Office = "";
							}
							$where_balance 	= "WHERE EmployeesRefId = '$RefId' AND EffectivityYear = '".date("Y",time())."'";
							$credit_balance 	= SelectEach("employeescreditbalance",$where_balance);
							if ($credit_balance) {
								while ($credit = mysqli_fetch_assoc($credit_balance)) {
									switch ($credit["NameCredits"]) {
										case 'VL':
											$VL_Balance = $credit["BeginningBalance"];
											break;
										case 'SL':
											$SL_Balance = $credit["BeginningBalance"];
											break;
									}
								}
							}
							$where_dtr 	= "WHERE EmployeesRefId = '$RefId' AND Year = '".date("Y",time())."' ORDER BY Month";
							$dtr 			= SelectEach("dtr_process",$where_dtr);
							if ($dtr) {
								while ($dtr_row = mysqli_fetch_assoc($dtr)) {
									$Month 			= $dtr_row["Month"];
									$Tardy 			= $dtr_row["Total_Tardy_Hr"];
									$UT 	 		= $dtr_row["Total_Undertime_Hr"];
									$VL_Used 		= $dtr_row["VL_Used"];
									$SL_Used 		= $dtr_row["SL_Used"];
									$Tardy_EQ 		= $dtr_row["Tardy_Deduction_EQ"];
									$UT_EQ 			= $dtr_row["Undertime_Deduction_EQ"];
									$VL_Earned 		= $dtr_row["VL_Earned"];
									$SL_Earned 		= $dtr_row["SL_Earned"];
									$Absent_Count 	= $dtr_row["Total_Absent_Count"];
									$VL_Days 		= $dtr_row["VL_Days"];
									$FL_Days 		= $dtr_row["FL_Days"];
									$SL_Days 		= $dtr_row["SL_Days"];
									$SPL_Days 		= $dtr_row["SPL_Days"];



									if ($VL_Days != "") {
										$vl_day_arr = explode("|", $VL_Days);
										foreach ($vl_day_arr as $vl_key => $vl_value) {
											if ($vl_value != "") {
												${"m".$Month."_d".$vl_value} = "<span class='bg-green'>VL</span>";
											}
										}
									}

									if ($SL_Days != "") {
										$sl_day_arr = explode("|", $SL_Days);
										foreach ($sl_day_arr as $sl_key => $sl_value) {
											if ($sl_value != "") {
												${"m".$Month."_d".$sl_value} = "<span class='bg-red'>SL</span>";
											}
										}
									}

									if ($FL_Days != "") {
										$fl_day_arr = explode("|", $FL_Days);
										foreach ($fl_day_arr as $fl_key => $fl_value) {
											if ($fl_value != "") {
												${"m".$Month."_d".$fl_value} = "<span class='bg-green'>FL</span>";
											}
										}
									}

									if ($SPL_Days != "") {
										$spl_day_arr = explode("|", $SPL_Days);
										foreach ($spl_day_arr as $spl_key => $spl_value) {
											if ($spl_value != "") {
												${"m".$Month."_d".$spl_value} = "<span class='bg-purple'>SPL</span>";
											}
										}
									}


									$TotalDeductionHr 	= intval($Tardy) + intval($UT);
									$TotalDeduction_EQ 	= $Tardy_EQ + $UT_EQ;

									if ($PrevVL_Balance == 0) {
										$NewVL_Bal 	= ($VL_Earned + $VL_Balance) - ($TotalDeduction_EQ + $VL_Used + $Absent_Count);
									} else {
										$NewVL_Bal 	= ($VL_Earned + $PrevVL_Balance) - ($TotalDeduction_EQ + $VL_Used + $Absent_Count);
									}
									$PrevVL_Balance = $NewVL_Bal;



									if ($PrevSL_Balance == 0) {
										$NewSL_Bal 	= $SL_Earned + $SL_Balance;
									} else {
										$NewSL_Bal 	= $SL_Earned + $PrevSL_Balance;
									}
									$PrevSL_Balance = $NewSL_Bal;
									

									${"m".$Month."_VLNewBalance"}	= number_format($NewVL_Bal,3);
									${"m".$Month."_SLNewBalance"}	= number_format($NewSL_Bal,3);
									${"m".$Month."_VLUsed"}			= number_format(($TotalDeduction_EQ + $VL_Used + $Absent_Count),3);
									${"m".$Month."_SLUsed"}			= $SL_Used;
									${"m".$Month."_VLEarned"} 		= number_format($VL_Earned,3);
									${"m".$Month."_SLEarned"} 		= number_format($SL_Earned,3);

									



									for ($d=1; $d <= 31; $d++) { 
										if ($d <= 9) $d = "0".$d;
										$Day_Late 	= $dtr_row[$d."_Late"];
										$Day_UT 		= $dtr_row[$d."_UT"];
										$Day_Deduct = intval($Day_Late) + intval($Day_UT);
										if ($Day_Deduct > 0) {
											${"m".$Month."_d".$d} = $Day_Deduct;
										}
									}
								}
							}
				?>
				<div class="row card-page" style="page-break-after: always;">
					<div class="col-xs-12">
						<div class="row">
							<div class="col-xs-12 text-center">
								<h3>LEAVE RECORD</h3>
								<br>
							</div>
						</div>
						<div class="row margin-top">
							<div class="col-xs-4">
								(Name)&nbsp;<?php echo strtoupper($FullName); ?>
							</div>
							<div class="col-xs-4">
								(Position)&nbsp;<?php echo strtoupper($Position); ?>
							</div>
							<div class="col-xs-4">
								(Department or Office)&nbsp;<?php echo strtoupper($Office); ?>
							</div>
						</div>
						<div class="row margin-top">
							<div class="col-xs-12">
								<table width="100%" border="1">
									<thead>
										<tr align="center">
											<td>Month</td>
											<td>1</td>
											<td>2</td>
											<td>3</td>
											<td>4</td>
											<td>5</td>
											<td>6</td>
											<td>7</td>
											<td>8</td>
											<td>9</td>
											<td>10</td>
											<td>11</td>
											<td>12</td>
											<td>13</td>
											<td>14</td>
											<td>15</td>
											<td>-</td>
											<td colspan="2">EARNED</td>
											<td colspan="2">USED</td>
											<td colspan="2">BALANCE</td>
											<td colspan="3">WITHOUT PAY</td>
											<td colspan="2">DATES OF LEAVE</td>
										</tr>
										<tr align="center">
											<td>YEAR</td>
											<td>16</td>
											<td>17</td>
											<td>18</td>
											<td>19</td>
											<td>20</td>
											<td>21</td>
											<td>22</td>
											<td>23</td>
											<td>24</td>
											<td>25</td>
											<td>26</td>
											<td>27</td>
											<td>28</td>
											<td>29</td>
											<td>30</td>
											<td>31</td>
											<td>VACATION</td>
											<td>SICK</td>
											<td>VACATION</td>
											<td>SICK</td>
											<td>VACATION</td>
											<td>SICK</td>
											<td>VACATION</td>
											<td>SICK</td>
											<td>Undertime</td>
											<td>VACATION</td>
											<td>SICK</td>
										</tr>	
									</thead>
									<tbody>
										<tr>
											<td></td>
											<td colspan="20">BALANCE BROUGHT FORWARD</td>
											<td class="text-center"><?php echo $VL_Balance; ?></td>
											<td class="text-center"><?php echo $SL_Balance; ?></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>
										<?php
											for ($c=1; $c <= 12; $c++) { 
												if ($c <= 9) $c = "0".$c;
										?>
										<tr align="center">
											<td class="text-left" rowspan="2"><?php echo $months[$c - 1]; ?></td>
											<td><?php echo ${"m".$c."_d01"}; ?></td>
											<td><?php echo ${"m".$c."_d02"}; ?></td>
											<td><?php echo ${"m".$c."_d03"}; ?></td>
											<td><?php echo ${"m".$c."_d04"}; ?></td>
											<td><?php echo ${"m".$c."_d05"}; ?></td>
											<td><?php echo ${"m".$c."_d06"}; ?></td>
											<td><?php echo ${"m".$c."_d07"}; ?></td>
											<td><?php echo ${"m".$c."_d08"}; ?></td>
											<td><?php echo ${"m".$c."_d09"}; ?></td>
											<td><?php echo ${"m".$c."_d10"}; ?></td>
											<td><?php echo ${"m".$c."_d11"}; ?></td>
											<td><?php echo ${"m".$c."_d12"}; ?></td>
											<td><?php echo ${"m".$c."_d13"}; ?></td>
											<td><?php echo ${"m".$c."_d14"}; ?></td>
											<td><?php echo ${"m".$c."_d15"}; ?></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>
										<tr align="center">
											<td><?php echo ${"m".$c."_d16"}; ?></td>
											<td><?php echo ${"m".$c."_d17"}; ?></td>
											<td><?php echo ${"m".$c."_d18"}; ?></td>
											<td><?php echo ${"m".$c."_d19"}; ?></td>
											<td><?php echo ${"m".$c."_d20"}; ?></td>
											<td><?php echo ${"m".$c."_d21"}; ?></td>
											<td><?php echo ${"m".$c."_d22"}; ?></td>
											<td><?php echo ${"m".$c."_d23"}; ?></td>
											<td><?php echo ${"m".$c."_d24"}; ?></td>
											<td><?php echo ${"m".$c."_d25"}; ?></td>
											<td><?php echo ${"m".$c."_d26"}; ?></td>
											<td><?php echo ${"m".$c."_d27"}; ?></td>
											<td><?php echo ${"m".$c."_d28"}; ?></td>
											<td><?php echo ${"m".$c."_d29"}; ?></td>
											<td><?php echo ${"m".$c."_d30"}; ?></td>
											<td><?php echo ${"m".$c."_d31"}; ?></td>
											<td><?php echo ${"m".$c."_VLEarned"}; ?></td>
											<td><?php echo ${"m".$c."_SLEarned"}; ?></td>
											<td><?php echo ${"m".$c."_VLUsed"}; ?></td>
											<td><?php echo ${"m".$c."_SLUsed"}; ?></td>
											<td><?php echo ${"m".$c."_VLNewBalance"}; ?></td>
											<td><?php echo ${"m".$c."_SLNewBalance"}; ?></td>
											<td><?php echo ${"m".$c."_VLWOP"}; ?></td>
											<td><?php echo ${"m".$c."_SLWOP"}; ?></td>
											<td><?php echo ${"m".$c."_Undertime"}; ?></td>
											<td><?php echo ${"m".$c."_VLDate"}; ?></td>
											<td><?php echo ${"m".$c."_SLDate"}; ?></td>
										</tr>
										<?php 
											}
										?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			<?php 
					}
				} 
			?>
			</div>
		</div>
	</div>
</body>
</html>