<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   include 'incSignatory.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName LIMIT 5";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
   
?>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <style type="text/css">
         body {
            font-family:Arial;
         }
         @media print {
            @page {
               size: landscape;
            }
         }
      </style>
   </head>
   <body>
      <div class="container-fluid">
         <div class="row">
            <div class="col-xs-12">
               <div class="row">
                  <div class="col-xs-12 text-center">
                     <span style="font-size: 16pt;">
                        <b>PHILIPPINE INSTITUTE FOR DEVELOPMENT STUDIES</b>
                     </span>
                     <br>
                     <span style="font-size: 12pt;">
                        18th Floor, Three Cyberpod Centris-North Tower, EDSA corner Quezon Avenue, Quezon City
                     </span>
                     <br>
                     <br>
                     <span style="font-size: 14pt;">
                        <b>AGENCY REMITTANCE ADVICE</b>
                     </span>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <span><b>Agency Name:</b> Philippine Institute for Development Studies</span>
                     <br>
                     <span><b>Agency BP Number:</b> BP1000011921</span>
                     <br>
                     <span>
                        <b>FORM E.</b> List of employees with changes / correction  in their Personal Data
                     </span>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <table style="width: 100%;">
                        <thead>
                           <tr class="colHEADER" valign="bottom">
                              <th rowspan="2">Member BP<br>Number</th>
                              <th colspan="2">Last Name</th>
                              <th colspan="2">First Name</th>
                              <th colspan="2">Suffix</th>
                              <th colspan="2">Middle Name</th>
                              <th colspan="2">Residential Address/Zip Code</th>
                              <th colspan="2">Mobile Number</th>
                              <th colspan="2">Email Address</th>
                              <th colspan="2">Status of Employment</th>
                              <th colspan="2">Date of Birth *</th>
                              <th colspan="2">Place of Birth</th>
                              <th colspan="2">Gender</th>
                              <th rowspan="2">Agency BP<br>Number</th>
                              <th rowspan="2">Agency<br>Name</th>
                           </tr>   
                           <tr class="colHEADER" valign="bottom">
                              <th>From</th>
                              <th>To</th>
                              <th>From</th>
                              <th>To</th>
                              <th>From</th>
                              <th>To</th>
                              <th>From</th>
                              <th>To</th>
                              <th>From</th>
                              <th>To</th>
                              <th>From</th>
                              <th>To</th>
                              <th>From</th>
                              <th>To</th>
                              <th>From</th>
                              <th>To</th>
                              <th>From</th>
                              <th>To</th>
                              <th>From</th>
                              <th>To</th>
                              <th>From</th>
                              <th>To</th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php
                              for ($a=1; $a <= 20 ; $a++) { 
                                 echo '<tr>';
                                 for ($b=1; $b <=25 ; $b++) { 
                                    echo '<td>&nbsp;</td>';
                                 }
                                 echo '</tr>';
                              }
                           ?>
                        </tbody>
                     </table>
                  </div>
               </div>
               <br>
               <br>
               <span>
                  Please attach scanned copy of the original NSO Birth Certificate including the NSO Official Receipt.
                  <br>
                  Member must be in ACTIVE Service upon request.
               </span>
               <br>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-6">
                     Prepared by:
                     <br><br><br>
                     <b><u><?php echo $prepared_by; ?></u></b>
                     <br>
                     <?php echo $prepared_by_position; ?>
                  </div>
                  <div class="col-xs-6">
                     Certified Correct:
                     <br><br><br>
                     <b><u><?php echo $corrected_by; ?></u></b>
                     <br>
                     <?php echo $corrected_by_position; ?>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>