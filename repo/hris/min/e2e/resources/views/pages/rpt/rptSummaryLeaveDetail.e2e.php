<?php
   require_once 'constant.e2e.php';
   require_once pathClass.'0620functions.e2e.php';
   require_once pathClass.'0620RptFunctions.e2e.php';
   require_once pathClass.'DTRFunction.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   //$whereClause .= " LIMIT 10";
   $table = "employees";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   $from = getvalue("txtAttendanceDateFrom");
   $to   = getvalue("txtAttendanceDateTo");
   $month = getvalue("txtAttendanceMonth");
   $year  = getvalue("txtAttendanceYear");
   $from    = $year."-".$month."-01";
   $to      = $year."-".$month."-".cal_days_in_month(CAL_GREGORIAN,$month,$year);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
      <?php
         $rsEmployees = SelectEach("employees",$whereClause);
         if ($rsEmployees) {
            while ($row = mysqli_fetch_assoc($rsEmployees)) { 
               if ($p_filter_value == "0" || $p_filter_table == "") {
                  $EmployeesRefId = $row["RefId"];
               } else {
                  $EmployeesRefId   = $row["EmployeesRefId"];
               }
               $CompanyRefId     = $row["CompanyRefId"];
               $BranchRefId      = $row["BranchRefId"];
               $emp_row          = FindFirst("empinformation","WHERE EmployeesRefId = ".$EmployeesRefId,"*");
               if ($emp_row) {
                  $appt    = $emp_row["ApptStatusRefId"];
                  $appt    = getRecord("apptstatus",$appt,"Name");
                  $div     = getRecord("division",$emp_row["DivisionRefId"],"Name");
                  $hired   = date("d F Y",strtotime($emp_row["HiredDate"]));
                  $worksched = $emp_row["WorkScheduleRefId"];
               } else {
                  $appt    = "";
                  $div     = "";
                  $hired   = "";
                  $worksched = "";
               }
               if ($worksched != "") {
                  rptHeader(getRptName(getvalue("drpReportKind")));
      ?>               
               <div class="row" style="padding:10px;">
                  <div class="col-sm-4">
                     <?php 
                        echo "NAME : ".$row["LastName"].", ".$row["FirstName"]." ".$row["MiddleName"];  
                     ?>
                  </div>
               </div>
               <table border="1" width="100%">
                  <thead>
                     <tr>
                        <th></th>
                        <th>Date</th>
                        <th>Used Leave</th>
                        <th>Deduction</th>
                        <th>Earnings</th>
                        <th>VL Balance</th>
                     </tr>
                  </thead>   
                  <tbody>
                     <tr>
                        <th>BEGINNING BALANCE</th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th>
                           <?php
                              $table = "employeescreditbalance";
                              $whereClause = "WHERE EmployeesRefId = ".$row["RefId"]." AND NameCredits = 'VL' AND EffectivityYear = '$year'";
                              $vl_rs = FindLast($table,$whereClause,"*");
                              if ($vl_rs) {
                                 $vl = $vl_rs["BeginningBalance"];
                              } else {
                                 $vl = 0;
                              }
                              ${"VLBal_".$EmployeesRefId} = $vl;
                              echo $vl;
                           ?>
                        </th>
                        <th></th>
                     </tr>
                     <?php
                        ${"arr_leave_".$EmployeesRefId} = [
                           "01"=>"",
                           "02"=>"",
                           "03"=>"",
                           "04"=>"",
                           "05"=>"",
                           "06"=>"",
                           "07"=>"",
                           "08"=>"",
                           "09"=>"",
                           "10"=>"",
                           "11"=>"",
                           "12"=>"",
                        ];
                        $where = "where EmployeesRefId = ".$EmployeesRefId." AND Status = 'Approved'";
                        $where .= " AND ApplicationDateFrom > '".$year."-01-01"."' ORDER BY ApplicationDateFrom";
                        $rsLeave = SelectEach("employeesleave",$where);
                        if ($rsLeave) {
                           while ($row = mysqli_fetch_assoc($rsLeave)) {
                              $dfrom   = date("d",strtotime($row["ApplicationDateFrom"]));
                              $dto     = date("d",strtotime($row["ApplicationDateTo"]));
                              $type    = getRecord("leaves",$row["LeavesRefId"],"Code");
                              $leave_month   = date("m",strtotime($row["ApplicationDateFrom"]));
                              $date    = $row["ApplicationDateFrom"];
                              $arr     = [$type,$row["ApplicationDateFrom"],$row["ApplicationDateTo"]];
                              ${"arr_leave_".$EmployeesRefId}[$leave_month] = [$date];
                              ${"arr_leave_".$EmployeesRefId}[$leave_month][$date] = $arr;
                           }
                        }
                        $arr_month =[
                          "January",
                          "February",
                          "March",
                          "April",
                          "May",
                          "June",
                          "July",
                          "August",
                          "September",
                          "October",
                          "November",
                          "December"
                        ];
                        for ($a=1; $a <=12 ; $a++) { 
                           if ($a <= 9) $a = "0".$a;
                           ${"VL_".$a} = 0;
                           ${"SL_".$a} = 0;
                        }
                        for ($i=1; $i <= 12; $i++) { 
                           $idx = $i;
                           if ($idx <= 9) $idx = "0".$idx;
                           $month      = $idx;
                           $lastday    = cal_days_in_month(CAL_GREGORIAN,$month,$year);
                           $final_date = $year."-".$month."-".$lastday;
                           $first_date = $year."-".$month."-01";
                           $dtr = FindFirst("dtr_process","WHERE EmployeesRefId = '$EmployeesRefId' AND Month = '$month' AND Year = '$year'","*");
                           if ($dtr) {
                              $vl      = $dtr["VL_Used"];
                              $sl      = $dtr["SL_Used"];
                              $fl_day  = $dtr["FL_Days"]; 
                              $spl     = $dtr["SPL_Used"];
                              $fl      = 0;
                              $late_eq = $dtr["Tardy_Deduction_EQ"];
                              $ut_eq   = $dtr["Undertime_Deduction_EQ"];
                              $absent  = $dtr["Total_Absent_EQ"];
                              $deduction = $late_eq + $ut_eq + $absent + $vl;
                              if($fl_day != "") {
                                 $fl_arr = explode("|", $fl_day);
                                 foreach ($fl_arr as $key => $value) {
                                    if ($value != "") $fl++;
                                 }
                              }
                              $vl_earned = $dtr["VL_Earned"];
                           } else {
                              $vl = $sl = $fl = $spl = $vl_earned = $deduction = 0;
                           }
                           echo '
                              <tr>
                                 <td class="text-center">'.monthName($idx,1).'</td>';
                           echo '<td>';
                                 
                           echo '</td>';      
                           echo '<td>';
                              if (${"arr_leave_".$EmployeesRefId}[$idx] != "") {
                                 foreach (${"arr_leave_".$EmployeesRefId}[$idx] as $key => $value) {
                                    $type = $value[0];
                                    $from = intval(date("d",strtotime($value[1])));
                                    $to   = intval(date("d",strtotime($value[2])));
                                    $val1 = date("F d,Y",strtotime($value[1]));
                                    $val2 = date("F d,Y",strtotime($value[2]));
                                    if (strtotime($value[1]) > 0) {
                                       if ($from == $to) {
                                          echo $val1." - ".$val2." (".$type.") ";
                                          switch ($type) {
                                             case 'VL':
                                                ${"VL_".$idx}++;
                                                break;
                                             case 'SL':
                                                ${"SL_".$idx}++;
                                                break;
                                          }
                                       } else {
                                          $count = "";
                                          for ($x=$from; $x <= $to; $x++) { 
                                             $count = $count.$x.",";
                                             switch ($type) {
                                                case 'VL':
                                                   ${"VL_".$idx}++;
                                                   break;
                                                case 'SL':
                                                   ${"SL_".$idx}++;
                                                   break;
                                             }
                                          }
                                          echo $val1." - ".$val2." (".$type.") ";
                                       }
                                    }
                                 }
                              }
                           echo '</td>';      
                           echo '<td class="text-center">';
                              echo $deduction;
                              ${"VLBal_".$EmployeesRefId} -= $deduction;
                           echo '</td>';      
                           echo '<td class="text-center">';
                              echo $vl_earned;
                              ${"VLBal_".$EmployeesRefId} += $vl_earned;
                           echo '</td>';      
                           echo '<td class="text-center">';
                              echo ${"VLBal_".$EmployeesRefId};
                           echo '</td>';        
                           echo '
                              </tr>
                           ';
                        }
                     ?>
                  </tbody>   
               </table>
               <p>
                  This is a system generated report. Signature is not required.
               </p>
         <?php 
               }
            }
         }
         ?>
      </div>
   </body>
</html>