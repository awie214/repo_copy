<?php
   require_once "constant.e2e.php";
   $ScrnMode = $code = $name = $remarks = $disabled = $msg = $province = "";
   $mainTable = $recordSet = "";
   if (isset($_GET["hScrnMode"]))
      $ScrnMode = $_GET["hScrnMode"];
   else if (isset($_POST["hScrnMode"]))
      $ScrnMode = $_POST["hScrnMode"];



   if ($ScrnMode == 1) {
      $refid = "";
   } else if ($ScrnMode == 3 || $ScrnMode == 2) {
      session_start();
      require_once $_SESSION['Classes'].'0620functions.e2e.php';
      require_once "conn.e2e.php"; 
      $msg = getvalue("msg");
      $mainTable = getvalue("hTable");
      if ($ScrnMode == 3) $disabled = "disabled";
      $refid = getvalue("hRefId");
      if ($refid) {
         $criteria  = " WHERE RefId = $refid";
         $criteria .= " LIMIT 1";
         $recordSet = f_Find($mainTable,$criteria);
         $rowcount = mysqli_num_rows($recordSet);
         $row = array();
         $row = mysqli_fetch_assoc($recordSet);
         if ($rowcount) {
            //$code = $row["Code"];
            $name = $row["Name"];
            $remarks = $row["Remarks"];
            $province = $row["ProvinceRefId"];
         }
      }
   }
   require_once "incUtilitiesJS.e2e.php";
?>
   <div class="container" id="EntryScrn">
      <div class="row">
         <div class="col-xs-6">
            <?php if ($ScrnMode != 1) { ?>
               <div class="row">
                  <ul class="nav nav-pills">
                     <li class="active" style="font-size:12pt;font-weight:600;">
                        <a>REFID : <span class="badge" style="font-size:12pt;font-weight:600;" id="idRefid">
                        <?php echo $refid; ?>
                        </span></a>
                     </li>
                  </ul>
               </div>
            <?php } spacer(10)?>
            <div class="row">
               <div class="form-group">
                  <label class="control-label" for="inputs">PROVINCE:</label><br>
                  <?php
                     createSelect("Province",
                                  "sint_ProvinceRefId",
                                  $province,100,"Name","Select Province","autofocus");
                  ?>
               </div>
            </div>
            <div class="row">
               <div class="form-group">
                  <label class="control-label" for="inputs">NAME:</label><br>
                  <input class="form-input saveFields-- uCase-- mandatory" type="text" placeholder="name" <?php echo $disabled; ?>
                     id="inputs" name="char_Name" style='width:75%' value="<?php echo $name; ?>">
               </div>
            </div>
            <div class="row">
               <div class="form-group">
                  <label class="control-label" for="inputs">REMARKS:</label>
                  <textarea class="form-input saveFields--" rows="5" name="char_Remarks" <?php echo $disabled; ?>
                  placeholder="remarks"><?php echo $remarks; ?></textarea>
               </div>
            </div>
         </div>
      </div>
   </div>