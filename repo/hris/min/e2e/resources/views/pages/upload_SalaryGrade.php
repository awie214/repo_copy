<?php
	include 'conn.e2e.php';
	include 'constant.e2e.php';
	include pathClass.'0620functions.e2e.php';

	$sql = "SELECT * FROM pms_salarygrade";
	$rs  = mysqli_query($conn,$sql);
	if ($rs) {
		while ($pms_row = mysqli_fetch_assoc($rs)) {
			$Name = $pms_row["salary_grade"];
			$Code = "SG".$Name;
			$Step1 = floatval($pms_row["step1"]);
			$Step2 = floatval($pms_row["step2"]);
			$Step3 = floatval($pms_row["step3"]);
			$Step4 = floatval($pms_row["step4"]);
			$Step5 = floatval($pms_row["step5"]);
			$Step6 = floatval($pms_row["step6"]);
			$Step7 = floatval($pms_row["step7"]);
			$Step8 = floatval($pms_row["step8"]);
			$EffectivityDateS1 = "2018-01-01";
			$EffectivityDateS2 = "2018-01-01";
			$EffectivityDateS3 = "2018-01-01";
			$EffectivityDateS4 = "2018-01-01";
			$EffectivityDateS5 = "2018-01-01";
			$EffectivityDateS6 = "2018-01-01";
			$EffectivityDateS7 = "2018-01-01";
			$EffectivityDateS8 = "2018-01-01";
			$Flds = "Code, Name, Step1, Step2, Step3, Step4, Step5, Step6, Step7, Step8, ";
			$Flds .= "EffectivityDateS1, EffectivityDateS2, EffectivityDateS3, EffectivityDateS4, ";
			$Flds .= "EffectivityDateS5, EffectivityDateS6, EffectivityDateS7, EffectivityDateS8, ";
			$Vals = "'$Code', '$Name', '$Step1', '$Step2', '$Step3', '$Step4', '$Step5', '$Step6', '$Step7', '$Step8',";
			$Vals .= "'$EffectivityDateS1', '$EffectivityDateS2', '$EffectivityDateS3', '$EffectivityDateS4', ";
			$Vals .= "'$EffectivityDateS5', '$EffectivityDateS6', '$EffectivityDateS7', '$EffectivityDateS8', ";
			$result = f_SaveRecord("NEWSAVE","salarygrade",$Flds,$Vals,"ADMIN");
			if (is_numeric($result)) {
				echo "Successfully Save $Code.<br>";
			}

		}
	}

?>
