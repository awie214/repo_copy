<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>

   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post">
         <?php $sys->SysHdr($sys,"ldms"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar("Competency Manual"); ?>
            <div class="container-fluid margin-top">
               <div class="mypanel">
                  <div class="row">
                     <div class="col-xs-12" id="div_CONTENT">
                        <div class="margin-top">
                           <div id="divList">
                              <span id="spGridTable">
                                 <?php
                                       $table = "ldmscompetency";
                                       $gridTableHdr_arr = ["Position","Name","Type","Level"];
                                       $gridTableFld_arr = ["PositionRefId","Name","Type","Level"];
                                       $sql = "SELECT * FROM ldmscompetency";
                                       $Action = [true,true,true,false];
                                       doGridTable($table,
                                                   $gridTableHdr_arr,
                                                   $gridTableFld_arr,
                                                   $sql,
                                                   $Action,
                                                   $_SESSION["module_gridTable_ID"]);
                                 ?>
                              </span>
                              <?php
                                 btnINRECLO([true,false,false]);
                              ?>
                           </div>
                           <div id="divView">
                              <div class="mypanel panel panel-default">
                                 <div class="panel-top">
                                    <span id="ScreenMode">INSERTING NEW COMPETENCY
                                 </div>
                                 <div class="panel-mid-litebg" id="EntryScrn">
                                    <div class="container" id="EntryScrn">
                                       <div class="row">
                                          <div class="col-xs-6">
                                             <div class="row">
                                                <div class="form-group">
                                                   <label class="control-label" for="inputs">POSITION:</label><br>
                                                   <?php
                                                      createSelect("Position",
                                                                   "sint_PositionRefId",
                                                                   "",100,"Name","Select Position","");
                                                   ?>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="form-group">
                                                   <label class="control-label" for="inputs">NAME:</label><br>
                                                   <input class="form-input saveFields-- mandatory uCase--" 
                                                          type="text"
                                                          name="char_Name"
                                                          id="char_Name">
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="form-group">
                                                   <label class="control-label" for="inputs">TYPE:</label><br>
                                                   <input class="form-input saveFields-- mandatory uCase--" 
                                                          type="text"
                                                          name="char_Type"
                                                          id="char_Type">
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="form-group">
                                                   <label class="control-label" for="inputs">LEVEL:</label><br>
                                                   <input class="form-input saveFields-- mandatory uCase--" 
                                                          type="text"
                                                          name="char_Level"
                                                          id="char_Level">
                                                </div>
                                             </div>
                                             
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel-bottom">
                                    <?php btnSACABA([true,true,true]); ?>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <?php
               footer();
               $table = "ldmscompetency";
               doHidden("paramTitle",getvalue("paramTitle"),"");
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
   <script language="JavaScript">
      $(document).ready(function () {
         remIconDL();
      });
      function afterNewSave() {
         alert("Successfully Saved");
         gotoscrn("ldms_competency_manual","");
      }
      function afterEditSave() {
         alert("Successfully Updated");
         gotoscrn("ldms_competency_manual","");   
      }
   </script>
</html>



