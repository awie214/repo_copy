<div id="famBG">
   <input type="checkbox" for="<?php echo $iFile; ?>" class="enabler--" id="FamilyBG" name="FamilyBG" refid="">
   <label for="FamilyBG" class="btn-cls4-sea">EDIT FAMILY BACKGROUND</label>
   <div class="mypanel" id="EntrySpouseInfo">
      <div class="panel-top" for="spInfo">
         SPOUSE's INFORMATION
         <input type="hidden" name="FamilyRefId" id="FamilyBG" value="">
      </div>
      <div class="panel-mid-litebg" id="spInfo">
         <div class="row">
            <div class="col-xs-3">
               <div class="form-group">
                  <label class="control-label" for="inputSLName">LAST NAME:</label>
                  <input class="form-input saveFields-- uCase--" type="text" id="inputSLName" group="2" name="char_SpouseLastName" autofocus>
               </div>
            </div>
            <div class="col-xs-3">
               <div class="form-group">
                  <label class="control-label" for="inputSFName">FIRST NAME:</label>
                  <input class="form-input saveFields-- uCase--" type="text" id="inputSFName" group="2" name="char_SpouseFirstName">
               </div>
            </div>
            <div class="col-xs-3">
               <div class="form-group">
                  <label class="control-label" for="inputSMName">MIDDLE NAME:</label>
                  <input class="form-input saveFields-- uCase--" type="text" id="inputSMName" group="2" name="char_SpouseMiddleName">
               </div>
            </div>
            <div class="col-xs-3">
               <div class="form-group">
                  <label class="control-label" for="inputs">EXT. NAME</label>
                  <input class="form-input saveFields-- uCase--" type="text" id="" group="2" name="char_SpouseExtName">
               </div>
            </div>
         </div>
         <div class="row">
            <!--<div class="col-xs-3">
               <div class="form-group">
                  <label class="control-label" for="">DATE OF BIRTH :</label><br>
                  <input class="form-input date-- saveFields--" type="text" id="" group="2" name="date_SpouseBirthDate">
               </div>
            </div>-->
            <div class="col-xs-3">
               <div class="form-group">
                  <label class="control-label" for="inputs">MOBILE NO.:</label>
                  <input class="form-input number-- saveFields--" type="text" id="inputs" group="2" name="char_SpouseMobileNo">
               </div>
            </div>
            <div class="col-xs-3">
               <div class="form-group">
                  <label class="control-label" for="inputOccupation">OCCUPATION:</label><br>
                  <?php
                     createSelect("Occupations",
                                  "sint_OccupationsRefId",
                                  "",100,"Name","Select Occupations","");
                  ?>
               </div>
            </div>
            <div class="col-xs-3">
               <div class="form-group">
                  <label class="control-label" for="">EMPLOYER's NAME</label>
                  <input class="form-input saveFields-- uCase--" type="text" group="2" name="char_EmployersName">
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-xs-6">
               <div class="form-group">
                  <label class="control-label" for="">BUSINESS ADDRESS</label>
                  <input class="form-input saveFields-- uCase--" type="text" id="inputs" group="2" name="char_BusinessAddress">
               </div>
            </div>

         </div>
      </div>
      <div class="panel-bottom"></div>
   </div>
   <div id="EntryFatherMotherInfo">
      <div class="mypanel margin-top">
         <div class="panel-top" for="fatherInfo">
            FATHER'S INFORMATION
         </div>
         <div class="panel-mid-litebg" id="fatherInfo">
            <div class="row">
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" for="inputFLName">LAST NAME:</label>
                     <input class="form-input saveFields-- uCase--" type="text" id="inputFLName" name="char_FatherLastName">
                  </div>
               </div>

               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" for="inputFFName">FIRST NAME:</label>
                     <input class="form-input saveFields-- uCase--" type="text" id="inputFFName" name="char_FatherFirstName">
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" for="inputFMName">MIDDLE NAME:</label>
                     <input class="form-input saveFields-- uCase--" type="text" id="inputFMName" name="char_FatherMiddleName">
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" for="inputFXName">EXT NAME:</label>
                     <input class="form-input saveFields-- uCase--" type="text" id="inputFXName" name="char_FatherExtName">
                  </div>
               </div>
            </div>
         </div>
         <div class="panel-bottom"></div>
         <div class="panel-top margin-top" for="motherInfo">
            MOTHER'S INFORMATION (Maiden Name)
         </div>
         <div class="panel-mid-litebg" id="motherInfo">
            <div class="row">
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" for="inputMLName">LAST NAME:</label>
                     <input class="form-input saveFields-- uCase--" type="text" id="inputMLName" name="char_MotherLastName">
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" for="inputMFName">FIRST NAME:</label>
                     <input class="form-input saveFields-- uCase--" type="text" id="inputMFName" name="char_MotherFirstName">
                  </div>
               </div>
               <div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" for="inputMMName">MIDDLE NAME:</label>
                     <input class="form-input saveFields-- uCase--" type="text" id="inputMMName" name="char_MotherMiddleName">
                  </div>
               </div>
               <!--<div class="col-xs-3">
                  <div class="form-group">
                     <label class="control-label" for="inputMXName">EXT NAME:</label>
                     <input class="form-input saveFields--" type="text" id="inputMXName" name="char_MotherExtName">
                  </div>
               </div>-->
            </div>
         </div>
         <div class="panel-bottom"></div>
      </div>
   </div>
</div>
<script type="text/javascript">
   $("[name='sint_OccupationsRefId']").attr("group",2);
</script>



