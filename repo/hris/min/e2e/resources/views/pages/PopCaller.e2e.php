<?php
   session_start();
   require_once "colors.e2e.php";
   require_once "conn.e2e.php";
   require_once "constant.e2e.php";
   require_once pathClass.'0620functions.e2e.php';
   require_once pathClass.'SysFunctions.e2e.php';
   require_once pathClass.'DTRFunction.e2e.php';

   
   $sys = new SysFunctions();
   $css = ["armyBlue1","armyBlue2","bgPIS"];
   $mynewfile = $sys->css_create($css);
   $LastName     = $FirstName = $MiddleName = "";
   $userlvl      = 0;
   $user_refid   = "";
   $program_used = "";
   $errmsg       = "";
   $sql          = "";
   $refid         = 0;

   $CompanyId     = getvalue("hCompanyID");
   $BranchId      = getvalue("hBranchID");
   $userSESSION   = getvalue("hSess");
   $user          = getvalue("hUser");
   $SubMenuID     = getvalue("mid");
   $ScreenName    = getvalue("n");
   if (getvalue("file") !== "") {
      $progFile = "pop_".getvalue("file").".e2e.php";
   } else {
      $progFile = "pop_".getvalue("hProg").".e2e.php";
   }
   $_SESSION["sysData"] = getvalue("hSysData");
   $p = fopen(path('syslog/'.$user.'_PopUsed_'.$today.'.log'), 'a+');
   if ($errmsg != "") {
      echo "<h1 style='color:red'>";
      echo $errmsg;
      echo "</h1>";
   }
   $_SESSION['debugging'] = true;
   fwrite($p,$datelog."->URL:".$_SERVER['REQUEST_URI']."\n");
   require_once $progFile;
   echo '
      <script>
         $(".newDataLibrary").hide();
         $("select").css("width","100%");
      </script>
   ';
   $conn = null;
   fclose($p);
?>


