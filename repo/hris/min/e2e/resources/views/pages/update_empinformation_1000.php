<?php
	include 'conn.e2e.php';
	include 'constant.e2e.php';
	include pathClass.'0620functions.e2e.php';
	//$pids_old = mysqli_connect("localhost","root","","pids_old");
	$pids_old = $conn;
	
	function saveFM($table,$flds,$vals,$name,$where = "") {
		include 'conn.e2e.php';
		$table 			= strtolower($table);
		$where 			= "WHERE Name = '$name' ".$where;
		$check_FM 		= FindFirst($table,$where,"RefId",$conn);
		if (is_numeric($check_FM)) {
			return $check_FM;
		} else {
			$date_today    	= date("Y-m-d",time());
	        $curr_time     	= date("H:i:s",time());
	        $trackingA_fld 	= "`LastUpdateDate`, `LastUpdateTime`, `LastUpdateBy`, `Data`";
	        $trackingA_val 	= "'$date_today', '$curr_time', 'Admin', 'A'";
	        $flds   	   	= $flds.$trackingA_fld;
	        $vals   		= $vals.$trackingA_val;
	        $sql 			= "INSERT INTO $table ($flds) VALUES ($vals)";
	        $rs 			= mysqli_query($conn,$sql) or die(mysqli_error($conn));
	        if ($rs) {
	        	return mysqli_insert_id($conn);
	        } else {
	        	echo $rs."<br>$sql";
	        }	
		}
	}

	$rs = SelectEach("employees","");
	while ($row = mysqli_fetch_assoc($rs)) {
		$emprefid 		= $row["RefId"];
		$empinformation = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","RefId");
		if (is_numeric($empinformation)) {
			$a_sql 		= "SELECT * FROM employee_work_experiences WHERE employee_personal_information_sheet_id = $emprefid ORDER BY start_date DESC LIMIT 1";
			$a_rs 		= mysqli_query($pids_old,$a_sql);
			if ($a_rs) {
				while ($a_row = mysqli_fetch_assoc($a_rs)) {
					$EffectivityDate 	= $a_row["start_date"];
					$ExpiryDate 		= $a_row["end_date"];
					$PositionRefId  	= realEscape($a_row["position_title"]);
					$AgencyRefId		= realEscape($a_row["company"]);
					$ApptStatusRefId 	= realEscape($a_row["status_of_appointment"]);

					$SalaryGradeRefId   = intval($a_row["salary_grade"]);
					$StepIncrementRefId = intval($a_row["step"]);
					$SalaryAmount 		= floatval($a_row["monthly_salary"]);
					$isGovtService 		= $a_row["government_service"];
					$LWOP 				= intval($a_row["leave_of_absence_without_pay"]);
					$Cause 				= realEscape($a_row["remarks"]);
					
					
					$PositionRefId    	= saveFM("position","Name, ","'$PositionRefId', ",$PositionRefId);
					$AgencyRefId    	= saveFM("agency","Name, ","'$AgencyRefId', ",$AgencyRefId);
					$ApptStatusRefId    = saveFM("apptstatus","Name, ","'$ApptStatusRefId', ",$ApptStatusRefId);
					
					/*echo "Hired Date: $EffectivityDate.<br>";
					echo "Position: $PositionRefId.<br>";
					echo "ApptStatus: $ApptStatusRefId.<br>";
					echo "Agency: $AgencyRefId.<br>";
					echo "Salary: $SalaryAmount.<br>";
					echo "SalaryGrade: $SalaryGradeRefId.<br>";
					echo "StepIncrement: $StepIncrementRefId.<br>";
					echo "----------------------<br>";*/
					$FldnVal = "HiredDate = '$EffectivityDate',";
					$FldnVal .= "AgencyRefId = '$AgencyRefId', ";
					$FldnVal .= "PositionRefId = '$PositionRefId', ";
					$FldnVal .= "ApptStatusRefId = '$ApptStatusRefId', ";
					$FldnVal .= "SalaryAmount = '$SalaryAmount', ";
					$FldnVal .= "SalaryGradeRefId = '$SalaryGradeRefId', ";
					$FldnVal .= "StepIncrementRefId = '$StepIncrementRefId', ";
					$FldnVal .= "WorkScheduleRefId = '1', ";

					if ($ExpiryDate != "") {
						$FldnVal .= "ResignedDate = '$ExpiryDate', ";
					}
					$result = f_SaveRecord("EDITSAVE","empinformation",$FldnVal,$empinformation,"Admin");
					if ($result == "") {
						echo "Successfully Updated Emp Information<br>";
					}



					/*
					if (date("Y-m-d",strtotime($EffectivityDate)) != "1970-01-01") {
						$Flds = "EmployeesRefId, EffectivityDate, PositionRefId, AgencyRefId, ApptStatusRefId, SalaryGradeRefId, StepIncrementRefId,";
						$Flds .= "SalaryAmount, isGovtService, Cause, ";
						$Vals = "'$emprefid', '$EffectivityDate', '$PositionRefId', '$AgencyRefId', '$ApptStatusRefId', '$SalaryGradeRefId',";
						$Vals .= "'$StepIncrementRefId', '$SalaryAmount', '$isGovtService', '$Cause', ";
						if ($ExpiryDate == "") {
							$Flds .= "Present, ";
							$Vals .= "1, ";
						} else {
							$Flds .= "ExpiryDate, ";
							$Vals .= "'$ExpiryDate', ";	
						}
						if ($LWOP > 0) {
							$Flds .= "LWOP, ";
							$Vals .= "'$LWOP', ";	
						}
					}*/
				}
			}
		}
	}
?>