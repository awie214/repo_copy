$(document).ready(function () {
   var cid = $("#hCompanyID").val();
   if (cid == "21") {
      $("[name*='btnApproved_']").html("RECEIVED");
      $("[name*='btnReject_']").hide();
   }
   $(".workSched").click(function () {
      $("#workSched_modal").modal();
   });

   $("[name*='btnApproved']").each(function() {
      $(this).click(function() {
         //alert($("#hUserRefID").val());
         if ($("#hTable").val() == "") {
            alert("Ooops!!! No Table Assigned");
            return;
         }
         $.post("DTRTrn.e2e.php",
         {
            fn:"PostStatus",
            hUser: $("#hUser").val(),
            tbl:$("#hTable").val(),
            refid:$(this).attr("refid"),
            apprvBy:$("#hUserRefID").val(),
            status:"Approved"
         },
         function(data,status) {
            if (status == "success") {
               try {
                  //alert(data);
                  eval(data);
                  //gotoscrn($("#hProg").val(),"");
               } catch (e) {
                  if (e instanceof SyntaxError) {
                     alert(e.message);
                  }
               }
            }
            else {
               alert("Ooops Error : " + status);
            }
         });
      });
   });

   $("[name*='btnReject']").each(function() {
      $(this).click(function() {
         $("#hRefIdReject").val($(this).attr("refid"));
         if ($("#hRefIdReject").val() > 0) {
            $("#modalReject").modal();
         } else {
            alert("Ooops!!! No RefId Assigned");
         }

      });
   });

   $("#PostReason").click(function () {

      if ($("#hTable").val() == "") {
         alert("Ooops!!! No Table Assigned");
         return;
      }

      $.post("DTRTrn.e2e.php",
      {
         fn:"PostStatus",
         hUser: $("#hUser").val(),
         tbl:$("#hTable").val(),
         refid:$("#hRefIdReject").val(),
         reason:$("[name='char_Reason']").val(),
         status:"Rejected"
      },
      function(data,status) {
         if (status == "success") {
            try {
               eval(data);
               gotoscrn($("#hProg").val(),"");
            } catch (e) {
               if (e instanceof SyntaxError) {
                  alert(e.message);
               }
            }
         }
         else {
            alert("Ooops Error : " + status);
         }
      });
   });
});