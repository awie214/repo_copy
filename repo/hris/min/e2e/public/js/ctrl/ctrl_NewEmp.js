$(document).ready(function() {     
   var cid = $("#hCompanyID").val();
   remIconDL();    
   if (cid == "35") getEmpID();
   
   $("[name='PositionRefId']").addClass("mandatory");
   $("#dummybtn").click(function () {
      var bdate = $("#BirthDate").val();
      var age = calcAge(bdate);
      if (age <= 18) {
         $("#BirthDate").focus();
         alert("Employee Must Above 18 years old.");
         return false;
      }
      var err = saveProceed();
      if (err == 0) {
         saveNewEmp();
      }
   });
   $("#btnNew201").click(function () {
      var bdate = $("#BirthDate").val();
      var age = calcAge(bdate);
      if (age <= 18) {
         $("#BirthDate").focus();
         alert("Employee Must Above 18 years old.");
         return false;
      }
      var err = saveProceed();
      if (err == 0) {
         var field = new Array();
         var data = "";
         $j = 0;
         $("#panel_NewEmployees .saveFields--").each(function () {
            field[$j] = $(this).attr("name");
            $j++;
            if ($(this).attr("name") == "pw") {
               data += '"' + $(this).attr("name") + '": "' + hash($(this).val()) + '", ';
            } else {
               data += '"' + $(this).attr("name") + '": "' + $(this).val() + '", ';
            }
         });
         data = data.trim();
         data = data.substring(0, data.length - 1);
         data = '{' + data + '}'; 
         $.get("trn.e2e.php",
         {
            fn: "SaveNewEmployeesInit",
            data: data,
            field: JSON.stringify(field),
            user: $("#hUser").val(),
            data: data
         },
         function (data, status) {
            if (status == "success") {
               try {
                  eval(data);
               } catch (e) {
                  if (e instanceof SyntaxError) {
                     alert(e.message);
                  }
               }
            }
         });
      }
   });
});   
function getEmpID() {
   $.get("trn.e2e.php",
   {
      fn:"getEmpID"
   },
   function(data,status){
      if (status == 'success') {
         eval(data);
      } else {
         alert(data);
      }
   });
}
function saveNewEmp() {
   var pass = calcHash($("[name='pw']").val());
   $("[name='hPass']").val(pass);
   $.ajax({
      url: "trn.e2e.php",
      type: "POST",
      data: new FormData($("[name='xForm']")[0]),
      success : function(responseTxt){
         responseTxt = responseTxt.trim();
         eval(responseTxt);
      },
      enctype: 'multipart/form-data',
      processData: false,
      contentType: false,
      cache: false
   });
}