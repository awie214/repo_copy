$(document).ready(function () {
   remIconDL();
   $("[name='bint_OverallScore']").blur(function () {
      var val = $(this).val(); 
      if (parseFloat(val) > 5) {
         alert("The limit is 1 to 5 only");
         $("[name='bint_OverallScore'], [name='bint_NumericalRating'], [name='char_Adjectival']").val("");
         return false;
      } else {
         val = Math.trunc(val);
         $("[name='bint_NumericalRating']").val(val);
         $("[name='char_Adjectival']").val(val);   
      }
      
   });
});
function refreshTable(emprefid) {
   $("#spGridTable").html("");
   $("#spGridTable").load("listRefresh.e2e.php",
   {
      EmployeesRefid : emprefid
   },
   function(responseTxt, statusTxt, xhr){
      if(statusTxt == "error")
         alert("Ooops Error: " + xhr.status + ": " + xhr.statusText);
         return false;
   });
}
function selectedItems(emprefid,lname,fname) {
   $(".list-group-item").removeClass("active");
   $("[name='txtEmpName'").val(lname + ", " + fname);
   $("#" + emprefid).addClass("active");
   $("[name='sint_EmployeesRefId']").val(emprefid);
   $.post("changeSessionValue.e2e.php",
   {
      hGridTblHdr:"Semester|Year|Overall Score|Adjectival",
      hGridTblFld:"Semester|YearPerformed|OverallScore|Adjectival",
      hGridTblId:"gridTable",
      hGridDBTable:"employeesperformance",
      sql:"SELECT * FROM `employeesperformance` WHERE `EmployeesRefId` = " + emprefid + " ORDER BY RefId Desc LIMIT 100",
      listAction:[true,true,false]
   },
   function(data,status){
      if (status=='success') {
         refreshTable(emprefid);
         tr_Click(emprefid);
      }
   });
}
// function selectMe(emprefid) {
//    $.get("EmpQuery.e2e.php",
//    {
//       emprefid:emprefid,
//       hCompanyID:$("#hCompanyID").val(),
//       hBranchID:$("#hBranchID").val(),
//       hEmpRefId:$("#hEmpRefId").val(),
//       hUserRefId:$("#hUserRefId").val()
//    },
//    function(data,status) {
//       if(status == "error") return false;
//       if(status == "success"){
//          var data = JSON.parse(data);
//          try
//          {
//             $('[name="sint_EmployeesRefId"]').val(data.RefId);
//             $('[name="txtFullName"]').val(data.LastName + ", " + data.FirstName + " " + data.MiddleName);
//             $("#modalEmpLookUp").modal("hide");
//             selectedItems(data.RefId,
//                           data.LastName,
//                           data.FirstName);
//          }
//          catch (e)
//          {
//             if (e instanceof SyntaxError) {
//                 alert(e.message);
//             }
//          }
//       }
//    });
// }
function afterNewSave(SaveSuccessfull){
   // $("#divView").hide();
   // $("#divList").show();
   // alert("Record Save !!!");
   // refreshTable($("[name='sint_EmployeesRefId']").val());
   alert("Successfully Saved");
   gotoscrn("spmsSummRating","");
}
function afterDelete() {
   // $("#divView").hide();
   // $("#divList").show();
   // alert("Record Deleted !!!");
   // refreshTable($("[name='sint_EmployeesRefId']").val());
   alert("Successfully Saved");
   gotoscrn("spmsSummRating","");
}